﻿using System;
using System.Collections.Generic;

namespace MvcInfos.Plugin.Interfaces
{
	public interface IPlugins
	{
		Dictionary<Guid, IPlugin> LoadedPlugins { get; }
		Dictionary<Guid, IPlugin> GetPlugins();
		string GetHTMLOptions();
		string GetHTMLOptionsStyles();
		System.Drawing.Bitmap GetPluginLogo(Guid id);
		Dictionary<string, string> GetForms();
	}
}
