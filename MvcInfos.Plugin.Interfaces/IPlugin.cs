﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace MvcInfos.Plugin.Interfaces
{
	public interface IPlugin
	{
		Guid PluginID { get; }
		string PluginName { get; }
		Bitmap Icon { get; }
		Action SendFile(string fileName, string settings);
		string GetSettingJSON(Dictionary<string, string> settings);
		string Form { get; }
		string ToString(string settings);
	}
}
