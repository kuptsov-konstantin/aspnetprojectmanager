﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcInfos.FileTypes
{
	public class FileTypeResolver
	{
		public static byte[] FileTypeResolve(string path)
		{
			var ext = Path.GetExtension(path);
			switch (ext)
			{
				case ".pdf":
					{
						return MvcInfos.FileTypes.Properties.Resources.pdf;
					}

				case ".doc":
				case ".docx":
					{
						return MvcInfos.FileTypes.Properties.Resources.doc;
					}

				case ".xls":
				case ".xlsx":
					{
						return MvcInfos.FileTypes.Properties.Resources.xls;
					}

				case ".ppt":
				case ".pptx":
					{
						return MvcInfos.FileTypes.Properties.Resources.ppt;
					}

				case ".txt":
					{
						return MvcInfos.FileTypes.Properties.Resources.txt;
					}

				case ".dwg":
					{
						return MvcInfos.FileTypes.Properties.Resources.doc;
					}

				case ".csv":
					{
						return MvcInfos.FileTypes.Properties.Resources.csv;
					}
				
				default:
					{
						return MvcInfos.FileTypes.Properties.Resources.file;
					}
			}
		}
	}
}
