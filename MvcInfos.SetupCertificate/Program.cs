﻿using System;
using System.IO;
using System.Linq;

namespace MvcInfos.Crypto.Certificate.Update
{
	class Program
	{
		static int Main(string[] args)
		{

			if (args.Contains("help") || args.Length == 0)
			{
				Console.WriteLine("Справка");
				Console.WriteLine("/createCertificate - создание сертификата");
				Console.WriteLine("/registerCertificate - регистрация сертификата");
				Console.WriteLine("--------");
				Console.WriteLine("Пример:");
				Console.WriteLine("/createCertificate /registerCertificate");
				Console.WriteLine("/registerCertificate cert.pfx");

			}

			if (args.Contains("/createCerificate"))
			{
				using (var certificate = Certificate.GenerateCertificate())
				{
					if (args.Contains("/registerCertificate"))
					{
						try
						{

							Certificate.RegisterCertificate(certificate);
							return 0;
						}
						catch (Exception e)
						{
							Console.WriteLine(e.Message);
							Console.ReadKey();
						}
					}
				}
			}
			else
			{
				if (args.Contains("/registerCertificate") && (args.Length >= 2))
				{
					for (int i = 1; i < args.Length; i++)
					{
						if((Path.GetExtension(args[i]).Equals(".pfx"))&&(File.Exists(args[i])))
						{
							Certificate.RegisterCertificate(null, args[i]);
						}
					}
				}
			}


			return -1;
		}
	}
}
