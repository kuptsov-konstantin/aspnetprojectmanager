﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(MvcInfos.Startup))]
namespace MvcInfos
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
