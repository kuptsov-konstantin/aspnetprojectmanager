﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется следующим образом
// набора атрибутов. Измените значения этих атрибутов для изменения сведений,
// связанных с этой сборкой.
[assembly: AssemblyTitle("MvcInfos")]
[assembly: AssemblyDescription("Веб приложение управления проектной организацией MvcInfos")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Купцов")]
[assembly: AssemblyProduct("Mvcinfos")]
[assembly: AssemblyCopyright("© Купцов, 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Установка значения False в параметре ComVisible делает типы в этой сборке невидимыми
// для компонентов COM.  Если требуется обратиться к типу в этой сборке через
// COM, задайте атрибуту ComVisible значение true для требуемого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для ID библиотеки типов typelib, если этот проект видим для COM
[assembly: Guid("ecd4981f-f265-447e-9c45-8179bb8abe29")]

// Сведения о версии сборки состоят из указанных ниже четырех значений:
//
//      основной номер версии;
//      Дополнительный номер версии
//      номер сборки;
//      редакция.
//
// Можно задать все значения или принять номер редакции и номер сборки по умолчанию,
// используя "*", как показано ниже:
[assembly: AssemblyVersion("2.0.0.0")]
[assembly: AssemblyFileVersion("2.0.0.0")]
[assembly: InternalsVisibleTo("MvcInfos.Tests")]
[assembly: InternalsVisibleTo("MvcInfos.Explorables")]
[assembly: InternalsVisibleTo("MvcInfos.Tests1")]

