﻿interface IUploadcareOptions {
	tabs?: string
	buttonLabel?: string
	buttonIcon?: string
	tooltipText?: string
	publicKey?: string
	crop?: string
	multiple?: boolean
	version?: string
	fileList?: TaskManager
}
interface IUploadcareDefaultOptions {
	allTabs?: string
	cdnBase?: string
	clearable?: boolean
	crop?: boolean
	debugUploads?: boolean
	doNotStore?: boolean
	imagePreviewMaxSize?: number
	imageShrink?: boolean
	imagesOnly?: boolean
	inputAcceptTypes?: string
	live?: boolean
	locale?: string
	localePluralize: any
	localeTranslations: any
	manualStart?: boolean
	multipartConcurrency?: number
	multipartMaxAttempts?: number
	multipartMinLastPartSize?: number
	multipartMinSize?: number
	multipartPartSize?: number
	multiple?: boolean
	multipleMax?: number
	multipleMaxStrict?: boolean
	multipleMin?: number
	parallelDirectUploads?: number
	passWindowOpen?: boolean
	pathValue?: boolean
	preferredTypes?: string
	previewStep?: boolean
	publicKey?: string
	pusherKey?: string
	scriptBase?: string
	secureExpire?: string
	secureSignature?: string
	socialBase?: string
	systemDialog?: boolean
	tabs?: string
	urlBase?: string
}

interface IUploadcareDragAndDropOptions {
	receiveDrop(el, callback): any
	support: boolean
	uploadDrop(el, callback, settings): any
}
interface IUploadcareCssCollector {
	styles: any[]
	urls: any[]
}
interface IUploadcare {
	start(options?: any): any
	Circle(element: any): any
	FileGroup?: (filesAndGroups: IUploadcareFileEventsOptions[], settings?: any) => IUploadcareFileGroupOptions
	MultipleWidget(el: any): any
	SingleWidget(el: any): any
	Widget(el: any): any
	closeDialog(): any
	defaults?: IUploadcareDefaultOptions
	dragdrop?: IUploadcareDragAndDropOptions
	fileFrom?: (type, data, s?) => IUploadcareFileEventsOptions
	filesFrom?: (type, data, s?) => IUploadcareFileEventsOptions[]
	globals(): any
	initialize(container): any
	jQuery(a, b): any
	loadFileGroup(groupIdOrUrl, settings)
	locales: string[]
	openDialog(files: IUploadcareFileGroupOptions, tab?: string, settings?): IUploadcareDialogOptions
	openDialog(tab): IUploadcareDialogOptions
	openDialog(tab, settings): IUploadcareDialogOptions


	openPanel(placeholder, files, tab, settings): any
	plugin(fn): any
	registerTab(tabName, constructor): any
	tabsCss: IUploadcareCssCollector
	version: string


}
interface IUploadcareCropOptions {
	height?: number
	left?: number
	top?: number
	width?: number
}
interface IUploadcareImageOptions {
	datetime_original: Date
	format?: string
	height?: number
	width?: number
}
interface IUploadcareFileOptions {
	file?: File
	source?: string
}
interface IUploadcareBlob {
	cdnUrl?: string
	cdnUrlModifiers?: string
	crop?: IUploadcareCropOptions
	isImage?: true
	isStored?: true
	mimeType?: string
	name?: string
	originalImageInfo?: IUploadcareImageOptions

	originalUrl?: string
	size?: number
	sourceInfo?: IUploadcareFileOptions
	uuid?: string
}


interface IUploadcareFileEventsOptions {
	always?: (callbackfn: () => any, thisArg?: any) => any
	cancel?: (callbackfn: () => any, thisArg?: any) => any
	done?: (callbackfn: (result) => any, thisArg?: any) => any
	fail?: (callbackfn: (result) => any, thisArg?: any) => any
	pipe?: (callbackfn: () => any, thisArg?: any) => any
	progress?: (callbackfn: (tabName) => any, thisArg?: any) => any
	promise?: (callbackfn: (result?) => any, thisArg?: any) => any
	state?: (callbackfn: (result?) => any, thisArg?: any) => any
	then?: (callbackfn: (result?) => any, thisArg?: any) => any
}

interface IUploadcareFileGroupOptions {
	files?: () => any
	promise?: () => any
}

interface IUploadcareDialogOptions extends IUploadcareFileEventsOptions {
	addFiles?: () => any
	dialogElement?: (i?) => any
	fileColl?: () => any
	hideTab?: () => any
	isTabVisible?: () => any
	onTabVisibility?: () => any
	pipe?: () => any
	promise?: () => any
	reject?: () => any
	resolve?: () => any
	showTab?: () => any
	state?: () => any
	switchTab?: () => any
	then?: () => any
}

interface IUploadcareFileModelView {
	is_stored: boolean;
	done: number;
	file_id: string;
	total: number;
	size: number;
	uuid: string;
	is_image: boolean;
	filename: string;
	is_ready: boolean;
	original_filename: string;
	mime_type: string;
	image_info: IUploadcareImageModelView;

}

interface IUploadcareImageModelView {
	format: string;
	height: number;
	width: number;
	datetime_original: Date;
}

declare var uploadcare: IUploadcare