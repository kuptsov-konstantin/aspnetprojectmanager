﻿declare interface JQuery {
    selectpicker(method: any): JQuery;
    treegrid(method: any): JQuery;
}

declare function escape(str: string): string;
declare function getProjectInfo(): ProjectInfo;
declare function gd(year: number, month: number, day: number): Date;


declare function init_sidebar();
declare var switcheryDictionary: { [key: string]: ISwitchery; }

interface ISwitchery {
	element?: HTMLInputElement
	events?: { [key: string]: any; }
	jack?: HTMLElement
	options?: Switchery.Options
	switcher?: HTMLSpanElement

	onDialogShown?: ($dialog, handler) => void

	bindClick?: () => void;
	colorize?: () => void;
	create?: () => void;
	destroy?: () => void;
	disable?: () => void;
	enable?: () => void;
	handleChange?: () => void;
	handleClick?: () => void;
	handleOnchange?: (state) => void;
	hide?: () => void;
	init?: () => void;
	insertAfter?: (reference, target) => void;
	isChecked?: () => boolean;
	isDisabled?: () => boolean;
	markAsSwitched?: () => void;
	markedAsSwitched?: () => void;
	setPosition?: (clicked) => void;
	setSize?: () => void;
	setSpeed?: () => void;
	show?: () => void;
}
