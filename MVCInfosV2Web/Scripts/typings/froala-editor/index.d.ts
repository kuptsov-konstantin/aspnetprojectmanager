﻿
interface IFrolalaBeautifierOptions {
	end_with_newline?: boolean
	indent_inner_html?: boolean
	extra_liners?: string
	brace_style?: string
	indent_char?: string
	indent_size?: number
	wrap_line_length?: number
}
interface IFrolalaCodeMirrorOptions{
	indentWithTabs?: boolean
	lineNumbers?: boolean
	lineWrapping?: boolean
	mode?: string
	tabMode?: string
	tabSize?: number
}
interface IFroalaEmoticons {
	code?: string
	desc?: string
}

interface IFroalaFileUploadParams {
	id?: number

}
interface IFroalaFileUploadToS3 {
	bucket?: string
	region?: string
	keyStart?: string
	callback?(url, key): any
	params?: {
		acl?: string// ACL according to Amazon Documentation.
		AWSAccessKeyId?: string // Access Key from Amazon.
		policy?: string // Policy string computed in the backend.
		signature?: string // Signature computed in the backend.
	}
}



interface IFrolalaOptions {
	charCounterCount?: boolean
	charCounterMax?: number
	codeBeautifierOptions?: IFrolalaBeautifierOptions
	codeMirror?: boolean
	codeMirrorOptions?: IFrolalaCodeMirrorOptions
	codeViewKeepActiveButtons?: string[]
	colorsBackground?: string[]
	colorsDefaultTab?: string
	colorsStep?: number
	colorsText?: string[]
	direction?: string
	disableRightClick?: boolean
	dragInline?: boolean
	editInPopup?: boolean
	editorClass?: string
	emoticonsSet?: IFroalaEmoticons[]
	emoticonsStep?: number
	emoticonsUseImage?: boolean
	enter?: string
	entities?: string

	fileAllowedTypes?: string[]
	fileInsertButtons?: string[]
	fileMaxSize?: number
	fileUploadMethod?: string
	fileUploadParam?: string
	fileUploadParams?: IFroalaFileUploadParams[]
	fileUploadToS3?: IFroalaFileUploadToS3
	fileUploadURL?: string
	fileUseSelectedText?: boolean

	fontFamily?: any
	fontFamilyDefaultSelection?: string
	fontFamilySelection?: any
	fontSize?: any
	fontSizeDefaultSelection?: any
	fontSizeSelection?: any

	fullPage?: any
	height?: any
	heightMax?: any
	heightMin?: any

	htmlAllowComments?: any
	htmlAllowedAttrs?: any
	htmlAllowedEmptyTags?: any
	htmlAllowedStyleProps?: any
	htmlAllowedTags?: any

	htmlDoNotWrapTags?: any
	htmlIgnoreCSSProperties?: any
	htmlRemoveTags?: any
	htmlSimpleAmpersand?: any
	htmlUntouched?: any

	iframe?: any
	iframeStyle?: any
	iframeStyleFiles?: any

	imageAllowedTypes?: string[]
	imageAltButtons?: string[]
	imageDefaultAlign?: string
	imageDefaultDisplay?: string
	imageDefaultWidth?: number
	imageEditButtons?: string[]
	imageInsertButtons?: string[]
	imageManagerDeleteMethod?: string
	imageManagerDeleteParams?: any
	imageManagerDeleteURL?: string
	imageManagerLoadMethod?: string
	imageManagerLoadParams?: any
	imageManagerLoadURL?: string
	imageManagerPageSize?: number
	imageManagerPreloader?: string
	imageManagerScrollOffset?: number
	imageMaxSize?: number
	imageMinWidth?: number
	imageMove?: boolean
	imageMultipleStyles?: boolean
	imageOutputSize?: boolean
	imagePaste?: boolean
	imagePasteProcess?: boolean
	imageResize?: boolean
	imageResizeWithPercent?: boolean
	imageRoundPercent?: boolean
	imageSizeButtons?: string[]
	imageSplitHTML?: boolean
	imageStyles?: any
	imageTextNear?: boolean
	imageUploadMethod?: string
	imageUploadParam?: string
	imageUploadParams?: any
	imageUploadToS3?: IFroalaFileUploadToS3
	imageUploadURL?: string

	initOnClick?: any
	inlineStyles?: any
	keepFormatOnDelete?: any
	language?: any
	lineBreakerOffset?: any
	lineBreakerTags?: any
	linkAlwaysBlank?: any
	linkAlwaysNoFollow?: any
	linkAttributes?: any
	linkAutoPrefix?: any
	linkConvertEmailAddress?: any
	linkEditButtons?: any
	linkInsertButtons?: any
	linkList?: any
	linkMultipleStyles?: any
	linkStyles?: any
	linkText?: any
	multiLine?: any
	paragraphFormat?: any
	paragraphFormatSelection?: any
	paragraphMultipleStyles?: any
	paragraphStyles?: any
	pasteAllowLocalImages?: boolean
	pasteAllowedStyleProps?: any
	pasteDeniedAttrs?: any
	pasteDeniedTags?: any
	pastePlain?: any
	placeholderText?: any
	pluginsEnabled?: string[]
	quickInsertButtons?: string[]
	quickInsertTags?: any
	requestHeaders?: any
	requestWithCORS?: any
	requestWithCredentials?: any
	saveInterval?: any
	saveMethod?: any
	saveParam?: any
	saveParams?: any
	saveURL?: any
	scrollableContainer?: any
	shortcutsEnabled?: string[]
	shortcutsHint?: any
	spellcheck?: any
	tabSpaces?: any
	tableCellMultipleStyles?: any
	tableCellStyles?: any
	tableColors?: any
	tableColorsButtons?: any
	tableColorsStep?: any
	tableEditButtons?: any
	tableInsertButtons?: any
	tableInsertHelper?: any
	tableInsertHelperOffset?: any
	tableInsertMaxSize?: any
	tableMultipleStyles?: any
	tableResizer?: any
	tableResizerOffset?: any
	tableResizingLimit?: any
	tableStyles?: any
	theme?: any
	toolbarBottom?: any
	toolbarButtons?: string[]
	toolbarButtonsMD?: string[]
	toolbarButtonsSM?: string[]
	toolbarButtonsXS?: string[]
	toolbarContainer?: any
	toolbarInline?: any
	toolbarSticky?: any
	toolbarStickyOffset?: any
	toolbarVisibleWithoutSelection?: any
	tooltips?: any
	typingTimer?: any
	useClasses?: any
	videoAllowedTypes?: any
	videoDefaultAlign?: any
	videoDefaultDisplay?: any
	videoDefaultWidth?: any
	videoEditButtons?: any
	videoInsertButtons?: any
	videoMaxSize?: number
	videoMove?: any
	videoResize?: any
	videoSizeButtons?: any
	videoSplitHTML?: any
	videoTextNear?: boolean
	videoUploadMethod?: any
	videoUploadParam?: any
	videoUploadParams?: any
	videoUploadToS3?: any
	videoUploadURL?: any
	width?: any
	wordAllowedStyleProps?: any
	wordDeniedAttrs?: any
	wordDeniedTags?: any
	zIndex?: any
}
interface JQuery {
	froalaEditor(options?: IFrolalaOptions): any;
	froalaEditor(method?: string): any;


	on(events: "froalaEditor.file.inserted", handler: (eventObject: any, editor, $file, response) => any): JQuery;
	on(events: "froalaEditor.file.inserted", selector: string, handler: (eventobject: any, editor, $file, response) => any): JQuery;
	on(events: "froalaEditor.file.inserted", selector: string, data: any, handler?: (eventobject: any, editor, $file, response) => any): JQuery;

	off(events: "froalaEditor.file.inserted", handler: (eventobject: any, editor, $file, response) => any): JQuery;
	off(events: "froalaEditor.file.inserted", selector?: string, handler?: (eventobject: any, editor, $file, response) => any): JQuery;

	on(events: "froalaEditor.html.get", handler: (eventObject: any, editor, html) => any): JQuery;
	on(events: "froalaEditor.html.get", selector: string, handler: (eventobject: any, editor, html) => any): JQuery;
	on(events: "froalaEditor.html.get", selector: string, data: any, handler?: (eventobject: any, editor, html) => any): JQuery;

	off(events: "froalaEditor.html.get", handler: (eventobject: any, editor, html) => any): JQuery;
	off(events: "froalaEditor.html.get", selector?: string, handler?: (eventobject: any, editor, html) => any): JQuery;

	on(events: "froalaEditor.html.beforeGet", handler: (eventObject: any, editor) => any): JQuery;
	on(events: "froalaEditor.html.beforeGet", selector: string, handler: (eventobject: any, editor) => any): JQuery;
	on(events: "froalaEditor.html.beforeGet", selector: string, data: any, handler?: (eventobject: any, editor) => any): JQuery;

	off(events: "froalaEditor.html.beforeGet", handler: (eventobject: any, editor) => any): JQuery;
	off(events: "froalaEditor.html.beforeGet", selector?: string, handler?: (eventobject: any, editor) => any): JQuery;

	on(events: "froalaEditor.html.afterGet", handler: (eventObject: any, editor) => any): JQuery;
	on(events: "froalaEditor.html.afterGet", selector: string, handler: (eventobject: any, editor) => any): JQuery;
	on(events: "froalaEditor.html.afterGet", selector: string, data: any, handler?: (eventobject: any, editor) => any): JQuery;

	off(events: "froalaEditor.html.afterGet", handler: (eventobject: any, editor) => any): JQuery;
	off(events: "froalaEditor.html.afterGet", selector?: string, handler?: (eventobject: any, editor) => any): JQuery;

	on(events: "froalaEditor.image.beforePasteUpload", handler: (eventObject: any, editor, img) => any): JQuery;
	on(events: "froalaEditor.image.beforePasteUpload", selector: string, handler: (eventobject: any, editor,  img) => any): JQuery;
	on(events: "froalaEditor.image.beforePasteUpload", selector: string, data: any, handler?: (eventobject: any, editor, img) => any): JQuery;

	off(events: "froalaEditor.image.beforePasteUpload", handler: (eventobject: any, editor, img) => any): JQuery;
	off(events: "froalaEditor.image.beforePasteUpload", selector?: string, handler?: (eventobject: any, editor, img) => any): JQuery;

	on(events: "froalaEditor.image.beforeRemove", handler: (eventObject: any, editor,$img) => any): JQuery;
	on(events: "froalaEditor.image.beforeRemove", selector: string, handler: (eventobject: any, editor,  $img) => any): JQuery;
	on(events: "froalaEditor.image.beforeRemove", selector: string, data: any, handler?: (eventobject: any, editor, $img) => any): JQuery;

	off(events: "froalaEditor.image.beforeRemove", handler: (eventobject: any, editor, $img) => any): JQuery;
	off(events: "froalaEditor.image.beforeRemove", selector?: string, handler?: (eventobject: any, editor, $img) => any): JQuery;


	on(events: "froalaEditor.image.error", handler: (eventObject: any, editor, error) => any): JQuery;
	on(events: "froalaEditor.image.error", selector: string, handler: (eventobject: any, editor, error) => any): JQuery;
	on(events: "froalaEditor.image.error", selector: string, data: any, handler?: (eventobject: any, editor, error) => any): JQuery;

	off(events: "froalaEditor.image.error", handler: (eventobject: any, editor, error) => any): JQuery;
	off(events: "froalaEditor.image.error", selector?: string, handler?: (eventobject: any, editor, error) => any): JQuery;

	on(events: "froalaEditor.image.inserted", handler: (eventObject: any, editor, $img, response) => any): JQuery;
	on(events: "froalaEditor.image.inserted", selector: string, handler: (eventobject: any, editor, $img, response) => any): JQuery;
	on(events: "froalaEditor.image.inserted", selector: string, data: any, handler?: (eventobject: any, editor, $img, response) => any): JQuery;

	off(events: "froalaEditor.image.inserted", handler: (eventobject: any, editor, $img, response) => any): JQuery;
	off(events: "froalaEditor.image.inserted", selector?: string, handler?: (eventobject: any, editor, $img, response) => any): JQuery;


}
