﻿interface IBootstrapTreeViewNodeState {
    checked?: boolean
    disabled?: boolean
    expanded?: boolean
    selected?: boolean
}

interface IBootstrapTreeViewNode {
    text?: string
    icon?: string
    selectedIcon?: string
    color?: string
    backColor?: string
    href?: string
    selectable?: boolean
    state?: IBootstrapTreeViewNodeState
    tags?: Array<string>
	nodes?: Array<IBootstrapTreeViewNode>
}

interface BootstrapTreeViewEventNode extends IBootstrapTreeViewNode {
    nodeId?: number;
    parentId?: number;
}

interface BootstrapTreeViewOptions {

    data?: IBootstrapTreeViewNode[] | any

    injectStyle?: boolean
    levels?: number
    expandIcon?: string
    collapseIcon?: string
    emptyIcon?: string
    nodeIcon?: string
    selectedIcon?: string
    checkedIcon?: string
    uncheckedIcon?: string

    color?: string
    backColor?: string
    borderColor?: string
    onhoverColor?: string
    selectedColor?: string
    selectedBackColor?: string
    searchResultColor?: string
    searchResultBackColor?: string

    enableLinks?: boolean
    highlightSelected?: boolean
    highlightSearchResults?: boolean
    showBorder?: boolean
    showIcon?: boolean
    showCheckbox?: boolean
    showTags?: boolean
    multiSelect?: boolean
}

interface JQuery {
	treeview(options: BootstrapTreeViewOptions): any;
    treeview(method: string, ...args: Array<any | Array<any>>): any
    
    on(events: "nodeChecked", handler: (eventObject: JQueryEventObject) => any): JQuery;
    on(events: "nodeChecked", selector: string, handler: (eventobject: JQueryEventObject) => any): JQuery;
    on(events: "nodeChecked", selector: string, data: any, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    off(events: "nodeChecked", handler: (eventobject: JQueryEventObject) => any): JQuery;
    off(events: "nodeChecked", selector?: string, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    on(events: "nodeCollapsed", handler: (eventObject: JQueryEventObject) => any): JQuery;
    on(events: "nodeCollapsed", selector: string, handler: (eventobject: JQueryEventObject) => any): JQuery;
    on(events: "nodeCollapsed", selector: string, data: any, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    off(events: "nodeCollapsed", handler: (eventobject: JQueryEventObject) => any): JQuery;
    off(events: "nodeCollapsed", selector?: string, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    on(events: "nodeDisabled", handler: (eventObject: JQueryEventObject) => any): JQuery;
    on(events: "nodeDisabled", selector: string, handler: (eventobject: JQueryEventObject) => any): JQuery;
    on(events: "nodeDisabled", selector: string, data: any, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    off(events: "nodeDisabled", handler: (eventobject: JQueryEventObject) => any): JQuery;
    off(events: "nodeDisabled", selector?: string, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    on(events: "nodeEnabled", handler: (eventObject: JQueryEventObject) => any): JQuery;
    on(events: "nodeEnabled", selector: string, handler: (eventobject: JQueryEventObject) => any): JQuery;
    on(events: "nodeEnabled", selector: string, data: any, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    off(events: "nodeEnabled", handler: (eventobject: JQueryEventObject) => any): JQuery;
    off(events: "nodeEnabled", selector?: string, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    on(events: "nodeExpanded", handler: (eventObject: JQueryEventObject) => any): JQuery;
    on(events: "nodeExpanded", selector: string, handler: (eventobject: JQueryEventObject) => any): JQuery;
    on(events: "nodeExpanded", selector: string, data: any, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    off(events: "nodeExpanded", handler: (eventobject: JQueryEventObject) => any): JQuery;
    off(events: "nodeExpanded", selector?: string, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    on(events: "nodeSelected", handler: (eventObject: JQueryEventObject, data: BootstrapTreeViewEventNode) => any): JQuery;
    on(events: "nodeSelected", selector: string, handler: (eventobject: JQueryEventObject, data: BootstrapTreeViewEventNode) => any): JQuery;
    on(events: "nodeSelected", selector: string, data: BootstrapTreeViewEventNode, handler?: (eventobject: JQueryEventObject, data: BootstrapTreeViewEventNode) => any): JQuery;

    off(events: "nodeSelected", handler: (eventobject: JQueryEventObject, data: BootstrapTreeViewEventNode) => any): JQuery;
    off(events: "nodeSelected", selector?: string, handler?: (eventobject: JQueryEventObject, data: BootstrapTreeViewEventNode) => any): JQuery;

    on(events: "nodeUnchecked", handler: (eventObject: JQueryEventObject) => any): JQuery;
    on(events: "nodeUnchecked", selector: string, handler: (eventobject: JQueryEventObject) => any): JQuery;
    on(events: "nodeUnchecked", selector: string, data: any, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    off(events: "nodeUnchecked", handler: (eventobject: JQueryEventObject) => any): JQuery;
    off(events: "nodeUnchecked", selector?: string, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    on(events: "nodeUnselected", handler: (eventObject: JQueryEventObject, data: BootstrapTreeViewEventNode) => any): JQuery;
    on(events: "nodeUnselected", selector: string, handler: (eventobject: JQueryEventObject, data: BootstrapTreeViewEventNode) => any): JQuery;
    on(events: "nodeUnselected", selector: string, data: BootstrapTreeViewEventNode, handler?: (eventobject: JQueryEventObject, data: BootstrapTreeViewEventNode) => any): JQuery;

    off(events: "nodeUnselected", handler: (eventobject: JQueryEventObject, data: BootstrapTreeViewEventNode) => any): JQuery;
    off(events: "nodeUnselected", selector?: string, handler?: (eventobject: JQueryEventObject, data: BootstrapTreeViewEventNode) => any): JQuery;

    on(events: "searchComplete", handler: (eventObject: JQueryEventObject) => any): JQuery;
    on(events: "searchComplete", selector: string, handler: (eventobject: JQueryEventObject) => any): JQuery;
    on(events: "searchComplete", selector: string, data: any, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    off(events: "searchComplete", handler: (eventobject: JQueryEventObject) => any): JQuery;
    off(events: "searchComplete", selector?: string, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    on(events: "searchCleared", handler: (eventObject: JQueryEventObject) => any): JQuery;
    on(events: "searchCleared", selector: string, handler: (eventobject: JQueryEventObject) => any): JQuery;
    on(events: "searchCleared", selector: string, data: any, handler?: (eventobject: JQueryEventObject) => any): JQuery;

    off(events: "searchCleared", handler: (eventobject: JQueryEventObject) => any): JQuery;
    off(events: "searchCleared", selector?: string, handler?: (eventobject: JQueryEventObject) => any): JQuery;
}


