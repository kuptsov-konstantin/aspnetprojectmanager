﻿
interface ISummernoteOptions {
	height?: number
	minHeight?: number
	maxHeight?: number
	focus?: boolean
	toolbar?: any
	lang?: string
	callbacks?: {
		onImageUpload?(files: any): any;
		onInit?(): any
	}
	uploadcare?: IUploadcareOptions
	placeholder?: string
}
interface ISummernoteUiOptions {
	editor?: any
	toolbar?: any
	editingArea?: any
	codable?: any
	editable?: any
	statusbar?: any
	airEditor?: any
	airEditable?: any
	buttonGroup?: any
	button?: any
	dropdown?: any
	dropdownCheck?: any
	palette?: any
	dialog?: any
	popover: any
	icon?: (iconClassName, tagName) => string
	toggleBtn?: ($btn, isEnable) => void
	toggleBtnActive?: ($btn, isActive) => void
	onDialogShown?: ($dialog, handler) => void
	onDialogHidden?: ($dialog, handler) => void
	showDialog?: ($dialog) => void
	hideDialog?: ($dialog) => void
	createLayout?: ($note, options) => {
		note?: any
		editor?: any
		toolbar?: any
		editingArea?: any
		editable?: any
		codable?: any
		statusbar?: any
	}
	removeLayout?: ($note, layoutInfo) => void
}

interface ISummernoteMainOptions{
	version: string
	ui: ISummernoteUiOptions
	dom: ISummernoteDomOptions

	plugins: {},

	options: {
		modules: any[]

		buttons: any

		lang: string

		// toolbar
		toolbar: any

		// popover
		popover: {
			image?: any
			link?: any
			air?: any
		},

		// air mode: inline editor
		airMode: boolean

		width: any
		height: any
		focus: boolean
		tabSize: number
		styleWithSpan: boolean
		shortcuts: boolean
		textareaAutoSync: boolean
		direction: any
		styleTags: any[]
		fontNames: any[]
		fontSizes: any[]
		// pallete colors(n x n)
		colors: any[]
		lineHeights: any[]
		tableClassName: string

		insertTableMaxSize?: {
			col: any
			row: any
		},

		dialogsInBody: boolean
		dialogsFade: boolean

		maximumImageFileSize: any

		callbacks?: {
			onInit: any
			onFocus: any
			onBlur: any
			onEnter: any
			onKeyup: any
			onKeydown: any
			onImageUpload: any
			onImageUploadError: any
		},

		codemirror?: {
			mode: string
			htmlMode: boolean
			lineNumbers: boolean
		},

		keyMap: {
			pc: any[]

			mac: any[]
		},
		icons: any[]
	}
}

interface ISummernoteDomOptions {

	NBSP_CHAR: string
	ZERO_WIDTH_NBSP_CHAR: string,
	blank: string
	emptyPara: string
	makePredByNodeName: (nodeName: string) => Function
	isEditable: (node: any) => boolean
	isControlSizing: (node: any) => boolean
	isText: (node: any) => boolean
	isElement: (node: any) => boolean
	isVoid: (node: any) => boolean
	isPara: (node: any) => boolean
	isPurePara: (node: any) => boolean
	isHeading: (node: any) => boolean
	isInline: (node: any) => boolean
	isBlock: () => boolean
	isBodyInline: (node: any) => boolean
	isBody: Function
	isParaInline: (node: any) => boolean
	isPre: Function
	isList: (node: any) => boolean
	isTable: Function
	isData: Function
	isCell: (node: any) => boolean
	isBlockquote: Function,
	isBodyContainer: (node: any) => boolean
	isAnchor: Function
	isDiv: Function
	isLi: Function
	isBR: Function
	isSpan: Function
	isB: Function
	isU: Function
	isS: Function
	isI: Function
	isImg: Function
	isTextarea: Function
	isEmpty: (node: any) => Boolean
	isEmptyAnchor: (item: any) => any
	isClosestSibling: (nodeA, nodeB) => Boolean
	/*withClosestSiblings: withClosestSiblings,
	nodeLength: nodeLength,
	isLeftEdgePoint: isLeftEdgePoint,
	isRightEdgePoint: isRightEdgePoint,
	isEdgePoint: isEdgePoint,
	isLeftEdgeOf: isLeftEdgeOf,
	isRightEdgeOf: isRightEdgeOf,
	isLeftEdgePointOf: isLeftEdgePointOf,
	isRightEdgePointOf: isRightEdgePointOf,
	prevPoint: prevPoint,
	nextPoint: nextPoint,
	isSamePoint: isSamePoint,
	isVisiblePoint: isVisiblePoint,
	prevPointUntil: prevPointUntil,
	nextPointUntil: nextPointUntil,
	isCharPoint: isCharPoint,
	walkPoint: walkPoint,
	ancestor: ancestor,
	singleChildAncestor: singleChildAncestor,
	listAncestor: listAncestor,
	lastAncestor: lastAncestor,
	listNext: listNext,
	listPrev: listPrev,
	listDescendant: listDescendant,
	commonAncestor: commonAncestor,
	wrap: wrap,
	insertAfter: insertAfter,
	appendChildNodes: appendChildNodes,
	position: position,
	hasChildren: hasChildren,
	makeOffsetPath: makeOffsetPath,
	fromOffsetPath: fromOffsetPath,
	splitTree: splitTree,
	splitPoint: splitPoint,
	create: create,
	createText: createText,
	remove: remove,
	removeWhile: removeWhile,
	replace: replace,
	html: html,
	value: value,
	posFromPlaceholder: posFromPlaceholder,
	attachEvents: attachEvents,
	detachEvents: detachEvents*/
	
}





interface JQuery {

	summernote(): any
	summernote(options?: ISummernoteOptions): any
	summernote(command: string, callback?: () => void): any
}

interface JQueryStatic  {
	summernote?: ISummernoteMainOptions
}
declare function ensureWidget(version: string): any;
declare var UPLOADCARE_MANUAL_START: any;

//declare module "summernote" {
//	export = $;
//}


