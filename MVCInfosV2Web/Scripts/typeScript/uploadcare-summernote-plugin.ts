﻿/*
 * Uploadcare Summernote plugin (1.0.0)
 */

class UploadcarePlugin{
	blobs: IUploadcareBlob[];
	event: Event;
	constructor() {
		this.event = document.createEvent('Event');
	}
	ensureWidget(version) {
		if (typeof uploadcare == 'undefined') $.getScript([
			'https://ucarecdn.com/widget/', version, '/uploadcare/uploadcare.min.js'
		].join(''))
	}

	createButton(context, opts) {
		return () => {
			var icon = opts.buttonIcon ? '<i class="fa fa-' + opts.buttonIcon + '" /> ' : '';

			return $.summernote.ui.button({
				contents: icon + opts.buttonLabel,
				tooltip: opts.tooltipText,
				click: function () {
					var dialog = uploadcare.openDialog({}, opts);
					uploadcarePlugin.blobs = new Array<IUploadcareBlob>();
					context.invoke('editor.saveRange');
					dialog.done(UploadcarePlugin.done(context, opts));
				}
			}).render();
		};
	}

	static done(context?, opts?) {
		return function (data) {
			var isMultiple = opts.multiple;
			var uploads = isMultiple ? data.files() : [data];

			$.when.apply(null, uploads).done(function () {
				var blobs = [].slice.apply(arguments);
				//context.invoke('editor.restoreRange');

				$.each(blobs, function (i, blob: IUploadcareBlob) {
					$(document).trigger("uploadcare.summernote.plugin", [blob]);				
				});
			});
		}
	}	
}


function initUploadPluginContext(context) {
	var opts = $.extend({
		crop: '',
		version: '2.9.0',
		buttonLabel: 'Uploadcare',
		tooltipText: 'Upload files via Uploadcare'
	}, context.options.uploadcare);
	uploadcarePlugin.ensureWidget(opts.version);
	context.memo('button.uploadcare', uploadcarePlugin.createButton(context, opts));
}

function initUploadPlugin() {
	$.extend($.summernote.plugins, { uploadcare: initUploadPluginContext });
}

let uploadcarePlugin = new UploadcarePlugin();