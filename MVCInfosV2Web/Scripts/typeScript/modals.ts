﻿/// <reference path="guid.ts" />
class Modal {
	private insertId = '#modalReplace';
	private header: string;
	private body: string;
	private footer: string;
	private id = "myModalLabel";
	private form = false;
	private main: string;
	private formTags = {
		open: '<form>',
		close: '</form>'
	};
	constructor() {
		this.modalId = Guid.newGuid().toString();
	}

	public set isForm(value) {
		this.form = value;
	}

	public get isForm() {
		return this.form;
	}

	public get insertDivId() {
		return this.insertId;
	}


	public set modalId(value: string) {
		this.id = value;
	}
	public get modalId() {
		return this.id;
	}

	public set modalHeader(value: string) {
		this.header = value;
	}

	public set modalBody(value: string) {
		this.body = value;
	}
	public set modalFooter(value: string) {
		this.footer = value;
	}
	public set mainModal(value: string) {
		this.main = value;
	}
	public get mainModal() {
		return this.main;
	}

	public updateModal() {
		let formTags = Object.assign({}, this.formTags);
		if (this.form == false) {
			formTags.open = '';
			formTags.close = '';
		}
		this.mainModal = `<div>
    <!-- Modal -->
    <div class="modal fade" id="myModal_${this.id}" tabindex="-1" role="dialog" aria-labelledby="${this.id}">
        <div class="modal-dialog" role="document">
			${this.formTags.open}
				<div class="modal-content">
					<div class="modal-header">
					   ${this.header}
					</div>
					<div class="modal-body">
					   ${this.body}
					</div>
					<div class="modal-footer">
					  ${this.footer}
					</div>
				</div>
			${this.formTags.close}
        </div>
    </div>
</div>`;
		return this.mainModal;
	}
	public setModalBody(modalBody: string) {
		this.modalBody = modalBody;
	}
	public getModal() {
		return this.updateModal();
	}

	public initButton(selector: string, func: () => any) {
		$(selector).on('click', () => {
			func();
		})
	}

	public modalReplacer() {
		$(`${this.insertId}`).replaceWith(`<div id="modalReplace">${this.getModal()}</div>`);
	}

	public modalShow() {
		$(`${this.insertId} .modal`).modal('show');
	}

	public modalClose() {
		$(`${this.insertId} .modal`).modal('hide');
	}

	public static getFormGroups(objs: IModalFormGroupGenerate[]): string[] {
		var str = new Array<string>();
		objs.forEach((value, index, array) => {
			str.push(Modal.getFormGroup(value));
		});
		return str;
	}

	public static getFormGroup(obj: IModalFormGroupGenerate): string {
		let required = "";
		if (obj.isRequired) {
			required = "required";
		}
		if (obj.type !== 'date') {
			return `<div class="form-group">
						<label for="${obj.id}" class="control-label col-md-3 col-xs-3" >${obj.label}</label>
						<div class="col-xs-9 col-lg-9 col-md-9">
							<input type="${obj.type}" class="form-control" id="${obj.id}" placeholder="${obj.label}" ${required} name="${obj.name}">
						</div>
					</div>`;
		} else {
			return `<div class="form-group">
						<label for="${obj.id}" class="control-label col-md-3 col-xs-3" >${obj.label}</label>
						<div class="col-xs-9 col-lg-9 col-md-9">
							<div class='input-group date' id='datetimepicker${obj.id}'>
								<input type="${obj.type}" class="form-control" id="datetimepicker1$${obj.id}" placeholder="${obj.label}" ${required} name="${obj.name}">
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar" />
								</span>
							</div>
						</div>
					</div>`;
		}
	}
}
interface IModalFormGroupGenerate {
	id: string;
	type: EInputType;
	label: string;
	name: string;
	isRequired: boolean;
}

class ModalEmployeeDelete extends Modal {

	private langDelete = "Удалить";
	private langYes = "Да";
	private langNo = "Нет";

	constructor() {
		super();
		this.modalHeader = `<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="${this.modalId}">${this.langDelete}</h4>`;
		this.modalBody = ``;
		this.modalFooter = `<button type="button" class="btn btn-success" data-dismiss="modal" data-type="no">${this.langNo}!</button>
 <button type="button" class="btn btn-danger" data-dismiss="modal" data-type="yes">${this.langYes}!</button>`;
		this.mainModal = this.updateModal();
	}

}

class ModalEmployeeInfo extends Modal {
	private langInfo = "Информация";
	private langClose = "Закрыть";
	constructor() {
		super();
		this.modalHeader = `<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="${this.modalId}">${this.langInfo}</h4>`;
		this.modalBody = ``;
		this.modalFooter = `<button type="button" class="btn btn-default" data-dismiss="modal" data-type="close">${this.langClose}</button>`;
		this.mainModal = this.updateModal();
	}
}

class ModalEmployeeEdit extends Modal {
	private langInfo = "Редактировать";
	private langClose = "Закрыть";
	constructor() {
		super();
		this.modalHeader = `<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="${this.modalId}">${this.langInfo}</h4>`;
		this.modalBody = ``;
		this.modalFooter = `<button type="button" class="btn btn-default" data-dismiss="modal" data-type="close">${this.langClose}</button>`;
		this.mainModal = this.updateModal();
	}
}

class ModalEmployeeAdd extends Modal {
	private langInfo = "Добавить";
	private langClose = "Закрыть";
	private langSave = "Сохранить";
	constructor() {
		super();
		this.modalHeader = `<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="${this.modalId}">${this.langInfo}</h4>`;
		this.modalBody = ``;
		this.modalFooter =
			`<button type="button" class="btn btn-default" data-dismiss="modal" data-type="close">${this.langClose}</button>` +
			`<button type="button" class="btn btn-success" data-type="save">${this.langSave}</button>`;
		this.mainModal = this.updateModal();
	}
	public initValidator() {
		$(`${this.insertDivId} form`).validate({
			rules: {
				password: "required",
				password_confirm: {
					equalTo: "#addPass"
				}
			}
		});
	}
	public isValid(): boolean {
		if (this.isForm == true) {
			return $(`${this.insertDivId} form`).valid();
		}
	}
}

class ModalMailEdit extends Modal {
	private langInfo = "Редактировать";
	private langClose = "Закрыть";
	private langSave = "Сохранить";
	constructor() {
		super();
		this.modalHeader = `<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="${this.modalId}">${this.langInfo}</h4>`;
		this.modalBody = ``;
		this.modalFooter = `<button type="button" class="btn btn-default" data-dismiss="modal" data-type="close">${this.langClose}</button>` +
			`<button type="button" class="btn btn-success" data-type="save">${this.langSave}</button>`;;
		this.mainModal = this.updateModal();
	}
	public initValidator() {
		$(`${this.insertDivId} form`).validate({
			rules: {
				password: "required",
				password_confirm: {
					equalTo: "#addPass"
				}
			}
		});
	}
	public isValid(): boolean {
		if (this.isForm == true) {
			return $(`${this.insertDivId} form`).valid();
		}
	}
}

class ModalMailDelete extends Modal {

	private langDelete = "Удалить";
	private langYes = "Да";
	private langNo = "Нет";

	constructor() {
		super();
		this.modalHeader = `<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="${this.modalId}">${this.langDelete}</h4>`;
		this.modalBody = ``;
		this.modalFooter = `<button type="button" class="btn btn-success" data-dismiss="modal" data-type="no">${this.langNo}!</button>
 <button type="button" class="btn btn-danger" data-dismiss="modal" data-type="yes">${this.langYes}!</button>`;
		this.mainModal = this.updateModal();
	}

}


class ModalNotificationInfo extends Modal {
	private langInfo = "Информация об уведмлениях";
	private langClose = "Закрыть";
	constructor() {
		super();
		this.modalHeader = `<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title" id="${this.modalId}">${this.langInfo}</h4>`;
		this.modalBody = ``;
		this.modalFooter = `<button type="button" class="btn btn-default" data-dismiss="modal" data-type="close">${this.langClose}</button>`;
		this.mainModal = this.updateModal();
	}
}