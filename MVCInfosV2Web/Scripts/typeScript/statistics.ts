﻿function getRandomColor() {
	var letters = '0123456789ABCDEF';
	var color = '#';
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}
class Color {
	H: number;
	S: number;
	L: number;
	A: number;


	//R: number;
	//G: number;
	//B: number;
	//A: number;
	//toRGB() {
	//	return 'rgb(' + (this.R) + ',' + (this.G) + ',' + (this.B) + ')';
	//}
	//toRGBA() {
	//	return 'rgba(' + (this.R) + ',' + (this.G) + ',' + (this.B) + ',' + (this.A) + ')';
	//}

	toHSL() {
		return `hsl(${this.H},${this.S}%,${this.L}%)`;
	}
	toHSLA() {
		return `hsl(${this.H},${this.S}%,${this.A}%)`;
	}
}
function getRandomArbitrary(min, max) {
	return Math.random() * (max - min) + min;
}
function getRundomRGBColor(): Color {
	//H - 75, 150
	//S - 20, 100
	//L - 20, 50

	//HSLA - 60, 75
	var color = new Color();

	/*var rint = Math.floor(0x100000000 * Math.random());
	
	color.R = rint & 255;
	color.G = rint >> 8 & 255;
	color.B = rint >> 16 & 255;*/
	color.H = Math.floor(getRandomArbitrary(75, 150));
	color.S = Math.floor(getRandomArbitrary(20, 100));
	color.L = Math.floor(getRandomArbitrary(30, 50));
	color.A = Math.floor(getRandomArbitrary(55, 75));



	return color;
}






function randomColor(format) {
	var rint = Math.floor(0x100000000 * Math.random());
	switch (format) {
		case 'hex':
			return '#' + ('00000' + rint.toString(16)).slice(-6).toUpperCase();
		case 'hexa':
			return '#' + ('0000000' + rint.toString(16)).slice(-8).toUpperCase();
		case 'rgb':
			return 'rgb(' + (rint & 255) + ',' + (rint >> 8 & 255) + ',' + (rint >> 16 & 255) + ')';
		case 'rgba':
			return 'rgba(' + (rint & 255) + ',' + (rint >> 8 & 255) + ',' + (rint >> 16 & 255) + ',' + (rint >> 24 & 255) / 255 + ')';
		default:
			return rint;
	}
}


interface IPlotNotificationClicksInRangeViewModel {
	start: Date;
	end: Date;
	firmId: number;
	legend: { [id: string]: string; }
	plotDictionary: { [id: string]: IPlotNotificationClicksInDayViewModel; }
}


interface IPlotNotificationClicksInDayViewModel {
	counts: { [id: string]: number; }
	date: Date;
}

interface IChartInfo {
	label?: string;
	percent?: number;
}

interface IChartInfoColor extends IChartInfo {
	color?: Color;
}
interface IGraphColors {
	color?: Color;
	id?: number;
}

interface IChartInfoSelectorInit {
	month?: number;
	year?: number;
}

interface IPlotInitialise {

	selectPickerSelector: string
	dateRangeSelector: string
	plot: {
		plotSelector: string
		dashBoardSelector: string
	}

}
class LoadPlot {
	plotInit: IPlotInitialise;
	arr_data1 = <jquery.flot.dataSeries>{
		label: "Бябя", clickable: true,
		color: 0,
		data: [
			[gd(2012, 1, 1), 17],
			[gd(2012, 1, 2), 74],
			[gd(2012, 1, 3), 6],
			[gd(2012, 1, 4), 39],
			[gd(2012, 1, 5), 20],
			[gd(2012, 1, 6), 85],
			[gd(2012, 1, 7), 7]
		]
	};

	arr_data2 = <jquery.flot.dataSeries>{
		label: "Дваыаываы",
		clickable: true,
		color: 1,
		data: [
			[gd(2012, 1, 1), 82],
			[gd(2012, 1, 2), 23],
			[gd(2012, 1, 3), 66],
			[gd(2012, 1, 4), 9],
			[gd(2012, 1, 5), 119],
			[gd(2012, 1, 6), 6],
			[gd(2012, 1, 7), 9]
		]
	};

	chart_plot_settings = <jquery.flot.plotOptions>{
		series: <jquery.flot.seriesOptions>{
			lines: {
				show: false,
				fill: true
			},
			splines: {
				show: true,
				tension: 0.4,
				lineWidth: 1,
				fill: 0.4
			},
			points: {
				radius: 0,
				show: true
			},
			shadowSize: 2
		},
		grid: {
			verticalLines: true,
			hoverable: true,
			clickable: true,
			tickColor: "#d5d5d5",
			borderWidth: 1,
			color: '#fff'
		},
		colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
		xaxis: {
			monthNames: moment.monthsShort(),
			tickColor: "rgba(51, 51, 51, 0.06)",
			mode: "time",
			tickSize: [1, "day"],
			//tickLength: 10,
			axisLabel: "Date",
			axisLabelUseCanvas: true,
			axisLabelFontSizePixels: 12,
			axisLabelFontFamily: 'Verdana, Arial',
			axisLabelPadding: 10
		},
		yaxis: {
			ticks: 8,
			tickColor: "rgba(51, 51, 51, 0.06)",
		}
		//,
		//legend: <jquery.flot.legendOptions>{
		//	le: "#73879C"
		//},
		//tooltip: true
	}



	initIDs(plotInit: IPlotInitialise) {
		this.plotInit = {
			dateRangeSelector: plotInit.dateRangeSelector,
			plot: plotInit.plot,
			selectPickerSelector: plotInit.selectPickerSelector
		};
		initSelector(this.plotInit.selectPickerSelector);
		$(this.plotInit.selectPickerSelector).on("changed.bs.select", (e) => {
			console.log(e);
			this.updateGraph();
		});
		this.init_daterangepicker();
		this.updateGraph();




	
	}


	updateGraph() {
		var value = $(this.plotInit.selectPickerSelector).val() as number;
		var startDate = (<daterangepicker.Settings>$(this.plotInit.dateRangeSelector).data('daterangepicker')).startDate;
		var endDate = (<daterangepicker.Settings>$(this.plotInit.dateRangeSelector).data('daterangepicker')).endDate;
		var forSend = {
			start: startDate,
			end: endDate,
			firmId: $(this.plotInit.selectPickerSelector).val()

		};
		$.ajax('GetNotificationClicksByRange', {
			data: {
				parametrs: btoa(escape(JSON.stringify(forSend)))
			},
			error: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => {


			},
			success: (data: IPlotNotificationClicksInRangeViewModel, textStatus: string, jqXHR: JQueryXHR) => {
				console.log(data.legend);
				var sumClicks = this.getClicksSum(data);
				var lagendRight = this.createProgressBars(data.legend, sumClicks);
				$(this.plotInit.plot.dashBoardSelector).html(lagendRight);
				updateProgressBars();
				var plotDatas = this.getPlotDatas(data);
				this.updatePlot(plotDatas);
				//this.init_flot_chart();
			}

		});
	}

	getPlotDatas(data: IPlotNotificationClicksInRangeViewModel) {
		var datas: { [id: string]: jquery.flot.dataSeries; };
		datas = {};
		var clolors: { [id: string]: IGraphColors; };
		var colorList = new Array<string>();
		clolors = {};
		var colorsCounter = 0;
		for (var key in data.legend) {
			var colorgen = getRundomRGBColor();
			clolors[key] = {
				id: colorsCounter,
				color: colorgen
			};
			colorList.push(colorgen.toHSL());
			colorsCounter++;
		}
		this.chart_plot_settings.colors = colorList;
		for (var key in data.plotDictionary) {
			var momentDate = moment(key);
			var date = momentDate.toDate().getTime();
			console.log(date);

			var value = data.plotDictionary[key];
			for (var keyCount in value.counts) {
				if (datas[keyCount] === undefined) {
					datas[keyCount] = {
						
						label: data.legend[keyCount],
						clickable: true,
						color: clolors[keyCount].color.toHSL(),
						data: []

					};
				}
				datas[keyCount].data.push([date, value.counts[keyCount]]);
			}
		}

		var dataList = new Array<jquery.flot.dataSeries>();
		for (var key in datas) {
			var value1 = datas[key];
			dataList.push(value1);
		}
		return dataList;
	}

	getClicksSum(data: IPlotNotificationClicksInRangeViewModel) {

		var clicks: { [id: string]: number; }
		clicks = {};
		for (var key in data.plotDictionary) {
			var value = data.plotDictionary[key];
			for (var keyCount in value.counts) {
				if (clicks[keyCount] === undefined) {
					clicks[keyCount] = 0;
				}
				clicks[keyCount] += value.counts[keyCount];
			}
		}
		return clicks;
	}

	createProgressBars(legend: { [id: string]: string; }, sumClicks: { [id: string]: number; }) {

		var allCount = 0;
		for (var key in sumClicks) {
			allCount += sumClicks[key];

		}

		var htmlInfo = "";
		var allHtmlInfo = "";
		var counter = 0; var allCounter = 0;
		for (var key in legend) {
			var value = legend[key];
			var sum = sumClicks[key];
			htmlInfo += `<div>
								<p>${value}</p>
								<div class="">
									<div class="progress progress_sm" style="width: 76%;">
										<div class="progress-bar bg-green" role="progressbar" data-transitiongoal="${sum / allCount * 100}"></div>
									</div>
								</div>
							</div>`;

			allCounter++;
			counter++;
			if (counter === 2) {
				allHtmlInfo += `<div class="col-md-6 col-sm-6 col-xs-6">${htmlInfo}</div>`
				htmlInfo = "";
				counter = 0;
			}

		}
		if (allCounter % 2 != 0) {
			allHtmlInfo += `<div class="col-md-6 col-sm-6 col-xs-6">${htmlInfo}</div>`
		}
		return allHtmlInfo;
	}

	init_daterangepicker() {

		if (typeof ($.fn.daterangepicker) === 'undefined') { return; }
		console.log('init_daterangepicker');

		var cb = (start, end, label) => {
			console.log(start.toISOString(), end.toISOString(), label);
			$(this.plotInit.dateRangeSelector + ' span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		};

		var optionSet1 = <daterangepicker.Settings>{
			startDate: moment().subtract(29, 'days'),
			endDate: moment(),
			minDate: '01/01/2012',
			maxDate: '12/31/2015',
			dateLimit: {
				days: 60
			},
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Сегодня': [moment(), moment()],
				'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
				'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
				'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
				'Прошлый месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'left',
			buttonClasses: ['btn btn-default'],
			applyClass: 'btn-small btn-primary',
			cancelClass: 'btn-small',
			format: 'MM/DD/YYYY',
			separator: ' по ',
			locale: {
				applyLabel: 'Применить',
				cancelLabel: 'Очистить',
				fromLabel: 'С',
				toLabel: 'По',
				customRangeLabel: 'Границы',
				//	daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
				//monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				//firstDay: 1
			}
		};

		$(this.plotInit.dateRangeSelector + ' span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
		$(this.plotInit.dateRangeSelector).daterangepicker(optionSet1, cb);
		$(this.plotInit.dateRangeSelector).on('show.daterangepicker', () => {
			console.log("show event fired");
		});
		$(this.plotInit.dateRangeSelector).on('hide.daterangepicker', () => {
			console.log("hide event fired");
		});
		$(this.plotInit.dateRangeSelector).on('apply.daterangepicker', (ev, picker) => {
			console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
			this.updateGraph();
		});
		$(this.plotInit.dateRangeSelector).on('cancel.daterangepicker', (ev, picker) => {
			console.log("cancel event fired");
		});
		$('#options1').click(() => {

			$(this.plotInit.dateRangeSelector).data('daterangepicker').setOptions(optionSet1, cb);
		});
		//$('#options2').click(function () {
		//	$('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
		//});
		$('#destroy').click(() => {
			$(this.plotInit.dateRangeSelector).data('daterangepicker').remove();
		});

	}

	//init_flot_chart() {
	//	if (typeof ($.plot) === 'undefined') { return; }
	//	console.log('init_flot_chart');
	//	this.updatePlot([this.arr_data1, this.arr_data2]);	
	//}

	updatePlot(datas: jquery.flot.dataSeries[]) {
		if ($(this.plotInit.plot.plotSelector).length) {
			console.log('update plot');			
			$.plot($(this.plotInit.plot.plotSelector), datas, this.chart_plot_settings);
		}
	}
}



class LoadCharts {
	chartsIds: Guid[];
	constructor() {
		this.chartsIds = new Array<Guid>()
	}

	generateColor(data: IChartInfo[]) {
		var withColors = new Array<IChartInfoColor>();
		var colorsGen = data.map((value, index, array) => {
			var color = getRundomRGBColor();
			color.A = 0.75;
			var colorData = <IChartInfoColor>value;
			colorData.color = color;
			return colorData;
		});
		return colorsGen;
	}


	addChart(id: Guid, firstDate: Date, lastDate: Date, idFirm: number, data: IChartInfoColor[]): string {

		//init_flot_chart();
		//init_daterangepicker();

		var trs = '';
		data.forEach((value, index, array) => {
			trs += `<tr><td><p><i class="fa fa-square" style="color: ${value.color.toHSL()};"></i>${value.label}</p></td><td>${value.percent}%</td></tr>`;
		});
		var chart = `<div class="col-md-4 col-sm-4 col-xs-12" id="chart-${id.id}">
			<div class="x_panel tile fixed_height_320 overflow_hidden">
				<div class="x_title">
					<h2>Письма за ${moment(firstDate).format('LL')} - ${moment(lastDate).format('LL')}</h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<table class="" style="width:100%">
						<tr>
							<th style="width:37%;"><p>За период</p></th>
							<th>
								<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7"><p class="">Тип</p></div>
								<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"><p class="">Процент</p></div>
							</th>
						</tr>
						<tr>
							<td><canvas class="canvasDoughnut" height="140" width="140" style="margin: 15px 10px 10px 0"></canvas></td>
							<td><table class="tile_info">${trs}</table></td>
						</tr>
					</table>
				</div>
			</div>
		</div>`;
		return chart;
	}

	initChartDoughnut(selectors: IChartInfoSelectorInit[]) {
		$.ajax('/Firm/GetMyFirms', {
			error: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => {

				console.log(textStatus);
			},
			success: (data: IFirmModel[], textStatus: string, jqXHR: JQueryXHR) => {
				data.forEach((firm, elem, array) => {
					selectors.forEach((index, elem, array) => {
						var firstDay = new Date(index.year, index.month, 1);
						var lastDay = new Date(index.year, index.month + 1, 0);
						this.loadMailStatistic(firstDay, lastDay, firm);
						//$.get(`/Statistic/MailStatistic?parametrs=${btoa(escape(JSON.stringify({
						//	start: firstDay.toJSON(),
						//	end: lastDay.toJSON(),
						//	firmId: firm.idFirm
						//})))}`, (data: IChartInfo[], textStatus, jqXHR) => {
						//	var newData = this.generateColor(data);
						//	var id = Guid.newGuid();
						//	var chart = this.addChart(id, firstDay, lastDay, firm.idFirm, newData);
						//	$('#mailsStatistic').append(chart);
						//	this.init_chart_doughnut(id, newData);
						//})
					});
				});
			}
		});
		//$.get('/Firm/GetMyFirms', (data: IFirmModel[], textStatus, jqXHR) => {

		//});
	}

	loadMailStatistic(firstDay, lastDay, firm: IFirmModel) {
		//$.get(`/Statistic/MailStatistic?parametrs=${btoa(escape(JSON.stringify({
		//	start: firstDay.toJSON(),
		//	end: lastDay.toJSON(),
		//	firmId: firm.idFirm
		//})))}`, (data: IChartInfo[], textStatus, jqXHR) => {
		//	var newData = this.generateColor(data);
		//	var id = Guid.newGuid();
		//	var chart = this.addChart(id, firstDay, lastDay, firm.idFirm, newData);
		//	$('#mailsStatistic').append(chart);
		//	this.init_chart_doughnut(id, newData);
		//	});
		$.ajax(`/Statistic/MailStatistic?parametrs=${btoa(escape(JSON.stringify({
			start: firstDay.toJSON(),
			end: lastDay.toJSON(),
			firmId: firm.idFirm
		})))}`, {
				error: (jqXHR: JQueryXHR, textStatus: string, errorThrown: string) => {

					console.log(textStatus);
				},
				success: (data: IChartInfo[], textStatus, jqXHR) => {
					var newData = this.generateColor(data);
					var id = Guid.newGuid();
					var chart = this.addChart(id, firstDay, lastDay, firm.idFirm, newData);
					$('#mailsStatistic').append(chart);
					this.init_chart_doughnut(id, newData);
				}
			});
	}

	init_chart_doughnut(id: Guid, data: IChartInfoColor[]) {

		if (typeof (Chart) === 'undefined') { return; }

		console.log('init_chart_doughnut ' + id.id);

		if ($(`#chart-${id.id} .canvasDoughnut`).length) {
			var labels = data.map((value, index, array) => {
				return value.label;
			});
			var datasets = data.map((value, index, array) => {
				return value.percent;
			});
			var backgroundColors = data.map((value, index, array) => {
				return value.color.toHSL();
			});
			var hoverBackgroundColor = data.map((value, index, array) => {
				return value.color.toHSLA();
			});


			var chart_doughnut_settings = {
				type: 'doughnut',
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: {
					labels: labels,
					datasets: [{
						data: datasets,
						backgroundColor: backgroundColors,
						hoverBackgroundColor: hoverBackgroundColor
					}]
				},
				options: {
					legend: false,
					responsive: false
				}
			}
			var ctx = $(`#chart-${id.id} .canvasDoughnut`);
			$(ctx).each((index, elem) => {
				var chart_doughnut = new Chart(elem as HTMLCanvasElement, chart_doughnut_settings);

			});
		}
	}
}
let loadPlot = new LoadPlot();
let loadCharts = new LoadCharts();