﻿/// <reference path="Mail/mailInterfaces.ts" />

function resolveNotificationClick(clickType: ENotificationsClickType): string {
	switch (clickType) {
		case 0: {
			return "Просмотрено письмо";
		}
		case 1: {
			return "Подтверждено получение";
		}
		case 2: {
			return "Уведомление отложено";
		}
		case 3: {
			return "Нет реакции";
		}
		default: {
			return "Нет информации";
		}
	};
}

class Mail {
	mailList: {
		[id: string]: IMailListItemModelView;
	}
	userId: string;

	constructor() {
		moment.locale('ru');
		this.mailList = {};
	}

	setUserId(userId: string) {
		this.userId = userId;
	}

	selected(elements: HTMLElement[], selectedId: number) {
		elements.forEach((value, index, array) => {
			if ($(value).parent().hasClass('current-page')) {
				$(value).parent().removeClass('current-page');
			}
			$(elements[selectedId]).parent().addClass('current-page');
		});
	}

	initLinks(element: string) {
		$(".collapse_menu").on('click', (e) => {
			$(e.target).parent().find('.mail_type_column_children').toggle(400);
		});

		var elements = $(element).toArray();

		if ((elements.length == 0) || (elements === undefined) || (elements === null)) {
			return;
		}

		elements.forEach((value, index, array) => {
			$(value).parent().on('click', (e) => {
				this.selected(elements, index);
				$(".mail_list_column > div.messages-header").html("Загрузка...");
				var url = $(e.target).find("a[link]").attr("link");
				if (url === undefined) {
					url = $(e.target).attr("link");
				}
				if (url === undefined) {
					url = $(e.target).parent().attr("link");
				}
				this.loadMails(url);
			});
		});
		this.selected(elements, 0);
		$(elements[0]).click();
	}

	loadMails(url: string) {
		$.getJSON(url, (data: IMailListItemModelView[], textStatus, jqXHR) => {
			$(".mail_list_column > div.messages-header").html("");
			sessionStorage.setItem('mailList', JSON.stringify(data));

			data.forEach((value, index, array) => {
				this.mailList[value.mailId] = value;
				this.appendMail(value);
			});
			$(".mail_list_column > div.messages-header > a").each((index, elem) => {
				$(elem).on('click', (e) => {
					$('.mail_list_column > div.messages-header > a .mail_list').removeClass('selected_mail');

					var anchor = e.currentTarget as HTMLAnchorElement;
					var mailId = $(anchor).attr("mailid");
					var mailList = $(anchor).find('.mail_list');
					mailList.addClass('selected_mail');


					this.loadMailInfo(mailId);

					console.log(mailId);
				});
			});
		})
	}

	loadMailInfo(mailId: string) {
		$.getJSON('/Mail/GetMail?mailId=' + mailId, (data: IMailModelView, textStatus, jqXHR) => {

			this.mailInfo(data);

			if (data.uploadGroupFile !== null && data.uploadGroupFile !== undefined) {
				data.uploadGroupFile.uploadFiles.forEach((value, index, array) => {
					$.getJSON(`/Upload/info/?jsonerrors=1&file_id=${value.uploadFileID}`, (data: IUploadcareFileModelView, textStatus, jqXHR) => {
						var url = "";
						if (data.is_image == true) {
							url = `<img src="/Upload/${value.uploadFileID}/-/quality/lightest/-/scale_crop/150x150/center/" alt= "img" />`;
						} else {
							url = `<img src="/Upload/FileTypeResolve?fileId=${value.uploadFileID}" alt="img" />`;
						}
						var file = `<li>
										<a href="#" class="atch-thumb">${url}</a>
										<div class="file-name">${data.original_filename}</div>
										<span>${formatBytes(data.size)}</span>
										<div class="links">
											<a href="/Upload/${value.uploadFileID}/-/preview/" alt="img" data-lightbox="additionalfiles" data-title="${data.original_filename}" class="original-filename"">Просмотр</a> -
											<a href="#" alt="img"  onclick="startDownload('/Upload/${value.uploadFileID}/-/preview/'); return false"">Скачать</a>
										</div>
									</li>`;
						$(`#file-preview`).append(file);
					});
				});
			}
		});
	}


	appendMail(mail: IMailListItemModelView) {
		var date = moment(mail.registerDate).format('L');
		var edit = '', hasFiles = '';
		if (mail.isEditeble === true) {
			edit = `<i class="fa fa-edit"></i>`;
		}
		if (mail.hasInnerFiles === true) {
			hasFiles = `<i class="fa fa-paperclip"></i>`;
		}

		var model = `<a href="#" mailId=${mail.mailId}>
							<div class="mail_list">
								<div class="left">
									${edit} ${hasFiles}
								</div>
								<div class="right">
									<h3>${mail.custumer} <small>${date}</small></h3>
									<p>${mail.description}</p>
								</div>
							</div>
						</a>`;
		$(".mail_list_column > div.messages-header").append(model);
	}



	mailInfo(mail: IMailModelView) {

		var registerDate = moment(mail.registerDate).format('LL');
		var finishDate = moment(mail.finishDate).format('LL');
		var documentDate = moment(mail.documentDate).format('LL');

		$('.mail_view').html('');
		var buttons = '';

		if (mail.isCanEdit) {
			buttons = `<div class="btn-group">
										<button id="replayMail" class="btn btn-sm btn-primary" type="button"><i class="fa fa-reply"></i> Ответить</button>
										<button id="notificationMail" class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Печать"><i class="fa fa-bell-o"></i></button>
										<button id="editMail" class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Редактировать"><i class="fa fa-pencil"></i></button>
										<button id="printMail" class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Печать"><i class="fa fa-print"></i></button>
										<button id="removeMail" class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Удалить"><i class="fa fa-trash-o"></i></button>
									</div>`;
		} else {
			buttons = `<div class="btn-group">
										<button id="replayMail" class="btn btn-sm btn-primary" type="button"><i class="fa fa-reply"></i> Ответить</button>
										<button id="printMail" class="btn btn-sm btn-default" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Печать"><i class="fa fa-print"></i></button>
									</div>`;
		}

		/*
<strong>Jon Doe</strong>
<span>(jon.doe@gmail.com)</span> to
<strong>me</strong>
<a class="sender-dropdown"><i class="fa fa-chevron-down"></i></a>
		 */
		var comment = '';
		/*var resolution = `<strong>Из ${this.mailList[mail.mailId].custumer} </strong><strong>мне</strong><a href="#" class="sender-dropdown"><i class="fa fa-chevron-down"></i></a>`;*/
		var tempListResolution = ``;
		if (mail.resolution !== null && mail.resolution !== undefined) {
			comment = `${mail.resolution.comment}`;
			mail.resolution.resolutionNotifier.forEach((value, index, array) => {
				if (value.userId !== this.userId) {
					tempListResolution += `<li>${value.fio}</li>`;
				}
			});
		}


		var resolutions = `<ul class="sender-list">${tempListResolution}</ul>`;

		var info = `<div class="mail-info">
	<dl>
		<dt>Отправитель: </dt>
		<dd> ${this.mailList[mail.mailId].custumer} </dd>
	</dl>
	<div>
		<label>
			Резолюция:
		</label>
		<ul>
			<li>Комментарий: ${comment}</li>
			<li>Сотрудникам: <a href="#" class="sender-dropdown">Развернуть список <i class="fa fa-chevron-down"></i></a></li>
		</ul>
		${resolutions}
	</div>
</div>`;


		var file = '';
		if (mail.uploadGroupFile !== null && mail.uploadGroupFile !== undefined) {
			file = `<p>
									<span><i class="fa fa-paperclip"></i> ${mail.uploadGroupFile.uploadFiles.length} прикрепленных файла — </span>
									<a href="#">Скачать все файлы</a> |
									<a href="#">Просмотреть все изображения</a>
								</p>
								<ul id="file-preview">
									
								</ul>`;
		}
		var mailTemplate = `<div class="inbox-body">
								<div class="mail_heading row">
									<div class="col-md-8">${buttons}</div>
									<div class="col-md-4 text-right">
										<p class="date"> ${registerDate} - ${finishDate}</p>
									</div>
									<div class="col-md-12">
										<h4>${mail.description} </h4>
									</div>
								</div>
								<div class="sender-info">
									<div class="row">
										<div class="col-md-12"> ${info}</div>
									</div>
								</div>
								<div class="view-mail">
		
								</div>
								<div class="attachment">
									${file}
								</div>
								${buttons}
							</div>`;
		$('.mail_view').html(mailTemplate);
		$('.sender-dropdown').off('click');
		$('.sender-dropdown').on('click', (e) => {
			$(".sender-list").toggle(400);
		});
		$('.sender-list').hide();
		$('[data-toggle="tooltip"]').tooltip({
			container: 'body'
		});
		if (mail.isCanEdit) {
			$('#notificationMail').off();
			$('#editMail').off();
			$('#removeMail').off();
			$('#editMail').on('click', (e) => {
				let modalAdd = new ModalMailEdit();
				//id: string, type: EInputType, label, name, isRequired
				let inputs = Modal.getFormGroups([
					{
						id: "editDocumentID",
						type: 'number',
						label: "Вход №№ документа п-п",
						name: "DocumentID",
						isRequired: true
					},
					{
						id: "editRegisterDate",
						type: 'date',
						label: "Дата регистрации входящего документа",
						name: "registerDate",
						isRequired: true
					},
					{
						id: "editCustumer",
						type: 'text',
						label: "От кого",
						name: "editCustumer",
						isRequired: true
					},
					{
						id: "editNumberDocument",
						type: 'text',
						label: "№№ документа",
						name: "numberDocument",
						isRequired: true
					},
					{
						id: "editDocumentDate",
						type: 'date',
						label: "Дата документа",
						name: "documentDate",
						isRequired: true
					},
					{
						id: "editDescription",
						type: 'text',
						label: "По какому вопросу",
						name: "description",
						isRequired: true
					},
					{
						id: "editFinishDate",
						type: 'date',
						label: "Срок исполнения",
						name: "finishDate",
						isRequired: true
					},




					{
						id: "addRepeatPass",
						type: 'password',
						label: "Повторить пароль",
						name: "password_confirm",
						isRequired: true
					},
					{
						id: "addFamily",
						type: 'text',
						label: "Фамилия",
						name: "family",
						isRequired: true
					},
					{
						id: "addName",
						type: 'text',
						label: "Имя",
						name: "name",
						isRequired: true
					},
					{
						id: "addMiddle",
						type: 'text',
						label: "Отчество",
						name: "middle",
						isRequired: true
					},
					{
						id: "addRegisterDate",
						type: 'date',
						label: "Дата",
						name: "registerDate",
						isRequired: true
					}
				]);
				//<input class="form-control text-box single-line valid" data-val="true" data-val-number="Значением поля Вход №№ документа п-п должно быть число." data-val-required="Требуется поле Вход №№ документа п-п." id="DocumentID" name="DocumentID" type="number" value="0" aria-required="true" aria-describedby="DocumentID-error" aria-invalid="false">
				let modalBody = `<div class="form-horizontal">${inputs.join('')}</div>`;
				modalAdd.isForm = true;
				modalAdd.setModalBody(modalBody);
				modalAdd.modalReplacer();
				modalAdd.initValidator();
				modalAdd.initButton('#modalReplace .modal-footer [data-type="save"]', () => {
					if (modalAdd.isValid()) {

					}
				});
				initDatePicker();
				modalAdd.modalShow();
			});
			$('#removeMail').on('click', (e) => {
				var modalDelete = new ModalMailDelete();

				var modalBody = `<dl class="dl-horizontal">
                        <dt>Описание:</dt><dd>${mail.description}</dd>
                    </dl>`;
				modalDelete.setModalBody(modalBody);
				modalDelete.modalReplacer();
				modalDelete.initButton('#modalReplace .modal-footer [data-type="no"]', () => {
					modalDelete.modalClose();
				});
				var url = `Delete?mailId=${mail.mailId}`;

				modalDelete.initButton('#modalReplace .modal-footer [data-type="yes"]', () => {
					$.ajax(url, {
						error: (xhr, ajaxOptions, thrownError) => {
							window.alert("При удалении произошла ошибка");
						},
						success: (result, textStatus: string, jqXHR: JQueryXHR) => {
							location.reload();
						}

					});
				});

				modalDelete.modalShow();
			});
			$('#notificationMail').on('click', (e) => {
				var modalInfo = new ModalNotificationInfo();
				$.getJSON(`/Notification/NotificationsInfo?mailId=${mail.mailId}`, (data: IMailNotificationsResultViewModel, textStatus, jqXHR) => {

					var body1 = '';
					data.notificationsResult.forEach((value, index, array) => {
						body1 = body1.concat(` <tr><td>${value.user.fio}  </td><td>${resolveNotificationClick(value.clickType)}</td></tr>`);
					});

					var modalBody = `<table class="table"><thead><tr><th>ФИО</th><th>Состояние</th></tr></thead><tbody>${body1}</tbody></table>`;

					modalInfo.setModalBody(modalBody);
					modalInfo.modalReplacer();
					modalInfo.initButton('#modalReplace .modal-footer [data-type="close"]', () => {
						modalInfo.modalClose();
					});


					modalInfo.modalShow();
				});


			});
		}
		$('#replayMail').off();
		$('#printMail').off();

		$('#replayMail').on('click', (e) => {

		});

		$('#printMail').on('click', (e) => {


		});

	}
}


/*
 <li>
										<a href="#" class="atch-thumb">
											<img src="../images/inbox.png" alt="img" />
										</a>
										<div class="file-name">
											image-name.jpg
										</div>
										<span>12KB</span>

										<div class="links">
											<a href="#">View</a> -
											<a href="#">Download</a>
										</div>
									</li>
									<li>
										<a href="#" class="atch-thumb">
											<img src="../images/inbox.png" alt="img" />
										</a>
										<div class="file-name">
											img_name.jpg
										</div>
										<span>40KB</span>
										<div class="links">
											<a href="#">View</a> -
											<a href="#">Download</a>
										</div>
									</li>
									<li>
										<a href="#" class="atch-thumb">
											<img src="../images/inbox.png" alt="img" />
										</a>
										<div class="file-name">
											img_name.jpg
										</div>
										<span>30KB</span>
										<div class="links">
											<a href="#">View</a> -
											<a href="#">Download</a>
										</div>
									</li>



 */




let mailController = new Mail();