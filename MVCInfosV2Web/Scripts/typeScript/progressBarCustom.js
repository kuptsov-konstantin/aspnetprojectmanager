var Guid = (function () {
    function Guid() {
    }
    Guid.newGuid = function () {
        var id = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
        var guid = new Guid();
        guid.id = id;
        return guid;
    };
    return Guid;
}());
var MyProgressbarStatusMessage = (function () {
    function MyProgressbarStatusMessage() {
        this.id = Guid.newGuid();
    }
    return MyProgressbarStatusMessage;
}());
var MyProgressbar = (function () {
    function MyProgressbar(progressBar, modalWindow, isCloseWhen100) {
        this._counter = 0;
        this._factor = 1;
        this._countMax = 0;
        this._allEmployees = 0;
        this._isCloseWhen100 = false;
        this._modalWindow = modalWindow;
        this._isCloseWhen100 = isCloseWhen100;
        this._progressBar = progressBar;
    }
    MyProgressbar.prototype.settingStatusMessage = function (messages) {
        this._statusMessage = messages;
        if (this._progressBar.hasClass('progress-my-status-info')) {
            var ul;
            if (this._progressBar.parent().parent().find("ul.progress-my-status-info-ul.progressbar-status").length == 0) {
                ul = document.createElement("ul");
                ul.className = 'progress-my-status-info-ul progressbar-status';
                this._progressBar.parent().before(ul);
            }
            else {
                this._progressBar.parent().parent().find("ul.progress-my-status-info-ul").empty();
                ul = this._progressBar.parent().parent().find("ul.progress-my-status-info-ul");
            }
            for (var i = 0; i < this._statusMessage.length; i++) {
                var li = document.createElement("li");
                li.id = this._progressBar.attr('id') + 'li-' + this._statusMessage[i].id.id;
                li.innerHTML = this._statusMessage[i].message + '<span> </span>';
                if (this._progressBar.parent().parent().find("ul.progress-my-status-info-ul.progressbar-status").length == 0) {
                    ul.appendChild(li);
                }
                else {
                    ul.append(li);
                }
            }
        }
    };
    /**
    Устанавливается максимум прогресбара
    */
    MyProgressbar.prototype.maximumWithoutFactor = function (allEmployees) {
        this._allEmployees = allEmployees;
        this._progressBar.attr("aria-valuemax", allEmployees);
    };
    Object.defineProperty(MyProgressbar.prototype, "length", {
        /**
        Максимум с умонежением на множитель
        */
        get: function () {
            return this._factor * this._allEmployees;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyProgressbar.prototype, "factor", {
        get: function () {
            return this._factor;
        },
        set: function (val) {
            this._factor = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MyProgressbar.prototype, "max", {
        set: function (value) {
            this._countMax = value;
            this._progressBar.attr("aria-valuemax", value);
        },
        enumerable: true,
        configurable: true
    });
    MyProgressbar.prototype.inc = function () {
        this._counter++;
        this.updateProgress();
        if (this.proc == 100) {
            if (this._isCloseWhen100 == true) {
                this._modalWindow.modal('hide');
            }
        }
    };
    Object.defineProperty(MyProgressbar.prototype, "proc", {
        get: function () {
            return this._counter * 100 / this.length;
        },
        enumerable: true,
        configurable: true
    });
    MyProgressbar.prototype.currentMessage = function () {
        var _this = this;
        var filtred = this._statusMessage.filter(function (e) {
            if ((e.start <= _this._counter) && (_this._counter <= e.stop)) {
                return e;
            }
        });
        if (filtred.length > 0) {
            return filtred[0].message;
        }
        else {
            return "";
        }
    };
    ///TODO: Проверить ресет
    MyProgressbar.prototype.resetProgress = function () {
        this._counter = 0;
        this.updateProgress();
    };
    MyProgressbar.prototype.updateStatausInfo = function () {
        var _this = this;
        if (this._progressBar.hasClass('progress-my-status-info')) {
            var ok = this._statusMessage.filter(function (e) {
                if (e.stop <= _this._counter) {
                    var elem = $('#' + _this._progressBar.attr('id') + 'li-' + e.id.id);
                    if (!elem.hasClass('progressbar-notok')) {
                        myRemoveClass(elem, 'progressbar-wait');
                        myRemoveClass(elem, 'progressbar-progress');
                        myAddClass(elem, 'progressbar-ok');
                    }
                    return e;
                }
            });
            var inProgress = this._statusMessage.filter(function (e) {
                if ((e.start <= _this._counter) && (_this._counter < e.stop)) {
                    var elem = $('#' + _this._progressBar.attr('id') + 'li-' + e.id.id);
                    if (!elem.hasClass('progressbar-notok')) {
                        myRemoveClass(elem, 'progressbar-wait');
                        myRemoveClass(elem, 'progressbar-ok');
                        myAddClass(elem, 'progressbar-progress');
                    }
                    return e;
                }
            });
            var wait = this._statusMessage.filter(function (e) {
                if (_this._counter < e.start) {
                    var elem = $('#' + _this._progressBar.attr('id') + 'li-' + e.id.id);
                    myAddClass(elem, 'progressbar-wait');
                    return e;
                }
            });
        }
    };
    MyProgressbar.prototype.updateProgress = function () {
        this.updateStatausInfo();
        if (this._progressBar.hasClass('progress-my-status')) {
            var div;
            if (this._progressBar.parent().parent().find("div.progress-my-status-info-mes").length == 0) {
                div = document.createElement('div');
                div.className = "progress-my-status-info-mes";
                this._progressBar.parent().after(div);
                div.innerHTML = (this.currentMessage());
            }
            else {
                this._progressBar.parent().parent().find("div.progress-my-status-info-mes").empty();
                div = this._progressBar.parent().parent().find("div.progress-my-status-info-mes");
                div.html(this.currentMessage());
            }
        }
        else {
        }
        this._progressBar.html(this.proc.toFixed(2) + '%');
        this._progressBar.attr('aria-valuenow', this._counter).css('width', this.proc.toFixed(2) + '%');
    };
    return MyProgressbar;
}());
//# sourceMappingURL=progressBarCustom.js.map