function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function updateRadioButtons(currentButton) {
    for (var i = 0; i < currentButton.parent().children().length; i++) {
        if (currentButton.parent().children()[i] != currentButton.get(0)) {
            myRemoveClass($(currentButton.parent().children()[i]), 'active');
            myRemoveClass($(currentButton.parent().children()[i]), 'btn-info');
            myAddClass($(currentButton.parent().children()[i]), 'btn-default');
        }
        else {
            myAddClass($(currentButton.parent().children()[i]), 'active');
            myRemoveClass($(currentButton.parent().children()[i]), 'btn-default');
            myAddClass($(currentButton.parent().children()[i]), 'btn-info');
        }
    }
}
function initDatePicker() {
    $('.date > input').attr("type", "text");
    $('.date > input').datetimepicker({
        locale: 'ru',
        format: 'DD.MM.YYYY'
    });
}
function initSelector() {
    $('.selectpicker').selectpicker({
        //style: 'btn-primery',
        //size: 4,
        liveSearch: true,
        hideDisabled: true,
        noneSelectedText: 'Ничего не выбрано'
    });
    console.log("bootstrap-select start");
}
var tree = [
    {
        text: "Parent 1",
        nodes: [
            {
                text: "Child 1",
                nodes: [
                    {
                        text: '<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>'
                    },
                    {
                        text: "Grandchild 2"
                    }
                ]
            },
            {
                text: "Child 2"
            }
        ]
    },
    {
        text: "Parent 2"
    },
    {
        text: "Parent 3"
    },
    {
        text: "Parent 4"
    },
    {
        text: "Parent 5"
    }
];
var node = {
    text: "Node 1",
    icon: "glyphicon glyphicon-stop",
    selectedIcon: "glyphicon glyphicon-stop",
    color: "#000000",
    backColor: "#FFFFFF",
    href: "#node-1",
    selectable: true,
    state: {
        checked: true,
        disabled: true,
        expanded: true,
        selected: true
    },
    tags: ['available'],
    nodes: tree
};
function getTree() {
    // Some logic to retrieve, or generate tree structure
    return tree;
}
//# sourceMappingURL=clientCustom.js.map