﻿class OrgObject {
    constructor() {
        this.departments = new Array<Department>();
        this.employees = new Array<Employee>();
        this.aboutFirm = new AboutFirm();
    }
    //step1
    aboutFirm: AboutFirm;
    //step2
    departments: Array<Department>;
    //step3
    employees: Array<Employee>;
};


class Department {
    constructor() {
        this.department = new Valid();
    }
    __RequestVerificationToken: string;
      department: Valid;
}


class AboutFirm {
    constructor() {
        this.orgName = new Valid();
        this.country = new Valid();
        this.city = new Valid();
        this.address = new Valid();
    }
    __RequestVerificationToken: string;
    orgName: Valid;
    country: Valid;
    city: Valid;
    address: Valid;
}

class Employee {
    __RequestVerificationToken: string;
    constructor() {
        this.familyName = new Valid();
        this.name = new Valid();
        this.middleName = new Valid();
        this.birthDay = new Valid();
        this.userName = new Valid();
        this.email = new Valid();
        this.pass = new Valid();
        this.role = new Valid();
        this.emploement = new Valid();
    }
    id: number;
    familyName: Valid;
    name: Valid;
    middleName: Valid;
    birthDay: Valid;
    //---
    userName: Valid;
    email: Valid;
    pass: Valid;
    role: Valid;
    emploement: Valid;
    get isValidate() {
        return this.familyName.isValidate && this.name.isValidate && this.middleName.isValidate && this.birthDay.isValidate
            && this.userName.isValidate && this.email.isValidate && this.pass.isValidate && this.role.isValidate;
    };
    department: number = -1;

};
class Valid {
    isValidate: boolean = false;
    str: string = "";
}

class ResponseCode {
    statusCode: number;
    firmId: number;
    userId: string;
    email: string;
}