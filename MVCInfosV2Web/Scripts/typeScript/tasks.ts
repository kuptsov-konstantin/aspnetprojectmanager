﻿class FontExtansions {
	static supported: string[] = [
		'txt', 'doc', 'docx', 'rtf', 'xls', 'xlsx',
		'ppt', 'pptx', 'dwg', 'bmp', 'gif', 'jpeg',
		'jpg', 'png', 'pdf', 'djvu', 'bz2', 'dmg',
		'gz', 'gzip', 'iso', 'rar', 'tar', 'tgz',
		'zip', 'doc', 'doc']

	static canIco(filename: string) {
		var ext = filename.split('.').pop().toLowerCase();
		return FontExtansions.supported.find((value, index, obj) => {
			return ext === value;
		});
	}
}


class ProjectMiniInfo implements IMainInfo {
	id: number;
	name: string;
	code: string;
	startDate: Date;
	endDate: Date;
	employees?: ProjectUserInfo[];
}
enum ETypeNode {
	Firm,
	Project,
	Stage
}
interface IMyBootstrapTreeViewNode extends IBootstrapTreeViewNode {
	value?: any
	nodes?: Array<IMyBootstrapTreeViewNode>
	typeNode?: ETypeNode
	
}
interface ICurrentTask {
	taskTheme: string
	taskDate: Date
	taskPriority: number
	taskDescription: string
	taskEndEmployee: string
	taskStage: number
	taskFiles?: IUploadcareBlob[]
}
class CurrentTask implements ICurrentTask {
	taskId?: string
	taskTheme: string
	taskDate: Date
	taskPriority: number
	taskDescription: string
	taskEndEmployee: string
	taskStage: number
	taskFiles?: IUploadcareBlob[]
}

class CMyBootstrapTreeViewNode implements IMyBootstrapTreeViewNode {
	value?: IMainInfo

	tasks?: Array<CurrentTask>

	text?: string
	icon?: string
	selectedIcon?: string
	color?: string
	backColor?: string
	href?: string
	selectable?: boolean
	state?: IBootstrapTreeViewNodeState
	tags?: Array<string>
	nodes?: Array<CMyBootstrapTreeViewNode>

	typeNode?: ETypeNode

	countOpenTasks?() {
		var countTasks = 0;
		this.nodes.forEach((value, index, array) => {
			value.updateTag();
			countTasks += value.countOpenTasks();
		});
		return countTasks;
	}

	updateTag?() {
		if (this.tasks !== undefined) {
			return this.tasks.length;
		} else {
			return null;
		}
	}
}
class CMyBootstrapTreeViewEventNode extends CMyBootstrapTreeViewNode implements BootstrapTreeViewEventNode {
	nodeId?: number;
	parentId?: number;
	nodes?: Array<CMyBootstrapTreeViewEventNode>
}


function parceDate(key, value) {
	var replaceDate = /(\/Date\(?)(.*?)(\)\/)/gm;
	var reISO = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*))(?:Z|(\+|-)([\d|:]*))?$/;
	var reMsAjax = /^\/Date\((d|-|.*)\)[\/|\\]$/;

	if (typeof value === 'string') {
		var a = reISO.exec(value);
		if (a)
			return new Date(value);
		a = reMsAjax.exec(value);
		if (a) {
			var b = a[1].split(/[-+,.]/);
			return new Date(b[0] ? +b[0] : 0 - +b[1]);
		}
	}
	return value;
}

class TaskManager {
	treeIdSelector: string;
	firmList: FirmModel[];
    treeNodes: CMyBootstrapTreeViewNode[];
	currentIdFirm: number;
	curentStageId: number;
	loadedStorage: boolean;
	isCurrent: boolean;

	newTask: CurrentTask;


    updateTreeNodeCache() {
        localStorage.removeItem('treeNodesTasks');
        localStorage.setItem('treeNodesTasks', JSON.stringify(this.treeNodes));
    }


    projectListDeferred = $.Deferred();
	firmListDeferred = $.Deferred();
	uploadFileDeferred = $.Deferred();

	loadingFromStorageDeferred = $.Deferred();
	setHideFiles(fileElements: JQuery, lengthElements: number) {
		for (var i = 0; i < lengthElements; i++) {
			if (i < Math.floor($('.files-loading .files-colapser').width() / ($('.files-loading .files-colapser .file').width() + 17))) {
				$(fileElements[i]).css('display', "inline-flex");
			} else {
				$(fileElements[i]).css('display',  "none");
			}
		}
		myRemoveClass([$('.files-colapser')], 'files-show');
	}
	showHideFiles(fileElements: JQuery, lengthElements: number) {
		for (var i = 0; i < lengthElements; i++) {
			$(fileElements[i]).css('display', "inline-flex");
		}
		myAddClass([$('.files-colapser')], 'files-show');
	}
	toogleHideFiles(length: number) {
		$('.files-loading .files-colapser .colapse-trigger').toggle();
		if ($('.files-loading .files-colapser .colapse-trigger').first().css('display') === 'none') {
			this.setHideFiles($('.files-colapser .file'), length);
			myRemoveClass([$('.files-collapse-shower i')], 'fa-chevron-up');
			myAddClass([$('.files-collapse-shower i')], 'fa-chevron-down');
			console.log("none");
		} else {
			this.showHideFiles($('.files-colapser .file'), length);
			myRemoveClass([$('.files-collapse-shower i')], 'fa-chevron-down');
			myAddClass([$('.files-collapse-shower i')], 'fa-chevron-up');
			console.log("notnone");
		}
	}

	hideHideFiles(length: number) {
		$('.files-loading .files-colapser .colapse-trigger').hide();
		if ($('.files-loading .files-colapser .colapse-trigger').first().css('display') === 'none') {
			this.setHideFiles($('.files-colapser .file'), length);
			myRemoveClass([$('.files-collapse-shower i')], 'fa-chevron-up');
			myAddClass([$('.files-collapse-shower i')], 'fa-chevron-down');
			console.log("none");
		} else {
			this.showHideFiles($('.files-colapser .file'), length);
			myRemoveClass([$('.files-collapse-shower i')], 'fa-chevron-down');
			myAddClass([$('.files-collapse-shower i')], 'fa-chevron-up');
			console.log("notnone");
		}
	}

	static createFileHTMLElement(blob: IUploadcareBlob):string {
		var ext = FontExtansions.canIco(blob.name);
		var fileIco = 'fa-file-o';
		if (ext !== undefined) {
			fileIco = 'fa-file-' + ext + '-o';
		}

		return '<div class="file file-image document-item" filename="' + blob.name + '" filetype="' + blob.name.split('.').pop().toLowerCase() + '" id="addflilist' + blob.uuid + '">' +
			'<span class="file-info" style="pointer-events: none;">' +
			'<i class="fa '+ fileIco +
			'"></i>' +
			'<label class="file-name">' + blob.name + '</label>' +
			'</span>' +
			'<span class="free_zone"></span>' +
			'<button type="button" class="close" onclick="taskManager.removeFile(\'' + blob.uuid + '\')">' +
			'<span class="file-remove fa fa-times" aria-hidden="true"></span>' +
			'</button>' +
			'</div>';
	}

    initElements(treeIdSelector: string) {
		this.treeIdSelector = treeIdSelector;
		$('#add-file-list').on('summernote.files.update', (e) => {
			this.newTask.taskFiles.forEach((blob, index, array) => {
				$('#add-file-list').append(TaskManager.createFileHTMLElement(blob));
			});	
		});
		$(window).on('resize', (handler) => {
			$('#add-file-list').html('');
			$('#add-file-list').trigger('summernote.files.update');
			if (($('.files-loading').width() + 20)< ($('.files-colapser .file').width() + 17) * /*this.newTask.files.length*/ 17) {
				$('.files-loading .files-colapser').width(($('.files-colapser .file').width() + 17) * Math.floor($('.files-loading').width() / ($('.files-loading .files-colapser .file').width() + 17)));

				this.setHideFiles($('.files-colapser .file'), this.newTask.taskFiles.length);
				myAddClass([$('.files-loading .files-collapse-shower')], 'f-show');
				$('.files-loading .files-collapse-shower').off('click');
				$('.files-loading .files-collapse-shower').on('click', (e) => {
					console.log(e);
					this.toogleHideFiles(this.newTask.taskFiles.length);
				});				
				$('.files-loading .files-collapse-shower').prop('disabled', false);
			} else {
				myRemoveClass([$('.files-loading .files-collapse-shower')], 'f-show');
				$('.files-loading .files-collapse-shower').prop('disabled', true);
			}
		})
		$('#myModal').on('shown.bs.modal', (e) => {
			this.newTask = new CurrentTask();
			this.newTask.taskFiles = new Array<IUploadcareBlob>();
			$(document).off("uploadcare.summernote.plugin");
			$(document).on("uploadcare.summernote.plugin", (event, blob: IUploadcareBlob) => {
				$('#add-file-list').append(TaskManager.createFileHTMLElement(blob));
				this.newTask.taskFiles.push(blob);
				$(window).trigger('resize');
				//console.log(blob);
			});
			$('#add-file-list').html('');
			this.setDateInDatePicke('#inDate', new Date());
			$('#summernote').html('');
			this.initRichTextSummernote('#summernote');		
			$('#task-theme').val('')
			$(window).trigger('resize');
		});

		$(window).on('click', (e) => {
			$('#selectorId').is('show');
			//var widget = uploadcare.Widget('[role=uploadcare-uploader]');
			//var file = widget.value();
			if (!$(event.target).is("li.list-group-item") && !$(event.target).is('.buttons-left .button-selector')) {
				$('#selectorId').hide();
			}
			if (!$(event.target).is(".files-colapser") &&
				!$(event.target).parents().is(".files-colapser") &&
				!$(event.target).is('.files-loading .files-collapse-shower') &&
				!$(event.target).is('.files-loading .files-collapse-shower i')) {
				if ((this.newTask !== undefined) && (this.newTask.taskFiles !== undefined)) {
					this.hideHideFiles(this.newTask.taskFiles.length);
				}
			}

		});
		
		$('.buttons-left .button-selector').on('click', (e) => {
			myAddClass([$('#selectorId')], 'selector-active');
			$('#selectorId').toggle();
		});
		$("#new-task").validate({
			invalidHandler: () => {
				$("label.error").hide();
			}
		})
		$('#myModal button[button-type="submit"]').on("click", (e) => {

			if ($("#new-task").valid() === false) {
				$("label.error").hide();
				return false;
			}
			if ($('#task-theme').val().length <= 1) {
				return false;
			}

			this.sendTask({
				taskDate: this.getDateInDatePicke('#inDate').toDate(),
				taskDescription: $('#summernote').summernote('code'),
				taskEndEmployee: $('#task-end-employee').val(),
				taskPriority: Number.parseInt($('#task-priority').val()),
				taskTheme: $('#task-theme').val(),
				taskFiles: this.newTask.taskFiles,
				taskStage: this.curentStageId
			});
		})
	}

	removeFile(uuid: string) {
		$('#add-file-list').remove('#addflilist' + uuid);
		var index = this.newTask.taskFiles.findIndex((value, index, obj) => {
			return value.uuid === uuid;
		});		
		this.newTask.taskFiles.splice(index, 1);
		$(window).trigger('resize');
	}


	sendTask(task: ICurrentTask) {
		
		var token = $("#__AjaxAntiForgeryForm input").val();
		var data = <IMyBootstrapTreeViewNode[]>($('#selectorId').treeview('getSelected'));
		if (data.length == 0) {
			return;
		}
		var idFirm = this.getIdByTypeInTree(data[0], ETypeNode.Firm);
		var idProject = this.getIdByTypeInTree(data[0], ETypeNode.Project);
        var params = 'idFirm=' + idFirm + '&idProject=' + idProject + '&taskJson=' + btoa(escape(JSON.stringify(task)));

		var url = 'AddNewTask';
		$.ajax({
			url: url,
			type: 'POST',
			data: { 'parametrs': btoa(escape(params)) },
			headers: { '__RequestVerificationToken': token },
			error: (xhr, ajaxOptions, thrownError) => {
				console.log(xhr);
			},
			success: (json) => {
				console.log(json);
			//	$('#saveLayersId').prop("disabled", true);
			//	location.reload();
			}
		});

	}
	findInTree(node: CMyBootstrapTreeViewNode, findingId: string): CMyBootstrapTreeViewNode {
		var id = undefined;
		if ((<IMainInfo>node.value).id instanceof Guid) {
			id = (<IMainInfo>node.value).id.id;
		} else {
			id = (<IMainInfo>node.value).id;
		}
		if (id == findingId) {
			return node;
		}

		if ((node.nodes !== undefined) && (node.nodes !== null)) {
			for (var i = 0; i < node.nodes.length; i++) {
				var result = this.findInTree(node.nodes[i], findingId);
				if (result !== null) {
					return result;
				}
			}
		}
		return null;
	}
    loadFromStorage() {
        var savedStage = localStorage['treeNodesTasks'];
        if (savedStage) {
            this.treeNodes = JSON.parse(savedStage, parceDate);
            this.loadedStorage = true;
        } else {
            this.loadedStorage = false;
        }

        var savedFirms = localStorage['firmsTask'];
        if (savedFirms) {
            this.firmList = JSON.parse(savedFirms);
            this.firmList.forEach((value, index, array) => {
                var savedProjects = localStorage['projects' + value.idFirm + 'Task'];
                if (savedProjects) {
                    value.projectList = JSON.parse(savedProjects);
                }
            });
           
        }
        this.initTreeView(this.treeIdSelector, this.treeNodes);
        this.loadingFromStorageDeferred.resolve();
    }

	initProjects(currentIdFirm: number, isCurrent: boolean) {
		this.currentIdFirm = currentIdFirm;
		this.isCurrent = isCurrent;
		this.loadFromStorage();
		$.when(this.loadingFromStorageDeferred).done(() => {
			/*var urlUsers = 'GetUserFromFirm?idFirm=' + currentIdFirm + '&isCurrent=' + isCurrent;
			$.get('/TaskManager/GetMyFirms', (result: ProjectUserInfo[]) => {

			});*/




			$.get('/TaskManager/GetMyFirms', (result: FirmModel[]) => {
				this.firmList = result;
				if (this.firmList.length > 0) {
					localStorage['firmsTask'] = JSON.stringify(this.firmList);
					this.firmListDeferred.resolve(result);
				} else {
					///TODO: Сделать вывод если нет фирм
				}
			});

			$.when(this.firmListDeferred).done(() => {
				if (this.treeNodes === undefined) {
					this.treeNodes = new Array<CMyBootstrapTreeViewNode>();
				}
				this.firmList.forEach((value, index, array) => {
					var oldFirm = this.treeNodes.find((value1, index1, array1) => {
						return value1.value.id == value.idFirm;
					});
					if (oldFirm === undefined) {
						this.treeNodes.push({
							text: value.nameFirm,
							value: {
								id: value.idFirm,
								name: value.nameFirm,
								employees: value.employees
							},
							tags: ['1'],
							typeNode: ETypeNode.Firm
						});
					} else {
						oldFirm.value.id = value.idFirm;
						oldFirm.value.name = value.nameFirm;
						oldFirm.value.employees = value.employees;
						oldFirm.text = value.nameFirm;
					}
					this.updateTreeNodeCache();
					this.loadProjects(value);

				});

				this.initTreeView(this.treeIdSelector, this.treeNodes);
				// $('#' + this.treeIdSelector).treeview('collapseAll', { silent: true });


			});
		});
	}

    loadProjects(firm: FirmModel) {
        $.get('/TaskManager/GetProjects?idFirm=' + firm.idFirm,
			(result: ProjectMiniInfo[]) => {
                firm.projectList = JSON.parse(JSON.stringify(result), parceDate);
                localStorage['projects' + firm.idFirm + 'Task'] = JSON.stringify(firm.projectList);

                if (this.firmList.length > 0) {
                    var firmInTree = this.treeNodes.find((value) => {
                        return value.value.id == firm.idFirm;
                    })
                    firmInTree.nodes = new Array<IMyBootstrapTreeViewNode>();
                    firm.projectList.forEach((value, index, array) => {
                        this.loadEmployeeInProject(firm, value);
                        this.loadLayers(firm.idFirm, value.id);

                        var oldProject = firmInTree.nodes.find((value1, index1, array1) => {
                            return value1.value.id == value.id;
                        });
                        if (oldProject === undefined) {
                            firmInTree.nodes.push({
                                text: value.code,
                                // value: value,
                                value: {
                                    endDate: value.endDate,
                                    startDate: value.startDate,
                                    id: value.id,
                                    name: value.name
                                },
                                tags: ['1'],
                                typeNode: ETypeNode.Project
                            });
                        } else {
                            oldProject.text = value.code;
                            oldProject.value.endDate = value.endDate;
                            oldProject.value.startDate = value.startDate;
                            oldProject.value.name = value.name;
                        }
                    });
                }else {

                }
                this.updateTreeNodeCache();
                this.initTreeView(this.treeIdSelector, this.treeNodes);

			});


	}
    loadEmployeeInProject(firm: FirmModel, project: ProjectMiniInfo) {
        $.get('/TaskManager/GetUserFromProject?idFirm=' + firm.idFirm + '&idProject=' + project.id,
			(result: ProjectUserInfo[]) => {
                var index = firm.projectList.findIndex((value, index, array) => {
                    return (value.id == project.id);
				});
                firm.projectList[index].employees = result;
                this.updateTreeNodeCache();
			});
	}

	getIdByTypeInTree(data: CMyBootstrapTreeViewEventNode, nodeType: ETypeNode) {
		if (data.typeNode === nodeType) {
			return data.value.id;
		}
		if (data.parentId !== undefined) {		
			var parent = $('#selectorId').treeview('getParent', data.nodeId);	
			return this.getIdByTypeInTree(parent, nodeType);
		} else {
			return null;
		}
	}

    initTreeView(id: string, data: IBootstrapTreeViewNode[]) {

		initTreeView(this.treeIdSelector, data);
		$('.project-control button[data-toggle="modal"]').prop('disabled', true);
		$('#' + id).on('nodeUnselected', (event, data: CMyBootstrapTreeViewEventNode) => {
			if (data.state.selected == false) {
				$('.buttons-left button')[0].innerText = $('.buttons-left button').attr('data-text');
				$('.buttons-left .button-selector')[0].innerText = $('.buttons-left .button-selector').attr('data-text');
				$('.project-control button[data-toggle="modal"]').prop('disabled', true);
			}
		});
		$('#' + id).on('nodeSelected', (event, data: CMyBootstrapTreeViewEventNode) => {
			$('.buttons-left button')[0].innerText = $('.buttons-left button').attr('data-text') + ' в ' + data.text;
			$('.buttons-left .button-selector')[0].innerText = data.text;
			this.updateDatePicker('#inDate', data.value);
			this.initRichTextSummernote('div#summernote');
			$('.project-control button[data-toggle="modal"]').prop('disabled', false);
			this.curentStageId = data.value.id;
			$('#myModal .modal-title small').text(data.text);
			if (data.typeNode === ETypeNode.Firm) {
				if (data.value.employees !== undefined) {
					$('#selector-task-end-employee').html('<select class="selectpicker" id="task-end-employee"></select>');
					data.value.employees.forEach((value, index, array) => {
						$('#task-end-employee').append('<option value="' + value.Id + '">' + value.Fio + '</option>');
					});
				}
            } else {
                var idFirm = this.getIdByTypeInTree(data, ETypeNode.Firm);
                var idProject = this.getIdByTypeInTree(data, ETypeNode.Project);

                var firm = this.firmList.find((value, index, array) => {
                    return value.idFirm == idFirm;
                });
                var project = firm.projectList.find((value, index, array) => {
                    return value.id === idProject;
                });

				if (project.employees !== undefined) {
					$('#selector-task-end-employee').html('<select class="selectpicker" id="task-end-employee"></select>');
					project.employees.forEach((value, index, array) => {
						$('#task-end-employee').append('<option value="' + value.Id + '">' + value.Fio + '</option>');
					});
				}
			}
			initSelector('#task-end-employee');
			//var idFirm = this.getIdByTypeInTree(data, ETypeNode.Firm);
			
		});
	}

	loadLayers(idFirm: number, idProject: number) {
		$.ajax({
			type: 'GET',
			url: '/ProjectManager/_GetStagesEdit?idFirm=' + idFirm + '&idProject=' + idProject,
			success: (json: Array<ProjectStage>) => {
				json = JSON.parse(JSON.stringify(json), parceDate);
				var positionIdFirm = this.treeNodes.findIndex((value) => {
					return value.value.id == idFirm;
				})
				var positionIdProject = this.treeNodes[positionIdFirm].nodes.findIndex((value) => {
					return value.value.id == idProject;
				})
				json.map((value, index, array) => {
					if (value.idParent == Guid.empty) {
						return value;
					}
				}).forEach((value, index, array) => {
					if (value !== undefined) {
						if (this.treeNodes[positionIdFirm].nodes === undefined) {
							this.treeNodes[positionIdFirm].nodes = new Array<IMyBootstrapTreeViewNode>();
						}
						if (this.treeNodes[positionIdFirm].nodes[positionIdProject].nodes === undefined) {
							this.treeNodes[positionIdFirm].nodes[positionIdProject].nodes = new Array<IMyBootstrapTreeViewNode>();
						}
						this.treeNodes[positionIdFirm].nodes[positionIdProject].nodes.push({
							text: value.stageName,
							value: {
								id: value.idCurrent,
								endDate: value.stopDate,
								startDate: value.startDate,
								name: value.stageName
							},
							tags: ['1'],
							typeNode: ETypeNode.Stage
						});
					}
				});
				json = json.filter((value, index, array) => {
					if (value.idParent !== Guid.empty) {
						return value;
					}
				});
				var counter = 0;
				var lastLength = json.length;
				while (json.length > 0) {
					lastLength = json.length;
					var added = new Array<string>();
					json = json.filter((value, index, array) => {
						var parent = this.findInTree(this.treeNodes[positionIdFirm].nodes[positionIdProject], value.idParent);
						if (parent !== null) {
							if (parent.nodes === undefined) {
								parent.nodes = new Array<IMyBootstrapTreeViewNode>()
							}
							added.push(value.idCurrent);
							parent.nodes.push({
								text: value.stageName,
								value: {
									id: value.idCurrent,
									endDate: value.stopDate,
									startDate: value.startDate,
									name: value.stageName
								}, tags: ['1']
							});
						} else {
							return value;
						}
					});
					if (lastLength == json.length) {
						counter++;
						if (counter == 50) {
							console.error('Достиг лимит рекурсии');
							break;
						}
					} else {
						counter = 0;
					}
				}
                this.updateTreeNodeCache();
				this.initTreeView(this.treeIdSelector, this.treeNodes);
				//this.loadTasks();
				// this.addingAddButtons();
				// $('#' + this.treeIdSelector).treeview('collapseAll', { silent: true });
			}
		});
	}

	loadTasks() {
		$('.block_content .title .settings_ellipsis_button').toArray().forEach((value, index, array) => {
			value.onmousemove = (ev) => {
				$('.block_content .title .buttons_manage').css('display', 'block');
			}
		});
		//onmousemove = "mouseMove(e)

	}

	addingAddButtons() {
		$('#selectorId li').toArray().forEach((value, index, array) => {
			if ($(value).find('button').length == 0) {
				$(value).append('<button class="btn btn-xs btn-success"><span class="fa fa-plus"></span></button>');
			}
		});
	}
	getDateInDatePicke(idElement: string) {
		return $(idElement).data("DateTimePicker").date();
	}
	setDateInDatePicke(idElement: string, date: Date) {
		return $(idElement).data("DateTimePicker").date(date);
	}

	updateDatePicker(idElement: string, node: IMainInfo) {
		var minDate = $(idElement).data("DateTimePicker").minDate();
		var maxDate = $(idElement).data("DateTimePicker").maxDate()
		if ((minDate === false) && (maxDate === false)) {
			/*var dateNow = moment();

			if (minDate !== false) {
				minDate = moment(node.startDate);
			}
			if (maxDate !== false) {
				maxDate = moment(node.endDate);
			}
			if (moment(node.startDate) <= dateNow) {
				if (moment(node.endDate) <= dateNow) {
					$(idElement).data("DateTimePicker").maxDate(node.endDate);
				}
				$(idElement).data("DateTimePicker").minDate(node.startDate);
			} else {

				$(idElement).data("DateTimePicker").minDate(node.startDate);
			}*/
			$(idElement).data("DateTimePicker").minDate(node.startDate);
			$(idElement).data("DateTimePicker").maxDate(node.endDate);
		} else {
			if (minDate !== false) {
				minDate = moment(minDate);
			}
			if (maxDate !== false) {
				maxDate = moment(maxDate);
			}
			var minDateNew = moment(node.startDate);
			var maxDateNew = moment(node.endDate);
			if (maxDateNew <= minDate) {
				$(idElement).data("DateTimePicker").minDate(node.startDate);
				$(idElement).data("DateTimePicker").maxDate(node.endDate);
			} else {
				if (minDateNew >= maxDate) {
					$(idElement).data("DateTimePicker").maxDate(node.endDate);
					$(idElement).data("DateTimePicker").minDate(node.startDate);
				} else {
					$(idElement).data("DateTimePicker").minDate(node.startDate);
					$(idElement).data("DateTimePicker").maxDate(node.endDate);
				}
			}
		}
	}

	initRichTextTinymce(style: string, script: string) {		
		tinymce.init({
			selector: '#task-description',//,  // change this value according to your HTML
			skin_url: style,
			//content_css: style,
			language_url: script // site absolute URL
		});
	}


	resetRichTextSummernote(selector: string) {
		$(selector).summernote('reset');
	}
	initRichTextSummernote(selector: string, savedText: string = '') {
		$(selector).html(savedText);
		$(selector).summernote({
			placeholder: "Описание задачи...",
			lang: 'ru-RU',
			maxHeight: 500,
			minHeight: 300,
			toolbar: [
			//	['uploadcare', ['uploadcare']], // here, for example
				['style', ['style']],
				['font', ['bold', 'italic', 'underline', 'clear']],
				['fontname', ['fontname']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['height', ['height']],
				['table', ['table']],
				['insert', ['media', 'link', 'hr', 'uploadcare','video']],
				['view', ['fullscreen', 'codeview']],
				['help', ['help']]
			],
			callbacks: {
				onInit: function () {
					var editor = $(selector).next(),
						placeholder = editor.find(".note-placeholder");

					function isEditorEmpty() {
						var code = $(selector).summernote("code");
						return code === "<p><br></p>" || code === "";
					}

					editor.on("focusin focusout", ".note-editable", function (e) {
						if (isEditorEmpty()) {
							placeholder[e.type === "focusout" ? "show" : "hide"]();
						}
					});
				},

				onImageUpload: (files: FileList) => {
					/*for (var i = 0; i < files.length; i++) {
						this.uploadTempFile(files[i]);
					}	*/
					console.log(files);
					var allFiles = uploadcare.filesFrom('object', files);
					var fileGroup = uploadcare.FileGroup(allFiles);
					var dialog = uploadcare.openDialog(fileGroup);



					dialog.done(UploadcarePlugin.done(null, {
						tabs: 'file url',
						buttonLabel: '',
						buttonIcon: 'picture-o',
						tooltipText: 'Загрузка изображения или файла',
						//publicKey: '461a7657aacbde4094c3',
						//crop: 'free',
						multiple: true,
						fileList: this
					} ));
					dialog.fail(function (result) {
						console.log(result);
						// Dialog closed and no file or file group was selected.
						// The result argument is either null, or the last selected file.
					});
					dialog.always(function () {
						// Handle dialog closing, regardless of file selection.
					});
					dialog.progress(function (tabName) {
						console.log(tabName);
						// tabName is selected
					});
					//	var file = uploadcare.filesFrom('object', files[0]);
					var img = '<img src="' + +'"/>'
					// upload image to server and create imgNode...
					//$(selector).summernote('insertNode', imgNode);
				}
			},
			uploadcare: {
				tabs: 'file url',
				buttonLabel: '',
				buttonIcon: 'picture-o',
				tooltipText: 'Загрузка изображения или файла',
				//publicKey: '461a7657aacbde4094c3',
				//crop: 'free',
				multiple: true,
				fileList: this
				}
		});
		
		$(selector).on('summernote.image.upload', function (we, files) {
			console.log('summernote\'s image is changed.');
//upload image to server and create imgNode...
			//$(selector).summernote('insertNode', imgNode);
		});
		
	}

	uploadTempFile(file: File) {
		$.ajax({
			url: 'FileUploadAsync',
			method: 'POST',
			data: { file: file },
			cache: false,
			contentType: false,
			processData: false,
			success: (data) => {
				//$(selector).summernote('insertNode', imgNode);
			},
			error: (xhr, ajaxOptions, thrownError) => {
				console.log(xhr);
			}

		});
	}

	initRichTextFrolala(selector: string, savedText: string = '') {
	
		$(selector).parent().html('<div id="froala-editor">' + savedText +'</div>');
		$(selector).froalaEditor({
			// Set the language code.
			language: 'ru',
			heightMin: 300,
			heightMax: 400,

			fileUploadMethod: 'POST',
			fileUploadURL: 'FileUploadAsync',

			imageUploadMethod: 'POST',
			imageUploadURL: 'FileUploadAsync',

			toolbarInline: false
		});
		
		$(selector).on("froalaEditor.file.inserted", (e, editor, $file, response) => {
			console.log(editor);
			console.log($file);
			console.log(response);
		});
	}
}

let taskManager = new TaskManager();


window["UPLOADCARE_MANUAL_START"] = true;
window["UPLOADCARE_LIVE"] = false;
window["UPLOADCARE_CLEARABLE"] = true;
jQuery(function ($) {
	initUploadPlugin();

	$.getJSON('/uploadcare.json', function (data) {
		uploadcare.start({
			tabs: 'file url',
			multiple: true,
			publicKey: data.publicKey, // overrides UPLOADCARE_PUBLIC_KEY
			crop: data.crop, // overrides UPLOADCARE_CROP
            urlBase: '/Upload/',
            cdnBase: '/Upload/'
		});
	});
});