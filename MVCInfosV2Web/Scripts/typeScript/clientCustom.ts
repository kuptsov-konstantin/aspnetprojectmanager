﻿var HD = HD || {};
HD.nameof = function (obj) {
	return Object.keys(obj)[0];
}
function getParameterByName(name: string, url: string) {
	if (!url) {
		url = window.location.href;
	}
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function updateRadioButtons(currentButton: JQuery) {
	for (var i = 0; i < currentButton.parent().children().length; i++) {
		if (currentButton.parent().children()[i] != currentButton.get(0)) {
			myRemoveClass([$(currentButton.parent().children()[i])], 'active');
			myRemoveClass([$(currentButton.parent().children()[i])], 'btn-info');
			myAddClass([$(currentButton.parent().children()[i])], 'btn-default');
		} else {
			// myAddClass($(currentButton.parent().children()[i]), '');
			myRemoveClass([$(currentButton.parent().children()[i])], 'btn-default');
			myAddClass([$(currentButton.parent().children()[i])], 'btn-info active');
		}
	}
}

function ConvertStringToBoolean(string: string): boolean {
	if (string == null) {
		return null;
	}
	if (string.toLocaleLowerCase() == 'true') {
		return true;
	}
	if (string.toLocaleLowerCase() == 'false') {
		return false;
	}
}
function clickOnButtonsRadio(idFirm: number) {
	$(".btn-group button").click((e) => {
		updateRadioButtons($(e.currentTarget));
		console.log(e.currentTarget.parentNode);
		loadProjects('mainProjectPage', idFirm, ConvertStringToBoolean($('.btn-group >.btn-info').attr('link')));
	});
}

function initDatePicker() {
	$('div.date > input').attr("type", "text");
	$('div.date').datetimepicker({
		locale: 'ru',
		format: 'DD.MM.YYYY'
	});
}

function initTimePicker() {
	$('.time > input').attr("type", "text");
	$('.time').datetimepicker({
		locale: 'ru',
		format: 'HH:mm'
	});
}

function updateProgressBars() {
	if ($(".progress .progress-bar")[0]) {
		$('.progress .progress-bar').progressbar();
	}
}

function initTreeView(id: string, data: IBootstrapTreeViewNode[]) {
	$('#' + id).treeview({
		data: data,
		showTags: true,
		levels: 5
	});
}
function initSelector(id: string) {
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
		if ($(id + " option").length > 10) {
			$(id).selectpicker({
				liveSearch: true,
				hideDisabled: true,
				noneSelectedText: 'Ничего не выбрано',
				mobile: true
			});
		} else {
			$(id).selectpicker({
				hideDisabled: true,
				noneSelectedText: 'Ничего не выбрано',
				mobile: true
			});
		}

		console.log("bootstrap-select start mobile")
	} else {
		if ($(id + " option").length > 10) {
			$(id).selectpicker({
				liveSearch: true,
				hideDisabled: true,
				noneSelectedText: 'Ничего не выбрано'
			});
		} else {
			$(id).selectpicker({
				hideDisabled: true,
				noneSelectedText: 'Ничего не выбрано'
			});
		}
		console.log("bootstrap-select start")
	}
}

function initTypeahead(element: string, url: string, idFirm: number) {
	$(element).typeahead<IO>(null,{
			name: 'best-pictures',
			display: 'value',
			source: function (query, process) {
				return $.get(url, { query: query, idFirm: idFirm }, function (data: IO[]) {
					return process(data);
				});
			}
		/*,
			templates: {
				notFound: [
					'<div class="empty-message">',
					'Не найдено',
					'</div>'
				].join('\n'),
				suggestion: Handlebars.compile('<div>{{value}} – {{name}}</div>')
			}*/
		});
}


interface IO{
	name: string
	id: string
}

function formatBytes(a: number, b?: number): string {
	if (0 == a) return "0 Байт";
	var c = 1e3,
		d = b || 2,
		e = ["Байт", "КБ", "МБ", "ГБ", "ТБ", "ПБ", "ЭБ", "ЗБ", "ИБ"],
		f = Math.floor(Math.log(a) / Math.log(c));
	return parseFloat((a / Math.pow(c, f)).toFixed(d)) + " " + e[f]
}

function startDownload(url: string) {
	window.location.href = url;
}

function PrintElem(elementSelector: string) {
	var mywindow = window.open('', 'PRINT', 'height=400,width=600');

	mywindow.document.write('<html><head><title>' + document.title + '</title>');
	mywindow.document.write('</head><body >');
	mywindow.document.write('<h1>' + document.title + '</h1>');
	mywindow.document.write($(elementSelector).html());
	mywindow.document.write('</body></html>');

	mywindow.document.close(); // necessary for IE >= 10
	mywindow.focus(); // necessary for IE >= 10*/

	mywindow.print();
	mywindow.close();

	return true;
}