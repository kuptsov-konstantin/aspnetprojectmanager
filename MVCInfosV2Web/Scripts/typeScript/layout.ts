﻿class LayoutTarget {
	public targetElementId: string;
	public innerHtml: string
}
interface IFirmModel {
	idFirm?: number;
	nameFirm?: string;
	employees?: ProjectUserInfo[];
	projectList?: ProjectMiniInfo[];
}

class FirmModel implements IFirmModel {
	public idFirm: number;
	public nameFirm: string;
	public employees?: ProjectUserInfo[];
	public projectList?: ProjectMiniInfo[];
}
jQuery.extend(jQuery.validator.messages, {
	required: "Данное поле является обязательным.",
    remote: "Пожалуйста исправьте это поле.",
    email: "Пожалуйста, введите действительный адрес электронной почты.",
    url: "Пожалуйста, введите корректный адрес.",
    date: "Пожалуйста, введите правильную дату.",
    dateISO: "Пожалуйста, введите правильную дату (ISO).",
    number: "Пожалуйста введите правильное число.",
    digits: "Пожалуйста, вводите только цифры.",
    equalTo: "Пожалуйста, ведите то же значение еще раз.",
    maxlength: $.validator.format("Пожалуйста, введите не более {0} символов."),
    minlength: $.validator.format("Пожалуйста, введите не менее {0} символов."),
    rangelength: $.validator.format("Пожалуйста, введите строку, соответствующей длины между {0} и {1}."),
    range: $.validator.format("Пожалуйста введите значение, которое будет между {0} и {1}."),
    max: $.validator.format("Пожалуйста введите значение меньше или равно {0}."),
    min: $.validator.format("Пожалуйста введите значение больше или равно {0}."),
    step: $.validator.format("Пожалуйста, введите число, кратное {0}.")
});
class Layout {
	private firmElement = '<li><a href= "@Url.Action("PrintProjects", "ProjectManager", new { @idFirm = item.idFirm, @isCurrent = true })">@item.nameFirm</a></li>'
	private firmListDeferred = $.Deferred();
	private firms: FirmModel[];
	private isManager: boolean = false;
	private targets: LayoutTarget[];
	public loadAdditional(isManager: boolean) {
		this.isManager = isManager;
	}


	public loadFirms(targets: LayoutTarget[]) {
		this.targets = targets;
		var url = "/Home/_GetFirms";
		$.get(url, (date: FirmModel[]) => {
			this.firms = date;
			this.firmListDeferred.resolve(date);
		});
		this.firmListDeferred.done((e) => {
			if (this.firms.length > 1) {
				this.targets.forEach((value, index, array) => {
					$(value.targetElementId + ' .child_menu').html('');
					this.firms.forEach((value, index, array) => {

					});
				});
			} else {
				if (this.firms.length == 1) {
					this.targets.forEach((value, index, array) => {
						$(value.targetElementId + ' .child_menu').remove();
						//(<HTMLAnchorElement>$(value + ' a')[0]).href =
					});
				} else {

				}
			}
		});

	}
	public static setContentHeight() {
		// reset height
		$('.right_col').css('min-height', $(window).height());

		var bodyHeight = $('body').outerHeight(),
			footerHeight = $('body').hasClass('footer_fixed') ? -10 : $('footer').height(),
			leftColHeight = $('.left_col').eq(1).height() + $('.sidebar-footer').height(),
			contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

		// normalize content
		contentHeight -= $('.nav_menu').height() + footerHeight;

		$('.right_col').css('min-height', contentHeight);
	};

	public layoutInit() {




		var firmListDeferred = $.Deferred();
		var taskListDeferred = $.Deferred();
		var mailListDeferred = $.Deferred();
		//	var str = '@Url.Action("_FirmsProjectsPartial", "Home")'
		$.get('/Home/_FirmsProjectsPartial', function (html) {
			console.log("load projects");
			$('#projectList').replaceWith(html);
			firmListDeferred.resolve();
		});

		//	var str = '@Url.Action("_FirmsTasksPartial", "Home")'
		$.get('/Home/_FirmsTasksPartial', function (html) {
			console.log("load projects");
			$('#taskList').replaceWith(html);
			taskListDeferred.resolve();
		});

		//var str = '@Url.Action("_FirmsMailsPartial", "Home")'
		$.get('/Home/_FirmsMailsPartial', function (html) {
			console.log("load projects");
			$('#mailList').replaceWith(html);
			mailListDeferred.resolve();
		});

		$.when(firmListDeferred, taskListDeferred, mailListDeferred).done(() => {
			init_sidebar();

			$('#menu_toggle').off('click');
			$('#menu_toggle').on('click', function () {
				console.log('clicked - menu toggle');
				if ($('body').hasClass('nav-md')) {
					$('#sidebar-menu').find('li.active ul').hide();
					$('#sidebar-menu').find('li.active').addClass('active-sm').removeClass('active');
				} else {
					$('#sidebar-menu').find('li.active-sm ul').show();
					$('#sidebar-menu').find('li.active-sm').addClass('active').removeClass('active-sm');
				}

				$('body').toggleClass('nav-md nav-hide');
				Layout.setContentHeight();
			});
			$(window).on('click', (e) => {
				if (event != undefined) {
					if (!$(event.target).parents().is(".left_col") && !$(event.target).parent().is(".left_col") && !$(event.target).parents().is(".nav.toggle")) {
						if ($('body').hasClass('nav-md')) {
							$('#menu_toggle').click();
						}
					}
				}
			});
			if ($('body').hasClass('nav-md')) {
				$('#menu_toggle').click();
			}
		});
	}
}

let layoutLoader = new Layout();
/*
$('.form-js-label').find('input').on('input', function (e) {
	$(e.currentTarget).attr('data-empty', !e.currentTarget.value);
});*/