﻿function idValid(element: string) {	
	var orgName = $(element).val();
    var name = orgName.replace(/\s+/g, " ");
    if (name.length > 1) {
		setOK([element]);
        return true;
    } else {
        setError([element]);
        return false;
    }
}
function setError(elements: string[]) {
	var $elem = elements.map((value, index, array) => {
		return $(value);
	});
	myRemoveClass($elem, 'my_valid_not_error');
	myAddClass($elem, 'my_valid_error');
}
function setOK(elements: string[]) {
	var $elem = elements.map((value, index, array) => {
		return $(value);
	});
    myRemoveClass($elem, 'my_valid_error');
    myAddClass($elem, 'my_valid_not_error');
}

interface FileReaderEventTarget extends EventTarget {
    result: string
}

interface FileReaderEvent extends Event {
    target: FileReaderEventTarget;
    getMessage(): string;
}

function findInDepartments(arr: Array<Department>, str: string) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].department.str == str) {
            return false;
        }
    }
    return true;
}


/**
 * Добавление класса с проверкой на существование
 * @param currentElement
 * @param removeClass
 */
function myAddClass(currentElements: JQuery[], newClass: string) {
	currentElements.forEach((value,index, array) => {
		if (!value.hasClass(newClass)) {
			value.addClass(newClass);
		}
	});
}
/**
 * Удаление класса с проверкой на существование
 * @param currentElement
 * @param removeClass
 */
function myRemoveClass(currentElements: JQuery[], removeClass: string) {
	currentElements.forEach((value, index, array) => {
		if (value.hasClass(removeClass)) {
			value.removeClass(removeClass);
		}
	});
}

function setErrorRow(current: string) {
	var $currentId = $(current);
	myAddClass([$currentId], 'my_valid_error_table');
	myRemoveClass([$currentId], 'my_valid_not_error_table');
}
function setOKRow(current: string) {
    var $currentId = $(current);
    myAddClass([$currentId], 'my_valid_not_error_table');
    myRemoveClass([$currentId], 'my_valid_error_table');
}



function validEdit(current: string, element) {
    var $elem = $(current);
    $elem.val(element.str);
    if (element.isValidate) {
		setOK([current]);
    } else {
		setError([current]);
    }
}



function validateAllSteps() {
    var isStepValid = true;

    return isStepValid;
}