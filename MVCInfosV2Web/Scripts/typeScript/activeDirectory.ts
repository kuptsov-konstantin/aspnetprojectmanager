﻿class ActiveDirectory {
	public init(url: string) {
		$('#syncAd').on("click", () => {
			var adSwitch = $('input[name="activeDirectorySync-switch"]').prop('checked') as boolean;
			console.log(adSwitch);
			var token = $("#__AjaxAntiForgeryForm input").val();
			$.ajax({
				url: url,
				data: JSON.stringify({
					parametrs: JSON.stringify({ 
						sync: adSwitch
					})
				}),				
				error: (xhr, ajaxOptions, thrownError) => {
					alert("Произошла ошибка при сохранении: " + thrownError);
				},
				success: (result: { result: boolean }) => {
					location.reload();
				},

				type: 'POST',
				contentType: "application/json",
				dataType: "json",
				headers: { '__RequestVerificationToken': token },
			});
		});
	}
}

let _activeDirectory = new ActiveDirectory();