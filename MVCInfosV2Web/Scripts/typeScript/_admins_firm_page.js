var EWhatGet;
(function (EWhatGet) {
    EWhatGet[EWhatGet["Employee"] = 0] = "Employee";
    EWhatGet[EWhatGet["Department"] = 1] = "Department";
})(EWhatGet || (EWhatGet = {}));
var DepartmentResponse = (function () {
    function DepartmentResponse() {
    }
    return DepartmentResponse;
}());
var AdministrationFirms = (function () {
    function AdministrationFirms(idFirm) {
        this._idFirm = idFirm;
    }
    AdministrationFirms.prototype.loadPartialPage = function (loadUrl, elementWhereReplace, whatget) {
        var _this = this;
        $.ajax({
            type: "GET",
            url: loadUrl,
            contentType: "application/json",
            error: function (xhr, ajaxOptions, thrownError) {
                elementWhereReplace.replaceWith('<div class="alert alert-block">' +
                    '<h4>Внимание!</h4>' +
                    'Данные не были загружены, ошибка - ' + xhr.status + '.' +
                    '</div>');
                console.log(xhr.status);
            },
            success: function (msg, textStatus, jqXHR) {
                //fadeReplace(elementWhereReplace, $(msg));
                elementWhereReplace.replaceWith(msg);
                if (whatget == EWhatGet.Employee) {
                    $('#addEmployeButton1').on('click', function (e) {
                        $.ajax({
                            type: "GET",
                            url: '/Administration/_modal_for_user_add?idFirm=' + -1 + '&efd=' + 2,
                            contentType: "application/json",
                            error: function (xhr, ajaxOptions, thrownError) {
                                //elementWhereReplace.replaceWith('<div class="alert alert-block">' +
                                //    '<h4>Внимание!</h4>' +
                                //    'Данные не были загружены, ошибка - ' + xhr.status + '.' +
                                //    '</div>');
                                console.log(xhr.status);
                            },
                            success: function (msg, textStatus, jqXHR) {
                                $("#modalUserAdd_zxc123zxc").html(msg);
                                $("#modalUserAdd_zxc123zxc").modal('show');
                                //$("#modalUserAdd_zxc123zxc").on('show.bs.modal', function () {
                                //    $("#modalUserAdd_zxc123zxc_in").focus();
                                //})
                            }
                        });
                    });
                    _this.loadEmploeeIds();
                }
                if (whatget == EWhatGet.Department) {
                    _this.loadDepartmentIds();
                }
            }
        });
    };
    AdministrationFirms.prototype.loadDepartmentIds = function (idDepartment) {
        var _this = this;
        if (idDepartment === void 0) { idDepartment = -1; }
        $.ajax({
            type: "GET",
            url: '/Administration/_DepartmentIds?idFirm=' + this._idFirm + '&idDepartment=' + idDepartment,
            contentType: "application/json",
            error: function (xhr, ajaxOptions, thrownError) {
                //elementWhereReplace.replaceWith('<div class="alert alert-block">' +
                //    '<h4>Внимание!</h4>' +
                //    'Данные не были загружены, ошибка - ' + xhr.status + '.' +
                //    '</div>');      
                console.log(xhr.status);
            },
            success: function (msg, textStatus, jqXHR) {
                //   console.log(msg);
                for (var i = 0; i < msg.length; i++) {
                    _this.loadDepartmentPartial(msg[i]);
                    if (msg[i].canSubDep == true) {
                        _this.loadDepartmentIds(msg[i].departmentId);
                    }
                }
            }
        });
    };
    AdministrationFirms.prototype.loadDepartmentPartial = function (department) {
        var html = '';
        var url = '';
        if (department.parentId == null) {
            html = '<tr id="department' + department.departmentId + '" class="treegrid-' + department.departmentId + '"><td></td><td><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div></td><td></td><td></td></tr>';
            url = '/Administration/_DepartmentRowPartial?idFirm=' + this._idFirm + '&nodeDep=' + department.departmentId;
        }
        else {
            html = '<tr id="department' + department.departmentId + '" class="treegrid-' + department.departmentId + ' treegrid-parent-' + department.parentId + '"><td></td><td><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div></td><td></td><td></td></tr>';
            url = '/Administration/_DepartmentRowPartial?idFirm=' + this._idFirm + '&nodeDep=' + department.departmentId + '&parentDep=' + department.parentId;
        }
        $('#departmentNodes').append(html);
        $('.treeDep').treegrid({
            expanderExpandedClass: 'glyphicon glyphicon-minus',
            expanderCollapsedClass: 'glyphicon glyphicon-plus'
        });
        $.ajax({
            type: "GET",
            url: url,
            contentType: "application/json",
            error: function (xhr, ajaxOptions, thrownError) {
                //elementWhereReplace.replaceWith('<div class="alert alert-block">' +
                //    '<h4>Внимание!</h4>' +
                //    'Данные не были загружены, ошибка - ' + xhr.status + '.' +
                //    '</div>');
                var element = $("#department" + department.departmentId);
                var error = '<tr class="alert alert-block" id="department' + department.departmentId + '"><td></td><td><i class="fa fa-exclamation-triangle fa-3x"></i>Ошибка:' + xhr.status + '</div></td><td></td><td></td></tr>';
                fadeReplace(element, $(error));
                console.log(xhr.status);
            },
            success: function (msg, textStatus, jqXHR) {
                // console.log(msg);
                var element = $("#department" + department.departmentId);
                fadeReplace(element, $(msg));
                loadTag();
                //.replaceWith(msg);
                //$('#employBody').append(msg);
            }
        });
    };
    AdministrationFirms.prototype.loadEmploeeIds = function (departmentId) {
        var _this = this;
        if (departmentId === void 0) { departmentId = -1; }
        $.ajax({
            type: "GET",
            url: '/Administration/_EmployeeIds?firmId=' + this._idFirm + '&departmentId=' + departmentId,
            contentType: "application/json",
            error: function (xhr, ajaxOptions, thrownError) {
                //elementWhereReplace.replaceWith('<div class="alert alert-block">' +
                //    '<h4>Внимание!</h4>' +
                //    'Данные не были загружены, ошибка - ' + xhr.status + '.' +
                //    '</div>');      
                console.log(xhr.status);
            },
            success: function (msg, textStatus, jqXHR) {
                //   console.log(msg);
                for (var i = 0; i < msg.length; i++) {
                    _this.loadEmployeePartial(msg[i]);
                }
            }
        });
    };
    AdministrationFirms.prototype.loadEmployeePartial = function (userId) {
        var html = '<tr id="employee' + userId + '"><td></td><td><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div></td><td></td><td></td></tr>';
        $('#employBody').append(html);
        $.ajax({
            type: "GET",
            url: '/Administration/_EmployeePartial?firmId=' + this._idFirm + '&userId=' + userId,
            contentType: "application/json",
            error: function (xhr, ajaxOptions, thrownError) {
                //elementWhereReplace.replaceWith('<div class="alert alert-block">' +
                //    '<h4>Внимание!</h4>' +
                //    'Данные не были загружены, ошибка - ' + xhr.status + '.' +
                //    '</div>');
                var element = $("#employee" + userId);
                var error = '<tr class="alert alert-block" id="employee' + userId + '"><td></td><td><i class="fa fa-exclamation-triangle fa-3x"></i>Ошибка:' + xhr.status + '</div></td><td></td><td></td></tr>';
                fadeReplace(element, $(error));
                console.log(xhr.status);
            },
            success: function (msg, textStatus, jqXHR) {
                // console.log(msg);
                var element = $("#employee" + userId);
                fadeReplace(element, $(msg));
                loadTag();
                //.replaceWith(msg);
                //$('#employBody').append(msg);
            }
        });
    };
    return AdministrationFirms;
}());
function fadeReplace(oldElement, newElement) {
    oldElement.fadeOut("slow", function () {
        var mess = newElement.hide();
        oldElement.replaceWith(mess);
        mess.fadeIn("slow");
    });
}
function loadTag() {
    $("myLoaderTag").ready(function (e) {
    });
}
function loadModalInfo(idFirm, firmName, url) {
    var html = '<div><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div>';
    $('#modalForLoadingData').find('.modal-body').html(html);
    $('#modalForLoadingData').modal('show');
}
function loadModalRemove(idFirm, firmName, url) {
    $('#modalDeleteFirm').find('#orgName').html(firmName);
    $('#modalDeleteFirm').find('#okdDel').html(url);
    $('#modalDeleteFirm').modal('show');
}
var ModalFirmEdit = (function () {
    function ModalFirmEdit(_directors, _name, _idFirm) {
        this.directors = _directors;
        this.nameFirm = _name;
        this.idFirm = _idFirm;
    }
    return ModalFirmEdit;
}());
var ModalDepartmentEdit = (function () {
    function ModalDepartmentEdit(_employee, _department, _idDep) {
        this.DepartmentName = _department;
        this.Employees = _employee;
        this.idDepartment = _idDep;
    }
    return ModalDepartmentEdit;
}());
function openModalEdit(firmId, nameFirm, url) {
    console.log(firmId + " >> " + url);
    var html = '<div><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div>';
    $('#modalEditFirm').find('.modal-title').html('Редактирование (' + nameFirm + ')');
    $('#modalEditFirm').find('#firmName').val(nameFirm);
    $('#modalEditFirm :submit').on('click', function (e) {
        var arr = new Array();
        var valid = false;
        console.log(e);
        var res = $('#modalEditFirm').find("option:selected");
        if (res.length > 0) {
            res.each(function (index, elem) {
                arr.push(elem.value);
            });
            valid = true;
        }
        else {
            valid = false;
        }
        var newName = ($('#modalEditFirm').find('#firmName').val());
        if ((newName == undefined) || (newName == null) || (newName.length < 2)) {
            valid = false;
        }
        if (valid == true) {
            var modal = new ModalFirmEdit(arr, newName, firmId);
            $.ajax('/Administration/_modal_firm_edit?id_firm=' + firmId, {
                type: "POST",
                data: JSON.stringify(modal),
                contentType: "application/json",
                dataType: "json",
                error: function (xhr, ajaxOptions, thrownError) {
                    ///TODO: Сделать обработку ошибок
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                },
                success: function (resp) {
                    location.reload();
                }
            });
        }
    });
    $('#modalEditFirm').find('#directorsDropdown').html(html);
    $('#modalEditFirm').modal('show');
    var urlE = '/Administration/_DropdownDirectorsPartial?id_firm=' + firmId;
    $.get(urlE, function (html) {
        var res = $('#modalEditFirm').find('#directorsDropdown');
        console.log($(html));
        res.replaceWith($(html));
        initSelector();
    });
}
function loadEditDep(idFirm, idDep, nameDep, enumForDelete, url) {
    console.log(idFirm + " >> " + idDep + " >> " + url);
    var html = '<div><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div>';
    $('#myModal_departmentEdit').find('.modal-title').html('Редактирование (' + nameDep + ')');
    $('#myModal_departmentEdit').find('#nameDepInput').val(nameDep);
    $('#myModal_departmentEdit :submit').on('click', function (e) {
        console.log(e);
        var arr = new Array();
        var valid = false;
        console.log(e);
        var res = $('#myModal_departmentEdit').find("option:selected");
        if (res.length > 0) {
            res.each(function (index, elem) {
                arr.push(elem.value);
            });
            valid = true;
        }
        else {
            valid = true;
        }
        var depName = ($('#myModal_departmentEdit').find('#nameDepInput').val());
        valid = (depName.length > 2);
        if (valid == true) {
            var token = $("#__AjaxAntiForgeryForm input").val();
            $.ajax('/Administration/EditDepartment?idFirm=' + idFirm, {
                type: "POST",
                headers: { '__RequestVerificationToken': token },
                data: JSON.stringify(new ModalDepartmentEdit(arr, depName, idDep)),
                contentType: "application/json",
                dataType: "json",
                error: function (xhr, ajaxOptions, thrownError) {
                    ///TODO: Сделать обработку ошибок
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                },
                success: function (resp) {
                    location.reload();
                }
            });
        }
    });
    $('#myModal_departmentEdit').find('#employeeDropdown').html(html);
    //  $('#myModal_departmentEdit').find('#isChiefDropdown .selectpicker').css('block', '')
    //$('#myModal_departmentEdit').find('#isChiefDropdown').html(html);
    $('#myModal_departmentEdit').modal('show');
    var urlE = '/Administration/_DropdownEmployeeDepartmentPartial?idFirm=' + idFirm + '&idDepartment=' + idDep;
    $.get(urlE, function (html) {
        var res = $('#myModal_departmentEdit').find('#employeeDropdown');
        $(html).attr('id', 'employeeDropdown');
        console.log($(html));
        res.replaceWith($(html));
        //var res = $('#myModal_departmentEdit').find("option:selected");
        //$('#myModal_departmentEdit').find('#isChiefDropdown .selectpicker').append(res);
        initSelector();
    });
}
//# sourceMappingURL=_admins_firm_page.js.map