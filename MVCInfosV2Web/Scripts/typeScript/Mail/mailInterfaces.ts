﻿interface IMailListItemModelView {
	mailId: string;
	registerDate: Date;
	custumer: string;
	description: string;
	hasInnerFiles: boolean;
	isEditeble: boolean;
}

interface IMailUploadFileModelView {
	uploadFileID: string;
}

interface IMailUploadGroupModelView {
	groupFileId: string;
	uploadFiles: IMailUploadFileModelView[];
}

interface IMailResolutionNotifierModelView {
	resolutionNotifierId: string;
	notifyTimeKey: number;
	userId: string;
	fio: string;
}

interface IMailResolutionModelView {
	resolutionId: string;
	comment: string;
	resolutionNotifier: IMailResolutionNotifierModelView[];
}

interface IMailModelView {
	mailId: string;
	documentID: number;
	registerDate: Date;
	numberDocument: string;
	documentDate: Date;
	description: string;
	finishDate: Date;
	uploadGroupFile: IMailUploadGroupModelView;
	resolution: IMailResolutionModelView;
	isCanEdit: boolean;
}

enum ENotificationsClickType {
	preview,
	aprove,
	setAside,
	none
}


interface IMailEmployeeInfo {
	userId: string;
	fio: string;
}

interface IMailNotificationsResultViewModel {
	mailId: string;
	date: Date;
	notificationsResult: INotificationsResultViewModel[];
}

interface INotificationsResultViewModel {
	user: IMailEmployeeInfo;
	resolutionNotificationId: string;
	date: Date;
	clickType: ENotificationsClickType;
}
