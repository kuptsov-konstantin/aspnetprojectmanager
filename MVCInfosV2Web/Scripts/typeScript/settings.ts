﻿/// <reference path="../typings/jquery/jquery.d.ts" />
/// <reference path="../typings/switchery/switchery.d.ts" />
/// <reference path="../typings/extends.d.ts" />


interface IHostingGet {
	hostingName?: string
	hostingId?: Guid
	pluginConnectionType?: Guid
	pluginInfo?: string;
	isNotDelete?: boolean;
	isDefault?: boolean;
}


interface IHosting extends IHostingGet {
	
	hostingUploadUrl?: string
	
	token?: string
	login?: string
	password?: string
}


///Сделать настройку!
class Hosting implements IHosting {
	hostingName?: string
	hostingId?: Guid
	hostingUploadUrl?: string
	pluginInfo?: string;
	pluginConnectionType?: Guid;

	isNotDelete?: boolean;
	isDefault?: boolean;

	token?: string
	login?: string
	password?: string
	constructor(hostInfo: IHosting) {


	}
}

interface IActiveDirectorySettings {
	isUsingActiveDirectory?: boolean;
	isNotUsingCurrentUser?: boolean;
	userName?: string;
	password?: string;
}

class SettingsPage {
	hosts: Hosting[];
	forms: any;
	adminValid: boolean;
	loadingFileHostings = $.Deferred();
	initFileHostings() {
		this.getHileHostings();
		$('button[button-type="save"]').on("click", (e) => {
			var dictionary = {	}
			$("input:checkbox").each((index, elem) => {
				var elemP = $(elem).parents('tr');
				console.log(elemP.attr("guid") + " >> " + $(elem).parent().hasClass("checked"));
				dictionary[elemP.attr("guid")] = $(elem).parent().hasClass("checked") as boolean;
			});
			
			var token = $("#__AjaxAntiForgeryForm input").val();
			var url = 'UpdateDefaultPlugin';
			$.ajax({
				type: 'POST',
				headers: { '__RequestVerificationToken': token },
				url: url,
				data: {
					'parametrs': JSON.stringify(dictionary)
				},
				error: (xhr, ajaxOptions, thrownError) => {
					console.log(xhr);
				},
				success: (result) => {
					location.reload();
				}
			});

			//location.reload();
		})


		$('#addHostButton').on('click', (e) => {
			$('#myModal button[button-type="submit"]').on("click", (e) => {
				var hostName = $('#host-name').val() as string;
				var uploadUrl = $('#host-uploaderurl').val() as string;
				if ((hostName === undefined) || (hostName.length < 5)) {
					return;
				}
				if ((uploadUrl === undefined) || (uploadUrl.length < 5)) {
					return;
				}
				this.sendSaveFileHosting({
					pluginConnectionType: Guid.setId( $("#pluginList").val()),					
					hostingName: $('#host-name').val(),
					hostingUploadUrl: $('#host-uploaderurl').val()
				});
				$('#myModal').modal("hide");
				//location.reload();
			})
			$('#myModal').modal('show');
		})
	}

	getHileHostings() {
		$.ajax({
			url: 'GetAllFileHosting',
			success: (hosts: IHostingGet[]) => {
				this.hosts = new Array<Hosting>();
				hosts.forEach((value, index, array) => {
					this.hosts.push({
						hostingId: value.hostingId,
						hostingName: value.hostingName,
						pluginConnectionType: value.pluginConnectionType,
						pluginInfo: value.pluginInfo,
						isDefault: value.isDefault,
						isNotDelete: value.isNotDelete
					});
				});
				this.loadingFileHostings.resolve(this.hosts);
			}
		});
		$.when(this.loadingFileHostings).done(() => {

			this.hosts.forEach((value, index, array) => {
				this.getFileHostingPartialInfo(value);
			});
			initICheck();
		});
	}

	getFileHostingPartialInfo(host: Hosting) {
		console.log(host);

		var row = `<tr id="host-${host.hostingId.id}" guid="${host.hostingId.id}">`;
		if (host.isDefault) {
			if (this.hosts.length == 1) {
				row = row + '<td><input type="checkbox" name="isDefault" checked class="flat" disabled></td>';
			} else {
				row = row + '<td><input type="checkbox" name="isDefault" checked class="flat" ></td>';
			}

		} else {
			row = row + '<td><input type="checkbox" name="isDefault" class="flat" ></td>';
		}
		row = row + '<td>' + host.hostingName + '</td>';
		row = row + '<td>' + host.pluginInfo + '</td>';

		if (host.isNotDelete) {
			row = row +
				'<td>' +
				'<button disabled class="btn btn-xs btn-info" onclick= "settingsPage.showInfoHost(\'' + host.hostingId + '\')" > <i class="fa fa-info" > </i> Просмотреть</button>' +
				'<button disabled class="btn btn-xs btn-primary" onclick= "settingsPage.showEditHost(\'' + host.hostingId + '\')" > <i class="fa fa-edit" > </i> Редактировать</button>' +
				'<button disabled class="btn btn-xs btn-danger" onclick= "settingsPage.showDelHost(\'' + host.hostingId + '\')" > <i class="fa fa-trash" > </i> Удалить</button>' +
				'</td>';

		} else {
			row = row +
				'<td>' +
				'<button class="btn btn-xs btn-info" onclick= "settingsPage.showInfoHost(\'' + host.hostingId + '\')" > <i class="fa fa-info" > </i> Просмотреть</button>' +
				'<button class="btn btn-xs btn-primary" onclick= "settingsPage.showEditHost(\'' + host.hostingId + '\')" > <i class="fa fa-edit" > </i> Редактировать</button>' +
				'<button class="btn btn-xs btn-danger" onclick= "settingsPage.showDelHost(\'' + host.hostingId + '\')" > <i class="fa fa-trash" > </i> Удалить</button>' +
				'</td>';
		}
		row = row + '</tr>';
		$('#hostsBody').append(row);

		$("input:checkbox").parent().on('click', function () {
			var $box = $(this).parent();
			if ($box.hasClass("checked")) {
				// the name of the box is retrieved using the .attr() method
				// as it is assumed and expected to be immutable
				var group = "input:checkbox[name='" + $box.attr("name") + "']";
				// the checked state of the group/box on the other hand will change
				// and the current value is retrieved using .prop() method
				$(group).parent().removeClass("checked");
				$box.addClass("checked");
			} else {
				$box.removeClass("checked");
			}
		});
	}

	editFileHostingById() {

	}
	sendSaveFileHosting(host: IHosting) {
		console.log(host);
		var token = $("#__AjaxAntiForgeryForm input").val();
		var url = 'SaveNewFileHosting';
		$.ajax({
			type: 'POST',
			headers: { '__RequestVerificationToken': token },
			url: url,
			data: {
				'parametrs': JSON.stringify(host)
			},

			error: (xhr, ajaxOptions, thrownError) => {
				console.log(xhr);
			},
			success: (result) => {
				location.reload();
			}
		});



	}
	saveFileHostingRow() {

	}
	pluginInit() {
		initSelector('#pluginList');
		$('#pluginList').on("changed.bs.select", (event) => {
			console.log(event);
		});
		this.updateForm($('#pluginList').val());
	}

	updateForm(id: string) {
		var val_name = $("#myModal form #host-name").val();
		if (val_name === undefined) {
			val_name = '';
		}
		$('#myModal form').replaceWith(this.forms[id]);
		$('#myModal form').append(`<div class="form-group"><label class="control-label col-md-3 col-sm-3 col-xs-12" for="host-theme">Название <span class="required">*</span></label>
								<div class="col-md-9 col-sm-9 col-xs-12"><input type="text" id="host-name" required class="form-control col-md-12 col-xs-12" value="${val_name}"></div></div>`);
	}

	setFormsCollection(collection: any) {
		this.forms = collection;

		//this.forms = new Map<string, string>(collection);
	}
	validateAdminDeferred = $.Deferred();

	updateAdSettings(options: IActiveDirectorySettings) {
		var token = $("#__AjaxAntiForgeryForm input").val();
		$.ajax({
			url: 'UpdateAdminActiveDirectory',
			data: JSON.stringify({
				parametrs: window.btoa(escape((JSON.stringify(options))))
			}),
			error: (xhr, ajaxOptions, thrownError) => {
				alert("Произошла ошибка при сохранении: " + thrownError);
			},
			success: (result: { result: boolean }) => {
				location.reload();
			},

			type: 'POST',
			contentType: "application/json",
			dataType: "json",
			headers: { '__RequestVerificationToken': token },
		});
	}
	initAD() {
		$('#saveAdSettings').on('click', (e) => {
			if ($('#activeDirectoryForm').valid()) {
				var adSwitch = $('input[name="activeDirectory-switch"]').prop('checked') as boolean;
				var adNotCurrentUser = $('input[name="activeDirectory-currentuser"]').prop('checked') as boolean;
				if (adSwitch === true) {
					if (adNotCurrentUser === true) {
						$('#testAdmin').click();
						this.validateAdminDeferred.done(() => {
							if (this.adminValid == true) {
								this.updateAdSettings({
									isUsingActiveDirectory: true,
									isNotUsingCurrentUser: true,
									password: $('#activeDirectory-pass-admin').val(),
									userName: $('#activeDirectory-login-admin').val()
								})
							} else {
								setError(['#activeDirectory-login-admin', '#activeDirectory-pass-admin']);
								this.adminValid = false;
							}
						});
						this.validateAdminDeferred.fail(() => {
							setError(['#activeDirectory-login-admin', '#activeDirectory-pass-admin']);
							this.adminValid = false;
						});
					} else {
						this.updateAdSettings({
							isUsingActiveDirectory: true,
							isNotUsingCurrentUser: false
						})
					}
				} else {
					this.updateAdSettings({ isUsingActiveDirectory: false })
				}
				console.log($('#activeDirectoryForm').serialize());
			}
		});
		$('#testAdmin').on('click', (e) => {
			var login = $('#activeDirectory-login-admin').val() as string;
			var pass = $('#activeDirectory-pass-admin').val() as string;
			if ((login.length == 0) || (pass.length == 0)) {
				setError(['#activeDirectory-login-admin', '#activeDirectory-pass-admin']);
				this.adminValid = false;
			} else {
				var token = $("#__AjaxAntiForgeryForm input").val();
				$.ajax({
					url: 'ValidateAdmin',
					data: JSON.stringify({
						parametrs: window.btoa(escape((JSON.stringify({
							login: login,
							password: pass
						}))))
					}),
					error: (xhr, ajaxOptions, thrownError) => {
						setError(['#activeDirectory-login-admin', '#activeDirectory-pass-admin']);
						this.adminValid = false;
						this.validateAdminDeferred.fail();
						/*myRemoveClass([$('#activeDirectory-login-admin'), $('#activeDirectory-pass-admin')], 'my_valid_not_error');
						myAddClass([$('#activeDirectory-login-admin'), $('#activeDirectory-pass-admin')], 'my_valid_error');*/
					},
					success: (result: { result: boolean }) => {
						this.adminValid = result.result;
						if (result.result === true) {
							setOK(['#activeDirectory-login-admin', '#activeDirectory-pass-admin']);
						} else {
							setError(['#activeDirectory-login-admin', '#activeDirectory-pass-admin']);
						}

						this.validateAdminDeferred.resolve();

						/*myRemoveClass([$('#activeDirectory-login-admin'), $('#activeDirectory-pass-admin')],  'my_valid_error');
						myAddClass([$('#activeDirectory-login-admin'), $('#activeDirectory-pass-admin')], 'my_valid_not_error');*/
					},

					type: 'POST',
					contentType: "application/json",
					dataType: "json",
					headers: { '__RequestVerificationToken': token },
				});
			}
		});
	}
	changeAd(name) {
		var checkedResult = switcheryDictionary[name].isChecked();
		$('input[name="' + name + '"]').prop('checked', checkedResult);
		var element = $('input[name="' + name + '"]').data('toggle');
		if (checkedResult === true) {
			$(element).show(400);
		} else {
			$(element).hide(400);
		}
	}

	isUsingCurrentUser() {
		switcheryDictionary["activeDirectory-currentuser"].isChecked();
	}

	createCertificate() {
		$('#createCertificate').on('click', (e) => {
			var token = $("#__AjaxAntiForgeryForm input").val();
			$.ajax({
				url: 'UpdateCerificates',
				data: JSON.stringify({
					parametrs: window.btoa(escape((JSON.stringify({ command: "update" }))))
				}),
				error: (xhr, ajaxOptions, thrownError) => {
					alert("Произошла ошибка при обновлении: " + thrownError);
				},
				success: (result: { result: boolean }) => {
					location.reload();
				},

				type: 'POST',
				contentType: "application/json",
				dataType: "json",
				headers: { '__RequestVerificationToken': token },
			});

		});
	}
}
let settingsPage = new SettingsPage();
