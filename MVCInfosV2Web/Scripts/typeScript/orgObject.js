var OrgObject = (function () {
    function OrgObject() {
        this.departments = new Array();
        this.employees = new Array();
        this.aboutFirm = new AboutFirm();
    }
    return OrgObject;
}());
;
var Department = (function () {
    function Department() {
        this.department = new Valid();
    }
    return Department;
}());
var AboutFirm = (function () {
    function AboutFirm() {
        this.orgName = new Valid();
        this.country = new Valid();
        this.city = new Valid();
        this.address = new Valid();
    }
    return AboutFirm;
}());
var Employee = (function () {
    function Employee() {
        this.department = -1;
        this.familyName = new Valid();
        this.name = new Valid();
        this.middleName = new Valid();
        this.birthDay = new Valid();
        this.userName = new Valid();
        this.email = new Valid();
        this.pass = new Valid();
        this.role = new Valid();
        this.emploement = new Valid();
    }
    Object.defineProperty(Employee.prototype, "isValidate", {
        get: function () {
            return this.familyName.isValidate && this.name.isValidate && this.middleName.isValidate && this.birthDay.isValidate
                && this.userName.isValidate && this.email.isValidate && this.pass.isValidate && this.role.isValidate;
        },
        enumerable: true,
        configurable: true
    });
    ;
    return Employee;
}());
;
var Valid = (function () {
    function Valid() {
        this.isValidate = false;
        this.str = "";
    }
    return Valid;
}());
var Response = (function () {
    function Response() {
    }
    return Response;
}());
//# sourceMappingURL=orgObject.js.map