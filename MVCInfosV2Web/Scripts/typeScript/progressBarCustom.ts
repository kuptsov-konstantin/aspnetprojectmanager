﻿/// <reference path="guid.ts" />
class MyProgressbarStatusMessage {
	id: Guid;
	constructor() {
		this.id = Guid.newGuid();
	}
	message: string;
	start: number;
	stop: number;
}

class MyProgressbar {
	private _counter: number = 0;
	private _factor: number = 1;
	private _countMax: number = 0;
	private _allEmployees: number = 0;
	private _progressBar: JQuery;
	private _modalWindow: JQuery;
	private _isCloseWhen100: boolean = false;
	///TODO: сделать статусы на каждый ход сохранений
	private _statusMessage: Array<MyProgressbarStatusMessage>;
	public constructor(progressBar: JQuery, modalWindow: JQuery, isCloseWhen100: boolean) {
		this._modalWindow = modalWindow;
		this._isCloseWhen100 = isCloseWhen100;
		this._progressBar = progressBar;

	}

	public settingStatusMessage(messages: Array<MyProgressbarStatusMessage>) {
		this._statusMessage = messages;
		if (this._progressBar.hasClass('progress-my-status-info')) {
			var ul;
			if (this._progressBar.parent().parent().find("ul.progress-my-status-info-ul.progressbar-status").length == 0) {
				ul = document.createElement("ul");
				(<HTMLUListElement>ul).className = 'progress-my-status-info-ul progressbar-status';
				this._progressBar.parent().before((<HTMLUListElement>ul));
			} else {
				this._progressBar.parent().parent().find("ul.progress-my-status-info-ul").empty();
				ul = this._progressBar.parent().parent().find("ul.progress-my-status-info-ul");
			}
			for (var i = 0; i < this._statusMessage.length; i++) {
				var li = document.createElement("li");
				li.id = this._progressBar.attr('id') + 'li-' + this._statusMessage[i].id.id;
				li.innerHTML = this._statusMessage[i].message + '<span> </span>';
				if (this._progressBar.parent().parent().find("ul.progress-my-status-info-ul.progressbar-status").length == 0) {
					(<HTMLUListElement>ul).appendChild(li);
				} else {
					(<JQuery>ul).append(li);
				}
			}
		}
	}

    /**
    Устанавливается максимум прогресбара
    */
	public maximumWithoutFactor(allEmployees: number) {
		this._allEmployees = allEmployees;
		this._progressBar.attr("aria-valuemax", allEmployees);
	}
    /**
    Максимум с умонежением на множитель
    */
	public get length(): number {
		return this._factor * this._allEmployees;
	}

	public set factor(val: number) {
		this._factor = val;
	}
	public get factor(): number {
		return this._factor;
	}

	public set max(value: number) {
		this._countMax = value;
		this._progressBar.attr("aria-valuemax", value);
	}
	public inc() {
		this._counter++;
		this.updateProgress();
		if (this.proc == 100) {
			if (this._isCloseWhen100 == true) {
				this._modalWindow.modal('hide');
			}
		}
	}

	public get proc() {
		return this._counter * 100 / this.length;
	}
	public currentMessage(): string {
		var filtred = this._statusMessage.filter(e => {
			if ((e.start <= this._counter) && (this._counter <= e.stop)) {
				return e;
			}
		});
		if (filtred.length > 0) {
			return filtred[0].message;
		} else {
			return "";
		}
	}
	///TODO: Проверить ресет
	public resetProgress() {
		this._counter = 0;
		this.updateProgress();
	}
	public updateStatausInfo() {
		if (this._progressBar.hasClass('progress-my-status-info')) {
			var ok = this._statusMessage.filter(e => {
				if (e.stop <= this._counter) {
					var elem = $('#' + this._progressBar.attr('id') + 'li-' + e.id.id);
					if (!elem.hasClass('progressbar-notok')) {
						myRemoveClass([elem], 'progressbar-wait');
						myRemoveClass([elem], 'progressbar-progress');
						myAddClass([elem], 'progressbar-ok');
					}
					return e;
				}
			});
			var inProgress = this._statusMessage.filter(e => {
				if ((e.start <= this._counter) && (this._counter < e.stop)) {
					var elem = $('#' + this._progressBar.attr('id') + 'li-' + e.id.id);
					if (!elem.hasClass('progressbar-notok')) {
						myRemoveClass([elem], 'progressbar-wait');
						myRemoveClass([elem], 'progressbar-ok');
						myAddClass([elem], 'progressbar-progress');
					}
					return e;
				}
			});
			var wait = this._statusMessage.filter(e => {
				if (this._counter < e.start) {
					var elem = $('#' + this._progressBar.attr('id') + 'li-' + e.id.id);
					myAddClass([elem], 'progressbar-wait');

					return e;
				}
			});
		}
	}
	public updateProgress() {
		this.updateStatausInfo();
		if (this._progressBar.hasClass('progress-my-status')) {
			var div;
			if (this._progressBar.parent().parent().find("div.progress-my-status-info-mes").length == 0) {
				div = document.createElement('div');
				(<HTMLDivElement>div).className = "progress-my-status-info-mes";
				this._progressBar.parent().after(div);
				(<HTMLDivElement>div).innerHTML = (this.currentMessage());
			} else {
				this._progressBar.parent().parent().find("div.progress-my-status-info-mes").empty();
				div = this._progressBar.parent().parent().find("div.progress-my-status-info-mes");
				(<JQuery>div).html(this.currentMessage());
			}



			//  this._progressBar.parent().length;
			// this._progressBar.after(this.currentMessage());
		} else {

		}
		this._progressBar.html(this.proc.toFixed(2) + '%');
		this._progressBar.attr('aria-valuenow', this._counter).css('width', this.proc.toFixed(2) + '%');
	}
}