﻿/// <reference path="modals.ts" />
enum EWhatGet {
    Employee, Department
}


class DepartmentResponse {
    parentId: number;
    departmentId: number;
    canSubDep: boolean;
}
enum EnumerableForDelete {
    Firm,
    Department,
    User,
    Project,
    None
}
type EInputType = "button" | "checkbox" | "file" | "hidden" | "image" | "password" | "reset" | "submit" | "text" |
    "color" | "date" | "datetime" | "datetime-local" | "email" | "number" | "range" |
    "search" | "tel" | "time" | "url" | "month" | "week";

class AdministrationFirms {
    _idFirm: number;
    employees: { [id: string]: EmployeePrint };
    name: string;
    constructor(idFirm: number) {
        this.employees = {};
        this._idFirm = idFirm;
    }
    setName(name: string) {
        this.name = name;
    }
    loadPartialPage(loadUrl: string, elementWhereReplace: JQuery, whatget: EWhatGet) {
        $.ajax({
            type: "GET",
            url: loadUrl,
            contentType: "application/json",
            error: (xhr, ajaxOptions, thrownError) => {
                elementWhereReplace.replaceWith('<div class="alert alert-block">' +
                    '<h4>Внимание!</h4>' +
                    'Данные не были загружены, ошибка - ' + xhr.status + '.' +
                    '</div>');
                console.log(xhr.status);
            },
            success: (msg, textStatus, jqXHR) => {
                //fadeReplace(elementWhereReplace, $(msg));
                elementWhereReplace.replaceWith(msg);
                if (whatget == EWhatGet.Employee) {
                    this.loadEmployee();
                }
                if (whatget == EWhatGet.Department) {
                    this.loadDepartmentIds();
                }
            }
        });
    }

    loadEmployee() {
        $('#addEmployeButton1').on('click', (e) => {
            let modalAdd = new ModalEmployeeAdd();
			//id: string, type: EInputType, label, name, isRequired
			let inputs = Modal.getFormGroups([
				{
					id: "addLogin",
					type: 'text',
					label: "Логин",
					name: "login",
					isRequired: true
				},
				{
					id: "addEmail",
					type: 'email',
					label: "E-Mail",
					name: "email",
					isRequired: true
				},
				{
					id: "addPass",
					type: 'password',
					label: "Пароль",
					name: "password",
					isRequired: true
				},
				{
					id: "addRepeatPass",
					type: 'password',
					label: "Повторить пароль",
					name: "password_confirm",
					isRequired: true
				},
				{
					id: "addFamily",
					type: 'text',
					label: "Фамилия",
					name: "family",
					isRequired: true
				},
				{
					id: "addName",
					type: 'text',
					label: "Имя",
					name: "name",
					isRequired: true
				},
				{
					id: "addMiddle",
					type: 'text',
					label: "Отчество",
					name: "middle",
					isRequired: true
				}
			]);

            let modalBody = `<div class="form-horizontal">${inputs.join('')}</div>`;
            modalAdd.isForm = true;
            modalAdd.setModalBody(modalBody);
            modalAdd.modalReplacer();
            modalAdd.initValidator();
            modalAdd.initButton('#modalReplace .modal-footer [data-type="save"]', () => {
                if (modalAdd.isValid()) {

                }
            });
            modalAdd.modalShow();
        })
        this.loadEmploeeIds();
    }
    loadDepartmentIds(idDepartment: number = -1) {
        $.ajax({
            type: "GET",
            url: `/Administration/_DepartmentIds?idFirm=${this._idFirm}&idDepartment=${idDepartment}`,
            contentType: "application/json",
            error: (xhr, ajaxOptions, thrownError) => {
                //elementWhereReplace.replaceWith('<div class="alert alert-block">' +
                //    '<h4>Внимание!</h4>' +
                //    'Данные не были загружены, ошибка - ' + xhr.status + '.' +
                //    '</div>');      

                console.log(xhr.status);
            },
            success: (msg: Array<DepartmentResponse>, textStatus: string, jqXHR: JQueryXHR) => {
                //   console.log(msg);
                for (var i = 0; i < msg.length; i++) {
                    this.loadDepartmentPartial(msg[i]);
                    if (msg[i].canSubDep == true) {
                        this.loadDepartmentIds(msg[i].departmentId);
                    }
                }

            }
        });
    }
    loadDepartmentPartial(department: DepartmentResponse) {
        var html = '';
        var url = '';
        if (department.parentId == null) {
            html = `<tr id="department${department.departmentId}" class="treegrid-${department.departmentId}"><td></td><td><div><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div></td><td></td><td></td></tr>`;
            url = `/Administration/_DepartmentRowPartial?idFirm=${this._idFirm}&nodeDep=${department.departmentId}`;
        } else {
            html = `<tr id="department${department.departmentId}" class="treegrid${department.departmentId} treegrid-parent-${department.parentId}"><td></td><td><div><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div></td><td></td><td></td></tr>`;
            url = `/Administration/_DepartmentRowPartial?idFirm=${this._idFirm}&nodeDep=${department.departmentId}&parentDep=${department.parentId}`;
        }
        $('#departmentNodes').append(html);
        $('.treeDep').treegrid({
            expanderExpandedClass: 'glyphicon glyphicon-minus',
            expanderCollapsedClass: 'glyphicon glyphicon-plus'
        });
        $.ajax({
            type: "GET",
            url: url,
            contentType: "application/json",
            error: (xhr, ajaxOptions, thrownError) => {
                //elementWhereReplace.replaceWith('<div class="alert alert-block">' +
                //    '<h4>Внимание!</h4>' +
                //    'Данные не были загружены, ошибка - ' + xhr.status + '.' +
                //    '</div>');
                var element = $("#department" + department.departmentId);
                var error = `<tr class="alert alert-block" id="department${department.departmentId}"><td></td><td><i class="fa fa-exclamation-triangle fa-3x"></i>Ошибка: ${xhr.status}</div></td><td></td><td></td></tr>`
                fadeReplace(element, $(error));
                console.log(xhr.status);
            },
            success: (msg, textStatus: string, jqXHR: JQueryXHR) => {
                // console.log(msg);
                var element = $("#department" + department.departmentId);
                fadeReplace(element, $(msg))


                loadTag();
                //.replaceWith(msg);
                //$('#employBody').append(msg);
            }
        });
    }

    loadEmploeeIds(departmentId: number = -1) {
        $.ajax({
            type: "GET",
            url: `/Administration/_EmployeeIds?firmId=${this._idFirm}&departmentId=` + departmentId,
            contentType: "application/json",
            error: (xhr, ajaxOptions, thrownError) => {
                //elementWhereReplace.replaceWith('<div class="alert alert-block">' +
                //    '<h4>Внимание!</h4>' +
                //    'Данные не были загружены, ошибка - ' + xhr.status + '.' +
                //    '</div>');      

                console.log(xhr.status);
            },
            success: (msg: Array<string>, textStatus: string, jqXHR: JQueryXHR) => {
                //   console.log(msg);
                for (var i = 0; i < msg.length; i++) {
                    this.loadEmployeePartial(msg[i]);
                }
            }
        });
    }

    loadEmployeePartial(userId: string) {
        var html = `<tr id="employee${userId}"><td></td><td><div><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div></td><td></td><td></td></tr>`
        $('#employBody').append(html);
        $.ajax({
            type: "GET",
            url: `/Administration/_EmployeePartial?firmId=${this._idFirm}&userId=${userId}`,
            contentType: "application/json",
            error: (xhr, ajaxOptions, thrownError) => {
                //elementWhereReplace.replaceWith('<div class="alert alert-block">' +
                //    '<h4>Внимание!</h4>' +
                //    'Данные не были загружены, ошибка - ' + xhr.status + '.' +
                //    '</div>');
                var element = $("#employee" + userId);
                var error = `<tr class="alert alert-block" id="employee${userId}"><td></td><td><i class="fa fa-exclamation-triangle fa-3x"></i>Ошибка: ${xhr.status}</div></td><td></td><td></td></tr>`
                fadeReplace(element, $(error));
                console.log(xhr.status);
            },
            success: (employ: EmployeePrint, textStatus: string, jqXHR: JQueryXHR) => {
                // console.log(msg);
                this.employees[employ.idUser] = employ;
                var element = $("#employee" + userId);
                fadeReplace(element, $(this.employeeTemplate(employ)))
                loadTag();
                //.replaceWith(msg);
                //$('#employBody').append(msg);
            }
        });
    }

    private employeeTemplate(employee: EmployeePrint): string {
		var buttons = 
`<button type="button" class="btn btn-primary btn-xs" title="Редактировать" onclick="${this.name}.editEmployeeButtonClick('${employee.idUser}')">
	<span class="glyphicon glyphicon-edit" /> Редактировать
</button>
<button type="button" class="btn btn-info btn-xs" title="Информация" onclick="${this.name}.infoEmployeeButtonClick('${employee.idUser}')">
	<span class="fa fa-info" /> Информация
</button>`;


        if ((employee.isNotDelete == false) && (employee.isADUser == false)) {
            var deleteButton = `<button type="button" class="btn btn-danger btn-xs" title="Удалить" onclick="${this.name}.deleteEmployeeButtonClick('${employee.idUser}')">
	<span class="fa fa-trash" /> Удалить
</button>`;
            buttons = buttons + deleteButton;
        }




        var mainInfo = `<td>${employee.userName}</td><td>${employee.eMail}</td><td id="fio_${employee.idUser}">${employee.fio}</td><td>${buttons}</td>`;
        var tr = `<tr id="employee${employee.idUser}" class="not-delete-${(employee.isNotDelete == false) || (employee.isADUser == false)}">${mainInfo}</tr>`;

        return tr;
    }



    public editEmployeeButtonClick(userId: string) {
        let modalEdit = new ModalEmployeeEdit();
        if (this.employees[userId].isNotDelete == true) return;
        if (this.employees[userId].isADUser == true) return;

        //<div class="form-group">
        //  <label for="exampleInputEmail1">Email address</label>
        //  <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
        //</div>
        let modalBody = `<div>




</div>`;
        modalEdit.setModalBody(modalBody);
        modalEdit.modalReplacer();
        modalEdit.initButton('#modalReplace .modal-footer [data-type="close"]', () => {
            modalEdit.modalClose();
        });
        modalEdit.modalShow();

    }
    public infoEmployeeButtonClick(userId: string) {
        let modalInfo = new ModalEmployeeInfo();
        let modalBody = `<dl class="dl-horizontal">
						<dt>ФИО:</dt><dd>${this.employees[userId].fio}</dd>
						<dt>Логин:</dt><dd>${this.employees[userId].userName}</dd>	
						<dt>Электронная почта:</dt><dd>${this.employees[userId].eMail}</dd>	
						<dt>Дата рождения:</dt><dd>${this.employees[userId].birthData}</dd>
					</dl>`;
        modalInfo.setModalBody(modalBody);
        modalInfo.modalReplacer();
        modalInfo.initButton('#modalReplace .modal-footer [data-type="close"]', () => {
            modalInfo.modalClose();
        });
        modalInfo.modalShow();
    }

    public deleteEmployeeButtonClick(userId: string) {
        if (this.employees[userId].isNotDelete == true) return;
        if (this.employees[userId].isADUser == true) return;
        var modalDelete = new ModalEmployeeDelete();

        var modalBody = `<dl class="dl-horizontal">
                        <dt>ФИО:</dt><dd>${this.employees[userId].fio}</dd>
                        <dt>Логин:</dt><dd>${this.employees[userId].userName}</dd>
                    </dl>`;
        modalDelete.setModalBody(modalBody);
        modalDelete.modalReplacer();
        modalDelete.initButton('#modalReplace .modal-footer [data-type="no"]', () => {
            modalDelete.modalClose();
        });
        var url = `Delete?id_user_to_delete=${this.employees[userId].idUser}&efd=${EnumerableForDelete.User}`;

        modalDelete.initButton('#modalReplace .modal-footer [data-type="yes"]', () => {
            $.ajax(url, {
                error: (xhr, ajaxOptions, thrownError) => {
                    window.alert("При удалении произошла ошибка");
                },
                success: (result, textStatus: string, jqXHR: JQueryXHR) => {
                    location.reload();
                }

            });
        });

        modalDelete.modalShow();

    }

}

class EmployeePrint {
    familyName?: string;
    name?: string;
    middleName?: string;
    fio?: string;
    birthData?: string;
    idUser?: string;
    userName?: string;
    eMail?: string;
    isADUser?: boolean;
    isNotDelete?: boolean;
}

function fadeReplace(oldElement: JQuery, newElement: JQuery) {
    oldElement.fadeOut("slow", () => {
        var mess = newElement.hide();
        oldElement.replaceWith(mess);
        mess.fadeIn("slow");
    });
}
function loadTag() {
    $("myLoaderTag").ready((e) => {

    });
}
function loadModalInfo(idFirm: number, firmName: string, url: string) {
    var html = '<div><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div>'
    $('#modalForLoadingData').find('.modal-body').html(html);
    $('#modalForLoadingData').modal('show');

}
function loadModalRemove(idFirm: number, firmName: string, url: string) {
    $('#modalDeleteFirm').find('#orgName').html(firmName);
    $('#modalDeleteFirm').find('#okdDel').html(url);
    $('#modalDeleteFirm').modal('show');
}


class ModalFirmEdit {
    directors: Array<string>;
    nameFirm: string;
    idFirm: number;
    constructor(_directors: Array<string>, _name: string, _idFirm: number) {
        this.directors = _directors;
        this.nameFirm = _name;
        this.idFirm = _idFirm;
    }
}

class ModalDepartmentEdit {
    Employees: Array<string>;
    DepartmentName: string;
    idDepartment: number;
    constructor(_employee, _department, _idDep) {
        this.DepartmentName = _department;
        this.Employees = _employee;
        this.idDepartment = _idDep;
    }
}


function openModalEdit(firmId: number, nameFirm: string, url: string) {
    console.log(firmId + " >> " + url);
    var html = '<div><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div>'
    $('#modalEditFirm').find('.modal-title').html('Редактирование (' + nameFirm + ')');
    $('#modalEditFirm').find('#firmName').val(nameFirm);
    $('#modalEditFirm :submit').on('click', (e) => {
        var arr = new Array<string>();
        var valid = false;
        console.log(e);
        var res = $('#modalEditFirm').find("option:selected");
        if (res.length > 0) {
            res.each((index, elem: HTMLOptionElement) => {
                arr.push(elem.value);
            });
            valid = true;
            // arr.push(
        } else {
            valid = false;
            //TODO: Сделать вывод ошибок
            //setError();
        }

        var newName = <string>($('#modalEditFirm').find('#firmName').val());
        if ((newName == undefined) || (newName == null) || (newName.length < 2)) {
            valid = false;
        }

        if (valid == true) {
            var modal = new ModalFirmEdit(arr, newName, firmId);
            $.ajax('/Administration/_modal_firm_edit?id_firm=' + firmId, {
                type: "POST",
                data: JSON.stringify(modal),
                contentType: "application/json",
                dataType: "json",
                error: (xhr: JQueryXHR, ajaxOptions: string, thrownError: string) => {
                    ///TODO: Сделать обработку ошибок
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                },
                success: (resp) => {
                    location.reload();
                }
            })
        }
    });
    $('#modalEditFirm').find('#directorsDropdown').html(html);
    $('#modalEditFirm').modal('show');
    var urlE = '/Administration/_DropdownDirectorsPartial?id_firm=' + firmId;
    $.get(urlE, (html: HTMLElement) => {
        var res = $('#modalEditFirm').find('#directorsDropdown');
        console.log($(html));
        res.replaceWith($(html));
        initSelector('.selectpicker');
    });
}


function loadEditDep(idFirm: number, idDep: number, nameDep: string, enumForDelete, url: string) {
    console.log(idFirm + " >> " + idDep + " >> " + url);
    var html = '<div><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div>';

    $('#myModal_departmentEdit').find('.modal-title').html('Редактирование (' + nameDep + ')');
    $('#myModal_departmentEdit').find('#nameDepInput').val(nameDep);
    $('#myModal_departmentEdit :submit').on('click', (e) => {
        console.log(e);
        var arr = new Array<string>();
        var valid = false;
        console.log(e);
        var res = $('#myModal_departmentEdit').find("option:selected");
        if (res.length > 0) {
            res.each((index, elem: HTMLOptionElement) => {
                arr.push(elem.value);
            });
            valid = true;
            // arr.push(
        } else {
            valid = true;
            //TODO: Сделать вывод ошибок
            //setError();
        }

        var depName = <string>($('#myModal_departmentEdit').find('#nameDepInput').val());
        valid = (depName.length > 2);

        if (valid == true) {
            var token = $("#__AjaxAntiForgeryForm input").val();
            $.ajax('/Administration/EditDepartment?idFirm=' + idFirm, {
                type: "POST",
                headers: { '__RequestVerificationToken': token },

                data: JSON.stringify(new ModalDepartmentEdit(arr, depName, idDep)),
                contentType: "application/json",
                dataType: "json",
                error: (xhr: JQueryXHR, ajaxOptions: string, thrownError: string) => {
                    ///TODO: Сделать обработку ошибок
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                },
                success: (resp) => {
                    location.reload();
                }
            })
        }
    });

    $('#myModal_departmentEdit').find('#employeeDropdown').html(html);
    //  $('#myModal_departmentEdit').find('#isChiefDropdown .selectpicker').css('block', '')
    //$('#myModal_departmentEdit').find('#isChiefDropdown').html(html);
    $('#myModal_departmentEdit').modal('show');


    var urlE = '/Administration/_DropdownEmployeeDepartmentPartial?idFirm=' + idFirm + '&idDepartment=' + idDep;
    $.get(urlE, (html: HTMLElement) => {
        var res = $('#myModal_departmentEdit').find('#employeeDropdown');
        $(html).attr('id', 'employeeDropdown');
        console.log($(html));
        res.replaceWith($(html));
        //var res = $('#myModal_departmentEdit').find("option:selected");
        //$('#myModal_departmentEdit').find('#isChiefDropdown .selectpicker').append(res);
        initSelector('.selectpicker');
    });
}
interface IResult {
	result;
}
function isInActiveDirectory(url: string, selector: string) {
	$.ajax({
		url: url,
		success: (resp: IResult) => {
			if (resp.result == true) {
				$(selector).show();
				$(selector + ' button').on("click", () => {
					document.location.href = $(selector + ' button').attr('href');
				});
			} else {
				$(selector).hide();
			}
		}
	});
}