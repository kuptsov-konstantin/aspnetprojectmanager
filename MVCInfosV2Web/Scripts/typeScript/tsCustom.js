function idValid(elementID) {
    var orgNameI = document.getElementById(elementID);
    var orgName = $('#' + elementID).val();
    var name = orgName.replace(/\s+/g, " ");
    if (name.length > 1) {
        setOK(elementID);
        return true;
    }
    else {
        setError(elementID);
        return false;
    }
}
function setError(currentId) {
    var $elem = $("#" + currentId);
    myRemoveClass($elem, 'my_valid_not_error');
    myAddClass($elem, 'my_valid_error');
}
function setOK(currentId) {
    var $elem = $("#" + currentId);
    myRemoveClass($elem, 'my_valid_error');
    myAddClass($elem, 'my_valid_not_error');
}
var AddPage = (function () {
    function AddPage() {
        this._orgObject = new OrgObject();
    }
    AddPage.prototype.getOrgObject = function () {
        return this._orgObject;
    };
    AddPage.prototype.addDepartmentsFromFile = function () {
        this.loadFromFile(0);
    };
    AddPage.prototype.loadFromFile = function (types) {
        var elem;
        if (types == 0) {
            elem = document.getElementById("fileDownloader");
        }
        else {
            if (types == 1) {
                elem = document.getElementById("fileDownloaderEmployees");
            }
        }
        if (elem.files.length > 0) {
            this.readText(elem.files[0], types);
        }
    };
    AddPage.prototype.readText = function (filePath, types) {
        var _this = this;
        // console.log(filePath + ":" + types);
        ///TODO: Добавить проверку на верность загруженного csv для отделов и для пользователей
        var reader = new FileReader();
        reader.onload = function (event) {
            var contents = event.target.result;
            if (types == 0) {
                _this._orgObject.departments.length = 0;
                var lines = contents.match(/[^\n]+/g);
                _this._progressbarForLoading = new MyProgressbar($('#progressValidateDepartment'), $('#myModal'), true);
                _this._progressbarForLoading.maximumWithoutFactor(lines.length);
                for (var i = 0; i < lines.length; i++) {
                    var department = new Department();
                    var res = lines[i].match(/[^;]+/g);
                    department.department.str = res[0];
                    _this.validDepartment(department.department, i);
                    _this._orgObject.departments.push(department);
                }
                _this.printCurrentDepartments(_this._orgObject.departments);
            }
            if (types == 1) {
                _this._orgObject.employees.length = 0;
                var lines = contents.match(/[^\n]+/g);
                _this._progressbarForLoading = new MyProgressbar($('#progressValidateEmoloyee'), $('#myModalEmployeeLoadFromFile'), true);
                _this._progressbarForLoading.factor = 4;
                _this._progressbarForLoading.maximumWithoutFactor(lines.length);
                for (var i = 0; i < lines.length; i++) {
                    var res = lines[i].match(/[^;\n]+/g);
                    var employee1 = new Employee();
                    employee1.id = i;
                    employee1.userName.str = res[0];
                    employee1.email.str = res[1];
                    employee1.pass.str = res[2];
                    employee1.familyName.str = res[3];
                    employee1.name.str = res[4];
                    employee1.middleName.str = res[5];
                    employee1.birthDay.str = res[6];
                    employee1.role.str = res[7];
                    employee1.emploement.str = res[8];
                    _this.validEmployee(employee1, i);
                    _this._orgObject.employees.push(employee1);
                }
                //  console.log(lines);
                // console.log(currentE);
                _this.printCurrentEmployees(_this._orgObject.employees);
            }
            //   console.log("Содержимое файла: " + contents);
        };
        reader.onerror = function (event) {
            console.error("Файл не может быть прочитан! ");
            console.error(event);
        };
        reader.readAsText(filePath);
    };
    AddPage.prototype.addEmployeesFromFile = function () {
        $("#employeeDownloaderButton").prop("disabled", true);
        this.loadFromFile(1);
        $("#employeeDownloaderButton").prop("disabled", false);
    };
    AddPage.prototype.addDepartment = function () {
        var department = $("#department-name").val();
        if (idValid("department-name")) {
            if (findInDepartments(this._orgObject.departments, department)) {
                this._orgObject.departments.push(department);
                $("#department-name").val("");
                this.printCurrentDepartments(this._orgObject.departments);
                setOK("department-name");
            }
            else {
                setError("department-name");
            }
        }
    };
    ///TODO: Select https://silviomoreto.github.io/bootstrap-select/options/
    AddPage.prototype.printCurrentEmployees = function (content) {
        var _this = this;
        var tableRef = document.getElementById('employeeListTable').getElementsByTagName('tbody')[0];
        //console.log(tableRef);
        $('#bodyTableEmpleId').empty();
        for (var i = 0; i < content.length; i++) {
            var newRow = tableRef.insertRow(tableRef.rows.length);
            newRow.setAttribute("id", "rowEmployee" + i);
            if (content[i].isValidate) {
                newRow.className = "my_valid_not_error_table";
            }
            else {
                newRow.className = "my_valid_error_table";
            }
            ////--------------
            var select = document.createElement("SELECT");
            select.setAttribute("id", "mySelect" + "E" + i);
            select.setAttribute("eId", (new String(i)).valueOf());
            // select.setAttribute("multiple", "multiple");
            //   select.className = "selectpicker";
            select.addEventListener("change", function (obj) {
                var elem = obj.target;
                _this._orgObject.employees[elem.getAttribute('eid')].department = elem.value;
                console.log(elem);
            });
            var newCellDepartment = newRow.insertCell(0);
            newCellDepartment.appendChild(select);
            var z = document.createElement("option");
            z.setAttribute("value", "another");
            ///TODO: "Как-нибудь потом (удалить?)"
            var t = document.createTextNode("Как-нибудь потом...");
            z.appendChild(t);
            document.getElementById("mySelect" + "E" + i).appendChild(z);
            for (var y = 0; y < this._orgObject.departments.length; y++) {
                var z1 = document.createElement("option");
                z1.setAttribute("value", new String(y).valueOf());
                var t1 = document.createTextNode(this._orgObject.departments[y].department.str);
                z1.appendChild(t1);
                document.getElementById("mySelect" + "E" + i).appendChild(z1);
            }
            ////--------------
            var newCell2 = newRow.insertCell(0);
            var newText = document.createTextNode(content[i].familyName.str + " " + content[i].name.str + " " + content[i].middleName.str);
            newCell2.appendChild(newText);
            ///TODO: Добавить какие поля с ошибками.
            ///TODO: Добавить редактирование
            var itEdit = document.createElement('i');
            var atEdit = document.createElement('a');
            atEdit.className = 'btn btn-info';
            itEdit.className = 'fa fa-edit';
            atEdit.href = "#";
            atEdit.setAttribute("idEmpl", new String(i).valueOf());
            atEdit.onclick = function (event) {
                var elem = event.target;
                var empl = _this._orgObject.employees[new Number(elem.parentElement.getAttribute("idEmpl")).valueOf()];
                validEdit('employee-family-name', empl.familyName);
                validEdit('employee-name', empl.name);
                validEdit('employee-middle-name', empl.middleName);
                validEdit('employee-birthdate', empl.birthDay);
                validEdit('employee-login', empl.userName);
                validEdit('employee-email', empl.email);
                validEdit('employee-employmentdate', empl.emploement);
                $('#saveEmployeeButton').click(function () {
                    ///TODO: Реализовать проверку изменений и сохранение.
                });
                if (empl.pass.isValidate) {
                    setOK('employee-pass');
                }
                else {
                    setError('employee-pass');
                }
                $('#myModalEmployeeAddNew').modal('show');
                return false;
                //  removeEmployee(i);
            };
            atEdit.appendChild(itEdit);
            var it = document.createElement('i');
            var at = document.createElement('a');
            at.className = 'btn btn-danger';
            it.className = 'fa fa-trash';
            at.href = "#";
            at.onclick = function (i) {
                return function () {
                    this.removeEmployee(i);
                };
            }(i);
            at.appendChild(it);
            var newCell3 = newRow.insertCell(-1);
            newCell3.appendChild(atEdit);
            newCell3.appendChild(at);
            var newCell1 = newRow.insertCell(0);
            var newText1 = document.createTextNode(new String(i + 1).valueOf());
            newCell1.appendChild(newText1);
            $('#wizard').smartWizard('fixHeight');
        }
        //$('.selectpicker').selectpicker({
        //    size: 4
        //});
        $('#wizard').smartWizard('fixHeight');
        // console.log(content);
    };
    AddPage.prototype.printCurrentDepartments = function (content) {
        var tableRef = document.getElementById('departmentListTable').getElementsByTagName('tbody')[0];
        //console.log(tableRef);
        $('#bodyTableId').empty();
        for (var i = 0; i < content.length; i++) {
            var newRow = tableRef.insertRow(tableRef.rows.length);
            newRow.setAttribute("id", "rowDeparment" + i);
            if (content[i].department.isValidate) {
                newRow.className = "my_valid_not_error_table";
            }
            else {
                newRow.className = "my_valid_error_table";
            }
            var newCell2 = newRow.insertCell(0);
            var newText = document.createTextNode(content[i].department.str);
            newCell2.appendChild(newText);
            var it = document.createElement('i');
            var at = document.createElement('a');
            at.className = 'btn btn-danger';
            it.className = 'fa fa-trash';
            at.href = "#";
            at.onclick = function (i) {
                return function () {
                    this.removeDepartment(i);
                };
            }(i);
            at.appendChild(it);
            var newCell3 = newRow.insertCell(-1);
            newCell3.appendChild(at);
            var newCell1 = newRow.insertCell(0);
            var newText1 = document.createTextNode('#');
            newCell1.appendChild(newText1);
            $('#wizard').smartWizard('fixHeight');
        }
        $('#wizard').smartWizard('fixHeight');
        //      console.log(content);
    };
    AddPage.prototype.removeDepartment = function (pos) {
        this._orgObject.departments.splice(pos, 1);
        this.printCurrentDepartments(this._orgObject.departments);
    };
    AddPage.prototype.removeEmployee = function (pos) {
        this._orgObject.employees.splice(pos, 1);
        this.printCurrentEmployees(this._orgObject.employees);
    };
    AddPage.prototype.validStep = function (stepnumber, isStepValid) {
        if (stepnumber == 1) {
            if (!idValid("org-name")) {
                isStepValid = false;
            }
            else {
                this._orgObject.aboutFirm.orgName.str = $("#org-name").val();
                $('#firmNameOnStep2').replaceWith('<td id="firmNameOnStep2">' + this._orgObject.aboutFirm.orgName.str + '</td>');
            }
            if (!idValid("country-name")) {
                isStepValid = false;
            }
            else {
                this._orgObject.aboutFirm.country.str = $("#country-name").val();
            }
            if (!idValid("country-city")) {
                isStepValid = false;
            }
            else {
                this._orgObject.aboutFirm.city.str = $("#country-city").val();
            }
            if (!idValid("country-address")) {
                isStepValid = false;
            }
            else {
                this._orgObject.aboutFirm.address.str = $("#country-address").val();
            }
        }
        if (stepnumber == 2) {
            if (this._orgObject.departments.length == 0) {
                setError("department-name");
                isStepValid = false;
            }
        }
        if (stepnumber == 3) {
            if (this._orgObject.employees.length == 0) {
                //  setError(elementID);
                isStepValid = false;
            }
        }
        return isStepValid;
    };
    AddPage.prototype.validDepartment = function (department, i) {
        if (department.str.length > 1) {
            department.isValidate = true;
            setOKRow('rowDeparment' + i);
        }
        else {
            setErrorRow('rowDeparment' + i);
            department.isValidate = false;
        }
        this._progressbarForLoading.inc();
    };
    AddPage.prototype.validUser = function (eud, obj, currentId, empl) {
        var _this = this;
        var str = '/NewFirm/ValidUserName?validate=' + eud + '&str=' + obj.str;
        $.get(str, function (html) {
            //   console.log(_progressValidEmployee.counterVaidEmpl + " >> " + currentId + " > " + html[0]);
            _this._progressbarForLoading.inc();
            obj.isValidate = html[0];
            if (empl.isValidate) {
                setOKRow(currentId);
            }
            else {
                setErrorRow(currentId);
            }
        });
    };
    AddPage.prototype.validEmployee = function (empl, i) {
        if (empl.pass.str.length > 6) {
            empl.pass.isValidate = true;
        }
        if ((empl.familyName.str.length > 0) && (empl.name.str.length > 0) && (empl.middleName.str.length > 0)) {
            empl.familyName.isValidate = true;
            empl.name.isValidate = true;
            empl.middleName.isValidate = true;
        }
        if (empl.role.str.length > 0) {
            empl.role.isValidate = true;
        }
        this.validUser(0, empl.userName, "rowEmployee" + i, empl);
        this.validUser(1, empl.email, "rowEmployee" + i, empl);
        this.validUser(2, empl.birthDay, "rowEmployee" + i, empl);
        if (new Date(empl.birthDay.str.replace(/(\d{2})\.(\d{2})\.(\d{4})/, '$3-$2-$1')) < new Date(empl.emploement.str.replace(/(\d{2})\.(\d{2})\.(\d{4})/, '$3-$2-$1'))) {
            this.validUser(3, empl.emploement, "rowEmployee" + i, empl);
        }
        else {
            empl.emploement.isValidate = false;
            setErrorRow("rowEmployee" + i);
            this._progressbarForLoading.inc();
        }
    };
    AddPage.prototype.saveValidEdit = function (currentId, element) {
        ///TODO: Сохранение и валидация
    };
    AddPage.prototype.saveToDatabase = function (urlStepOne, urlStepTwo, urlStepThree, urlStepFour) {
        this._saveRegister = new SaveRegister(this._orgObject, urlStepOne, urlStepTwo, urlStepThree, urlStepFour);
    };
    return AddPage;
}());
var SaveRegister = (function () {
    function SaveRegister(orgObject, urlStepOne, urlStepTwo, urlStepThree, urlStepFour) {
        this.counter = 0;
        this._urlStepFour = urlStepFour;
        this._orgObject = orgObject;
        this._urlStepOne = urlStepOne;
        this._urlStepTwo = urlStepTwo;
        this._urlStepThree = urlStepThree;
        var modalWindowForSave = $('#myModalSave');
        modalWindowForSave.modal({ backdrop: 'static', show: true, keyboard: false });
        modalWindowForSave.on('hidden.bs.modal', function (e) {
            console.log(e);
        });
        modalWindowForSave.on('hide.bs.modal', function (e) {
            console.log(e);
        });
        this._progressbarForLoading = new MyProgressbar($('#progressRegister'), modalWindowForSave, false);
        this._progressbarForLoading.settingStatusMessage([
            { message: "Регистрация организации", start: 0, stop: 1, id: Guid.newGuid() },
            { message: "Добавление отделов", start: 1, stop: this._orgObject.departments.length, id: Guid.newGuid() },
            { message: "Добавление сотрудников", start: this._orgObject.departments.length + 1, stop: this._orgObject.departments.length + 1 + this._orgObject.employees.length * 2, id: Guid.newGuid() }
        ]);
        this._progressbarForLoading.maximumWithoutFactor(this._orgObject.employees.length * 2 + 1 + this._orgObject.departments.length);
        /* window.setTimeout(() => {
             this._progressbarForLoading.inc();
         }, 1000);*/
        this.saveToDatabase();
    }
    SaveRegister.prototype.saveToDatabase = function () {
        /// TODO: Может сделать подтверждение сохранения?
        this.saveStepOne();
    };
    SaveRegister.prototype.saveStepOne = function () {
        var _this = this;
        var token = $('[name="__RequestVerificationToken"]').val();
        this._orgObject.aboutFirm.__RequestVerificationToken = token;
        $.ajax({
            headers: {
                "__RequestVerificationToken": token
            },
            type: "POST",
            url: this._urlStepOne,
            data: JSON.stringify(this._orgObject.aboutFirm),
            contentType: "application/json",
            dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                ///TODO: Сделать обработку ошибок
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            },
            success: function (msg) {
                console.log(msg);
                console.log('Success');
                _this._progressbarForLoading.inc();
                _this.saveStepTwo(msg.firmId);
            }
        });
    };
    SaveRegister.prototype.saveStepTwo = function (firmId) {
        var _this = this;
        var counter = 0;
        for (var i = 0; i < this._orgObject.departments.length; i++) {
            $.ajax({
                type: "POST",
                url: this._urlStepTwo + '?firmId=' + firmId,
                data: JSON.stringify(this._orgObject.departments[i]),
                contentType: "application/json",
                dataType: "json",
                error: function (xhr, ajaxOptions, thrownError) {
                    ///TODO: Сделать обработку ошибок
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                },
                success: function (msg) {
                    counter++;
                    console.log(counter + ' ' + 'Success');
                    console.log(msg);
                    console.log(counter + ' ' + _this._orgObject.departments.length);
                    _this._progressbarForLoading.inc();
                    if (_this._orgObject.departments.length == counter) {
                        console.log('стартует третий');
                        _this.saveStepThree(msg.firmId);
                    }
                    //  this.saveStepThree(msg.firmId);
                }
            });
        }
    };
    SaveRegister.prototype.saveStepThree = function (firmId) {
        var _this = this;
        this.counter = 0;
        for (var i = 0; i < this._orgObject.employees.length; i++) {
            $.ajax({
                type: "POST",
                url: this._urlStepThree + '?firmId=' + firmId,
                data: JSON.stringify(this._orgObject.employees[i]),
                contentType: "application/json",
                dataType: "json",
                error: function (xhr, ajaxOptions, thrownError) {
                    ///TODO: Сделать обработку ошибок
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                    console.log(thrownError);
                },
                success: function (msg) {
                    _this._progressbarForLoading.inc();
                    console.log(msg);
                    var first = _this._orgObject.employees.filter(function (e) { return e.email.str == msg.email; })[0];
                    _this.saveStepFour(firmId, msg.userId, first);
                }
            });
        }
    };
    SaveRegister.prototype.saveStepFour = function (firmId, userId, employ) {
        var _this = this;
        $.ajax({
            type: "POST",
            url: this._urlStepFour + '?firmId=' + firmId + '&departments=' + JSON.stringify(this._orgObject.departments) + '&userId=' + userId,
            data: JSON.stringify(employ),
            contentType: "application/json",
            dataType: "json",
            error: function (xhr, ajaxOptions, thrownError) {
                ///TODO: Сделать обработку ошибок
                console.log(xhr.status);
                console.log(xhr.responseText);
                console.log(thrownError);
            },
            success: function (msg) {
                _this._progressbarForLoading.inc();
                _this.counter++;
                console.log(_this.counter + ' ' + 'Success');
                console.log(msg);
                console.log(_this.counter + ' ' + _this._orgObject.employees.length);
                if (_this.counter == _this._orgObject.employees.length) {
                    $("#finishButoonOnSave").prop("disabled", false);
                }
            }
        });
    };
    return SaveRegister;
}());
function findInDepartments(arr, str) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].department.str == str) {
            return false;
        }
    }
    return true;
}
function setErrorRow(currentId) {
    var $currentId = $('#' + currentId);
    myAddClass($currentId, 'my_valid_error_table');
    myRemoveClass($currentId, 'my_valid_not_error_table');
}
function myAddClass(currentElement, newClass) {
    if (!currentElement.hasClass(newClass)) {
        currentElement.addClass(newClass);
    }
}
function myRemoveClass(currentElement, removeClass) {
    if (currentElement.hasClass(removeClass)) {
        currentElement.removeClass(removeClass);
    }
}
function setOKRow(currentId) {
    var $currentId = $('#' + currentId);
    myAddClass($currentId, 'my_valid_not_error_table');
    myRemoveClass($currentId, 'my_valid_error_table');
}
function validEdit(currentId, element) {
    var $elem = $("#" + currentId);
    $elem.val(element.str);
    if (element.isValidate) {
        setOK(currentId);
    }
    else {
        setError(currentId);
    }
}
function validateAllSteps() {
    var isStepValid = true;
    return isStepValid;
}
//# sourceMappingURL=tsCustom.js.map