﻿class ProjectUserInfo {
	Id: string;
	Fio: string;
	isChanged: boolean = false;
}

class ProjectStage {
	idCurrent: string;
	stageName: string;
	hasChildren: boolean;
	levelId: number;
	intersectionDate: boolean;
	idParent: string;
	startDate: Date;
	stopDate: Date;
}

class ProjectInfo {
	Id: number;
	ProjectName: string;
	ProjectCode: string;
	ProjectStartDate: string;
	ProjectEndDate: string;
	ProjectStartDateAsDate: Date;
	ProjectEndDateAsDate: Date;
	TeamLead: ProjectUserInfo;
}

class LevelType {
	id: number;
	level: number;
	type: string;
}
interface IMainInfo {
	employees?: ProjectUserInfo[]
	name: string;
	startDate?: Date;
	endDate?: Date;
	id: any;
}
interface IProjectStages extends IMainInfo {
	isChanged?: boolean;
	isRoot?: boolean;
	id: Guid;
	realId?: number;
	children?: Array<IProjectStages>;
	parent?: IProjectStages;
	intersectionDate?: boolean;
	levelId?: number;
}



class ProjectStages implements IProjectStages {
	isChanged?: boolean = false;
	isRoot?: boolean = false;
	id: Guid;
	realId?: number;
	children?: Array<ProjectStages>;
	parent?: ProjectStages;
	name: string = "";
	startDate?: Date;
	endDate?: Date;
	intersectionDate?: boolean;
	levelId?: number;

	constructor() {
		this.children = new Array<ProjectStages>()
		this.id = Guid.newGuid();
	}

	add?(stage: ProjectStages) {
		var statusAdd = false;
		if ((this.startDate.valueOf() <= stage.startDate.valueOf()) && (stage.endDate.valueOf() <= this.endDate.valueOf())) {
			if (this.intersectionDate == true) {
				if (this.children.length > 0) {
					if ((this.startDate.valueOf() <= stage.startDate.valueOf()) && (stage.endDate.valueOf() <= this.children[0].startDate.valueOf())) {
						stage.parent = this;
						this.children.push(stage);
						statusAdd = true;

					} else {
						if ((this.children[this.children.length - 1].endDate.valueOf() <= stage.startDate.valueOf()) && (stage.endDate.valueOf() <= this.endDate.valueOf())) {
							stage.parent = this;
							this.children.push(stage);
							statusAdd = true;
						} else {
							for (var i = 0; i < this.children.length - 1; i++) {
								if ((this.children[i].endDate.valueOf() <= stage.startDate.valueOf()) && (stage.endDate.valueOf() <= this.children[i + 1].startDate.valueOf())) {
									stage.parent = this;
									this.children.push(stage);
									statusAdd = true;
								}
							}
						}
					}
				} else {
					stage.parent = this;
					statusAdd = true;
					this.children.push(stage);
				}
			} else {
				stage.parent = this;
				statusAdd = true;
				this.children.push(stage);
			}
			var newArray = this.children.sort((a, b) => {
				if (a.endDate < b.startDate) {
					return -1;
				}
				if (a.endDate == b.startDate) {
					return 0;
				}
				if (a.endDate > b.startDate) {
					return 1;
				}
			});
			return statusAdd;
		}
		return false;
	}
}

function getFreeStage(localStages: Array<ProjectStages>, parent: ProjectStages, projectInfo: ProjectInfo) {
	var maxEndDate: Date = null;
	var endDate: Date = null;
	for (var i = 0; i < localStages.length; i++) {
		if (maxEndDate == null) {
			maxEndDate = localStages[i].endDate;
		} else {
			if (maxEndDate.valueOf() < localStages[i].endDate.valueOf()) {
				maxEndDate = localStages[i].endDate;
			}
		}
	}

	if (parent == null) {
		if (maxEndDate == null) {
			maxEndDate = projectInfo.ProjectStartDateAsDate;
		}
		endDate = projectInfo.ProjectEndDateAsDate;
	} else {
		if (maxEndDate == null) {
			maxEndDate = parent.startDate;
		}
		endDate = parent.endDate;
	}

	if (maxEndDate.valueOf() == endDate.valueOf()) {
		return null;
	} else {
		return { startDate: maxEndDate, endDate };
	}
}

function initTree(elemClass: string) {
	$('.' + elemClass).treegrid({
		expanderExpandedClass: 'glyphicon glyphicon-minus',
		expanderCollapsedClass: 'glyphicon glyphicon-plus'
	});
}
enum InsertType {
	Add,
	Replace
}


class ProjectManage {
	projectInfoDeferred = $.Deferred();
	loadUserDeferred = $.Deferred();
	localStages: ProjectStages;
	projectInfo: ProjectInfo;
	selectedList: HTMLDivElement;
	idFirm: number;
	idProject: number;
	levelsElementId: string;
	stageElementId: string;
	employeeElementId: string;
	layers: Array<LevelType>;
	employees: ProjectUserInfo[];
	setIds(_idFirm: number, _idProject: number) {
		this.idFirm = _idFirm;
		this.idProject = _idProject;
	}

	constructor() {
		this.localStages = new ProjectStages();
		this.layers = new Array<LevelType>();
	}

	findInTree(node: ProjectStages, findingId: string): ProjectStages {
		if (node.id.id == findingId) {
			return node;
		}
		if ((node.children !== undefined) && (node.children !== null)) {
			for (var i = 0; i < node.children.length; i++) {
				var result = this.findInTree(node.children[i], findingId);
				if (result !== null) {
					return result;
				}
			}
		}
		return null;
	}


	initEditStages(idElem: string) {
		this.stageElementId = idElem;
		$('#' + idElem + 'Tree tbody').empty();
		$('#' + idElem + 'Tree tbody').append('<tr id="' + idElem + '"><td><i class="fa fa-spinner fa-spin fa-3x"></i></td><td> Загрузка...</td><td></td><td></td><td></td></tr>');
		var url = '_GetStagesEdit?idFirm=' + this.idFirm + '&idProject=' + this.idProject;
		$.ajax({
			url: url,
			success: (json: ProjectStage[]) => {
				json = JSON.parse(JSON.stringify(json), parceDate);
				console.log(json);
				if (json.length == 0) {
					$('#' + idElem + 'Tree tbody').empty();
					$('#' + idElem + 'Tree tbody').append('<tr><td></td><td></td><td></td><td>Пусто</td><td></td><td></td><tr>');
				} else {
					json.map((value, index, array) => {
						if (value.idParent == Guid.empty) {
							return value;
						}
					}).forEach((value, index, array) => {
						if (value !== undefined) {
							this.localStages.children.push({
								id: Guid.setId(value.idCurrent),
								endDate: value.stopDate,
								startDate: value.startDate,
								levelId: value.levelId,
								intersectionDate: value.intersectionDate,
								name: value.stageName
							});
						}
					});
					json = json.filter((value, index, array) => {
						if (value.idParent !== Guid.empty) {
							return value;
						}
					});

					var counter = 0;
					var lastLength = json.length;
					while (json.length > 0) {
						lastLength = json.length;
						var added = new Array<string>();
						json = json.filter((value, index, array) => {
							var parent = this.findInTree(this.localStages, value.idParent);
							if (parent !== null) {
								if (parent.children === undefined) {
									parent.children = new Array<ProjectStages>()
								}
								added.push(value.idCurrent)
								parent.children.push({
									id: Guid.setId(value.idCurrent),
									endDate: value.stopDate,
									startDate: value.startDate,
									levelId: value.levelId,
									intersectionDate: value.intersectionDate,
									name: value.stageName,
									parent: parent
								});
							} else {
								return value;
							}
						});
						if (lastLength == json.length) {
							counter++;
							if (counter == 50) {
								break;
							}
						} else {
							counter = 0;
						}
					}
					this.updateStages(this.localStages.children);
				}
			}
		});
		$('#addStageId').on("click", (e) => {
			var range = getFreeStage(this.localStages.children, null, this.projectInfo);
			if (range == null) {
				return;
			}
			$('#addStageId').attr('disabled', 'disabled');
			var newStage = new ProjectStages();
			newStage.levelId = this.layers[0].id;
			newStage.startDate = range.startDate;
			newStage.endDate = range.endDate;
			this.insertNewStage(newStage);
		});
		$('#saveStagesId').on("click", (e) => {
			var res = this.normaliseStagesToSaveInit();
			var token = $("#__AjaxAntiForgeryForm input").val();
			var params = 'idFirm=' + this.idFirm + '&idProject=' + this.idProject + '&treeStage=' + JSON.stringify(res);
			var url = '_SaveNewStage';
			$.ajax({
				type: 'POST',
				headers: { '__RequestVerificationToken': token },
				url: url,
				data: {
					'parametrs': btoa(escape(params))
				},

				error: (xhr, ajaxOptions, thrownError) => {
					console.log(xhr);
				},
				success: (result) => {
					location.reload();
				}
			});

		});
		initTree('treeStages');
	}

	normaliseStagesToSaveInit() {
		var localStorageForSave = new Array<ProjectStages>();
		this.normaliseStagesToSave(this.localStages.children, localStorageForSave);
		return localStorageForSave;
	}

	normaliseStagesToSave(stages: ProjectStages[], stagesForSave: ProjectStages[]) {
		for (var i = 0; i < stages.length; i++) {
			var children = new Array<ProjectStages>();
			this.normaliseStagesToSave(stages[i].children, children)
			stagesForSave.push({
				intersectionDate: stages[i].intersectionDate,
				endDate: stages[i].endDate,
				id: stages[i].id,
				name: stages[i].name,
				startDate: stages[i].startDate,
				levelId: stages[i].levelId,
				isChanged: stages[i].isChanged,
				children: children
			});
		}
	}

	initEditEmployee(idElem: string) {
		//$('#' + idElem).replaceWith('<div class="x_content" id="' + idElem + '"><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div>');
		this.employeeElementId = idElem;
		$("#emEdit .x_title .btn-group button").on("click", (e) => {
			updateRadioButtons($(e.currentTarget));
			this.loadEditEmployee(ConvertStringToBoolean($(e.currentTarget).attr("link")));
		});
		this.loadEditEmployee(true);
	}

	setCurrentInButtonsGroup(state: boolean) {
		if (state == true) {
			updateRadioButtons($("#emEdit .x_title .btn-group button[link='True']"));
		}
		if (state == null) {
			updateRadioButtons($("#emEdit .x_title .btn-group button[link='null']"));
		}
		if (state == false) {
			updateRadioButtons($("#emEdit .x_title .btn-group button[link='False']"));
		}
	}

	loadEditEmployee(isCurrent: boolean) {
		this.setCurrentInButtonsGroup(isCurrent);
		$('#' + this.employeeElementId + 'Tree tbody').empty();
		$('#' + this.employeeElementId + 'Tree tbody').append('<tr id="' + this.employeeElementId + '"><td><i class="fa fa-spinner fa-spin fa-3x"></i></td><td> Загрузка...</td><td></td></tr>');
		var url = '_GetEmployeesEdit?idFirm=' + this.idFirm + '&idProject=' + this.idProject + '&isCurrent=' + isCurrent;
		$.ajax({
			url: url,
			type: 'GET',
			error: (xhr, ajaxOptions, thrownError) => {
				console.log(xhr);
			},
			success: (json: ProjectUserInfo[]) => {
				if (json.length == 0) {
					$('#' + this.employeeElementId + 'Tree tbody').empty();
					$('#' + this.employeeElementId + 'Tree tbody').append('<tr><td></td><td>' + "Пусто" + '</td><td></td></tr >');
				} else {
					this.employees = json.map((value, index, array) => {
						var empl = new ProjectUserInfo();
						empl.Fio = value.Fio;
						empl.Id = value.Id;
						return empl;
					});
					$('#emEdit  #saveEmployeesId').on("click", (e) => {
						this.saveEmployee();
					});
					if (isCurrent == true) {
						$('#emEdit .selectpickerDiv').html('<div class="selectpicker"> </div>');
						$.when(this.loadUserDeferred).done(() => {
							$('#emEdit .selectpicker').replaceWith($(this.selectedList).html());//'.selectpicker'
							$('#emEdit #emEditAddButton').prop("hidden", false);
							initSelector('#emEdit .selectpicker');
							$('#emEdit .selectpicker').on('changed.bs.select', (e) => {
								console.log(e);
								$('#emEdit #emEditAddButton').prop("disabled", false);
							});
							$('#emEdit #emEditAddButton').on("click", (e) => {
								var selectedId = $('#emEdit .selectpicker').val();
								if ((selectedId === undefined) && (selectedId === null)) {
									return;
								}
								var selectedValue = $('#emEdit .selectpicker').find('option[value="' + selectedId + '"]').prop("label");
								var userInf = new ProjectUserInfo();
								userInf.isChanged = true;
								userInf.Id = selectedId;
								userInf.Fio = selectedValue;
								this.employees.push(userInf);
								$('#emEdit #emEditAddButton').prop("disabled", true);
								$('#emEdit  #saveEmployeesId').prop("disabled", false);
								this.updateEditEmplyee(isCurrent);
							});
							$('#emEdit  .field-validation-valid').remove();
						});
					} else {
						$('#emEdit .selectpickerDiv').html('<div class="selectpicker1"> </div>');
					}
					$.when(this.projectInfoDeferred).done(() => {
						this.updateEditEmplyee(isCurrent);
					});
				}
				console.log(json);
			}
		});
	}
	saveEmployee() {
		var token = $("#__AjaxAntiForgeryForm input").val();
		var url = '_SaveEmployeeEdit?idFirm=' + this.idFirm + '&idProject=' + this.idProject;
		$.ajax({
			url: url,
			type: 'POST',
			headers: { '__RequestVerificationToken': token },
			error: (xhr, ajaxOptions, thrownError) => {
				console.log(xhr.status);
			},
			data: {
				'data': btoa(escape(JSON.stringify(this.employees)))
			},
			success: (result) => {
				console.log();
				$('#emEdit  #saveEmployeesId').prop("disabled", true);
			}
		});

	}
	updateEditEmplyee(isCurrent: boolean) {
		if ((this.projectInfo === undefined) || (this.employees === undefined)) {
			return;
		}
		$('#' + this.employeeElementId + 'Tree tbody').empty();
		this.employees.forEach((value, index, array) => {
			var tr = '';
			$('#emEdit .selectpicker option[value="' + value.Id + '"]').prop("disabled", true);
			$('#emEdit .selectpicker').selectpicker('refresh');
			var delButton = '';
			if (isCurrent == true) {
				delButton = '<button class="btn btn-danger btn-xs" id="delEmoloyee' + value.Id + '"><i class="fa fa-trash"></i></button>';
			}
			tr = '<tr id="emplueeId' + value.Id + '"><td>' + value.Fio + '</td><td></td><td>' + delButton + '</td></tr>';
			$('#' + this.employeeElementId + 'Tree tbody').append(tr);
			if ((this.projectInfo.TeamLead.Id == value.Id) && ((isCurrent == true))) {
				$('#delEmoloyee' + value.Id).prop("disabled", true);
				$('#emplueeId' + value.Id).addClass("info");
			}


			$('#delEmoloyee' + value.Id).on("click", (e) => {
				$('#emEdit .selectpicker option[value="' + value.Id + '"]').prop("disabled", false);
				var finding = this.employees.findIndex((localValue) => {
					return (localValue.Id == value.Id);
				});
				this.employees.splice(finding, 1);

				$('#emEdit  #saveEmployeesId').prop("disabled", false);
				this.updateEditEmplyee(isCurrent);

			});
		});
	}

	initEditProject(idElem: string) {
		$('#' + idElem).replaceWith('<div class="x_content" id="' + idElem + '"><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div>');
		$.ajax({
			type: 'GET',
			url: '_ProjectInfoBasicEditorPartial?idFirm=' + this.idFirm + '&idProject=' + this.idProject,
			error: (xhr, ajaxOptions, thrownError) => {
				console.log(xhr.status);
			},
			success: (html: HTMLDivElement) => {
				$('#' + idElem).replaceWith(html);
				this.projectInfo = getProjectInfo();
				this.projectInfo.ProjectStartDateAsDate = new Date(parseInt(this.projectInfo.ProjectStartDate.replace("/Date(", "").replace(")/", ""), 10));
				this.projectInfo.ProjectEndDateAsDate = new Date(parseInt(this.projectInfo.ProjectEndDate.replace("/Date(", "").replace(")/", ""), 10));
				this.localStages.startDate = this.projectInfo.ProjectStartDateAsDate;
				this.localStages.endDate = this.projectInfo.ProjectEndDateAsDate;
				///TODO: Сделать настройку пересечения дат
				this.localStages.intersectionDate = true;
				this.localStages.isRoot = true;
				var finded = $('#' + idElem).find('.input-edit-hover');
				for (var i = 0; i < finded.length; i++) {
					var myDiv = $('<div class="editor-buttons"><button class="btn btn-xs" onclick="projectManager.showEditor(\'' + $(finded[i]).attr('edit') + '\')"><i class="fa fa-edit"></i></button></div>');
					var text = $(finded[i]).attr("value");
					$(finded[i]).text(text);
					$(finded[i]).parent().parent().append(myDiv);
				}
				this.projectInfoDeferred.resolve(this.projectInfo);
			}
		});
	}

	initEditLayers(idElem: string) {
		this.levelsElementId = idElem;
		$('#' + idElem).replaceWith('<tr id="' + idElem + '"><td></td><td><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</td><td></td></tr>');
		this.updateLayers();
		$('#saveLayersId').on('click', (e) => {
			console.log(e);
			this.sendLayers();
		});
	}

	updateLayers() {
		$.ajax({
			type: 'GET',
			url: '_GetLayers?idFirm=' + this.idFirm + '&idProject=' + this.idProject,
			success: (json: Array<LevelType>) => {
				if (json.length == 0) {
					$('#' + this.levelsElementId).replaceWith('<tr id="' + this.levelsElementId + '"><td></td><td>' + "Пусто" + '</td></tr >');
				} else {
					this.layers = new Array<LevelType>();
					json.sort((a, b) => {
						if (a.level < b.level) {
							return -1;
						}
						if (a.level > b.level) {
							return 1;
						}
						if (a.level == b.level) {
							return 0;
						}
					});
					for (var i = 0; i < json.length; i++) {
						this.layers.push(json[i]);
					}
					this.printLayers();
				}

			}
		});
	}

	printLayers() {
		$('#' + this.levelsElementId + 'Body').html("");
		for (var i = 0; i < this.layers.length; i++) {

			var buttonUP = "", buttonDOWN = "";

			if (i == 0) {
				buttonUP = '<button class="btn btn-xs btn-success" onclick="projectManager.upLevel(' + i + ')"><i class="fa fa-chevron-down"></i></button>';
				buttonDOWN = '<button class="btn btn-xs btn-success" onclick="projectManager.downLevel(' + i + ')" disabled><i class="fa fa-chevron-up"></i></button>';
			} else {
				if (i == this.layers.length - 1) {
					buttonUP = '<button class="btn btn-xs btn-success" onclick="projectManager.upLevel(' + i + ')" disabled><i class="fa fa-chevron-down"></i></button>';
					buttonDOWN = '<button class="btn btn-xs btn-success" onclick="projectManager.downLevel(' + i + ')"><i class="fa fa-chevron-up"></i></button>';
				} else {
					if (this.layers.length == 1) {
						buttonUP = '<button class="btn btn-xs btn-success" onclick="projectManager.upLevel(' + i + ')" disabled><i class="fa fa-chevron-down"></i></button>';
						buttonDOWN = '<button class="btn btn-xs btn-success" onclick="projectManager.downLevel(' + i + ')" disabled><i class="fa fa-chevron-up"></i></button>';
					} else {
						if ((0 < i) && (i < this.layers.length)) {
							buttonUP = '<button class="btn btn-xs btn-success" onclick="projectManager.upLevel(' + i + ')"><i class="fa fa-chevron-down"></i></button>';
							buttonDOWN = '<button class="btn btn-xs btn-success" onclick="projectManager.downLevel(' + i + ')"><i class="fa fa-chevron-up"></i></button>';
						}
					}
				}
			}
			$('#' + this.levelsElementId + 'Body').append('<tr><td>' + i + '</td><td>' + this.layers[i].type + '</td><td>' +
				'<div class="btn-group-vertical" role="group">'
				+ buttonDOWN + buttonUP +
				'</div>' +
				'<button class="btn btn-danger" style="margin-left:6px;" onclick="projectManager.delLevel(' + i + ')"><i class="fa fa-trash"></i></button>' +
				'</td></tr >');
		}
	}

	upLevel(id: number) {
		var nextElem = this.layers[id + 1];
		this.layers[id + 1] = this.layers[id];
		this.layers[id] = nextElem;
		this.printLayers();
		$('#saveLayersId').prop('disabled', false);
	}

	downLevel(id: number) {
		var nextElem = this.layers[id - 1];
		this.layers[id - 1] = this.layers[id];
		this.layers[id] = nextElem;
		this.printLayers();
		$('#saveLayersId').prop("disabled", false);
	}

	delLevel(id: number) {
		this.layers.splice(id, 1);
		this.printLayers();
		$('#saveLayersId').prop("disabled", false);
	}

	sendLayers() {
		var token = $("#__AjaxAntiForgeryForm input").val();
		var params = 'idFirm=' + this.idFirm + '&idProject=' + this.idProject + '&layersJson=' + JSON.stringify(this.layers);
		var url = '_SetLayer'/*?parametrs=' + btoa(escape(params))*/;
		$.ajax({
			url: url,
			type: 'POST',
			data: { 'parametrs': btoa(escape(params)) },
			headers: { '__RequestVerificationToken': token },
			error: (xhr, ajaxOptions, thrownError) => {
				console.log(xhr);
			},
			success: (json) => {
				$('#saveLayersId').prop("disabled", true);
				location.reload();
			}
		});
	}

	sendLayerName(name: string, position: number) {
		var token = $("#__AjaxAntiForgeryForm input").val();
		var params = 'idFirm=' + this.idFirm + '&idProject=' + this.idProject + '&layerName=' + name + '&layerPosition=' + position;
		var url = '_SetLayer'/*?parametrs=' + btoa(escape(params))*/;
		$.ajax({
			url: url,
			data: { 'parametrs': btoa(escape(params)) },
			type: 'POST',
			headers: { '__RequestVerificationToken': token },
			success: (json) => {
				console.log(json);
			}
		});
	}

	addLayer() {
		var addL = $("#addLayerId");
		var str = <string>addL.val();
		if (str.length > 1) {
			setOK(['#addLayerId']);
			var findinCopy = this.layers.filter((e) => { return e.type == str });
			if (findinCopy.length == 0) {
				var level = new LevelType();
				level.type = str;
				this.layers.push(level);
				$("#addLayerId").val("");
				this.printLayers();
				$('#saveLayersId').prop("disabled", false);
			} else {
				setError(['#addLayerId']);
			}
		} else {
			setError(['#addLayerId']);
		}
	}

	insertNewStage(element: ProjectStages) {
		var findingElement = $('#tr' + element.id.id);
		var trTable: HTMLTableRowElement;
		var findingInPage = false;
		if (findingElement.length == 0) {
			trTable = document.createElement("tr");
			if (element.parent == null) {
				$('#' + this.stageElementId + 'Tree' + 'Body').append(trTable);
			} else {
				///TODO: Сортировка по датам 
				if ($('.treegrid-parent-' + element.parent.id.id + ':last').length == 0) {
					$(trTable).insertAfter('#tr' + element.parent.id.id);
				} else {
					$(trTable).insertAfter('.treegrid-parent-' + element.parent.id.id + ':last');
				}
			}
		} else {
			findingInPage = true;
			trTable = <HTMLTableRowElement>document.getElementById('tr' + element.id.id);
			$(trTable).empty();
		}

		if (element.parent == null) {
			trTable.className = 'treegrid-' + element.id.id;
		} else {
			trTable.className = 'treegrid-' + element.id.id + ' treegrid-parent-' + element.parent.id.id;
		}
		trTable.id = 'newStage-' + Guid.newGuid().id;

		var tdHash = document.createElement("td");
		tdHash.innerText = "#";
		trTable.appendChild(tdHash);

		var divInputName = document.createElement("div");
		divInputName.className = 'input-group col-xs-8 col-md-10';
		var inputName = document.createElement("input");
		inputName.id = 'stageName' + element.id.id;
		inputName.type = 'text';
		inputName.value = element.name;
		inputName.onchange = (ev: Event) => {
			console.log(ev);
		}
		inputName.className = 'form-control';
		divInputName.appendChild(inputName);
		var tdInputName = document.createElement("td");
		tdInputName.appendChild(divInputName);
		trTable.appendChild(tdInputName);

		var divStartDate = document.createElement("div");
		divStartDate.id = 'datetimepicker';
		divStartDate.className = 'input-group date col-xs-8 col-md-10';
		var inputStartDate = document.createElement("input");
		inputStartDate.className = 'form-control';
		inputStartDate.id = 'stageStartDate' + element.id.id;
		inputStartDate.type = 'text';
		inputStartDate.onchange = (ev: Event) => {
			console.log(ev);
		}
		inputStartDate.value = element.startDate.getDate() + '.' + (element.startDate.getMonth() + 1) + '.' + element.startDate.getFullYear();
		divStartDate.appendChild(inputStartDate);
		var spanGroupStartDate = document.createElement("span");
		spanGroupStartDate.className = "input-group-addon";
		var spanGlyphStartDate = document.createElement("span");
		spanGlyphStartDate.className = "glyphicon glyphicon-calendar";
		spanGroupStartDate.appendChild(spanGlyphStartDate);
		divStartDate.appendChild(spanGroupStartDate);
		var tdStartDate = document.createElement("td");
		tdStartDate.appendChild(divStartDate);
		trTable.appendChild(tdStartDate);

		$(divStartDate).on("dp.change", (e) => {
			console.log(e);
		})


		var divEndDate = document.createElement("div");
		divEndDate.id = 'datetimepicker';
		divEndDate.className = 'input-group date col-xs-8 col-md-10';
		var inputEndDate = document.createElement("input");
		inputEndDate.value = element.endDate.getDate() + '.' + (element.endDate.getMonth() + 1) + '.' + element.endDate.getFullYear();
		inputEndDate.className = 'form-control';
		inputEndDate.id = 'stageEndDate' + element.id.id;
		inputEndDate.type = 'text';
		divEndDate.appendChild(inputEndDate);
		inputEndDate.onchange = (ev: Event) => {
			console.log(ev);
		}
		var spanGroupEndDate = document.createElement("span");
		spanGroupEndDate.className = "input-group-addon";
		var spanGlyphEndDate = document.createElement("span");
		spanGlyphEndDate.className = "glyphicon glyphicon-calendar";
		spanGroupEndDate.appendChild(spanGlyphEndDate);
		divEndDate.appendChild(spanGroupEndDate);
		var tdEndDate = document.createElement("td");
		tdEndDate.appendChild(divEndDate);
		trTable.appendChild(tdEndDate);
		$(divEndDate).on("dp.change", (e) => {
			console.log(e);
		})

		var tdCheckbox = document.createElement("td");
		var divCheckbox = document.createElement("div");
		var labelCheckbox = document.createElement("label");
		var inputCheckbox = document.createElement("input");
		divCheckbox.className = "checkbox";
		inputCheckbox.type = "checkbox";
		inputCheckbox.className = "flat";
		inputCheckbox.checked = element.intersectionDate;
		inputCheckbox.id = 'intersectionDate' + element.id.id;
		labelCheckbox.appendChild(inputCheckbox);
		divCheckbox.appendChild(labelCheckbox);
		tdCheckbox.appendChild(divCheckbox);
		trTable.appendChild(tdCheckbox);



		var divButton = document.createElement("div");
		divButton.className = 'input-group col-xs-6';
		var buttonSave = document.createElement("button");
		buttonSave.className = 'btn btn-xs';
		buttonSave.onclick = (event: MouseEvent) => {
			if (findingInPage) {
				if (this.saveStage(InsertType.Replace, element)) {
					element.isChanged = true;
					$('#addStageId').prop("disabled", false);
					this.updateStages(this.localStages.children);
				}
			} else {
				if (this.saveStage(InsertType.Add, element)) {
					element.isChanged = true;
					$('#addStageId').prop("disabled", false);
					this.updateStages(this.localStages.children);
				}
			}
		};
		buttonSave.innerHTML = '<i class="fa fa-floppy-o"></i>';

		var buttonClose = document.createElement("button");
		buttonClose.className = 'btn btn-danger btn-xs';
		buttonClose.onclick = (event: MouseEvent) => {
			$('#addSubStage' + element.id.id).prop("disabled", false);
			$('#addStageId').prop("disabled", false);
			this.updateStages(this.localStages.children);
			// this.closeNewStage(trTable.id);
		};
		buttonClose.innerHTML = '<i class="fa fa-close"></i>';


		var tdButton = document.createElement("td");
		divButton.appendChild(buttonSave);
		divButton.appendChild(buttonClose);
		tdButton.appendChild(divButton);
		trTable.appendChild(tdButton);

		initDatePicker();
		initICheck();
		initTree('treeStages');
	}

	closeNewStage(idElement: string) {
		$('#' + idElement).remove();
		$('#addStageId').prop("disabled", false);
	}

	saveStage(insertType: InsertType, element: ProjectStages) {
		var name = $('#stageName' + element.id.id);
		var startDate = $('#stageStartDate' + element.id.id);
		var endDate = $('#stageEndDate' + element.id.id);
		var intersectionDate = $('#intersectionDate' + element.id.id);
		if (name.val() == "") {
			setError(['#stageName' + element.id.id]);
			return false;
		} else {
			setOK(['#stageName' + element.id.id]);
		}
		moment.locale("ru");
		var momentStartDate = moment(startDate.parent().data('date'), 'DD.MM.YYYY');
		var momentEndDate = moment(endDate.parent().data('date'), 'DD.MM.YYYY');
		if (momentStartDate > momentEndDate) {
			setError(['#stageStartDate' + element.id.id]);
			setError(['#stageEndDate' + element.id.id]);
			return false;
		} else {
			setOK(['#stageStartDate' + element.id.id]);
			setOK(['#stageEndDate' + element.id.id]);
		}
		element.name = name.val();
		element.startDate = momentStartDate.toDate();
		element.endDate = momentEndDate.toDate();
		element.intersectionDate = (<HTMLInputElement>intersectionDate[0]).checked;
		if (insertType == InsertType.Add) {
			if (element.parent == null) {
				if (this.localStages.add(element) == false) {
					return false;
				}
			} else {
				if (element.parent.add(element) == false) {
					return false;
				}
			}
		}
		$('#saveStagesId').prop("disabled", false);
		return true;
	}

	updateStages(paramStages: ProjectStages[]) {
		$('#stagesEditTreeBody').html('');
		this._updateStages(paramStages, 0);
		initTree('treeStages');
		initICheck();
	}

	private _updateStages(paramStages: ProjectStages[], level: number) {
		for (var i = 0; i < paramStages.length; i++) {
			var className = '';
			if ((paramStages[i].parent == null) || (paramStages[i].parent.isRoot == true)) {
				className = 'treegrid-' + paramStages[i].id.id;
			} else {
				className = 'treegrid-' + paramStages[i].id.id + ' treegrid-parent-' + paramStages[i].parent.id.id;
			}

			var checkedElem = '';
			if (paramStages[i].intersectionDate) {
				checkedElem = 'checked="checked"';
			}
			var buttonAddSub = '';
			if (level < this.layers.length - 1) {
				buttonAddSub = '<button id="addSubStage' + paramStages[i].id.id + '" idInArray="' + i + '" class="btn btn-success btn-xs">Добавить под* <i class="fa fa-plus"></i></button>'
			}
			var elementHtml = '<tr class="' + className + '" id=tr' + paramStages[i].id.id + ' ><td>#</td><td>' +
				paramStages[i].name
				+ '</td><td>' +
				paramStages[i].startDate.toLocaleDateString()
				+ '</td><td>' +
				paramStages[i].endDate.toLocaleDateString() + '</td><td>' +
				'<div class="checkbox"><label> <input type="checkbox" class="flat" ' + checkedElem + ' disabled> </label></div>'
				+ '</td><td>'
				+ '<div><button id="editStage' + paramStages[i].id.id + '" idInArray="' + i + '" class="btn btn-xs"><i class="fa fa-pencil-square-o"></i></button>'
				+ '<button id="delStage' + paramStages[i].id.id + '" idInArray="' + i + '" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>'
				+ buttonAddSub
				+ '</div>'
				+ '</td></tr>';
			if ((paramStages[i].parent == null) || (paramStages[i].parent.isRoot == true)) {
				$('#stagesEditTreeBody').append(elementHtml);
			} else {
				///TODO: Сортировка по датам 
				if ($('.treegrid-parent-' + paramStages[i].parent.id.id + ':last').length == 0) {
					$(elementHtml).insertAfter('#tr' + paramStages[i].parent.id.id);
				} else {
					$(elementHtml).insertAfter('.treegrid-parent-' + paramStages[i].parent.id.id + ':last');
				}
			}

			$('#editStage' + paramStages[i].id.id).on("click", (e) => {
				var id = new Number($(e.currentTarget).attr("idInArray"));
				this.insertNewStage(paramStages[id.valueOf()]);
			});
			$('#delStage' + paramStages[i].id.id).on("click", (e) => {


			});
			$('#addSubStage' + paramStages[i].id.id).on("click", (e) => {

				var id = new Number($(e.currentTarget).attr("idInArray"));
				var range = getFreeStage(paramStages[id.valueOf()].children, paramStages[id.valueOf()], this.projectInfo);
				if (range == null) {
					window.alert("Нет свободной даты");
					return;
				}
				$('#addStageId').prop("disabled", true);
				$(e.currentTarget).prop("disabled", true);
				var newStage = new ProjectStages();
				newStage.levelId = this.layers[level + 1].id;
				newStage.startDate = range.startDate;
				newStage.endDate = range.endDate;
				newStage.parent = paramStages[id.valueOf()];
				this.insertNewStage(newStage);
			});

			if (paramStages[i].children !== undefined) {
				if (paramStages[i].children.length > 0) {
					this._updateStages(paramStages[i].children, level + 1);
				}
			}
		}
	}



	loadUsers() {
		$.get('_DropdownPartial?idFirm=' + this.idFirm + '&idProject=' + this.idProject,
			(html: HTMLDivElement) => {
				this.selectedList = html;
				this.loadUserDeferred.resolve(this.selectedList);
				//this.updateEditEmplyee();
				console.log("loaded");
			});
		//initSelector();
	}

	showEditor(click: string) {
		var elem = $(document).find('[edit="' + click + '"]');
		var type = $(document).find('[edit="' + click + '"]').attr("type");
		switch (type) {
			case 'text': {
				var myDiv = $('<div class="editor-buttons"><button class="btn btn-xs" onclick="projectManager.saveEditor(\'' + click + '\')"><i class="fa fa-floppy-o"></i></button><button class="btn btn-danger btn-xs" onclick="projectManager.closeEditor(\'' + click + '\')"><i class="fa fa-times"></i></button></div>');
				elem.parent().parent().find(".editor-buttons").replaceWith(myDiv);
				elem.html('<input type="text" value="' + elem.attr('value') + '" />');
				break;
			}
			case 'list': {
				if (this.selectedList == undefined) {
					return;
				}
				var myDiv = $('<div class="editor-buttons"><button class="btn btn-xs" onclick="projectManager.saveEditor(\'' + click + '\')"><i class="fa fa-floppy-o"></i></button><button class="btn btn-danger btn-xs" onclick="projectManager.closeEditor(\'' + click + '\')"><i class="fa fa-times"></i></button></div>');
				elem.parent().parent().find(".editor-buttons").replaceWith(myDiv);
				elem.html($(this.selectedList).html());
				initSelector('#infoEdit .selectpicker');
				$('#infoEdit .selectpicker').selectpicker('val', elem.attr('valueId'));
				break;
			}
			case 'date': {
				var myInput = '<div class="input-group date" id="datetimepicker"><input class="form-control" value="' + elem.attr('value') + '"/><span class="input-group-addon"><span class="glyphicon glyphicon-calendar" /></span></div>';
				var myDiv = $('<div class="editor-buttons"><button class="btn btn-xs" onclick="projectManager.saveEditor(\'' + click + '\')"><i class="fa fa-floppy-o"></i></button><button class="btn btn-danger btn-xs" onclick="projectManager.closeEditor(\'' + click + '\')"><i class="fa fa-times"></i></button></div>');
				elem.parent().parent().find(".editor-buttons").replaceWith(myDiv);
				elem.html(myInput);
				initDatePicker();
				break;
			}
		}
	}

	saveEditor(click: string) {
		var elem = $(document).find('[edit="' + click + '"]');
		var type = $(document).find('[edit="' + click + '"]').attr("type");
		switch (type) {
			case 'text': {
				if (elem.find('input').val() != elem.attr("value")) {
					this.saveEditorAjax(click, elem.find('input').val());
					elem.attr("value", elem.find('input').val());
				}
				this.closeEditor(click);
				break;
			}

			case 'list': {
				var selectedElement = elem.find('option:selected');
				if (selectedElement.val() != elem.attr('valueId')) {
                    /**
                    * ID
                    */
					elem.attr('valueId', selectedElement.val());
                    /**
                    * FIO
                    */
					elem.attr('value', selectedElement.html());
					this.saveEditorAjax(click, selectedElement.val());
				}
				this.closeEditor(click);
				break;
			}

			case 'date': {
				var dateElement = elem.find('input');
				if (dateElement.val() != elem.attr('value')) {
					elem.attr('value', dateElement.val());
					this.saveEditorAjax(click, dateElement.val());
				}
				this.closeEditor(click);
				break;
			}
		}
	}

	saveEditorAjax(whatReplace: string, thanReplace: string) {
		//  var cryptoObj = window.crypto || window.msCrypto; // для IE 11
		var token = $("#__AjaxAntiForgeryForm input").val();
		var url = '_SaveMainProjectInfo?idFirm=' + this.idFirm + '&idProject=' + this.idProject + '&whatReplace=' + window.btoa(whatReplace) + '&thanReplace=' + window.btoa(escape(thanReplace));
		$.ajax({
			url: url,
			type: 'POST',
			contentType: "application/json",
			dataType: "json",
			headers: { '__RequestVerificationToken': token },
			success: (suc) => {
				console.log(suc);
			}
		});
	}

	closeEditor(click: string) {
		var elem = $(document).find('[edit="' + click + '"]');
		var myDiv = $('<div class="editor-buttons"><button class="btn btn-xs" onclick="projectManager.showEditor(\'' + elem.attr('edit') + '\')"><i class="fa fa-edit"></i></button></div>');
		elem.parent().parent().find(".editor-buttons").replaceWith(myDiv);
		elem.text(elem.attr("value"));
	}


}

function initICheck() {
	if ($("input.flat")[0]) {
		$('input.flat').iCheck({
			checkboxClass: 'icheckbox_flat-green',
			radioClass: 'iradio_flat-green'
		});

	}
}



function loadProjects(target: string, idFirm: number, isCurrent: boolean) {
	var urlGetProjects = 'GetProjects?idFirm=' + idFirm + '&isCurrent=' + isCurrent;
	var msgProjectNone = '<div id="mainProjectPage" class="alert alert-danger"><strong> Внимание! </strong> <p>У вас пока не создано проектов</p></div>';
	var msgProjectLastNone = '<div id="mainProjectPage" class="alert alert-info"><strong> Внимание! </strong> <p>У вас пока нет завершенных проектов</p></div>';
	var loadingMessage = '<div id="mainProjectPage"><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</div>';
	var table = '<div id="mainProjectPage"><p>Текущие проекты</p><table class="table table-striped projects" id="projectsTable" style="table-layout: fixed;"><thead><tr><th style="width: 10%">#</th><th style="width: 20%">Наименование проекта</th><th>Участники</th><th style="width: 10%"> Прогресс </th><th style="width: 12%"> Статус </th><th style= "width: 30%">#Редактирование</th></tr></thead><tbody></tbody></table></div>';
	$('#' + target).replaceWith(loadingMessage);
	$.ajax({
		url: urlGetProjects,
		error: (xhr, ajaxOptions, thrownError) => {
			$('#' + target).replaceWith('<div id="mainProjectPage" class="alert alert-block">' +
				'<h4>Внимание!</h4>' +
				'Данные не были загружены, ошибка - ' + xhr.status + '.' +
				'</div>');
			console.log(xhr.status);
		},
		success: (msg: Array<number>) => {
			if (msg.length == 0) {
				if ((isCurrent == null) || (isCurrent == true)) {
					$('#' + target).replaceWith(msgProjectNone);
				}
				if (isCurrent == false) {
					$('#' + target).replaceWith(msgProjectLastNone);
				}

			} else {
				$('#' + target).replaceWith(table);
				for (var i = 0; i < msg.length; i++) {
					loadProject(target, idFirm, msg[i]);
				}
			}
		}
	});
}

function loadProject(target: string, idFirm: number, idProject: number) {
	var loadingMessageRow = '<tr id="proj' + idProject + '"><td></td><td></td><td></td><td><i class="fa fa-spinner fa-spin fa-3x"></i> Загрузка...</td><td></td><td></td></tr>';
	$('#' + target + ' tbody').append(loadingMessageRow);
	var urlGetProjects = '_ProjectRowPartial?idFirm=' + idFirm + '&idProject=' + idProject;

	$.ajax({
		url: urlGetProjects,
		error: (xhr, ajaxOptions, thrownError) => {
			$('#proj' + idProject).replaceWith('<tr id="proj' + idProject + '" class="alert alert-block">' +
				'<h4>Внимание!</h4>' +
				'Данные не были загружены, ошибка - ' + xhr.status + '.' +
				'</tr>');
			console.log(xhr.status);
		},
		success: (html) => {
			$('#proj' + idProject).replaceWith(html);
		}
	});
}

let projectManager = new ProjectManage();