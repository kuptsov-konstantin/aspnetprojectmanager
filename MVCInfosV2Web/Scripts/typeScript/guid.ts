﻿class Guid {
	static newGuid(): Guid {
		var id = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
			var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
			return v.toString(16);
		}
		);
		var guid = new Guid();
		guid.id = id;
		return guid;
	}
	static empty: string = '00000000-0000-0000-0000-000000000000';
	id: string;
	public static setId(id: string) {
		var guid = new Guid();
		guid.id = id;
		return guid;
	}
	public toString = (): string => {
		return this.id;
	};
}
