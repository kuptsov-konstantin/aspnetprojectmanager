﻿var orgObject = {
    //step1
    orgName: "",
    country: "",
    city: "",
    address: "",
    //step2
    departments: [],

    //step3
    employees: []
};

var employee = {
    familyName: "",
    name: "",
    middleName: "",
    birthDay: "",
    //---
    userName: "",
    email: "",
    pass: "",
    repeatPass: ""

};
///TODO: Добавить подсакзки валидации
function loadOrgRegisterPage() {
    console.log("click on project");
	var str = '@Url.Action(@*"_CreateStepOneFirm"*@"_AddFirmPartial")';
    $.get(str, function (html) {
        $('#targetReplace').replaceWith(html);
		var step1 = '@Url.Action("_CreateStepOneFirm")';
        /* $.get(step1, function (html1) {
             console.log("ШАГ 1 ЗАГРУЖЕН");
             $('#step-1-inner').replaceWith(html1);
         });*/
        $('#wizard').smartWizard({
            labelNext: 'Далее', // label for Next button
            labelPrevious: 'Назад', // label for Previous button
            labelFinish: 'Готово',  // label for Finish button

            onLeaveStep: function (obj, context) {
                console.log("next");
                if (context.toStep < context.fromStep) {
                    return true;
                }
                return validateSteps(context.fromStep); // return false to stay on step and true to continue navigation
            },
            onFinish: function (objs, context) {
                console.log("finish");
                if (validateAllSteps()) {
                    $('form').submit();
                }
            }
        });
        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');


    });
}

function leaveAStepCallback(obj, context) {
    alert("Leaving step " + context.fromStep + " to go to step " + context.toStep);
    return validateSteps(context.fromStep); // return false to stay on step and true to continue navigation
}



// Your Step validation logic
function validateSteps(stepnumber) {
    var isStepValid = true;
    // validate step 1
    if (stepnumber === 1) {
        if (!idValid("org-name")) {
            isStepValid = false;
        } else {
            orgObject.orgName = document.getElementById("org-name").value;
            $('#firmNameOnStep2').replaceWith('<td id="firmNameOnStep2">' + orgObject.orgName + '</td>');
        }
        if (!idValid("country-name")) {
            isStepValid = false;
        } else {
            orgObject.country = document.getElementById("country-name").value;
        }
        if (!idValid("country-city")) {
            isStepValid = false;
        } else {
            orgObject.city = document.getElementById("country-city").value;
        }
        if (!idValid("country-address")) {
            isStepValid = false;
        } else {
            orgObject.address = document.getElementById("country-address").value;
        }
    }
    if (stepnumber === 2) {
        if (orgObject.departments.length === 0) {
            isStepValid = false;
        }
    }

    return isStepValid;
}
// ...
function idValid(elementID) {
    var orgNameI = document.getElementById(elementID);
    var orgName = orgNameI.value;
    var name = orgName.replace(/\s+/g, " ");
    if (name.length > 1) {
        setOK(elementID);
        return true;
    } else {
        setError(elementID);
        return false;
    }
}

function validateAllSteps() {
    var isStepValid = true;

    return isStepValid;
}
function setError(currentId) {
    var $elem = $("#" + currentId);
    if ($elem.hasClass('my_valid_not_error')) {
        $elem.removeClass('my_valid_not_error');
    }
    $elem.addClass('my_valid_error');
}
function setOK(currentId) {
    var $elem = $("#" + currentId);
    if ($elem.hasClass('my_valid_error')) {
        $elem.removeClass('my_valid_error');
    }
    $elem.addClass('my_valid_not_error');
}
///TODO: Добавить проверку на повторяемость
function addDepartment() {
    var department = document.getElementById("department-name").value;
    if (idValid("department-name")) {

        orgObject.departments.push(department);
        document.getElementById("department-name").value = "";
        printCurrentDepartments(orgObject.departments);
    }
}

function addDepartmentsFromFile() {
    loadDepts();
    orgObject.departments = content;
    printCurrentDepartments(content);
    $('#myModal').modal('hide');

}

var content = "";
function readText(filePath) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var contents = event.target.result;
        content = contents.match(/[^;]+/g);
        console.log("Содержимое файла: " + contents);
    };

    reader.onerror = function (event) {
        console.error("Файл не может быть прочитан! код " + event.target.error.code);
    };

    reader.readAsText(filePath);
}

function loadDepts() {
    if (document.getElementById("fileDownloader").files.length > 0) {
        readText(document.getElementById("fileDownloader").files[0]);
    }
}

function printCurrentDepartments(content) {
    var tableRef = document.getElementById('departmentListTable').getElementsByTagName('tbody')[0];
    console.log(tableRef);

    $('#bodyTableId').empty();
    for (var i = 0; i < content.length; i++) {
        var newRow = tableRef.insertRow(tableRef.rows.length);
        var newCell1 = newRow.insertCell(0);
        var newText1 = document.createTextNode('#');
        newCell1.appendChild(newText1);

        var newCell2 = newRow.insertCell(0);
        var newText = document.createTextNode(content[i]);
        newCell2.appendChild(newText);
        var it = document.createElement('i');
        var at = document.createElement('a');
        at.className = 'btn btn-danger';
        it.className = 'fa fa-trash';
        at.onclick = 'removeDepartment(' + i + ')';
        at.appendChild(it);
        var newCell3 = newRow.insertCell(-1);
        newCell3.appendChild(at);
        $('#wizard').smartWizard('fixHeight');
    }

    console.log(content);
}

function removeDepartment(pos) {
    orgObject.departments.splice(pos, 1);
    printCurrentDepartments(orgObject.departments);
}