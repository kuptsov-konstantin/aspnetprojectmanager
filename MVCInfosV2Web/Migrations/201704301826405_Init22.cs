namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init22 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IsDeleted", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Person", "MiddleName", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Person", "MiddleName", c => c.String(nullable: false));
            DropColumn("dbo.AspNetUsers", "IsDeleted");
        }
    }
}
