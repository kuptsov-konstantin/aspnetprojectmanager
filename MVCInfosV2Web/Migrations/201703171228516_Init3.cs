namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PersonalTasks",
                c => new
                    {
                        TaskID = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeadLine = c.DateTime(nullable: false),
                        TaskPriority = c.Int(nullable: false),
                        ProjectEmployeeFirm_ID = c.Guid(),
                    })
                .PrimaryKey(t => t.TaskID)
                .ForeignKey("dbo.ProjectEmployeeFirms", t => t.ProjectEmployeeFirm_ID)
                .Index(t => t.ProjectEmployeeFirm_ID);
            
            CreateTable(
                "dbo.AdditionalFiles",
                c => new
                    {
                        AdditionalFileID = c.Guid(nullable: false),
                        Url = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        HostingFile_HostingFileID = c.Guid(),
                        PersonalTask_TaskID = c.Guid(),
                    })
                .PrimaryKey(t => t.AdditionalFileID)
                .ForeignKey("dbo.HostingFiles", t => t.HostingFile_HostingFileID)
                .ForeignKey("dbo.PersonalTasks", t => t.PersonalTask_TaskID)
                .Index(t => t.HostingFile_HostingFileID)
                .Index(t => t.PersonalTask_TaskID);
            
            CreateTable(
                "dbo.HostingFiles",
                c => new
                    {
                        HostingFileID = c.Guid(nullable: false),
                        Hosting = c.String(),
                        Login = c.String(),
                        Password = c.String(),
                        Token = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.HostingFileID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PersonalTasks", "ProjectEmployeeFirm_ID", "dbo.ProjectEmployeeFirms");
            DropForeignKey("dbo.AdditionalFiles", "PersonalTask_TaskID", "dbo.PersonalTasks");
            DropForeignKey("dbo.AdditionalFiles", "HostingFile_HostingFileID", "dbo.HostingFiles");
            DropIndex("dbo.AdditionalFiles", new[] { "PersonalTask_TaskID" });
            DropIndex("dbo.AdditionalFiles", new[] { "HostingFile_HostingFileID" });
            DropIndex("dbo.PersonalTasks", new[] { "ProjectEmployeeFirm_ID" });
            DropTable("dbo.HostingFiles");
            DropTable("dbo.AdditionalFiles");
            DropTable("dbo.PersonalTasks");
        }
    }
}
