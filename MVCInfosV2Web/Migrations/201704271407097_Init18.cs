namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init18 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeFirms", "IsActiveDirectory", c => c.Boolean(nullable: false));
            AddColumn("dbo.AspNetUsers", "IsActiveDirectory", c => c.Boolean(nullable: false));
            AddColumn("dbo.Employees", "IsActiveDirectory", c => c.Boolean(nullable: false));
            AddColumn("dbo.Person", "IsActiveDirectory", c => c.Boolean(nullable: false));
            AddColumn("dbo.Firms", "IsActiveDirectory", c => c.Boolean(nullable: false));
            DropColumn("dbo.AspNetUsers", "IsADUser");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "IsADUser", c => c.Boolean(nullable: false));
            DropColumn("dbo.Firms", "IsActiveDirectory");
            DropColumn("dbo.Person", "IsActiveDirectory");
            DropColumn("dbo.Employees", "IsActiveDirectory");
            DropColumn("dbo.AspNetUsers", "IsActiveDirectory");
            DropColumn("dbo.EmployeeFirms", "IsActiveDirectory");
        }
    }
}
