namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init15 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IsADUser", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsADUser");
        }
    }
}
