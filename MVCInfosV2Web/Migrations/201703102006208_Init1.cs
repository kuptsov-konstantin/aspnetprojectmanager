namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProjectStages", "ProjectStage_ProjectStageId", "dbo.ProjectStages");
            DropPrimaryKey("dbo.ProjectStages");
            AlterColumn("dbo.ProjectStages", "ProjectStageId", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.ProjectStages", "ProjectStageId");
            AddForeignKey("dbo.ProjectStages", "ProjectStage_ProjectStageId", "dbo.ProjectStages", "ProjectStageId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectStages", "ProjectStage_ProjectStageId", "dbo.ProjectStages");
            DropPrimaryKey("dbo.ProjectStages");
            AlterColumn("dbo.ProjectStages", "ProjectStageId", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.ProjectStages", "ProjectStageId");
            AddForeignKey("dbo.ProjectStages", "ProjectStage_ProjectStageId", "dbo.ProjectStages", "ProjectStageId");
        }
    }
}
