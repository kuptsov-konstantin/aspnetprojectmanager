namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init28 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AdditionalFiles", "GroupFile_GroupFileId", "dbo.GroupFiles");
            DropPrimaryKey("dbo.GroupFiles");
            AlterColumn("dbo.GroupFiles", "GroupFileId", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.GroupFiles", "GroupFileId");
            AddForeignKey("dbo.AdditionalFiles", "GroupFile_GroupFileId", "dbo.GroupFiles", "GroupFileId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AdditionalFiles", "GroupFile_GroupFileId", "dbo.GroupFiles");
            DropPrimaryKey("dbo.GroupFiles");
            AlterColumn("dbo.GroupFiles", "GroupFileId", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.GroupFiles", "GroupFileId");
            AddForeignKey("dbo.AdditionalFiles", "GroupFile_GroupFileId", "dbo.GroupFiles", "GroupFileId");
        }
    }
}
