namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init8 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.EmployeeFirmDepartments", newName: "DepartmentEmployeeFirms");
            DropPrimaryKey("dbo.DepartmentEmployeeFirms");
            AddColumn("dbo.AdditionalFiles", "AdditionalFileType", c => c.Int(nullable: false));
            AddColumn("dbo.AdditionalFiles", "UploadedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.HostingFiles", "HostingName", c => c.String());
            AddColumn("dbo.HostingFiles", "UrlForUpload", c => c.String());
            AddPrimaryKey("dbo.DepartmentEmployeeFirms", new[] { "Department_DepartmentID", "EmployeeFirm_Id" });
            DropColumn("dbo.HostingFiles", "Hosting");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HostingFiles", "Hosting", c => c.String());
            DropPrimaryKey("dbo.DepartmentEmployeeFirms");
            DropColumn("dbo.HostingFiles", "UrlForUpload");
            DropColumn("dbo.HostingFiles", "HostingName");
            DropColumn("dbo.AdditionalFiles", "UploadedDate");
            DropColumn("dbo.AdditionalFiles", "AdditionalFileType");
            AddPrimaryKey("dbo.DepartmentEmployeeFirms", new[] { "EmployeeFirm_Id", "Department_DepartmentID" });
            RenameTable(name: "dbo.DepartmentEmployeeFirms", newName: "EmployeeFirmDepartments");
        }
    }
}
