namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init37 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeRoles", "IdentificationNumber", c => c.String());
            DropColumn("dbo.EmployeeRoles", "Identification_number");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EmployeeRoles", "Identification_number", c => c.String());
            DropColumn("dbo.EmployeeRoles", "IdentificationNumber");
        }
    }
}
