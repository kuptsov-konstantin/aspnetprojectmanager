namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init29 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.DepartmentEmployeeFirms", newName: "EmployeeFirmDepartments");
            DropForeignKey("dbo.AdditionalFiles", "HostingFile_HostingFileID", "dbo.HostingFiles");
            DropForeignKey("dbo.AdditionalFiles", "PersonalTask_PersonalTaskID", "dbo.PersonalTasks");
            DropForeignKey("dbo.AdditionalFiles", "GroupFile_GroupFileId", "dbo.GroupFiles");
            DropForeignKey("dbo.AdditionalFiles", "Mail_MailId", "dbo.Mails");
            DropForeignKey("dbo.ResulutionNotifiers", "EmployeeFirm_Id", "dbo.EmployeeFirms");
            DropForeignKey("dbo.ResulutionNotifiers", "Mail_MailId", "dbo.Mails");
            DropIndex("dbo.AdditionalFiles", new[] { "HostingFile_HostingFileID" });
            DropIndex("dbo.AdditionalFiles", new[] { "PersonalTask_PersonalTaskID" });
            DropIndex("dbo.AdditionalFiles", new[] { "GroupFile_GroupFileId" });
            DropIndex("dbo.AdditionalFiles", new[] { "Mail_MailId" });
            DropIndex("dbo.ResulutionNotifiers", new[] { "EmployeeFirm_Id" });
            DropIndex("dbo.ResulutionNotifiers", new[] { "Mail_MailId" });
            DropPrimaryKey("dbo.Mails");
            DropPrimaryKey("dbo.EmployeeFirmDepartments");
            CreateTable(
                "dbo.UploadFiles",
                c => new
                    {
                        UploadFileID = c.Guid(nullable: false),
                        Url = c.String(),
                        AdditionalFileType = c.Int(nullable: false),
                        UploadedDate = c.DateTime(nullable: false),
                        OrignFileName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        HostingFile_HostingFileID = c.Guid(),
                        PersonalTask_PersonalTaskID = c.Guid(),
                        UploadGroupFile_GroupFileId = c.Guid(),
                    })
                .PrimaryKey(t => t.UploadFileID)
                .ForeignKey("dbo.HostingFiles", t => t.HostingFile_HostingFileID)
                .ForeignKey("dbo.PersonalTasks", t => t.PersonalTask_PersonalTaskID)
                .ForeignKey("dbo.UploadGroupFiles", t => t.UploadGroupFile_GroupFileId)
                .Index(t => t.HostingFile_HostingFileID)
                .Index(t => t.PersonalTask_PersonalTaskID)
                .Index(t => t.UploadGroupFile_GroupFileId);
            
            CreateTable(
                "dbo.UploadGroupFiles",
                c => new
                    {
                        GroupFileId = c.Guid(nullable: false),
                        DatetimeCreated = c.DateTime(nullable: false),
                        DatetimeStored = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.GroupFileId);
            
            CreateTable(
                "dbo.ResolutionNotifiers",
                c => new
                    {
                        ResolutionNotifierId = c.Guid(nullable: false),
                        NotifyTime = c.Time(nullable: false, precision: 7),
                        IsDelete = c.Boolean(nullable: false),
                        IsNotifi = c.Boolean(nullable: false),
                        EmployeeFirm_Id = c.Int(),
                        Mail_MailId = c.Guid(),
                    })
                .PrimaryKey(t => t.ResolutionNotifierId)
                .ForeignKey("dbo.EmployeeFirms", t => t.EmployeeFirm_Id)
                .ForeignKey("dbo.Mails", t => t.Mail_MailId)
                .Index(t => t.EmployeeFirm_Id)
                .Index(t => t.Mail_MailId);
            
            AddColumn("dbo.Mails", "UploadGroupFile_GroupFileId", c => c.Guid());
            AlterColumn("dbo.Mails", "MailId", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Mails", "MailId");
            AddPrimaryKey("dbo.EmployeeFirmDepartments", new[] { "EmployeeFirm_Id", "Department_DepartmentID" });
            CreateIndex("dbo.Mails", "UploadGroupFile_GroupFileId");
            AddForeignKey("dbo.Mails", "UploadGroupFile_GroupFileId", "dbo.UploadGroupFiles", "GroupFileId");
            DropTable("dbo.AdditionalFiles");
            DropTable("dbo.GroupFiles");
            DropTable("dbo.ResulutionNotifiers");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ResulutionNotifiers",
                c => new
                    {
                        NotifierId = c.Guid(nullable: false, identity: true),
                        NotifyTime = c.Time(nullable: false, precision: 7),
                        IsDelete = c.Boolean(nullable: false),
                        IsNotifi = c.Boolean(nullable: false),
                        EmployeeFirm_Id = c.Int(),
                        Mail_MailId = c.Guid(),
                    })
                .PrimaryKey(t => t.NotifierId);
            
            CreateTable(
                "dbo.GroupFiles",
                c => new
                    {
                        GroupFileId = c.Guid(nullable: false),
                        DatetimeCreated = c.DateTime(nullable: false),
                        DatetimeStored = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.GroupFileId);
            
            CreateTable(
                "dbo.AdditionalFiles",
                c => new
                    {
                        AdditionalFileID = c.Guid(nullable: false),
                        Url = c.String(),
                        AdditionalFileType = c.Int(nullable: false),
                        UploadedDate = c.DateTime(nullable: false),
                        OrignFileName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        HostingFile_HostingFileID = c.Guid(),
                        PersonalTask_PersonalTaskID = c.Guid(),
                        GroupFile_GroupFileId = c.Guid(),
                        Mail_MailId = c.Guid(),
                    })
                .PrimaryKey(t => t.AdditionalFileID);
            
            DropForeignKey("dbo.Mails", "UploadGroupFile_GroupFileId", "dbo.UploadGroupFiles");
            DropForeignKey("dbo.ResolutionNotifiers", "Mail_MailId", "dbo.Mails");
            DropForeignKey("dbo.ResolutionNotifiers", "EmployeeFirm_Id", "dbo.EmployeeFirms");
            DropForeignKey("dbo.UploadFiles", "UploadGroupFile_GroupFileId", "dbo.UploadGroupFiles");
            DropForeignKey("dbo.UploadFiles", "PersonalTask_PersonalTaskID", "dbo.PersonalTasks");
            DropForeignKey("dbo.UploadFiles", "HostingFile_HostingFileID", "dbo.HostingFiles");
            DropIndex("dbo.Mails", new[] { "UploadGroupFile_GroupFileId" });
            DropIndex("dbo.ResolutionNotifiers", new[] { "Mail_MailId" });
            DropIndex("dbo.ResolutionNotifiers", new[] { "EmployeeFirm_Id" });
            DropIndex("dbo.UploadFiles", new[] { "UploadGroupFile_GroupFileId" });
            DropIndex("dbo.UploadFiles", new[] { "PersonalTask_PersonalTaskID" });
            DropIndex("dbo.UploadFiles", new[] { "HostingFile_HostingFileID" });
            DropPrimaryKey("dbo.EmployeeFirmDepartments");
            DropPrimaryKey("dbo.Mails");
            AlterColumn("dbo.Mails", "MailId", c => c.Guid(nullable: false, identity: true));
            DropColumn("dbo.Mails", "UploadGroupFile_GroupFileId");
            DropTable("dbo.ResolutionNotifiers");
            DropTable("dbo.UploadGroupFiles");
            DropTable("dbo.UploadFiles");
            AddPrimaryKey("dbo.EmployeeFirmDepartments", new[] { "Department_DepartmentID", "EmployeeFirm_Id" });
            AddPrimaryKey("dbo.Mails", "MailId");
            CreateIndex("dbo.ResulutionNotifiers", "Mail_MailId");
            CreateIndex("dbo.ResulutionNotifiers", "EmployeeFirm_Id");
            CreateIndex("dbo.AdditionalFiles", "Mail_MailId");
            CreateIndex("dbo.AdditionalFiles", "GroupFile_GroupFileId");
            CreateIndex("dbo.AdditionalFiles", "PersonalTask_PersonalTaskID");
            CreateIndex("dbo.AdditionalFiles", "HostingFile_HostingFileID");
            AddForeignKey("dbo.ResulutionNotifiers", "Mail_MailId", "dbo.Mails", "MailId");
            AddForeignKey("dbo.ResulutionNotifiers", "EmployeeFirm_Id", "dbo.EmployeeFirms", "Id");
            AddForeignKey("dbo.AdditionalFiles", "Mail_MailId", "dbo.Mails", "MailId");
            AddForeignKey("dbo.AdditionalFiles", "GroupFile_GroupFileId", "dbo.GroupFiles", "GroupFileId");
            AddForeignKey("dbo.AdditionalFiles", "PersonalTask_PersonalTaskID", "dbo.PersonalTasks", "PersonalTaskID");
            AddForeignKey("dbo.AdditionalFiles", "HostingFile_HostingFileID", "dbo.HostingFiles", "HostingFileID");
            RenameTable(name: "dbo.EmployeeFirmDepartments", newName: "DepartmentEmployeeFirms");
        }
    }
}
