namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init33 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Mails", "Firm_FirmID", c => c.Int());
            CreateIndex("dbo.Mails", "Firm_FirmID");
            AddForeignKey("dbo.Mails", "Firm_FirmID", "dbo.Firms", "FirmID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Mails", "Firm_FirmID", "dbo.Firms");
            DropIndex("dbo.Mails", new[] { "Firm_FirmID" });
            DropColumn("dbo.Mails", "Firm_FirmID");
        }
    }
}
