namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AdditionalFiles", "PersonalTask_TaskID", "dbo.PersonalTasks");
            RenameColumn(table: "dbo.AdditionalFiles", name: "PersonalTask_TaskID", newName: "PersonalTask_PersonalTaskID");
            RenameIndex(table: "dbo.AdditionalFiles", name: "IX_PersonalTask_TaskID", newName: "IX_PersonalTask_PersonalTaskID");
            DropPrimaryKey("dbo.PersonalTasks");
            AddColumn("dbo.PersonalTasks", "PersonalTaskID", c => c.Guid(nullable: false));
            AddColumn("dbo.PersonalTasks", "PersonalTask_PersonalTaskID", c => c.Guid());
            AddPrimaryKey("dbo.PersonalTasks", "PersonalTaskID");
            CreateIndex("dbo.PersonalTasks", "PersonalTask_PersonalTaskID");
            AddForeignKey("dbo.PersonalTasks", "PersonalTask_PersonalTaskID", "dbo.PersonalTasks", "PersonalTaskID");
            AddForeignKey("dbo.AdditionalFiles", "PersonalTask_PersonalTaskID", "dbo.PersonalTasks", "PersonalTaskID");
            DropColumn("dbo.PersonalTasks", "TaskID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PersonalTasks", "TaskID", c => c.Guid(nullable: false));
            DropForeignKey("dbo.AdditionalFiles", "PersonalTask_PersonalTaskID", "dbo.PersonalTasks");
            DropForeignKey("dbo.PersonalTasks", "PersonalTask_PersonalTaskID", "dbo.PersonalTasks");
            DropIndex("dbo.PersonalTasks", new[] { "PersonalTask_PersonalTaskID" });
            DropPrimaryKey("dbo.PersonalTasks");
            DropColumn("dbo.PersonalTasks", "PersonalTask_PersonalTaskID");
            DropColumn("dbo.PersonalTasks", "PersonalTaskID");
            AddPrimaryKey("dbo.PersonalTasks", "TaskID");
            RenameIndex(table: "dbo.AdditionalFiles", name: "IX_PersonalTask_PersonalTaskID", newName: "IX_PersonalTask_TaskID");
            RenameColumn(table: "dbo.AdditionalFiles", name: "PersonalTask_PersonalTaskID", newName: "PersonalTask_TaskID");
            AddForeignKey("dbo.AdditionalFiles", "PersonalTask_TaskID", "dbo.PersonalTasks", "TaskID");
        }
    }
}
