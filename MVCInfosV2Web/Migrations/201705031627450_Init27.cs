namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init27 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.GroupFiles",
                c => new
                    {
                        GroupFileId = c.Guid(nullable: false, identity: true),
                        DatetimeCreated = c.DateTime(nullable: false),
                        DatetimeStored = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.GroupFileId);
            
            AddColumn("dbo.AdditionalFiles", "GroupFile_GroupFileId", c => c.Guid());
            CreateIndex("dbo.AdditionalFiles", "GroupFile_GroupFileId");
            AddForeignKey("dbo.AdditionalFiles", "GroupFile_GroupFileId", "dbo.GroupFiles", "GroupFileId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AdditionalFiles", "GroupFile_GroupFileId", "dbo.GroupFiles");
            DropIndex("dbo.AdditionalFiles", new[] { "GroupFile_GroupFileId" });
            DropColumn("dbo.AdditionalFiles", "GroupFile_GroupFileId");
            DropTable("dbo.GroupFiles");
        }
    }
}
