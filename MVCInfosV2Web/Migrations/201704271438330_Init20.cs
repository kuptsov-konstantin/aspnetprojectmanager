namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init20 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.ActiveDirectortSyncHistories");
            AddColumn("dbo.ActiveDirectortSyncHistories", "ActiveDirectortSyncHistoryId", c => c.Guid(nullable: false, identity: true));
            AddPrimaryKey("dbo.ActiveDirectortSyncHistories", "ActiveDirectortSyncHistoryId");
            DropColumn("dbo.ActiveDirectortSyncHistories", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ActiveDirectortSyncHistories", "Id", c => c.Guid(nullable: false, identity: true));
            DropPrimaryKey("dbo.ActiveDirectortSyncHistories");
            DropColumn("dbo.ActiveDirectortSyncHistories", "ActiveDirectortSyncHistoryId");
            AddPrimaryKey("dbo.ActiveDirectortSyncHistories", "Id");
        }
    }
}
