namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init30 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.NotificationsClicks",
                c => new
                    {
                        NotificationsClickId = c.Guid(nullable: false),
                        ClickedTime = c.DateTime(nullable: false),
                        NotificationsClickType = c.Int(nullable: false),
                        ResolutionNotifier_ResolutionNotifierId = c.Guid(),
                    })
                .PrimaryKey(t => t.NotificationsClickId)
                .ForeignKey("dbo.ResolutionNotifiers", t => t.ResolutionNotifier_ResolutionNotifierId)
                .Index(t => t.ResolutionNotifier_ResolutionNotifierId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NotificationsClicks", "ResolutionNotifier_ResolutionNotifierId", "dbo.ResolutionNotifiers");
            DropIndex("dbo.NotificationsClicks", new[] { "ResolutionNotifier_ResolutionNotifierId" });
            DropTable("dbo.NotificationsClicks");
        }
    }
}
