namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init34 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MailStatus",
                c => new
                    {
                        MailStatusId = c.Guid(nullable: false),
                        MailStatusEnum = c.Int(nullable: false),
                        AproveDate = c.DateTime(nullable: false),
                        Mail_MailId = c.Guid(),
                    })
                .PrimaryKey(t => t.MailStatusId)
                .ForeignKey("dbo.Mails", t => t.Mail_MailId)
                .Index(t => t.Mail_MailId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MailStatus", "Mail_MailId", "dbo.Mails");
            DropIndex("dbo.MailStatus", new[] { "Mail_MailId" });
            DropTable("dbo.MailStatus");
        }
    }
}
