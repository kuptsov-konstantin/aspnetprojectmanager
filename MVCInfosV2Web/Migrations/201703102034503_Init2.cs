namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init2 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ProjectStages", name: "ProjectStage_ProjectStageId", newName: "Parent_ProjectStageId");
            RenameIndex(table: "dbo.ProjectStages", name: "IX_ProjectStage_ProjectStageId", newName: "IX_Parent_ProjectStageId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.ProjectStages", name: "IX_Parent_ProjectStageId", newName: "IX_ProjectStage_ProjectStageId");
            RenameColumn(table: "dbo.ProjectStages", name: "Parent_ProjectStageId", newName: "ProjectStage_ProjectStageId");
        }
    }
}
