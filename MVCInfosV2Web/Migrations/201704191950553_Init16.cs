namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init16 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EmployeeFirms", "EmploymentDate", c => c.DateTime(nullable: false, storeType: "date"));
            DropColumn("dbo.EmployeeFirms", "Employment_date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EmployeeFirms", "Employment_date", c => c.DateTime(nullable: false, storeType: "date"));
            DropColumn("dbo.EmployeeFirms", "EmploymentDate");
        }
    }
}
