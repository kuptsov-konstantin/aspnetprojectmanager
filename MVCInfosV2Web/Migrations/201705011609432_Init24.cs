namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init24 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contracts", "Prise", c => c.Double(nullable: false));
            AddColumn("dbo.Customers", "Firm_FirmID", c => c.Int());
            CreateIndex("dbo.Customers", "Firm_FirmID");
            AddForeignKey("dbo.Customers", "Firm_FirmID", "dbo.Firms", "FirmID");
            DropColumn("dbo.Contracts", "Cena");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contracts", "Cena", c => c.Double(nullable: false));
            DropForeignKey("dbo.Customers", "Firm_FirmID", "dbo.Firms");
            DropIndex("dbo.Customers", new[] { "Firm_FirmID" });
            DropColumn("dbo.Customers", "Firm_FirmID");
            DropColumn("dbo.Contracts", "Prise");
        }
    }
}
