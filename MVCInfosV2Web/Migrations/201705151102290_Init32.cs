namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init32 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Resolutions",
                c => new
                    {
                        ResolutionId = c.Guid(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.ResolutionId);
            
            AddColumn("dbo.Mails", "Resolution_ResolutionId", c => c.Guid());
            AddColumn("dbo.ResolutionNotifiers", "Resolution_ResolutionId", c => c.Guid());
            CreateIndex("dbo.Mails", "Resolution_ResolutionId");
            CreateIndex("dbo.ResolutionNotifiers", "Resolution_ResolutionId");
            AddForeignKey("dbo.ResolutionNotifiers", "Resolution_ResolutionId", "dbo.Resolutions", "ResolutionId");
            AddForeignKey("dbo.Mails", "Resolution_ResolutionId", "dbo.Resolutions", "ResolutionId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Mails", "Resolution_ResolutionId", "dbo.Resolutions");
            DropForeignKey("dbo.ResolutionNotifiers", "Resolution_ResolutionId", "dbo.Resolutions");
            DropIndex("dbo.ResolutionNotifiers", new[] { "Resolution_ResolutionId" });
            DropIndex("dbo.Mails", new[] { "Resolution_ResolutionId" });
            DropColumn("dbo.ResolutionNotifiers", "Resolution_ResolutionId");
            DropColumn("dbo.Mails", "Resolution_ResolutionId");
            DropTable("dbo.Resolutions");
        }
    }
}
