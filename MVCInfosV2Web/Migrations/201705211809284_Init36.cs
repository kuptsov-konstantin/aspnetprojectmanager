namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init36 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Mails", "IsDelete", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Mails", "IsDelete");
        }
    }
}
