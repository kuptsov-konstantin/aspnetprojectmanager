namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init21 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Departments", "IsActiveDirectory", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Departments", "IsActiveDirectory");
        }
    }
}
