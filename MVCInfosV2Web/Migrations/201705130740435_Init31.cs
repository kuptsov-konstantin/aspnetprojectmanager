namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init31 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ResolutionNotifiers", "NotifyTimeKey", c => c.Int(nullable: false));
            AddColumn("dbo.Mails", "Project_ProjectID", c => c.Int());
            CreateIndex("dbo.Mails", "Project_ProjectID");
            AddForeignKey("dbo.Mails", "Project_ProjectID", "dbo.Projects", "ProjectID");
            DropColumn("dbo.ResolutionNotifiers", "NotifyTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ResolutionNotifiers", "NotifyTime", c => c.Time(nullable: false, precision: 7));
            DropForeignKey("dbo.Mails", "Project_ProjectID", "dbo.Projects");
            DropIndex("dbo.Mails", new[] { "Project_ProjectID" });
            DropColumn("dbo.Mails", "Project_ProjectID");
            DropColumn("dbo.ResolutionNotifiers", "NotifyTimeKey");
        }
    }
}
