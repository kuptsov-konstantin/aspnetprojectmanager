namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init14 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HostingFiles", "PluginConnectionType", c => c.Guid(nullable: false));
            DropColumn("dbo.HostingFiles", "PlugginConnectionType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HostingFiles", "PlugginConnectionType", c => c.Guid(nullable: false));
            DropColumn("dbo.HostingFiles", "PluginConnectionType");
        }
    }
}
