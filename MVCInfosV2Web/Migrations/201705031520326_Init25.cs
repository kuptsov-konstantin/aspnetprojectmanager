namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init25 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Mails",
                c => new
                    {
                        MailId = c.Guid(nullable: false, identity: true),
                        DocumentID = c.Int(nullable: false),
                        RegisterDate = c.DateTime(nullable: false),
                        NumberDocument = c.String(nullable: false),
                        DocumentDate = c.DateTime(nullable: false),
                        Description = c.String(nullable: false),
                        FinishDate = c.DateTime(nullable: false),
                        Customer_CustomerID = c.Int(),
                    })
                .PrimaryKey(t => t.MailId)
                .ForeignKey("dbo.Customers", t => t.Customer_CustomerID)
                .Index(t => t.Customer_CustomerID);
            
            CreateTable(
                "dbo.ResulutionNotifiers",
                c => new
                    {
                        NotifierId = c.Guid(nullable: false, identity: true),
                        NotifyTime = c.Time(nullable: false, precision: 7),
                        IsDelete = c.Boolean(nullable: false),
                        IsNotifi = c.Boolean(nullable: false),
                        EmployeeFirm_Id = c.Int(),
                        Mail_MailId = c.Guid(),
                    })
                .PrimaryKey(t => t.NotifierId)
                .ForeignKey("dbo.EmployeeFirms", t => t.EmployeeFirm_Id)
                .ForeignKey("dbo.Mails", t => t.Mail_MailId)
                .Index(t => t.EmployeeFirm_Id)
                .Index(t => t.Mail_MailId);
            
            AddColumn("dbo.AdditionalFiles", "Mail_MailId", c => c.Guid());
            CreateIndex("dbo.AdditionalFiles", "Mail_MailId");
            AddForeignKey("dbo.AdditionalFiles", "Mail_MailId", "dbo.Mails", "MailId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ResulutionNotifiers", "Mail_MailId", "dbo.Mails");
            DropForeignKey("dbo.ResulutionNotifiers", "EmployeeFirm_Id", "dbo.EmployeeFirms");
            DropForeignKey("dbo.Mails", "Customer_CustomerID", "dbo.Customers");
            DropForeignKey("dbo.AdditionalFiles", "Mail_MailId", "dbo.Mails");
            DropIndex("dbo.ResulutionNotifiers", new[] { "Mail_MailId" });
            DropIndex("dbo.ResulutionNotifiers", new[] { "EmployeeFirm_Id" });
            DropIndex("dbo.Mails", new[] { "Customer_CustomerID" });
            DropIndex("dbo.AdditionalFiles", new[] { "Mail_MailId" });
            DropColumn("dbo.AdditionalFiles", "Mail_MailId");
            DropTable("dbo.ResulutionNotifiers");
            DropTable("dbo.Mails");
        }
    }
}
