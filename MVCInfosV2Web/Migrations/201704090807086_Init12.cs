namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init12 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HostingFiles", "Settings", c => c.String());
            DropColumn("dbo.HostingFiles", "UrlForUpload");
            DropColumn("dbo.HostingFiles", "Login");
            DropColumn("dbo.HostingFiles", "Password");
            DropColumn("dbo.HostingFiles", "Token");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HostingFiles", "Token", c => c.String());
            AddColumn("dbo.HostingFiles", "Password", c => c.String());
            AddColumn("dbo.HostingFiles", "Login", c => c.String());
            AddColumn("dbo.HostingFiles", "UrlForUpload", c => c.String());
            DropColumn("dbo.HostingFiles", "Settings");
        }
    }
}
