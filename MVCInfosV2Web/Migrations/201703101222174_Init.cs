namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AddressTables",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Address = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        City_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CityTables", t => t.City_ID)
                .Index(t => t.City_ID);
            
            CreateTable(
                "dbo.CityTables",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        City = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Country_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.CountryTables", t => t.Country_ID)
                .Index(t => t.Country_ID);
            
            CreateTable(
                "dbo.CountryTables",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Country = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Firms",
                c => new
                    {
                        FirmID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Address_ID = c.Int(),
                    })
                .PrimaryKey(t => t.FirmID)
                .ForeignKey("dbo.AddressTables", t => t.Address_ID)
                .Index(t => t.Address_ID);
            
            CreateTable(
                "dbo.Departments",
                c => new
                    {
                        DepartmentID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        Firm_FirmID = c.Int(),
                        Department_DepartmentID = c.Int(),
                    })
                .PrimaryKey(t => t.DepartmentID)
                .ForeignKey("dbo.Firms", t => t.Firm_FirmID)
                .ForeignKey("dbo.Departments", t => t.Department_DepartmentID)
                .Index(t => t.Firm_FirmID)
                .Index(t => t.Department_DepartmentID);
            
            CreateTable(
                "dbo.EmployeeFirms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Employment_date = c.DateTime(nullable: false, storeType: "date"),
                        isChief = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Employee_ApplicationUserId = c.String(maxLength: 128),
                        ApplicationUserId_Id = c.String(nullable: false, maxLength: 128),
                        EmployeeFirm_Id = c.Int(),
                        Firm_FirmID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.Employee_ApplicationUserId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId_Id, cascadeDelete: true)
                .ForeignKey("dbo.EmployeeFirms", t => t.EmployeeFirm_Id)
                .ForeignKey("dbo.Firms", t => t.Firm_FirmID, cascadeDelete: true)
                .Index(t => t.Employee_ApplicationUserId)
                .Index(t => t.ApplicationUserId_Id)
                .Index(t => t.EmployeeFirm_Id)
                .Index(t => t.Firm_FirmID);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        ApplicationUserId = c.String(nullable: false, maxLength: 128),
                        IsDeleted = c.Boolean(nullable: false),
                        Person_PersonId = c.Guid(),
                    })
                .PrimaryKey(t => t.ApplicationUserId)
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUserId)
                .ForeignKey("dbo.Person", t => t.Person_PersonId)
                .Index(t => t.ApplicationUserId)
                .Index(t => t.Person_PersonId);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        PersonId = c.Guid(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        FamilyName = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        MiddleName = c.String(nullable: false),
                        PhotoUrl = c.String(),
                        BirthData = c.DateTime(nullable: false, storeType: "date"),
                    })
                .PrimaryKey(t => t.PersonId);
            
            CreateTable(
                "dbo.CustomerEmployees",
                c => new
                    {
                        CustomerEmployeeID = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Phone = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CustomerFirm_CustomerFirmID = c.Int(),
                        Person_PersonId = c.Guid(),
                    })
                .PrimaryKey(t => t.CustomerEmployeeID)
                .ForeignKey("dbo.CustomerFirms", t => t.CustomerFirm_CustomerFirmID)
                .ForeignKey("dbo.Person", t => t.Person_PersonId)
                .Index(t => t.CustomerFirm_CustomerFirmID)
                .Index(t => t.Person_PersonId);
            
            CreateTable(
                "dbo.CustomerFirms",
                c => new
                    {
                        CustomerFirmID = c.Int(nullable: false, identity: true),
                        Company_name = c.String(nullable: false, maxLength: 100),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CustomerFirmID);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.EmployeePCs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        PCname = c.String(),
                        EmployeeFirm_Id = c.Int(),
                        OSs_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EmployeeFirms", t => t.EmployeeFirm_Id)
                .ForeignKey("dbo.OS", t => t.OSs_ID)
                .Index(t => t.EmployeeFirm_Id)
                .Index(t => t.OSs_ID);
            
            CreateTable(
                "dbo.LatestLogons",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        Login = c.String(),
                        IP = c.String(),
                        EmployeePC_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EmployeePCs", t => t.EmployeePC_ID)
                .Index(t => t.EmployeePC_ID);
            
            CreateTable(
                "dbo.OS",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IsDeleted = c.Boolean(nullable: false),
                        OSname = c.String(),
                        VersionOs = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EmployeeRoles",
                c => new
                    {
                        EmployeeRoleID = c.Int(nullable: false, identity: true),
                        Role = c.String(),
                        Identification_number = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        isDirector = c.Boolean(nullable: false),
                        EmployeeFirmID = c.Int(nullable: false),
                        Firm_FirmID = c.Int(),
                    })
                .PrimaryKey(t => t.EmployeeRoleID)
                .ForeignKey("dbo.EmployeeFirms", t => t.EmployeeFirmID, cascadeDelete: true)
                .ForeignKey("dbo.Firms", t => t.Firm_FirmID)
                .Index(t => t.EmployeeFirmID)
                .Index(t => t.Firm_FirmID);
            
            CreateTable(
                "dbo.ProjectEmployeeFirms",
                c => new
                    {
                        ID = c.Guid(nullable: false),
                        isTeamLead = c.Boolean(nullable: false),
                        isCurrent = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        StartWorking = c.DateTime(nullable: false),
                        StopWorking = c.DateTime(),
                        EmployeeFirm_Id = c.Int(nullable: false),
                        Project_ProjectID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.EmployeeFirms", t => t.EmployeeFirm_Id, cascadeDelete: true)
                .ForeignKey("dbo.Projects", t => t.Project_ProjectID, cascadeDelete: true)
                .Index(t => t.EmployeeFirm_Id)
                .Index(t => t.Project_ProjectID);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        ProjectID = c.Int(nullable: false, identity: true),
                        ProjectCode = c.String(),
                        ProjectName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        ProjectStartDate = c.DateTime(nullable: false, storeType: "date"),
                        PlannedFinishDate = c.DateTime(nullable: false, storeType: "date"),
                        Firm_FirmID = c.Int(),
                    })
                .PrimaryKey(t => t.ProjectID)
                .ForeignKey("dbo.Firms", t => t.Firm_FirmID)
                .Index(t => t.Firm_FirmID);
            
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        ContractID = c.Int(nullable: false, identity: true),
                        Cena = c.Double(nullable: false),
                        Name = c.String(),
                        ДатаЗаключенияКонтракта = c.DateTime(nullable: false),
                        ДатаНачалаВыполнения = c.DateTime(nullable: false),
                        ДатаЗавершенияВыполнения = c.DateTime(nullable: false),
                        Reason_of_contract = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        Customer_CustomerID = c.Int(),
                        Project_ProjectID = c.Int(),
                    })
                .PrimaryKey(t => t.ContractID)
                .ForeignKey("dbo.Customers", t => t.Customer_CustomerID)
                .ForeignKey("dbo.Projects", t => t.Project_ProjectID)
                .Index(t => t.Customer_CustomerID)
                .Index(t => t.Project_ProjectID);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerID = c.Int(nullable: false, identity: true),
                        isFirm = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        CustomerFirm_CustomerFirmID = c.Int(),
                    })
                .PrimaryKey(t => t.CustomerID)
                .ForeignKey("dbo.CustomerFirms", t => t.CustomerFirm_CustomerFirmID)
                .Index(t => t.CustomerFirm_CustomerFirmID);
            
            CreateTable(
                "dbo.ProjectStages",
                c => new
                    {
                        ProjectStageId = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        StartStage = c.DateTime(storeType: "date"),
                        StopStage = c.DateTime(storeType: "date"),
                        Status = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        IntersectionDate = c.Boolean(nullable: false),
                        Project_ProjectID = c.Int(),
                        ProjectStage_ProjectStageId = c.Guid(),
                        TypeOfStage_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ProjectStageId)
                .ForeignKey("dbo.Projects", t => t.Project_ProjectID)
                .ForeignKey("dbo.ProjectStages", t => t.ProjectStage_ProjectStageId)
                .ForeignKey("dbo.ProjectStageTypes", t => t.TypeOfStage_ID)
                .Index(t => t.Project_ProjectID)
                .Index(t => t.ProjectStage_ProjectStageId)
                .Index(t => t.TypeOfStage_ID);
            
            CreateTable(
                "dbo.ProjectStageTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Type = c.String(),
                        Level = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        Project_ProjectID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Projects", t => t.Project_ProjectID)
                .Index(t => t.Project_ProjectID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SettingKey = c.String(nullable: false),
                        SettingValue = c.String(nullable: false),
                        SettingDescription = c.String(),
                        isNotDelete = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EmployeeFirmDepartments",
                c => new
                    {
                        EmployeeFirm_Id = c.Int(nullable: false),
                        Department_DepartmentID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.EmployeeFirm_Id, t.Department_DepartmentID })
                .ForeignKey("dbo.EmployeeFirms", t => t.EmployeeFirm_Id, cascadeDelete: true)
                .ForeignKey("dbo.Departments", t => t.Department_DepartmentID, cascadeDelete: true)
                .Index(t => t.EmployeeFirm_Id)
                .Index(t => t.Department_DepartmentID);
            
            CreateTable(
                "dbo.ProjectDepartments",
                c => new
                    {
                        Project_ProjectID = c.Int(nullable: false),
                        Department_DepartmentID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Project_ProjectID, t.Department_DepartmentID })
                .ForeignKey("dbo.Projects", t => t.Project_ProjectID, cascadeDelete: true)
                .ForeignKey("dbo.Departments", t => t.Department_DepartmentID, cascadeDelete: true)
                .Index(t => t.Project_ProjectID)
                .Index(t => t.Department_DepartmentID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Departments", "Department_DepartmentID", "dbo.Departments");
            DropForeignKey("dbo.Departments", "Firm_FirmID", "dbo.Firms");
            DropForeignKey("dbo.ProjectEmployeeFirms", "Project_ProjectID", "dbo.Projects");
            DropForeignKey("dbo.ProjectStages", "TypeOfStage_ID", "dbo.ProjectStageTypes");
            DropForeignKey("dbo.ProjectStageTypes", "Project_ProjectID", "dbo.Projects");
            DropForeignKey("dbo.ProjectStages", "ProjectStage_ProjectStageId", "dbo.ProjectStages");
            DropForeignKey("dbo.ProjectStages", "Project_ProjectID", "dbo.Projects");
            DropForeignKey("dbo.Projects", "Firm_FirmID", "dbo.Firms");
            DropForeignKey("dbo.ProjectDepartments", "Department_DepartmentID", "dbo.Departments");
            DropForeignKey("dbo.ProjectDepartments", "Project_ProjectID", "dbo.Projects");
            DropForeignKey("dbo.Contracts", "Project_ProjectID", "dbo.Projects");
            DropForeignKey("dbo.Contracts", "Customer_CustomerID", "dbo.Customers");
            DropForeignKey("dbo.Customers", "CustomerFirm_CustomerFirmID", "dbo.CustomerFirms");
            DropForeignKey("dbo.ProjectEmployeeFirms", "EmployeeFirm_Id", "dbo.EmployeeFirms");
            DropForeignKey("dbo.EmployeeFirms", "Firm_FirmID", "dbo.Firms");
            DropForeignKey("dbo.EmployeeRoles", "Firm_FirmID", "dbo.Firms");
            DropForeignKey("dbo.EmployeeRoles", "EmployeeFirmID", "dbo.EmployeeFirms");
            DropForeignKey("dbo.EmployeePCs", "OSs_ID", "dbo.OS");
            DropForeignKey("dbo.LatestLogons", "EmployeePC_ID", "dbo.EmployeePCs");
            DropForeignKey("dbo.EmployeePCs", "EmployeeFirm_Id", "dbo.EmployeeFirms");
            DropForeignKey("dbo.EmployeeFirms", "EmployeeFirm_Id", "dbo.EmployeeFirms");
            DropForeignKey("dbo.EmployeeFirmDepartments", "Department_DepartmentID", "dbo.Departments");
            DropForeignKey("dbo.EmployeeFirmDepartments", "EmployeeFirm_Id", "dbo.EmployeeFirms");
            DropForeignKey("dbo.EmployeeFirms", "ApplicationUserId_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Employees", "Person_PersonId", "dbo.Person");
            DropForeignKey("dbo.CustomerEmployees", "Person_PersonId", "dbo.Person");
            DropForeignKey("dbo.CustomerEmployees", "CustomerFirm_CustomerFirmID", "dbo.CustomerFirms");
            DropForeignKey("dbo.EmployeeFirms", "Employee_ApplicationUserId", "dbo.Employees");
            DropForeignKey("dbo.Employees", "ApplicationUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Firms", "Address_ID", "dbo.AddressTables");
            DropForeignKey("dbo.CityTables", "Country_ID", "dbo.CountryTables");
            DropForeignKey("dbo.AddressTables", "City_ID", "dbo.CityTables");
            DropIndex("dbo.ProjectDepartments", new[] { "Department_DepartmentID" });
            DropIndex("dbo.ProjectDepartments", new[] { "Project_ProjectID" });
            DropIndex("dbo.EmployeeFirmDepartments", new[] { "Department_DepartmentID" });
            DropIndex("dbo.EmployeeFirmDepartments", new[] { "EmployeeFirm_Id" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.ProjectStageTypes", new[] { "Project_ProjectID" });
            DropIndex("dbo.ProjectStages", new[] { "TypeOfStage_ID" });
            DropIndex("dbo.ProjectStages", new[] { "ProjectStage_ProjectStageId" });
            DropIndex("dbo.ProjectStages", new[] { "Project_ProjectID" });
            DropIndex("dbo.Customers", new[] { "CustomerFirm_CustomerFirmID" });
            DropIndex("dbo.Contracts", new[] { "Project_ProjectID" });
            DropIndex("dbo.Contracts", new[] { "Customer_CustomerID" });
            DropIndex("dbo.Projects", new[] { "Firm_FirmID" });
            DropIndex("dbo.ProjectEmployeeFirms", new[] { "Project_ProjectID" });
            DropIndex("dbo.ProjectEmployeeFirms", new[] { "EmployeeFirm_Id" });
            DropIndex("dbo.EmployeeRoles", new[] { "Firm_FirmID" });
            DropIndex("dbo.EmployeeRoles", new[] { "EmployeeFirmID" });
            DropIndex("dbo.LatestLogons", new[] { "EmployeePC_ID" });
            DropIndex("dbo.EmployeePCs", new[] { "OSs_ID" });
            DropIndex("dbo.EmployeePCs", new[] { "EmployeeFirm_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.CustomerEmployees", new[] { "Person_PersonId" });
            DropIndex("dbo.CustomerEmployees", new[] { "CustomerFirm_CustomerFirmID" });
            DropIndex("dbo.Employees", new[] { "Person_PersonId" });
            DropIndex("dbo.Employees", new[] { "ApplicationUserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.EmployeeFirms", new[] { "Firm_FirmID" });
            DropIndex("dbo.EmployeeFirms", new[] { "EmployeeFirm_Id" });
            DropIndex("dbo.EmployeeFirms", new[] { "ApplicationUserId_Id" });
            DropIndex("dbo.EmployeeFirms", new[] { "Employee_ApplicationUserId" });
            DropIndex("dbo.Departments", new[] { "Department_DepartmentID" });
            DropIndex("dbo.Departments", new[] { "Firm_FirmID" });
            DropIndex("dbo.Firms", new[] { "Address_ID" });
            DropIndex("dbo.CityTables", new[] { "Country_ID" });
            DropIndex("dbo.AddressTables", new[] { "City_ID" });
            DropTable("dbo.ProjectDepartments");
            DropTable("dbo.EmployeeFirmDepartments");
            DropTable("dbo.Settings");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.ProjectStageTypes");
            DropTable("dbo.ProjectStages");
            DropTable("dbo.Customers");
            DropTable("dbo.Contracts");
            DropTable("dbo.Projects");
            DropTable("dbo.ProjectEmployeeFirms");
            DropTable("dbo.EmployeeRoles");
            DropTable("dbo.OS");
            DropTable("dbo.LatestLogons");
            DropTable("dbo.EmployeePCs");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.CustomerFirms");
            DropTable("dbo.CustomerEmployees");
            DropTable("dbo.Person");
            DropTable("dbo.Employees");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.EmployeeFirms");
            DropTable("dbo.Departments");
            DropTable("dbo.Firms");
            DropTable("dbo.CountryTables");
            DropTable("dbo.CityTables");
            DropTable("dbo.AddressTables");
        }
    }
}
