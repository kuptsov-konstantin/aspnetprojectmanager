namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init23 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CustomerFirms", "Company_name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CustomerFirms", "Company_name", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
