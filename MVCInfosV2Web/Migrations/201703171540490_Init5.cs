namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PersonalTasks", "ProjectStages_ProjectStageId", c => c.Guid());
            CreateIndex("dbo.PersonalTasks", "ProjectStages_ProjectStageId");
            AddForeignKey("dbo.PersonalTasks", "ProjectStages_ProjectStageId", "dbo.ProjectStages", "ProjectStageId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PersonalTasks", "ProjectStages_ProjectStageId", "dbo.ProjectStages");
            DropIndex("dbo.PersonalTasks", new[] { "ProjectStages_ProjectStageId" });
            DropColumn("dbo.PersonalTasks", "ProjectStages_ProjectStageId");
        }
    }
}
