namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init13 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HostingFiles", "IsNotDelete", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HostingFiles", "IsNotDelete");
        }
    }
}
