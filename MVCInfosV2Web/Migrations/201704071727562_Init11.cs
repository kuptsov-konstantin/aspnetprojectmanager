namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HostingFiles", "PlugginConnectionType", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HostingFiles", "PlugginConnectionType");
        }
    }
}
