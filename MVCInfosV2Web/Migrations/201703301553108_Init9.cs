namespace MvcInfos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init9 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdditionalFiles", "OrignFileName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AdditionalFiles", "OrignFileName");
        }
    }
}
