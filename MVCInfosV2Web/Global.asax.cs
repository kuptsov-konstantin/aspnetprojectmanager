﻿using MvcInfos.Date;
using MvcInfos.Repository.IdentityModels;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MvcInfos
{

	public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Database.SetInitializer(new ContextInitializer());
            ModelBinders.Binders.Add(typeof(DateTime?), new DateTimeModelBinder());

			if (bool.Parse(ConfigurationManager.AppSettings["MigrateDatabaseToLatestVersion"]))
			{
				var configuration = new Migrations.Configuration();
				var migrator = new DbMigrator(configuration);
				migrator.Update();
			}
		}

        protected void Session_End(object sender, EventArgs e)
        {

        }
        protected void Application_End(object sender, EventArgs e)
        {

        }
#if TEST
		public static void RegisterRoutes(RouteCollection routes)
		{
			RouteConfig.RegisterRoutes(routes);
		}
#endif
	}
}
