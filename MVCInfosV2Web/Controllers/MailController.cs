﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MvcInfos.ActiveDirectory;
using MvcInfos.ActiveDirectory.Extensions;
using MvcInfos.Attributes;
using MvcInfos.Entities;
using MvcInfos.Entities.Firm;
using MvcInfos.Entities.Mails;
using MvcInfos.Entities.UserConfigurations;
using MvcInfos.Extensions;
using MvcInfos.IdentityConfig;
using MvcInfos.Models.ViewModels.Mail;
using MvcInfos.Plugin.Interfaces;
using MvcInfos.Repository;
using static MvcInfos.Entities.UserConfigurations.NotificationPeriod;

namespace MvcInfos.Controllers
{
	[AuthorizeActiveDirectory]
	public class MailController : Controller
	{
		private IApplicationRepository DBRepository { get; set; }
		private NotificationServiceManager NotificationServiceManager { get; set; }
		private IPlugins Plugins { get; set; }
		public MailController(IApplicationRepository applicationRepository, NotificationServiceManager notificationServiceManager, IPlugins plugins)
		{
			Plugins = plugins;
			NotificationServiceManager = notificationServiceManager;
			DBRepository = applicationRepository;
		}

		// GET: Mail
		[ActionName("MailList")]
		public async Task<ActionResult> MailListAsync(int? idFirm, bool? isCurrent)
		{
			var currentUserId = User.Identity.GetUserId();
			ViewData["userId"] = currentUserId;

			if (idFirm == null)
			{
				var user = await ((DbSet<ApplicationUser>)DBRepository.Users).FindAsync(currentUserId);
				var firm = user?.Employee?.EmployeeFirms.FirstOrDefault();
				if (firm == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				idFirm = firm.Id;
			}
	

			EmployeeFirm employeeFirm = await GetEmployeeFirm(idFirm, currentUserId);
			if (employeeFirm != default(EmployeeFirm))
			{
				ViewData["firmName"] = employeeFirm.Firm.Name;
				ViewData["idFirm"] = idFirm;
				var projects = await GetProjectsListAsync(idFirm, isCurrent);
				return View(projects);
			}

			return View();
		}

		private async Task<EmployeeFirm> GetEmployeeFirm(int? idFirm, string currentUserId)
		{
			return await DBRepository.EmployeeFirms.Where(firm => firm.Firm.FirmID == idFirm).Where(ff => ff.ApplicationUserId.Id.Equals(currentUserId)).Select(p => p).FirstOrDefaultAsync();
		}

		[ActionName("Inbox")]
		public ActionResult Inbox(int idFirm, bool? isCurrent)
		{
			return this.JsonNetIso(new { }, JsonRequestBehavior.AllowGet);
		}


		[ActionName("Outbox")]
		public ActionResult Outbox(int idFirm, bool? isCurrent)
		{
			return this.JsonNetIso(new { }, JsonRequestBehavior.AllowGet);
		}

		[ActionName("GetMail")]
		public async Task<JsonResult> GetMailAsync(Guid mailId)
		{
			var mail = await DBRepository.Mails.FindAsync(mailId);
			var mailView = new MailViewModel()
			{
				Description = mail.Description,
				DocumentDate = mail.DocumentDate,
				DocumentID = mail.DocumentID,
				FinishDate = mail.FinishDate,
				MailId = mail.MailId,
				NumberDocument = mail.NumberDocument,
				RegisterDate = mail.RegisterDate
			};
			if (User.IsHasAccessRights(EnumerableRoles.administrator_server)|| User.IsHasAccessRights(EnumerableRoles.administrator_firm)|| User.IsHasAccessRights(EnumerableRoles.manager))
			{
				mailView.IsCanEdit = true;
			}
			else
			{
				mailView.IsCanEdit = false;
			}

			if (mail.UploadGroupFile != null)
			{
				mailView.UploadGroupFile = new MailUploadGroupViewModel()
				{
					GroupFileId = mail.UploadGroupFile.GroupFileId,
					UploadFiles = mail.UploadGroupFile.UploadFiles.Select(file => new MailUploadFileViewModel()
					{
						UploadFileID = file.UploadFileID
					}).ToList()
				};
			}
			if (mail.Resolution != null)
			{
				mailView.Resolution = new MailResolutionViewModel()
				{
					Comment = mail.Resolution.Comment,
					ResolutionId = mail.Resolution.ResolutionId
				};
				mailView.Resolution.ResolutionNotifier = mail.Resolution.ResolutionNotifier
					.Where(notifire => notifire.IsDelete == false)
					.Where(notifire => notifire.IsNotifi == true)
					.Select(notifire => new MailResolutionNotifierViewModel()
					{
						UserId = Guid.Parse(notifire.EmployeeFirm.ApplicationUserId.Id),
						FIO = notifire.EmployeeFirm.ApplicationUserId.Employee.Person.Fio,
						NotifyTimeKey = notifire.NotifyTimeKey,
						ResolutionNotifierId = notifire.ResolutionNotifierId
					}).ToList();
			}

			return this.JsonNetIso(mailView, JsonRequestBehavior.AllowGet);

		}
		[ActionName("GetProjects")]
		public async Task<ActionResult> GetProjectsAsync(int? idFirm, bool? isCurrent)
		{
			if (idFirm == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			List<MailProjectViewModel> myProjects = await GetProjectsListAsync(idFirm, isCurrent);
			return Json(myProjects, JsonRequestBehavior.AllowGet);
		}


		[ActionName("GetMails")]
		public async Task<JsonResult> GetMailsAsync(int idFirm, int? idProject, MailStatusEnum mailStatus, bool? isDelete = null)
		{
			var IsDelete = isDelete ?? false; 
			string currentUserId = User.Identity.GetUserId();
			var firm = await DBRepository.Firms.FindAsync(idFirm);
			if (firm == null)
			{
				return this.JsonNetIso(new { }, JsonRequestBehavior.AllowGet);
			}
			if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
			{
				var project = await DBRepository.Projects.FindAsync(idProject);
				var mails = (from mail in ((idProject != null) && (project != null)) ? project.Mails : firm.Mails
							 where mail?.MailStatuses?.Last().MailStatusEnum == mailStatus
							 where mail?.IsDelete == IsDelete
							 select new MailListItemViewModel()
							 {
								 Custumer = mail?.Customer?.CustomerFirm?.CompanyName,
								 Description = mail.Description,
								 RegisterDate = mail.RegisterDate,
								 MailId = mail.MailId,
								 HasInnerFiles = mail.UploadGroupFile != null,
								 IsEditeble = true
							 }).ToList();
				return this.JsonNetIso(mails, JsonRequestBehavior.AllowGet);
			}
			if (User.IsHasAccessRights(EnumerableRoles.administrator_firm))
			{
				var project = firm.Projects.Where(pef => pef.ProjectID == idProject).FirstOrDefault();
				var mails = (from mail in ((idProject != null) && (project != null)) ? project.Mails : firm.Mails
							 where mail.MailStatuses.Last().MailStatusEnum == mailStatus
							 where mail?.IsDelete == IsDelete
							 select new MailListItemViewModel()
							 {
								 Custumer = mail?.Customer?.CustomerFirm?.CompanyName,
								 Description = mail.Description,
								 RegisterDate = mail.RegisterDate,
								 MailId = mail.MailId,
								 HasInnerFiles = mail.UploadGroupFile != null,
								 IsEditeble = true
							 }).ToList();
				return this.JsonNetIso(mails, JsonRequestBehavior.AllowGet);
			}
			if (User.IsHasAccessRights(EnumerableRoles.manager))
			{
				var project = firm.Projects.Where(pef => pef.ProjectID == idProject).FirstOrDefault();
				var mails = (from mail in ((idProject != null) && (project != null)) ? project.Mails : firm.Mails
							 where mail.MailStatuses.Last().MailStatusEnum == mailStatus
							 where mail?.IsDelete == IsDelete
							 select new MailListItemViewModel()
							 {
								 Custumer = mail?.Customer?.CustomerFirm?.CompanyName,
								 Description = mail.Description,
								 RegisterDate = mail.RegisterDate,
								 MailId = mail.MailId,
								 HasInnerFiles = mail.UploadGroupFile != null,
								 IsEditeble = true
							 }).ToList();
				return this.JsonNetIso(mails, JsonRequestBehavior.AllowGet);
			}
			if (User.IsHasAccessRights(EnumerableRoles.project_manager))
			{
				//var project = firm.Projects.Where(proj => proj.ProjectID == idProject).Select(proj => proj).FirstOrDefault();
				var employeeFirm = await DBRepository.EmployeeFirms
					.Where(ef => ef.ApplicationUserId.Id.Equals(currentUserId))
					.Where(ef => ef.Firm.FirmID == idFirm)
					.Where(ef => ef.IsDeleted == false)
					.FirstOrDefaultAsync();

				if (employeeFirm == null)
				{
					return this.JsonNetIso(new { }, JsonRequestBehavior.AllowGet);
				}
				var project = employeeFirm.ProjectEmployeeFirms
					.Where(pef => pef.IsDeleted == false)
					.Where(pef => pef.IsTeamLead == true)
					.Where(pef => pef.Project.ProjectID == idProject)
					.FirstOrDefault();

				if (project == null)
				{
					return this.JsonNetIso(new { }, JsonRequestBehavior.AllowGet);
				}

				var mails = (from mail in project.Project.Mails
							 where mail.MailStatuses.Last().MailStatusEnum == mailStatus
							 where mail?.IsDelete == false
							 select new MailListItemViewModel()
							 {
								 Custumer = mail?.Customer?.CustomerFirm?.CompanyName,
								 Description = mail.Description,
								 RegisterDate = mail.RegisterDate,
								 MailId = mail.MailId,
								 HasInnerFiles = mail.UploadGroupFile != null
							 }).ToList();
				return this.JsonNetIso(mails, JsonRequestBehavior.AllowGet);
			}
			if (User.IsHasAccessRights(EnumerableRoles.user))
			{
				var project = firm.Projects.Where(proj => proj.ProjectID == idProject).Select(proj => proj).FirstOrDefault();
				var mails = (from mail in ((idProject != null) && (project != null)) ? project.Mails : firm.Mails
							 where mail.MailStatuses.Last().MailStatusEnum == mailStatus
							 where mail?.IsDelete == false
							 where (from notifire in mail.Resolution.ResolutionNotifier
									where notifire.EmployeeFirm.ApplicationUserId.Id.Equals(currentUserId)
									select notifire).Count() != 0
							 select new MailListItemViewModel()
							 {
								 Custumer = mail?.Customer?.CustomerFirm?.CompanyName,
								 Description = mail?.Description,
								 RegisterDate = mail.RegisterDate,
								 MailId = mail.MailId,
								 HasInnerFiles = mail.UploadGroupFile != null
							 }).ToList();
				return this.JsonNetIso(mails, JsonRequestBehavior.AllowGet);
			}
			return this.JsonNetIso(new { }, JsonRequestBehavior.AllowGet);
		}

		[ActionName("Delete")]
		[ValidateJsonAntiForgeryToken]
		[AuthorizeActiveDirectory(Roles = "administrator_firm,administrator_server,manager", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager,ManagerServerSManager")]
		public async Task<JsonResult> DeleteAsync(Guid mailId)
		{
			var mail = await DBRepository.Mails.FindAsync(mailId);
			if (mail == null)
			{
				return Json(HttpNotFound(), JsonRequestBehavior.AllowGet);
			}
			mail.IsDelete = true;
			DBRepository.Entry(mail).State = EntityState.Modified;
			await DBRepository.SaveChangesAsync();

			return Json(new { }, JsonRequestBehavior.AllowGet);
		}


		[ActionName("Recover")]
		[ValidateJsonAntiForgeryToken]
		[AuthorizeActiveDirectory(Roles = "administrator_firm,administrator_server,manager", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager,ManagerServerSManager")]
		public async Task<JsonResult> RecoverAsync(Guid mailId)
		{
			var mail = await DBRepository.Mails.FindAsync(mailId);
			if (mail == null)
			{
				return Json(HttpNotFound(), JsonRequestBehavior.AllowGet);
			}
			mail.IsDelete = false;
			DBRepository.Entry(mail).State = EntityState.Modified;
			await DBRepository.SaveChangesAsync();

			return Json(new { }, JsonRequestBehavior.AllowGet);
		}


		



		#region Новое письмо

		[ActionName("NewMail")]
		[AuthorizeActiveDirectory(Roles = "administrator_firm,administrator_server,manager", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager,ManagerServerSManager")]
		public async Task<ActionResult> NewMailAsync(int idFirm)
		{
			var employees = await GetAllEmployeeToSelectedItemAsync(idFirm);
			var projects = await GetAllProjectsToSelectedItemAsync(idFirm);
			var notify = await GetAllNotificationTimesToSelectedItemAsync();
			ViewData["idFirm"] = idFirm;
			ViewData["firmName"] = (await DBRepository.Firms.FindAsync(idFirm))?.Name;
			return View(new NewMailVewModel()
			{
				Employees = employees,
				NotifyTimes = notify,
				Projects = projects
			});
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		[ActionName("NewMail")]
		[AuthorizeActiveDirectory(Roles = "administrator_firm,administrator_server,manager", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager,ManagerServerSManager")]
		public async Task<ActionResult> NewMailAsync([Bind]NewMailVewModel parametrs, int idFirm)
		{
			if ((ModelState.IsValid) && (parametrs.RegisterDate <= parametrs.FinishDate))
			{
				var mailResult = await DBRepository.Mails.Where(mailLinq => mailLinq.DocumentID == parametrs.DocumentID).FirstOrDefaultAsync();
				if (mailResult != null)
				{
					ModelState.AddModelError("", "Номер такого документа уже существует.");
					goto ErrorNewMail;
				}

				var customer = await DBRepository.Customers
					.Where(cust => cust.Firm.FirmID == idFirm)
					.Where(cust => cust.IsDeleted == false)
					.Where(cust => cust.CustomerFirm.CompanyName.Contains(parametrs.Custumer))
					.Select(cust => cust).FirstOrDefaultAsync();
				if (customer == null)
				{
					var customerFirm = new Entities.Firm.Dictionary.CustomerFirm()
					{
						CompanyName = parametrs.Custumer
					};
					DBRepository.Entry(customerFirm).State = EntityState.Added;

					DBRepository.CustomerFirms.Add(customerFirm);
					customer = new Customer()
					{
						CustomerFirm = customerFirm
					};
				}

				var firm = await DBRepository.Firms.FindAsync(idFirm);

				var mail = new Mail()
				{
					Description = parametrs.Description,
					DocumentDate = parametrs.DocumentDate,
					NumberDocument = parametrs.NumberDocument,
					DocumentID = parametrs.DocumentID,
					FinishDate = parametrs.FinishDate,
					RegisterDate = parametrs.RegisterDate,
					Firm = firm,
					Customer = customer
				};
				customer.Mails.Add(mail);
				var mailStatus = new MailStatus()
				{
					MailStatusEnum = MailStatusEnum.InAction,
					AproveDate = DateTime.Now,
					Mail = mail
				};
				mail.MailStatuses.Add(mailStatus);
				DBRepository.Entry(mailStatus).State = EntityState.Added;
				firm.Mails.Add(mail);
				var projectId = Convert.ToInt32(parametrs.ProjectCode);
				if (projectId > -1)
				{
					var project = await DBRepository.Projects.FindAsync(projectId);
					mail.Project = project;
					project.Mails.Add(mail);
					DBRepository.Entry(project).State = EntityState.Modified;
				}
				var resolution = new Resolution()
				{
					Comment = parametrs.Comment
				};
				List<Guid> notificationsIds = new List<Guid>();
				foreach (var item in parametrs.Resolution)
				{
					var user = await ((DbSet<ApplicationUser>)DBRepository.Users).FindAsync(item);
					var employee = user.Employee.EmployeeFirms.Where(ef => ef.Firm.FirmID == idFirm).FirstOrDefault();

					var notifire = new ResolutionNotifier()
					{
						EmployeeFirm = employee,
						Mail = mail
					};

					if (parametrs.IsNotify == true)
					{
						notifire.IsNotifi = true;
						notifire.NotifyTimeKey = Convert.ToInt32(parametrs.NotifyTime);
						notificationsIds.Add(notifire.ResolutionNotifierId);
					}

					employee.ResolutionNotifiers.Add(notifire);
					resolution.ResolutionNotifier.Add(notifire);

					DBRepository.Entry(notifire).State = EntityState.Added;
				}
				DBRepository.Entry(resolution).State = EntityState.Added;
				DBRepository.Resolutions.Add(resolution);
				mail.Resolution = resolution;
				if (parametrs.GroupFiles != null)
				{
					var filesGroupId = parametrs.GroupFiles.Substring(parametrs.GroupFiles.LastIndexOf('/', parametrs.GroupFiles.Length - 2) + 1, parametrs.GroupFiles.LastIndexOf('/') - parametrs.GroupFiles.LastIndexOf('/', parametrs.GroupFiles.Length - 2) - 1);
					var fgroup = await DBRepository.GroupFiles.FindAsync(Guid.Parse(filesGroupId));
					if (fgroup != null)
					{
						mail.UploadGroupFile = fgroup;
						var plugin = await DBRepository.HostingFiles.Where(hosting => hosting.IsDefault == true).FirstOrDefaultAsync();
						foreach (var file in fgroup.UploadFiles)
						{
							if (plugin != null)
							{
								if (Plugins.LoadedPlugins.ContainsKey(plugin.PluginConnectionType))
								{
									Plugins.LoadedPlugins[plugin.PluginConnectionType].SendFile(file.Url , plugin.Settings);
									file.HostingFile = plugin;
									plugin.UploadFiles.Add(file);
									DBRepository.Entry(file).State = EntityState.Modified;
									DBRepository.Entry(plugin).State = EntityState.Modified;
								}
							}
						}

						
					}
				}
				DBRepository.Entry(mail).State = EntityState.Added;

				await DBRepository.SaveChangesAsync();

				if (notificationsIds.Count > 0)
				{
					try
					{

						await NotificationServiceManager.AddNotificationAsync(notificationsIds.ToArray());
					}
					catch (Exception e)
					{
						Trace.TraceError(e.Message);
					}
				}
				return RedirectToAction("MailList", "Mail", new { @idFirm = idFirm, @isCurrent = true });
			}

			ErrorNewMail:
			ViewData["idFirm"] = idFirm;
			parametrs.Employees = await GetAllEmployeeToSelectedItemAsync(idFirm);
			return View(parametrs);
		}
		#endregion





		public async Task<JsonResult> Customers(int idFirm, string query)
		{
			var customer = await DBRepository.Customers
				.Where(cust => cust.Firm.FirmID == idFirm)
				.Where(cust => cust.IsDeleted == false)
				.Where(cust => cust.CustomerFirm.CompanyName.Contains(query))
				.Select(cust => new { @name = cust.CustomerFirm.CompanyName, @id = cust.CustomerFirm.CustomerFirmID }).ToListAsync();

			return Json(customer, JsonRequestBehavior.AllowGet);

		}


		private async Task<List<MailProjectViewModel>> GetProjectsListAsync(int? idFirm, bool? isCurrent)
		{
			List<MailProjectViewModel> myProjects = new List<MailProjectViewModel>();


			if (User.IsHasAccessRights(EnumerableRoles.user))
			{
				var currentUserId = User.Identity.GetUserId();
				var employeeFirm = await DBRepository.EmployeeFirms
					.Where(firm => firm.Firm.FirmID == idFirm)
					.Where(ff => ff.ApplicationUserId.Id.Equals(currentUserId))
					.Select(p => p).FirstOrDefaultAsync();
				if (employeeFirm != default(EmployeeFirm))
				{
					ViewData["firmName"] = employeeFirm.Firm.Name;
					ViewData["idFirm"] = idFirm;
					if (isCurrent != null)
					{
						myProjects = employeeFirm.ProjectEmployeeFirms.Where(prj => (prj.IsDeleted == false) && (prj.IsCurrent == isCurrent)).Select(o => new MailProjectViewModel()
						{
							ProjectId = o.Project.ProjectID,
							ProjectCode = o.Project.ProjectCode
						}).ToList();
					}
					else
					{
						myProjects = employeeFirm.ProjectEmployeeFirms.Where(prj => (prj.IsDeleted == false)).Select(o => new MailProjectViewModel()
						{
							ProjectId = o.Project.ProjectID,
							ProjectCode = o.Project.ProjectCode
						}).ToList();

					}
				}
			}


			if (User.IsHasAccessRights(EnumerableRoles.project_manager))
			{
				var currentUserId = User.Identity.GetUserId();
				var employeeFirm = await DBRepository.EmployeeFirms.Where(firm => firm.Firm.FirmID == idFirm).Where(ff => ff.ApplicationUserId.Id.Equals(currentUserId)).Select(p => p).FirstOrDefaultAsync();
				if (employeeFirm != default(EmployeeFirm))
				{
					ViewData["firmName"] = employeeFirm.Firm.Name;
					ViewData["idFirm"] = idFirm;
					if (isCurrent != null)
					{
						myProjects = employeeFirm.ProjectEmployeeFirms.Where(prj => (prj.IsDeleted == false) && (prj.IsCurrent == isCurrent)).Select(o => new MailProjectViewModel()
						{
							ProjectId = o.Project.ProjectID,
							ProjectCode = o.Project.ProjectCode
						}).ToList();
					}
					else
					{
						myProjects = employeeFirm.ProjectEmployeeFirms.Where(prj => (prj.IsDeleted == false)).Select(o => new MailProjectViewModel()
						{
							ProjectId = o.Project.ProjectID,
							ProjectCode = o.Project.ProjectCode
						}).ToList();

					}
				}
			}
			if (User.IsHasAccessRights(EnumerableRoles.manager) || User.IsHasAccessRights(EnumerableRoles.administrator_firm))
			{
				var currentUserId = User.Identity.GetUserId();
				var employeeFirm = await DBRepository.EmployeeFirms.Where(firm => firm.Firm.FirmID == idFirm).Where(ff => ff.ApplicationUserId.Id.Equals(currentUserId)).Select(p => p).FirstOrDefaultAsync();
				if (employeeFirm != default(EmployeeFirm))
				{
					ViewData["firmName"] = employeeFirm.Firm.Name;
					ViewData["idFirm"] = idFirm;
					if (isCurrent != null)
					{
						myProjects = await DBRepository.Projects.Where(project => (project.PlannedFinishDate > DateTime.Now) == isCurrent).Select(o => new MailProjectViewModel()
						{
							ProjectId = o.ProjectID,
							ProjectCode = o.ProjectCode
						}).ToListAsync();
					}
					else
					{
						myProjects = await DBRepository.Projects.Select(o => new MailProjectViewModel()
						{
							ProjectId = o.ProjectID,
							ProjectCode = o.ProjectCode
						}).ToListAsync();
					}
				}
			}
			if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
			{
				ViewData["firmName"] = (await DBRepository.Firms.FindAsync(idFirm)).Name;
				ViewData["idFirm"] = idFirm;
				if (isCurrent != null)
				{
					myProjects = await DBRepository.Projects.Where(project => (project.PlannedFinishDate > DateTime.Now) == isCurrent).Select(o => new MailProjectViewModel()
					{
						ProjectId = o.ProjectID,
						ProjectCode = o.ProjectCode
					}).ToListAsync();
				}
				else
				{
					myProjects = await DBRepository.Projects.Select(o => new MailProjectViewModel()
					{
						ProjectId = o.ProjectID,
						ProjectCode = o.ProjectCode
					}).ToListAsync();
				}
			}

			return myProjects;
		}

		#region SelectedLists

		/// <summary>
		/// Получает список сотрудников по отделам
		/// </summary>
		/// <param name="idFirm">ID организации</param>
		/// <returns></returns>
		private async Task<List<SelectListItem>> GetAllEmployeeToSelectedItemAsync(int idFirm)
		{
			return await Task.Run(async () =>
			{
				var notInDep = new SelectListGroup() { Name = "Не состоит в отделе" };
				var employee = (from _department in (await DBRepository.Firms.FindAsync(idFirm)).Departments.Select(_department => new { SGroup = new SelectListGroup() { Name = _department.Name }, id = _department.DepartmentID })
								from _ef in DBRepository.Departments.Find(_department.id).EmployeeFirms
								select new SelectListItem()
								{
									Selected = false,
									Value = _ef.ApplicationUserId.Id,
									Text = _ef.ApplicationUserId?.Employee?.Person?.Fio ?? _ef.ApplicationUserId.UserName,
									Group = _department.SGroup
								}).ToList();

				var employeeWithoutDep = (await DBRepository.Firms.FindAsync(idFirm)).EmployeeFirms.Where(_ef => (_ef.Departments == null) || (_ef.Departments.Count() == 0)).Select(_ef =>
								  new SelectListItem()
								  {
									  Selected = false,
									  Value = _ef.ApplicationUserId.Id,
									  Text = _ef.ApplicationUserId?.Employee?.Person?.Fio ?? _ef.ApplicationUserId.UserName,
									  Group = notInDep
								  }).ToList();


				employee.AddRange(employeeWithoutDep);
				return employee;
			});
		}

		/// <summary>
		/// Получает список заказчиков
		/// </summary>
		/// <param name="idFirm">ID организации</param>
		/// <returns></returns>
		private async Task<List<SelectListItem>> GetAllCustomersToSelectedItemAsync(int idFirm)
		{
			return await Task.Run(async () =>
			{
				var customer = await DBRepository.Customers.Where(cust => cust.Firm.FirmID == idFirm).Where(cust => cust.IsDeleted == false).Select(cust => new SelectListItem()
				{
					Value = cust.CustomerFirm.CompanyName,
					Selected = false
				}).ToListAsync();
				return customer;
			});
		}

		private async Task<List<SelectListItem>> GetAllProjectsToSelectedItemAsync(int idFirm)
		{
			return await Task.Run(async () =>
			{
				List<SelectListItem> list = new List<SelectListItem>
				{
					new SelectListItem() { Value = "-1", Text = "Не выбрано", Selected = true }
				};
				var customer = await DBRepository.Projects.Where(cust => cust.Firm.FirmID == idFirm).Where(cust => cust.IsDeleted == false).Select(cust => new SelectListItem()
				{
					Text = cust.ProjectCode,
					Value = SqlFunctions.StringConvert((double)cust.ProjectID).Trim(),
					Selected = false
				}).ToListAsync();
				list.AddRange(customer);
				return list;
			});
		}

		private async Task<List<SelectListItem>> GetAllNotificationTimesToSelectedItemAsync()
		{
			return await Task.Run(() =>
			{
				List<SelectListItem> list = new List<SelectListItem>();
				foreach (var item in NotificationPeriod.NotificationTimes)
				{
					string mes = "";
					if (item.Value.PeriodTimes == PeriodTimes.EveryDay)
					{
						mes = $"Каждый день в {item.Value.Period.ToString(@"hh\:mm")}";
					}
					else
					{
						mes = $"Каждые {item.Value.Period.ToString(@"hh\:mm")}";
					}
					list.Add(new SelectListItem() { Value = Convert.ToString(item.Key), Text = mes });
				}
				list.Last().Selected = true;
				return list;
			});
		}

		#endregion
	}
}