﻿namespace MvcInfos.Controllers
{
	public enum EnumerableForDelete
    {
        Firm,
        Department,
        User,
        Project,
        None
    }
}
