﻿using Microsoft.AspNet.Identity;
using MvcInfos.Attributes;
using MvcInfos.Entities;
using MvcInfos.Entities.Firm;
using MvcInfos.Entities.Firm.Dictionary;
using MvcInfos.Entities.UserConfigurations;
using MvcInfos.Repository;
using MvcInfos.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using MvcInfos.Settings;
using MvcInfos.Extensions;
using MvcInfos.ActiveDirectory.Extensions;
using MvcInfos.ActiveDirectory;
using MvcInfos.IdentityConfig;

namespace MvcInfos.Controllers
{
	[AuthorizeActiveDirectory(Roles = "administrator_firm,administrator_server", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager")]
	public class AdministrationController : Controller
	{
		//private AdministrationViewModels avm = new AdministrationViewModels();
		private IApplicationRepository DBRepository { get; set; }
		private ApplicationUserManager UserManager { get; set; }
		private ApplicationRoleManager RoleManager { get; set; }

#if (DEBUG || RELEASE)
		private ActiveDirectorySetting ActiveDirectorySetting { get; set; }
		public AdministrationController(IApplicationRepository _repository, ApplicationUserManager _userManager, ApplicationRoleManager _roleManager, ActiveDirectorySetting _activeDirectorySetting)
		{
			ActiveDirectorySetting = _activeDirectorySetting;
#else
		public AdministrationController(IApplicationRepository repos, ApplicationUserManager _userManager, ApplicationRoleManager _roleManager)
        {

#endif
			RoleManager = _roleManager;
			UserManager = _userManager;
			DBRepository = _repository;

		}

		public ActionResult AdministrationFirm()
		{
			var applicationUser = DBRepository.Users.Find(User.Identity.GetUserId());
			AdministrationViewModels administrationViewModels = new AdministrationViewModels();
			//  adc.Firms.Find();
			foreach (var item in DBRepository.Users)
			{
				administrationViewModels.AllUsers.Add(new AdministrationEmployeePrintViewModels(item));
			}
			return View(administrationViewModels);

		}
		public ActionResult Index()
		{
			return RedirectToAction("Statistics");
		}

		public ActionResult Details(string id_user_to_details, EnumerableForDelete efd)
		{
			if (id_user_to_details == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			ApplicationUser au = DBRepository.Users.Find(id_user_to_details);

			if (au == null)
			{
				return HttpNotFound();
			}

			return PartialView(au);
			// return View(au);

		}


		public ActionResult EditDepartment(string id)
		{
			if (User.IsHasAccessRights(EnumerableRoles.user) || User.IsHasAccessRights(EnumerableRoles.manager))
			{
				return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
			}
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var employee = DBRepository.Users.Find(id);
			if (employee == null)
			{
				return HttpNotFound();
			}
			return View(new AdministrationEmployeePrintViewModels(employee));
		}

		[HttpPost]
		[ValidateJsonAntiForgeryToken]
		public async Task<ActionResult> EditDepartment([Bind] AdministrationModalEditDepartment data, int idFirm)
		{
			if (User.IsHasAccessRights(EnumerableRoles.user) || User.IsHasAccessRights(EnumerableRoles.manager))
			{
				return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
			}
			if (ModelState.IsValid)
			{
				if (User.IsHasAccessRights(EnumerableRoles.administrator_firm))
				{
					var currentUserId = User.Identity.GetUserId();
					var canUpdate = (await DBRepository.Firms.FindAsync(idFirm)).EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(currentUserId)).FirstOrDefault();
					if (canUpdate == null)
					{
						return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
					}
				}
				var department = (await DBRepository.Firms.FindAsync(idFirm)).Departments.Where(dep => dep.DepartmentID == data.idDepartment).FirstOrDefault();
				if (department == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
				}
				var userInDep = department.EmployeeFirms.Select(ef => ef.ApplicationUserId.Id).ToList();
				foreach (var user in userInDep)
				{
					if (data.Employees.Contains(user) == false)
					{
						var employeeForRemoveFromDep = (await DBRepository.Firms.FindAsync(idFirm)).EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(user)).FirstOrDefault();

						department.EmployeeFirms.Remove(employeeForRemoveFromDep);
					}
				}
				foreach (var item in data.Employees)
				{
					var employee = (await DBRepository.Firms.FindAsync(idFirm)).EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(item)).FirstOrDefault();
					department.EmployeeFirms.Add(employee);
				}
				department.Name = data.DepartmentName;
				DBRepository.Entry(department).State = EntityState.Modified;
				await DBRepository.SaveChangesAsync();
				return Json(new { code = HttpStatusCode.OK });
			}
			return Json(new { code = HttpStatusCode.InternalServerError });
		}

		public ActionResult Edit(string id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var employee = DBRepository.Users.Find(id);
			if (employee == null)
			{
				return HttpNotFound();
			}
			return View(new AdministrationEmployeePrintViewModels(employee));
		}

		// POST: Employees/Edit/5
		// Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
		// сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		[ActionName("Edit")]
		public ActionResult Edit([Bind(Include = "Id,Family_name,Name,Middle_name,BirthData,PhoneNumber,department_string,firm_string")] AdministrationUserModalWindow employee, string userID)
		{
			//if (ModelState.IsValid)
			//{
			//    var db_user = DBRepository.Users.Find(employee.Id);
			//    var firms_vved = employee.FirmString.Split(',');
			//    var users_in_firm = (
			//        from db_firm in DBRepository.Firms.ToList()
			//        from vved_firm in firms_vved
			//        where db_firm.Name.CompareTo(vved_firm) == 0
			//        select db_firm);
			//    foreach (var item in users_in_firm)
			//    {
			//        var item_ef = item.AddUser(db_user);
			//        db_user.Employee.AddToFirm(item_ef);
			//    }
			//    db_user.Employee.Person.MiddleName = employee.MiddleName;
			//    db_user.Employee.Person.Name = employee.Name;
			//    db_user.Employee.Person.FamilyName = employee.FamilyName;
			//    db_user.Employee.Person.BirthData = employee.BirthDate;
			//    db_user.Employee.EmployeeFirms.Last().EmploymentDate = DateTime.Now;
			//    db_user.PhoneNumber = employee.PhoneNumber;
			//    DBRepository.Entry(db_user).State = EntityState.Modified;
			//    await DBRepository.SaveChangesAsync();
			//    return RedirectToAction("AdminsServer");
			//}

			return PartialView("Admin/Modal/_modal_for_user", employee);
		}



		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> Delete(string id_user_to_delete = "", int idFirm = -1, int idDepartment = -1, EnumerableForDelete efd = EnumerableForDelete.None)
		{
			if (User.IsHasAccessRights(EnumerableRoles.administrator_server) || User.IsHasAccessRights(EnumerableRoles.administrator_firm))
			{
				switch (efd)
				{
					case EnumerableForDelete.Department:
						{
							if (idDepartment == -1)
							{
								return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Нет id отдела, удалить не возможно");
							}
							var Department_ = DBRepository.Departments.Find(idDepartment);
							if (Department_ == null)
							{
								return HttpNotFound();
							}
							DBRepository.Departments.Remove(Department_);
							await DBRepository.SaveChangesAsync();
							return RedirectToAction("Index");
						}


					case EnumerableForDelete.Firm:
						{
							if (idFirm == -1)
							{
								return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Нет id фирмы, удалить не возможно");
							}

							var Firm_ = DBRepository.Firms.Find(idFirm);
							if (Firm_ == null)
							{
								return HttpNotFound();
							}

							DBRepository.Firms.Remove(Firm_);
							await DBRepository.SaveChangesAsync();
							return RedirectToAction("Index");
						}
					case EnumerableForDelete.User:
						{
							if (id_user_to_delete.CompareTo("") == 0)
							{
								return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Нет id пользователя, удалить не возможно");
							}

							var Users_ = DBRepository.Users.Find(id_user_to_delete);
							if (Users_ == null)
							{
								return HttpNotFound();
							}
							var employee_ = DBRepository.Employees.Find(id_user_to_delete);
							if (employee_ == null)
							{
								//return HttpNotFound();
							}
							else
							{
								DBRepository.Employees.Remove(employee_);
							}
							DBRepository.Users.Remove(Users_);
							await DBRepository.SaveChangesAsync();
							return RedirectToAction("Index");
						}
					default:
						break;
				}
			}
			else
			{
			}
			return View();
		}

		[ActionName("_modal_for_user")]
		public async Task<ActionResult> ModalForUser([Bind(Prefix = "id_user_to_details")]string idUserToDetails, EnumerableForDelete efd)
		{
			if (idUserToDetails == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var user_ = DBRepository.Users.Find(idUserToDetails);
			if (user_ == null)
			{
				return HttpNotFound();
			}
			var Result = await Task.Run(async () =>
		   {
			   var empl1 = await DBRepository.Employees.Where(empl => empl.ApplicationUserId.Equals(idUserToDetails)).Select(_employee =>
			   new AdministrationUserModalWindow()
			   {
				   Email = _employee.ApplicationUser.Email,
				   UserName = _employee.ApplicationUser.UserName,
				   FamilyName = (_employee.Person == null) ? "" : _employee.Person.FamilyName,
				   Name = (_employee.Person == null) ? "" : _employee.Person.Name,
				   MiddleName = (_employee.Person == null) ? "" : _employee.Person.MiddleName,
				   BirthDate = (_employee.Person == null) ? DateTime.Now : _employee.Person.BirthData,
				   PhoneNumber = _employee.ApplicationUser.PhoneNumber,
				   Id = _employee.ApplicationUserId /*,
               firms = DBRepository.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(id_user_to_details)).Select(firm => new {
                    firmName = firm.Firm.Name,
                    employmentDate = firm.Employment_date,
                    depart = string.Join(",",(firm.Departments.Select(dep => dep.Name))),
                    dolzn = string.Join(",", (firm.EmployeeRoles.Select(rol => rol.Role)))
                })*/
			   }).FirstOrDefaultAsync();
			   //if (empl1 != null)
			   //{


			   // var firms = await  DBRepository.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(idUserToDetails)).Select(firm => new
			   // {
			   //  firmName = firm.Firm.Name,
			   //  employmentDate = firm.EmploymentDate,
			   //  depart = string.Join(",", (firm.Departments.Select(dep => dep.Name))),
			   //  dolzn = string.Join(",", (firm.EmployeeRoles.Select(rol => rol.Role)))
			   // }).ToListAsync();
			   // empl1.firms = firms.Select(firm => firm.firmName).ToList();
			   // empl1.


			   //}
			   return empl1 ?? new AdministrationUserModalWindow();
		   });
			//return PartialView("Admin/Modal/_modal_for_user", task.Result);

			return this.JsonNetIso(Result);
		}

		[ActionName("_modal_for_user_edit")]
		public async Task<ActionResult> ModalViewUserEdit([Bind(Prefix = "id_user_to_details")]Guid idUserToDetails, EnumerableForDelete efd, int firmId = -1)
		{
			var user_ = await ((DbSet<ApplicationUser>)DBRepository.Users).FindAsync(idUserToDetails.ToString());
			if (user_ == null)
			{
				return HttpNotFound();
			}
			ViewData["isValid"] = false;

			var empl1 = await DBRepository.Employees
				.Where(empl => empl.ApplicationUserId.Equals(idUserToDetails))
				.Select(_employee => new
				{
					EMail = _employee.ApplicationUser.Email,
					UserName = _employee.ApplicationUser.UserName,
					Family_name = (_employee.Person == null) ? "" : _employee.Person.FamilyName,
					Name = (_employee.Person == null) ? "" : _employee.Person.Name,
					Middle_name = (_employee.Person == null) ? "" : _employee.Person.MiddleName,
					BirthData = (_employee.Person == null) ? DateTime.Now : _employee.Person.BirthData,
					PhoneNumber = _employee.ApplicationUser.PhoneNumber ?? "",
					IdUser = _employee.ApplicationUserId

				}).FirstOrDefaultAsync();

			var firm1 = (firmId == -1) ? null : await DBRepository.EmployeeFirms
				.Where(ef => ef.ApplicationUserId.Id.Equals(idUserToDetails))
				.Where(ef => ef.Firm.FirmID == firmId)
				.Select(firm => new
				{
					firmName = firm.Firm.Name,
					employmentDate = firm.EmploymentDate,
					depart = (firm.Departments.Select(dep => dep.Name)),
					dolzn = (firm.EmployeeRoles.Select(rol => rol.Role))
				}).FirstOrDefaultAsync();

			Func<int, object, string> kf = (firmId1, d) =>
			{
				return "";
			};
			;
			AdministrationUserModalWindow administrationUserModalWindow = new AdministrationUserModalWindow()
			{
				Name = empl1.Name,
				MiddleName = empl1.Middle_name,
				FamilyName = empl1.Family_name,
				Email = empl1.EMail,
				BirthDate = empl1.BirthData,
				UserName = empl1.UserName,
				PhoneNumber = empl1.PhoneNumber,
				Id = empl1.IdUser
				//,DepartmentString = (firmId == -1) ? "" : (firm1 != null) ? string.Join(",", firm1.depart) : "",
				//Dolznost = (firmId == -1) ? "" : (firm1 != null) ? string.Join(",", firm1.depart) : "",
				//firm_string = (firmId == -1) ? string.Join(",", empl1.firms) : empl1.firm.firmName
			};

			ViewData["firmId"] = firmId;
			if (firmId > -1)
			{
				//administrationUserModalWindow.EmployeeDate = user_?.Employee?.GetEF(firmId)?.EmploymentDate;
			}
			return PartialView("Admin/Modal/_modal_for_user/_modal_for_user_edit", administrationUserModalWindow);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		[ActionName("_modal_for_user_edit")]
		public ActionResult ModalViewUserEdit([Bind(Include = "Id,Family_name,Name,Middle_name,BirthDate,PhoneNumber,department_string,firm_string,EmployeeDate,dolznost_string")] AdministrationUserModalWindow employee, int firmId)
		{
			if (firmId < 0)
			{
				ModelState.Remove("EmployeeDate");
			}
			if (ModelState.IsValid)
			{

				//var db_user = DBRepository.Users.Find(employee.Id);
				//if (db_user.Employee == null)
				//{
				//    db_user.Employee = new Employee();
				//}
				//if (employee.FirmString == null)
				//{
				//    var ef_ = db_user.Employee.EmployeeFirms.ToList();
				//    if (ef_.Count > 0)
				//    {

				//    }
				//}
				//else
				//{
				//    var firms_vved = employee.FirmString.Split(',');
				//    var users_in_firm = (from db_firm in DBRepository.Firms.ToList()
				//                         from vved_firm in firms_vved
				//                         where db_firm.Name.CompareTo(vved_firm.Trim()) == 0
				//                         select db_firm);
				//    foreach (var item in users_in_firm)
				//    {
				//        var item_ef = item.AddUser(db_user);
				//        db_user.Employee.AddToFirm(item_ef);
				//    }

				//}
				//if (employee.DepartmentString == null)
				//{

				//}
				//else
				//{
				//    if (firmId > 0)
				//    {
				//        var departm_vved = employee.DepartmentString.Split(',');
				//        var dep_vved = from dep_s in departm_vved
				//                       select dep_s.Trim();
				//        foreach (var dep_name in dep_vved)
				//        {
				//            DBRepository.Firms.Find(firmId).AddDepartment(dep_name.Trim());
				//        }
				//        var dep_in_firm = (from db_depa in DBRepository.Firms.Find(firmId).Departments.ToList()
				//                           from dep in dep_vved
				//                           where db_depa.Name.CompareTo(dep) == 0
				//                           select db_depa).ToList();
				//        foreach (var dep in dep_in_firm)
				//        {
				//            dep.AddUser(db_user.Employee.GetEF(firmId));
				//            db_user.Employee.AddToFirm(db_user.Employee.GetEF(firmId));
				//        }
				//        DBRepository.Firms.Find(firmId).GetEF(employee.Id).EmploymentDate = employee.EmployeeDate.Value;

				//        var dolzn_vved = employee.DolznostString.Split(',');
				//        var dol_vved = from dol_s in dolzn_vved
				//                       select dol_s.Trim();
				//        foreach (var dol_name in dol_vved)
				//        {
				//            DBRepository.Users.Find(employee.Id).Employee.AddDolzn(firmId, dol_name.Trim());
				//        }
				//    }
				//}
				//if (db_user.Employee.Person == null)
				//{
				//    db_user.Employee.Person = new Person() { MiddleName = employee.MiddleName, Name = employee.Name, FamilyName = employee.FamilyName, BirthData = employee.BirthDate };
				//}
				//else
				//{
				//    db_user.Employee.Person.MiddleName = employee.MiddleName;
				//    db_user.Employee.Person.Name = employee.Name;
				//    db_user.Employee.Person.FamilyName = employee.FamilyName;
				//    db_user.Employee.Person.BirthData = employee.BirthDate;
				//}

				//// dd.Email = employee.Email;
				//if (db_user.Employee.EmployeeFirms.Count > 0)
				//{
				//    //db_user.Employee.EmployeeFirms.Last().Employment_date = DateTime.Now;
				//}
				//db_user.PhoneNumber = employee.PhoneNumber;
				//DBRepository.Entry(db_user).State = EntityState.Modified;
				//await DBRepository.SaveChangesAsync();
				//ViewData["firmId"] = firmId;
				//ViewData["isValid"] = true;
				//ViewData["success"] = true;
				return PartialView("Admin/Modal/_modal_for_user/_modal_for_user_edit", employee);

			}
			ViewData["firmId"] = firmId;
			ViewData["success"] = false;
			ViewData["isValid"] = false;
			return PartialView("Admin/Modal/_modal_for_user/_modal_for_user_edit", employee);
		}


		[ActionName("_modal_for_user_add")]
		public async Task<ActionResult> ModalForUserAddAsync(int idFirm, EnumerableForDelete efd)
		{
			AdministrationAddEmployee_from_server au = new AdministrationAddEmployee_from_server()
			{
				Items = User.GetRoles(RoleManager.Roles),
				idFirm = idFirm
			};
			if (idFirm < 0)
			{
				var _firms = await (from _firm in DBRepository.Firms
									select _firm.Name).ToListAsync();
				au.firms = _firms;
			}
			else
			{
				var _deps = await (from _dep in DBRepository.Departments
								   where _dep.Firm.FirmID == idFirm
								   select _dep.Name).ToListAsync();

				var dd2 = new List<SelectListItem>();
				au.departmetns = _deps;
				dd2 = _deps.Select(dep => new SelectListItem() { Value = dep }).ToList();

				ViewData["id_firm"] = idFirm;
				ViewData["NameFirm"] = DBRepository.Firms.Find(idFirm).Name;
				ViewData["AllDepartmentJSON"] = this.Json(_deps.ToArray());
				ViewData["AllDepartmentStri"] = _deps.ToArray();
				ViewData["AllDepartment"] = new SelectList(dd2);
			}
			ViewData["isValid"] = false;
			return PartialView("Admin/Modal/_modal_for_user/_modal_for_user_add", au);

		}

		private bool FindNameDepartment(ICollection<Entities.Firm.Department> L_dep, string nameDepartment)
		{

			foreach (var item in L_dep)
			{
				if (item.Name.CompareTo(nameDepartment) == 0)
				{
					return true;
				}
			}
			return false;
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<ActionResult> _modal_for_user_add([Bind(Include = "Id,Family_name,Name,Middle_name,BirthData,PhoneNumber,department_string,firm_string,EmploymentData,Password,UserName,Email,ConfirmPassword,selectedItem")]AdministrationAddEmployee_from_server employee, int idFirm, EnumerableForDelete efd)
		{

			employee.Items = User.GetRoles(RoleManager.Roles);
			if (ModelState.IsValid)
			{

				var user = new ApplicationUser { UserName = employee.UserName, Email = employee.Email };
				var result = await UserManager.CreateAsync(user, employee.Password);
				if (result.Succeeded)
				{

					UserManager.AddToRole(user.Id, employee.selectedItem);
					Person per = new Person() { FamilyName = employee.Family_name, MiddleName = employee.Middle_name, Name = employee.Name, BirthData = employee.BirthData };
					user.Employee = new Employee() { Person = per, ApplicationUserId = user.Id };
					if (user.Employee.EmployeeFirms == null)
					{
						user.Employee.EmployeeFirms = new HashSet<EmployeeFirm>();
					}
					if (employee.firm_string.Length > 0)
					{

						var users_in_firm = (from db_firm1 in DBRepository.Firms.ToList()
											 where db_firm1.Name.CompareTo(employee.firm_string) == 0
											 select db_firm1).ToList();

						foreach (var item in users_in_firm)
						{
							var rez_add_touser = item.AddUser(user);
							rez_add_touser.EmploymentDate = employee.EmploymentData;
							var rez_add_Department = item.AddDepartment(employee.department_string);
							if (rez_add_Department != null)
							{
								user.Employee.AddDepartment(item, rez_add_Department as Entities.Firm.Department);
							}
							var rez_add_tofirm = user.Employee.AddToFirm(rez_add_touser);
						}
					}
					DBRepository.Entry(user).State = EntityState.Modified;
					await DBRepository.SaveChangesAsync();


					ViewData["isValid"] = true;
					ViewData["success"] = true;
					return PartialView("Admin/Modal/_modal_for_user/_modal_for_user_add", employee);
				}
				AddErrors(result);
			}
			ViewData["success"] = false;
			ViewData["isValid"] = false;
			return PartialView("Admin/Modal/_modal_for_user/_modal_for_user_add", employee);

		}
		public async Task<ActionResult> _modal_for_departments(int? id_firm)
		{
			if (id_firm == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			var firm_ = await DBRepository.Firms.FindAsync(id_firm);
			if (firm_ == null)
			{
				return HttpNotFound();
			}
			AdministrationFirmDepartmentViewModels au = new AdministrationFirmDepartmentViewModels(new Entities.Firm.Department(), firm_.FirmID, firm_.Name, new List<string>());
			return PartialView("Admin/Modal/_modal_for_departments", au);
		}



		public ActionResult _modal_for_department_function(int? id_firm, int id_dep)
		{
			if (id_firm == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}

			var firm_ = DBRepository.Firms.Find(id_firm.Value);
			if (firm_ == null)
			{
				return HttpNotFound();
			}
			var dep_ = DBRepository.Departments.Find(id_dep);
			if (dep_ == null)
			{
				return HttpNotFound();
			}
			var fios = dep_.EmployeeFirms.Select(ef => ef.ApplicationUserId?.Employee?.Person?.Fio).ToList();
			fios.Remove(null);
			AdministrationFirmDepartmentViewModels au = new AdministrationFirmDepartmentViewModels(dep_, firm_.FirmID, firm_.Name, fios);
			return PartialView("Admin/Modal/_modal_for_department_function", au);

		}

		public ActionResult _modal_firm_del(/*int id_firm, EnumerableForDelete efd*/)
		{
			return PartialView("Admin/Admins/AdminsFirms/Modal/_modal_firm_del"/*, new ModalFirmDelInfo() { idFirm = id_firm, NameFirm = _firm.Name }*/);

		}

		private async Task<List<SelectListItem>> GetAllEmployeeToSelectedItemAsync(int idFirm, ICollection<string> directors)
		{
			return await Task.Run(async () =>
			{
				var departments = (await DBRepository.Firms.FindAsync(idFirm)).Departments.Select(_department => new { SGroup = new SelectListGroup() { Name = _department.Name }, id = _department.DepartmentID });
				var defaultGr = new SelectListGroup() { Name = "Не состоит в отделе" };
				var employee = (from _department in departments
								from _ef in DBRepository.Departments.Find(_department.id).EmployeeFirms
								select new SelectListItem()
								{
									Selected = directors.Where(dir => dir.Equals(_ef.ApplicationUserId.Id)).Select(dir => dir).Count() > 0,
									Value = _ef.ApplicationUserId.Id,
									Text = _ef.ApplicationUserId?.Employee?.Person?.Fio ?? _ef.ApplicationUserId.UserName,
									Group = _department.SGroup
								}).ToList();

				var employeeWithoutDep = (await DBRepository.Firms.FindAsync(idFirm)).EmployeeFirms.Where(_ef => (_ef.Departments == null) || (_ef.Departments.Count() == 0)).Select(_ef =>
								  new SelectListItem()
								  {
									  Selected = directors.Where(dir => dir.Equals(_ef.ApplicationUserId.Id)).Select(dir => dir).Count() > 0,
									  Value = _ef.ApplicationUserId.Id,
									  Text = _ef.ApplicationUserId?.Employee?.Person?.Fio ?? _ef.ApplicationUserId.UserName,
									  Group = defaultGr
								  }).ToList();


				employee.AddRange(employeeWithoutDep);
				return employee;
			});
		}
		public async Task<List<SelectListItem>> GetAllEmployeeToSelectedItemAsync(int idFirm)
		{
			return await Task.Run(async () =>
			{
				var departments = (await DBRepository.Firms.FindAsync(idFirm)).Departments.Select(_department => new { SGroup = new SelectListGroup() { Name = _department.Name }, id = _department.DepartmentID });
				var defaultGr = new SelectListGroup() { Name = "Не состоит в отделе" };
				var employee = (from _department in departments
								from _ef in DBRepository.Departments.Find(_department.id).EmployeeFirms
								select new SelectListItem()
								{
									Selected = false,
									Value = _ef.ApplicationUserId.Id,
									Text = _ef.ApplicationUserId?.Employee?.Person?.Fio ?? _ef.ApplicationUserId.UserName,
									Group = _department.SGroup
								}).ToList();

				var employeeWithoutDep = (await DBRepository.Firms.FindAsync(idFirm)).EmployeeFirms.Where(_ef => (_ef.Departments == null) || (_ef.Departments.Count() == 0)).Select(_ef =>
								  new SelectListItem()
								  {
									  Selected = false,
									  Value = _ef.ApplicationUserId.Id,
									  Text = _ef.ApplicationUserId?.Employee?.Person?.Fio ?? _ef.ApplicationUserId.UserName,
									  Group = defaultGr
								  }).ToList();


				employee.AddRange(employeeWithoutDep);
				return employee;
			});
		}
		public async Task<List<SelectListItem>> GetAllEmployeeSelectDepartmentToSelectedItemAsync(int idFirm, int idDep)
		{
			return await Task.Run(async () =>
			{
				var departments = (await DBRepository.Firms.FindAsync(idFirm)).Departments.Select(_department => new { SGroup = new SelectListGroup() { Name = _department.Name }, id = _department.DepartmentID });
				var defaultGr = new SelectListGroup() { Name = "Не состоит в отделе" };
				var employee = (from _department in departments
								from _ef in DBRepository.Departments.Find(_department.id).EmployeeFirms
								select new SelectListItem()
								{
									Selected = (idDep == _department.id),
									Value = _ef.ApplicationUserId.Id,
									Text = _ef.ApplicationUserId?.Employee?.Person?.Fio ?? _ef.ApplicationUserId.UserName,
									Group = _department.SGroup
								}).ToList();

				var employeeWithoutDep = (await DBRepository.Firms.FindAsync(idFirm)).EmployeeFirms.Where(_ef => (_ef.Departments == null) || (_ef.Departments.Count() == 0)).Select(_ef =>
								  new SelectListItem()
								  {
									  Selected = false,
									  Value = _ef.ApplicationUserId.Id,
									  Text = _ef.ApplicationUserId?.Employee?.Person?.Fio ?? _ef.ApplicationUserId.UserName,
									  Group = defaultGr
								  }).ToList();


				employee.AddRange(employeeWithoutDep);
				return employee;
			});
		}
		public async Task<ActionResult> GetEmployeeInfoWithDirector(int id_firm)
		{
			var _firm = await DBRepository.Firms.FindAsync(id_firm);
			var res = (await DBRepository.Firms.FindAsync(id_firm)).EmployeeFirms.AsParallel().Where(ef_ => (ef_.EmployeeRoles.AsParallel().Where(role => role.IsDirector == true).AsParallel().Select(role => role.Role)).AsParallel().Count() > 0).AsParallel().Select(_ef => _ef.ApplicationUserId.Id).AsParallel().ToList();
			var select = await GetAllEmployeeToSelectedItemAsync(id_firm, res);

			return Json(select, JsonRequestBehavior.AllowGet);
		}

		public async Task<ActionResult> _DropdownDirectorsPartial(int id_firm)
		{
			var _firm = await DBRepository.Firms.FindAsync(id_firm);
			var res = (await DBRepository.Firms.FindAsync(id_firm)).EmployeeFirms.AsParallel().Where(ef_ => (ef_.EmployeeRoles.AsParallel().Where(role => role.IsDirector == true).AsParallel().Select(role => role.Role)).Count() > 0).AsParallel().Select(_ef => _ef.ApplicationUserId.Id).AsParallel().ToList();
			var select = await GetAllEmployeeToSelectedItemAsync(id_firm, res);
			return PartialView("Admin/Admins/AdminsFirms/partial/_DropdownDirectorsPartial", new ForDropdownMany() { Items = select, Name = "Директора" });
		}
		public async Task<ActionResult> _DropdownEmployeeDepartmentPartial(int idFirm, int idDepartment)
		{

			var _firm = await DBRepository.Firms.FindAsync(idFirm);
			var select = await GetAllEmployeeSelectDepartmentToSelectedItemAsync(idFirm, idDepartment);
			return PartialView("Admin/Admins/AdminsFirms/partial/_DropdownDirectorsPartial", new ForDropdownMany() { Items = select, Name = "Сотрудники отдела" });
		}

		//TODO: Ввести штатное расписание
		public async Task<ActionResult> _modal_firm_edit(int id_firm, EnumerableForDelete efd)
		{

			var _firm = await DBRepository.Firms.FindAsync(id_firm);
			var res = (await DBRepository.Firms.FindAsync(id_firm)).EmployeeFirms.Where(ef_ => (ef_.EmployeeRoles.Where(role => role.IsDirector == true).Select(role => role.Role)).Count() > 0).Select(_ef => _ef.ApplicationUserId.Id).ToList();
			var select = await GetAllEmployeeToSelectedItemAsync(id_firm, res);



			ViewData["isValid"] = false;
			ViewData["success"] = false;
			return PartialView("Admin/Admins/AdminsFirms/Modal/_modal_firm_edit", new ForDropdownMany() { Items = select });

		}





		[HttpPost]
		// [ValidateAntiForgeryToken]
		public async Task<ActionResult> _modal_firm_edit([Bind]ModalFirmEdit modal)
		{
			if (ModelState.IsValid)
			{

				var _firm = await DBRepository.Firms.FindAsync(modal.idFirm);
				bool save = false;
				if (_firm.Name.CompareTo(modal.NameFirm) != 0)
				{
					save = true;
					_firm.Name = modal.NameFirm;
				}
				var res = (await DBRepository.Firms.FindAsync(modal.idFirm)).EmployeeFirms.Where(ef_ => (ef_.EmployeeRoles.Where(role => role.IsDirector == true).Select(role => role.Role)).Count() > 0).Select(_ef => _ef.ApplicationUserId.Id).ToList();
				var ef = (await DBRepository.Firms.FindAsync(modal.idFirm)).EmployeeRoles.Where(role => role.IsDirector == true).Select(role => role.EmployeeFirm);


				/*    var newDirs = 
                    from ef in (await DBRepository.Firms.FindAsync(modal.idFirm)).EmployeeFirms
                    from newDir in modal.Directors


                    */


				/*  (await DBRepository.Firms.FindAsync(modal.idFirm)).EmployeeFirms
                      .Where(ef_ => (ef_.EmployeeRoles.Where(role => role.isDirector == true).Select(role => role.Role)).Count() > 0)
                      .Select(_ef => _ef.ApplicationUserId.Id).ToList();
  */
				if (save == true)
				{
					DBRepository.Entry(_firm).State = EntityState.Modified;
					await DBRepository.SaveChangesAsync();
					ViewData["isValid"] = true;
					ViewData["success"] = true;
				}
				return PartialView("Admin/Admins/AdminsFirms/Modal/_modal_firm_edit", modal);

			}
			ViewData["success"] = false;
			ViewData["isValid"] = false;
			return PartialView("Admin/Admins/AdminsFirms/Modal/_modal_firm_edit", modal);
		}

		public ActionResult _modal_firm_view(int id_firm, EnumerableForDelete efd)
		{

			var task = Task.Run(async () =>
			 {
				 var _firm = await DBRepository.Firms.FindAsync(id_firm);
				 var dirs = await Task.Run(() =>
				 {
					 return (from ef in _firm.EmployeeFirms
							 from er in ef.EmployeeRoles
							 where er.IsDirector == true
							 select ef.ApplicationUserId.Employee.Person.Fio).ToList();
				 });
				 return new { idFirm = _firm.FirmID, firmName = _firm.Name, dirs = dirs, employeeCount = _firm.EmployeeFirms.Count() };

			 });

			Task.WaitAny(task);


			return PartialView("Admin/Admins/AdminsFirms/Modal/_modal_firm_view", new ModalFirmView()
			{
				ColVoSotr = task.Result.employeeCount,
				Directors = string.Join(",", task.Result.dirs),
				IdFirm = task.Result.idFirm,
				Name = task.Result.firmName


			});
		}

		public async Task<ActionResult> AddDepartment([Bind(Include = "Name,idFirm,FirmName")] AdministrationFirmDepartmentViewModels model)
		{
			if (ModelState.IsValid)
			{

				var _firm = await DBRepository.Firms.FindAsync(model.IdFirm);
				_firm.AddDepartment(model.Name);
				DBRepository.Entry(_firm).State = EntityState.Modified;
				await DBRepository.SaveChangesAsync();
				return RedirectToAction("Index");

			}
			return PartialView("Admin/Modal/_modal_for_user", model);
		}
		public ActionResult _Employee(int firmId = -1, int departmentId = -1)
		{
			if (firmId < 0)
			{
				if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
				{
					return PartialView();
				}
				else
				{
					return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
				}
			}
			else
			{
				ViewData["firmId"] = firmId;
				return PartialView();
			}
		}

		public async Task<ActionResult> _DepartmentIds(int idFirm, int idDepartment = -1)
		{
			if (User.IsHasAccessRights(EnumerableRoles.administrator_firm))
			{
				var currentUserId = User.Identity.GetUserId();
				var canUpdate = (await DBRepository.Firms.FindAsync(idFirm)).EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(currentUserId)).FirstOrDefault();
				if (canUpdate == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.NotFound);
				}
			}
			if (idDepartment < 0)
			{
				var departmentsRoot = (await DBRepository.Firms.FindAsync(idFirm)).Departments.Select(_dep => new { departmentId = _dep.DepartmentID, canSubDep = _dep.SubDepartments.Count() > 0 });
				return Json(departmentsRoot, JsonRequestBehavior.AllowGet);
			}
			else
			{
				var department = (await DBRepository.Firms.FindAsync(idFirm)).Departments.Where(_dep => _dep.DepartmentID == idDepartment).FirstOrDefault();
				if (department == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.NotFound);
				}

				var departmentsNode = department.SubDepartments.Select(_subDep => new { departmentId = _subDep.DepartmentID, parentId = idDepartment, canSubDep = _subDep.SubDepartments.Count() > 0 });
				return Json(departmentsNode, JsonRequestBehavior.AllowGet);
			}
		}
		public async Task<ActionResult> _DepartmentRowPartial(int idFirm, int nodeDep, int parentDep = -1)
		{
			if (User.IsHasAccessRights(EnumerableRoles.administrator_firm))
			{
				var currentUserId = User.Identity.GetUserId();
				var canUpdate = (await DBRepository.Firms.FindAsync(idFirm)).EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(currentUserId)).FirstOrDefault();
				if (canUpdate == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.NotFound);
				}
			}

			if (parentDep < 0)
			{
				var department = (await DBRepository.Firms.FindAsync(idFirm)).Departments.Where(_dep => _dep.DepartmentID == nodeDep).FirstOrDefault();
				if (department == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.NotFound);
				}
				return PartialView("Admin/Admins/AdminsFirms/partial/_DepartmentRowPartial", new DepartmentNode()
				{
					idFirm = idFirm,
					idNode = nodeDep,
					NameDepartment = department.Name,
					ColVoInDep = department.EmployeeFirms.Count()
				});
			}
			else
			{
				var parentDepartment = (await DBRepository.Firms.FindAsync(idFirm)).Departments.Where(_dep => _dep.DepartmentID == parentDep).FirstOrDefault();
				if (parentDepartment == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.NotFound);
				}
				var nodeDepartment = parentDepartment.SubDepartments.Where(_dep => _dep.DepartmentID == nodeDep).FirstOrDefault();
				if (nodeDepartment == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.NotFound);
				}
				return PartialView("Admin/Admins/AdminsFirms/partial/_DepartmentRowPartial", new DepartmentNode()
				{
					idFirm = idFirm,
					idNode = nodeDep,
					NameDepartment = nodeDepartment.Name,
					ColVoInDep = nodeDepartment.EmployeeFirms.Count(),
					Parent = parentDep
				});
			}

		}
		public async Task<ActionResult> _EmploFirmyeeIds(int firmId = -1, int departmentId = -1)
		{
			if (firmId < 0)
			{
				if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
				{
					return Json(await DBRepository.Users.Select(user => user.Id).ToListAsync(), JsonRequestBehavior.AllowGet);
				}
				else
				{
					return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
				}

			}
			else
			{
				ViewData["firmId"] = firmId;
				var firm = await DBRepository.Firms.FindAsync(firmId);
				if (departmentId < 0)
				{
					var _employees = await (from _employee in DBRepository.EmployeeFirms
											where _employee.Firm.FirmID == firmId
											select _employee.ApplicationUserId.Id).ToListAsync();
					return Json(_employees, JsonRequestBehavior.AllowGet);

				}
				else
				{
					var _employees = await (from _employee in DBRepository.EmployeeFirms
											from _dep in _employee.Departments
											where _employee.Firm.FirmID == firmId
											where _dep.DepartmentID == departmentId
											select _employee.ApplicationUserId.Id).ToListAsync();
					return Json(_employees, JsonRequestBehavior.AllowGet);
				}
			}
		}

		public async Task<ActionResult> _EmployeePartial(string userId, int firmId = -1)
		{
			var isAdminServer = (await UserManager.GetRolesAsync(userId)).Contains(EnumerableRoles.administrator_server);
			var isAdminFirm = (await UserManager.GetRolesAsync(userId)).Contains(EnumerableRoles.administrator_server);
			if (User.IsHasAccessRights(EnumerableRoles.administrator_server) || User.IsHasAccessRights(EnumerableRoles.administrator_firm))
			{
				if (firmId > 0)
				{


					var _employees = await (from _employee in DBRepository.EmployeeFirms
											where _employee.Firm.FirmID == firmId
											where _employee.ApplicationUserId.Id.Equals(userId)
											select new AdministrationEmployeePrintViewModels()
											{
												EMail = _employee.ApplicationUserId.Email,
												UserName = _employee.ApplicationUserId.UserName,
												FamilyName = (_employee.ApplicationUserId.Employee == null) ? "" : ((_employee.ApplicationUserId.Employee.Person == null) ? "" : _employee.ApplicationUserId.Employee.Person.FamilyName),
												Name = (_employee.ApplicationUserId.Employee == null) ? "" : ((_employee.ApplicationUserId.Employee.Person == null) ? "" : _employee.ApplicationUserId.Employee.Person.Name),
												MiddleName = (_employee.ApplicationUserId.Employee == null) ? "" : ((_employee.ApplicationUserId.Employee.Person == null) ? "" : _employee.ApplicationUserId.Employee.Person.MiddleName),
												BirthData = (_employee.ApplicationUserId.Employee == null) ? DateTime.Now : ((_employee.ApplicationUserId.Employee.Person == null) ? DateTime.Now : _employee.ApplicationUserId.Employee.Person.BirthData),
												IdUser = _employee.ApplicationUserId.Id,
												IsADUser = _employee.ApplicationUserId.IsActiveDirectory,
												IsNotDelete = isAdminFirm || isAdminServer
											}).FirstOrDefaultAsync();
					ViewData["firmId"] = firmId;
					if (_employees == null)
					{
						return HttpNotFound("Не найден пользователь в указанной организации");
					}
					else
					{
						return this.JsonNetIso(_employees, JsonRequestBehavior.AllowGet);
					}
				}
				else
				{
					if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
					{
						var _employees = await (from _user in DBRepository.Users
												where _user.Id.Equals(userId)
												select new AdministrationEmployeePrintViewModels()
												{
													EMail = _user.Email,
													UserName = _user.UserName,
													FamilyName = (_user.Employee == null) ? "" : ((_user.Employee.Person == null) ? "" : _user.Employee.Person.FamilyName),
													Name = (_user.Employee == null) ? "" : ((_user.Employee.Person == null) ? "" : _user.Employee.Person.Name),
													MiddleName = (_user.Employee == null) ? "" : ((_user.Employee.Person == null) ? "" : _user.Employee.Person.MiddleName),
													BirthData = (_user.Employee == null) ? DateTime.Now : ((_user.Employee.Person == null) ? DateTime.Now : _user.Employee.Person.BirthData),
													IdUser = _user.Id,
													IsADUser = _user.IsActiveDirectory,
													IsNotDelete = isAdminFirm || isAdminServer
												}).FirstOrDefaultAsync();
						return this.JsonNetIso(_employees);

					}
				}
			}
			return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
		}
		[ActionName("_admins_firm_page")]
		public async Task<ActionResult> FirmInfoAdministrationAsync(int idFirm)
		{
			var firm_ = await DBRepository.Firms.FindAsync(idFirm);
			var ColVoSotr = firm_.EmployeeFirms.ToList().Count;
			if (firm_ != null)
			{
				ViewData["id_firm"] = idFirm;
				return PartialView("Admin/Admins/AdminsFirms/_admins_firm_page", new CFirm()
				{
					id_firm = idFirm,
					Name = firm_.Name,
					ColVoSotr = ColVoSotr
				});
			}
			else
			{
				return new HttpStatusCodeResult(HttpStatusCode.NotFound);
			}

		}
		public async Task<ActionResult> _Department(int firmId)
		{
			List<AdministrationFirmDepartmentViewModels> ls = new List<AdministrationFirmDepartmentViewModels>();
			var firm = await DBRepository.Firms.FindAsync(firmId);
			ViewData["id_firm"] = firm.FirmID;
			var result = await (from dep in DBRepository.Departments
								where dep.Firm.FirmID == firmId
								select new
								{
									depId = dep.DepartmentID,
									depName = dep.Name,
									depEmploy = (from ef in dep.EmployeeFirms
												 select ef.ApplicationUserId.Employee.Person.FamilyName + " " + ef.ApplicationUserId.Employee.Person.Name + " " + ef.ApplicationUserId.Employee.Person.MiddleName).ToList()
								}).ToListAsync();



			foreach (var item in result)
			{
				ls.Add(new AdministrationFirmDepartmentViewModels() { FirmName = firm.Name, FIO = item.depEmploy, IdDepartment = item.depId, IdFirm = firmId, Name = item.depName });

			}
			return PartialView(ls);
		}
		/// <summary>
		/// Отделы, по фирме.
		/// </summary>
		/// <param name="firmId">ID организации в базе</param>
		/// <returns></returns>
		public async Task<ActionResult> _DepartmentInFirm(int firmId)
		{
			try
			{
				if (firmId < 0)
				{
					return new HttpStatusCodeResult(HttpStatusCode.NotFound);
				}
				var departments = await DBRepository.Departments.Where(dep => dep.Firm.FirmID == firmId).Select(dep => dep.DepartmentID).ToArrayAsync();
				return Json(departments, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				Trace.TraceError(e.Message);
				return PartialView();
			}
		}

		public async Task<ActionResult> _DepartmentInfo(int firmId, int departmentId)
		{
			var firm = await DBRepository.Firms.FindAsync(firmId);
			var department = await DBRepository.Departments.FindAsync(departmentId);
			if (firmId == department.Firm.FirmID)
			{
				return Json(new AdministrationFirmDepartmentViewModels(department, firm.FirmID, firm.Name, department.EmployeeFirms.Select(ef => ef.ApplicationUserId.Employee.Person.Fio)));
			}
			return new HttpStatusCodeResult(HttpStatusCode.NotFound);
		}



		public ActionResult _Firm(string userId = null)
		{
			try
			{
				if (userId == null)
				{
					if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
					{
						var task = Task.Run(async () =>
						{
							List<AdministrationFirmViewModels> allFirms = new List<AdministrationFirmViewModels>();
							var allFirmsinDb = await DBRepository.Firms.Select(_fr => new { _fr.Name, _fr.FirmID, _fr.EmployeeFirms.Count }).ToListAsync();
							foreach (var firm in allFirmsinDb)
							{
								allFirms.Add(new AdministrationFirmViewModels() { IdFirm = firm.FirmID, Name = firm.Name, ColVoSotr = firm.Count });
							}
							return allFirms;

						});
						Task.WaitAny(task);
						return PartialView(task.Result);
					}
				}
				else
				{
					ApplicationUser au = DBRepository.Users.Find(userId);
					var user_in_firms = from ef in au.Employee.EmployeeFirms
										select ef.Firm.Name;
					return PartialView(user_in_firms);
				}

				return PartialView();
			}

			catch (Exception e)
			{
				Trace.TraceError(e.Message);
				return PartialView();
			}
		}


		private void AddErrors(IdentityResult result)
		{
			foreach (var error in result.Errors)
			{
				ModelState.AddModelError("", error);
			}
		}

		public ActionResult _FirmDetail(int idFirm)
		{
			var dd = DBRepository.Firms.Find(idFirm);
			return View(dd);

		}

		public ActionResult _admFirms(string userName)
		{
			ApplicationUser au = DBRepository.Users.SingleOrDefault(user => user.UserName == userName);
			var user_in_firms = from ef in au.Employee.EmployeeFirms
								select ef.Firm;
			List<AdministrationFirmViewModels> ls = new List<AdministrationFirmViewModels>();
			if (user_in_firms.Any() == true)
			{
				foreach (var item in user_in_firms)
				{
					ls.Add(new AdministrationFirmViewModels() { IdFirm = item.FirmID, Name = item.Name, ColVoSotr = item.EmployeeFirms.Count });
				}
			}
			return PartialView("Admin/Admins/AdminsFirms/_admFirms", ls);

		}

		public ActionResult AutocompleteFirm()
		{
			var token = string.Join(",", DBRepository.Firms.ToList().Select(x => x.Name).ToList());
			return Json(new { data = token.ToString().Split(',') }, JsonRequestBehavior.AllowGet);

		}
		public ActionResult AutocompleteDepart(int idFirm)
		{
			if (idFirm == -1)
			{
				return Json(new { data = new string[] { "" } }, JsonRequestBehavior.AllowGet);
			}

			var token = string.Join(",", DBRepository.Firms.Find(idFirm).Departments.ToList().Select(x => x.Name).ToList());
			return Json(new { data = token.ToString().Split(',') }, JsonRequestBehavior.AllowGet);

		}
		public ActionResult AutocompleteDolzn(int idFirm)
		{
			try
			{

				var dolzn = (from _ef in DBRepository.Firms.Find(idFirm).EmployeeFirms
							 from _er in _ef.EmployeeRoles
							 select _er.Role).ToList().Distinct().ToList();
				var token = string.Join(",", dolzn.ToList().Select(x => x).ToList());
				return Json(new { data = token.ToString().Split(',') }, JsonRequestBehavior.AllowGet);

			}
			catch (Exception)
			{
				return Json(new { data = new string[] { "" } }, JsonRequestBehavior.AllowGet);
			}
		}
		//AutocompleteDolzn
		public async Task<ActionResult> AdminsServer()
		{
			List<AdministrationFirmViewModels> ls = new List<AdministrationFirmViewModels>();
			var _firms = await (from ef in DBRepository.EmployeeFirms
								select ef.Firm).Select(_fr => new { _fr.Name, _fr.FirmID, _fr.EmployeeFirms.Count }).ToListAsync();
			foreach (var firm in _firms)
			{
				ls.Add(new AdministrationFirmViewModels() { IdFirm = firm.FirmID, Name = firm.Name, ColVoSotr = firm.Count });
			}
			return PartialView("Admin/AdminsServer", ls);

		}

		public ActionResult AllUsers()
		{
			return View();
		}
		public ActionResult AllFirms()
		{
			return View();
		}
		public async Task<ActionResult> Admins()
		{
			try
			{
				var findFirm = await Task.Factory.StartNew(async () =>
				{
					List<AdministrationFirmViewModels> ls = new List<AdministrationFirmViewModels>();
					var userid = User.Identity.GetUserId();
					var myFirms = await DBRepository.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(userid)).Select(sel =>
					new
					{
						firmId = sel.Firm.FirmID,
						firmName = sel.Firm.Name,
						colvoSotr = sel.Firm.EmployeeFirms.Count()
					}
					).ToListAsync();

					if (myFirms != null)
					{
						if (myFirms.Any() == true)
						{
							foreach (var item in myFirms)
							{
								ls.Add(new AdministrationFirmViewModels() { Name = item.firmName, ColVoSotr = item.colvoSotr, IdFirm = item.firmId });
							}
						}
					}
					return ls;

				});
				return PartialView("Admin/Admins/AdminsFirms/_admFirms", await findFirm);

			}
			catch (Exception e)
			{
				Trace.Fail(e.Message);
				return PartialView("Admin/Admins/AdminsFirms/_admFirms", new List<AdministrationFirmViewModels>());
			}
		}

		public async Task<ActionResult> _AdminsFirms()
		{
			var task = await Task.Run(() =>
			{
				List<AdministrationFirmViewModels> ls = new List<AdministrationFirmViewModels>();
				var user = DBRepository.Users.Find(User.Identity.GetUserId());
				var _ad = (from ef in user.Employee.EmployeeFirms
						   select new { ef.Firm.FirmID, ef.Firm.Name, ef.Firm.EmployeeFirms.Count }).ToList();
				if (_ad.Any() == true)
				{
					foreach (var firm in _ad)
					{
						ls.Add(new AdministrationFirmViewModels() { IdFirm = firm.FirmID, Name = firm.Name, ColVoSotr = firm.Count });
					}
				}
				return ls;

			});
			ViewData["IsInActiveDirectory"] = ActiveDirectorySetting.IsUsingActiveDirectory;
			//Task.WaitAny(task);
			return PartialView("Admin/Admins/_AdminsFirms", task);

		}
		public ActionResult _admins_firms_departments(int? idFirm)
		{
			if (idFirm == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			List<AdministrationDepartmentView> ls = new List<AdministrationDepartmentView>();
			var user = DBRepository.Users.Find(User.Identity.GetUserId());
			var firm = DBRepository.Firms.Find(idFirm.Value);

			var _ad = from dep in firm.Departments
					  select dep;
			if (_ad.Any() == true)
			{
				foreach (var item in _ad)
				{
					ls.Add(new AdministrationDepartmentView() { idFirm = item.Firm.FirmID, Name = item.Name });
				}
			}
			return PartialView("Admin/Admins/admins_firm/firm/_admins_firms_departments");
		}
		public ActionResult _admins_firms_emploees(int idFirm)
		{
			return PartialView("Admin/Admins/admins_firm/firm/_admins_firms_emploees");
		}

		public ActionResult _LayoutAdminFirm()
		{
			return PartialView("Admin/Admins/admins_firm/_LayoutAdminFirm");
		}

		/*    Layout = "~/Views/Administration/Admin/Admins/admins_firm/_LayoutAdminFirm.cshtml";
        */
	}
}
