﻿using MvcInfos.ActiveDirectory;
using MvcInfos.Attributes;
using MvcInfos.Crypto.Certificate;
using MvcInfos.HKey;
using MvcInfos.Models.ViewModels;
using MvcInfos.Plugin.Interfaces;
using MvcInfos.Repository;
using MvcInfos.Settings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MvcInfos.Extensions;
using MvcInfos.Entities.Files;

namespace MvcInfos.Controllers
{
	[AuthorizeActiveDirectory(Roles = "administrator_server,administrator_firm", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager")]
	public class SettingsController : Controller
	{
		private IApplicationRepository DBRepository { get; set; }
		private IPlugins Plugins { get; set; }
		private IHKCurrentUser HKeySettings { get; set; }
		private Certificate Certificate { get; set; }
#if (DEBUG || RELEASE)
		ActiveDirectorySetting ActiveDirectorySetting { get; set; }
#endif
		public SettingsController(IApplicationRepository repos,
#if (DEBUG || RELEASE)
			ActiveDirectorySetting activeDirectorySetting,
#endif
			IPlugins plugins,
			Certificate certificate,
			IHKCurrentUser hKeySettings)
		{
#if (DEBUG || RELEASE)
			ActiveDirectorySetting = activeDirectorySetting;
#endif

			Certificate = certificate;
			HKeySettings = hKeySettings;
			Plugins = plugins;
			DBRepository = repos;
		}

		// GET: FileHosting
		[AuthorizeActiveDirectory(Roles = "administrator_server", Groups = "AdministratorServerSManager")]
		public ActionResult FileHosting()
		{
			//var dict = new System.Collections.Generic.Dictionary<string, string>();
			//dict.Add("Folder", "/Storage/");
			ViewData["plugins"] = Plugins.GetHTMLOptions();
			ViewData["plugins-style"] = Plugins.GetHTMLOptionsStyles();
			ViewData["plugins-forms"] = Plugins.GetForms();
			return View();
		}

		[AuthorizeActiveDirectory(Roles = "administrator_server", Groups = "AdministratorServerSManager")]
		[ActionName("GetAllFileHosting")]
		public async Task<JsonResult> GetAllFileHostingAsync()
		{
			var hostings = await DBRepository.HostingFiles.Select(hosting => new
			{
				hosting.HostingName,
				hosting.HostingFileID,
				hosting.PluginConnectionType,
				hosting.Settings,
				hosting.IsDefault,
				hosting.IsNotDelete
			}).ToListAsync();
			var list = new List<object>();
			foreach (var item in hostings)
			{
				list.Add(new HostingGetViewModel
				{
					HostingName = item.HostingName,
					HostingId = (MyGuid) item.HostingFileID,
					PluginConnectionType = (MyGuid)item.PluginConnectionType,
					PluginInfo = Plugins.LoadedPlugins[item.PluginConnectionType].ToString(item.Settings),
					IsDefault = item.IsDefault,
					IsNotDelete = item.IsNotDelete,
				});
			}
			return this.JsonNetIso(list, JsonRequestBehavior.AllowGet);
			//return Json(list, JsonRequestBehavior.AllowGet);
		}
		[AuthorizeActiveDirectory(Roles = "administrator_server", Groups = "AdministratorServerSManager")]
		[ValidateJsonAntiForgeryToken]
		[ActionName("SaveNewFileHosting")]
		public async Task<JsonResult> SaveNewFileHostingAsync([Bind] string parametrs)
		{
			var host = JsonConvert.DeserializeObject<HostingViewModel>(parametrs);


			Dictionary<string, string> settings = new Dictionary<string, string>
			{
				{ "Folder", host.HostingUploadUrl }
			};
			var newFileHost = new HostingFile
			{
				HostingName = host.HostingName,
				PluginConnectionType = (Guid)host.PluginConnectionType,
				Settings = JsonConvert.SerializeObject(settings)
			};
			
			DBRepository.Entry(newFileHost).State = EntityState.Modified;
			DBRepository.HostingFiles.Add(newFileHost);
			await DBRepository.SaveChangesAsync();


			return this.JsonNetIso(new { }, JsonRequestBehavior.AllowGet);

		}

		[AuthorizeActiveDirectory(Roles = "administrator_server", Groups = "AdministratorServerSManager")]
		[ValidateJsonAntiForgeryToken]
		[ActionName("UpdateDefaultPlugin")]
		public async Task<JsonResult> UpdateDefaultPluginAsync([Bind] string parametrs)
		{
			var host = JsonConvert.DeserializeObject<Dictionary<Guid, bool>>(parametrs);
			foreach (var item in host)
			{
				var host2 = await DBRepository.HostingFiles.FindAsync(item.Key);
				if (host2!= null)
				{
					host2.IsDefault = item.Value;
					DBRepository.Entry(host2).State = EntityState.Modified;
				}
		
			
			}


			await DBRepository.SaveChangesAsync();


			return this.JsonNetIso(new { }, JsonRequestBehavior.AllowGet);

		}



		public ActionResult Organization()
		{
			return View();
		}







#if (DEBUG || RELEASE)
		[ActionName("ActiveDirectory")]
		public ActionResult ActiveDirectory()
		{
			ViewData["IsUsingActiveDirectory"] = ActiveDirectorySetting.IsUsingActiveDirectory;
			ViewData["IsNotUsingCurrentUser"] = ActiveDirectorySetting.IsNotUsingCurrentUser;

			ViewData["IsCurrentServerInDomain"] = Environment.UserDomainName;
			ViewData["ActiveDirectoryDomainLogin"] = ActiveDirectorySetting.UserName;


			return View();
		}

		[AuthorizeActiveDirectory(Roles = "administrator_server", Groups = "AdministratorServerSManager")]
		[ActionName("ValidateAdmin"), ValidateJsonAntiForgeryToken]
		public async Task<ActionResult> ValidateAdminAsync([Bind]string parametrs)
		{
			if (parametrs == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var result = await Task.Factory.StartNew(() =>
			{
				var localThanReplace = HttpUtility.UrlDecode(System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(parametrs)));
				var serializedResult = JsonConvert.DeserializeObject<ValidAdminViewModel>(localThanReplace);
				return ActiveDirectorySetting.ValidateCredentials(serializedResult.Login, serializedResult.Password);
			});
			return Json(new { @result = result });
		}

		[AuthorizeActiveDirectory(Roles = "administrator_server", Groups = "AdministratorServerSManager")]
		[ActionName("UpdateAdminActiveDirectory"), ValidateJsonAntiForgeryToken]
		public async Task<ActionResult> UpdateAdminActiveDirectoryAsync([Bind]string parametrs)
		{
			if (parametrs == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			await Task.Factory.StartNew(() =>
			{
				var localThanReplace = HttpUtility.UrlDecode(System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(parametrs)));
				var serializedResult = JsonConvert.DeserializeObject<ActiveDirectoryAdminSettingViewModel>(localThanReplace);
				if (serializedResult.IsUsingActiveDirectory)
				{
					if (serializedResult.IsNotUsingCurrentUser)
					{
						if (serializedResult.UserName != string.Empty && serializedResult.Password != string.Empty)
						{
							var result = ActiveDirectorySetting.ValidateCredentials(serializedResult.UserName, serializedResult.Password);
							if (result)
							{
								ActiveDirectorySetting.SetJson(localThanReplace);
								ActiveDirectorySetting.Save();
							}
						}
					}
					else
					{
						ActiveDirectorySetting.SetJson(localThanReplace);
						ActiveDirectorySetting.Save();
					}
				}
				else
				{
					ActiveDirectorySetting.SetJson(localThanReplace);
					ActiveDirectorySetting.Save();
				}
			});

			return Json(new { @result = true });

		}

#endif

		[AuthorizeActiveDirectory(Roles = "administrator_server", Groups = "AdministratorServerSManager")]
		public ActionResult Cerificates()
		{
			var cert = Certificate.NewCertificate;
			if (cert == null)
			{
				ViewData["cerificateNewEndDate"] = null;
			}
			else
			{
				ViewData["cerificateNewEndDate"] = cert.GetExpirationDateString();
			}

			var oldCert = Certificate.OldCertificate;
			if (oldCert == null)
			{
				ViewData["cerificateOldEndDate"] = null;
			}
			else
			{
				ViewData["cerificateOldEndDate"] = oldCert.GetExpirationDateString();
			}

			return View();
		}

		[AuthorizeActiveDirectory(Roles = "administrator_server", Groups = "AdministratorServerSManager")]
		[ActionName("UpdateCerificates"), ValidateJsonAntiForgeryToken]
		public async Task<ActionResult> UpdateCerificatesAsync([Bind]string parametrs)
		{
			await Certificate.UpdateCertificateAsync();

			return Json(new { @result = true });
		}
	}

}