﻿using Microsoft.AspNet.Identity;
using MvcInfos.ActiveDirectory;
using MvcInfos.ActiveDirectory.Extensions;
using MvcInfos.Attributes;
using MvcInfos.Entities;
using MvcInfos.Entities.Firm;
using MvcInfos.Entities.UserConfigurations;
using MvcInfos.IdentityConfig;
using MvcInfos.Models.ViewModels.ActiveDirectory;
using MvcInfos.Repository;
using MvcInfos.Settings;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MvcInfos.Controllers.Administration
{
	[AuthorizeActiveDirectory(Roles = "administrator_firm,administrator_server", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager")]
	public class ActiveDirectoryController : Controller
	{
		private ActiveDirectorySetting ActiveDirectorySetting { get; set; }
		private IApplicationRepository DBRepository { get; set; }
		private ActiveDirectorySyncService ActiveDirectorySyncService { get; set; }
		private ApplicationUserManager UserManager { get; set; }
		public ActiveDirectoryController(IApplicationRepository _repository, ActiveDirectorySetting _activeDirectorySetting, ApplicationUserManager _userManager, ActiveDirectorySyncService _activeDirectorySyncService)
		{
			ActiveDirectorySyncService = _activeDirectorySyncService;
			ActiveDirectorySetting = _activeDirectorySetting;
			DBRepository = _repository;
			UserManager = _userManager;
		}
		// GET: ActiveDirectory
		public ActionResult Index()
		{
			var allUsersAD = ActiveDirectorySetting.GetDirectoryEntry().GetDirectorySercherResult("(&(objectclass=user)(company=*))").Where(res => res.Properties.Contains("sAMAccountName")).Select(res => res.Properties["sAMAccountName"][0].ToString()).ToList().Distinct();
			var allUsersADinASP = DBRepository.Users.Where(user => user.IsActiveDirectory == true).Select(user => user.UserName).Intersect(allUsersAD).Count();

			var allFirmsAD = ActiveDirectorySetting.GetDirectoryEntry().GetDirectorySercherResult("(&(objectclass=user)(company=*))").Where(res => res.Properties.Contains("company")).Select(res => res.Properties["company"][0].ToString()).ToList().Distinct();
			var allFirmsADinASP = DBRepository.Firms.Where(firm => firm.IsActiveDirectory == true).Where(firm => firm.IsDeleted == false).Select(firm => firm.Name).Intersect(allFirmsAD).Count();

			var allDepartmentAD = ActiveDirectorySetting.GetDirectoryEntry().GetDirectorySercherResult("(&(objectclass=user)(department=*))").Where(res => res.Properties.Contains("department")).Select(res => res.Properties["department"][0].ToString()).ToList().Distinct();
			var allDepartmentADinASP = DBRepository.Departments.Where(department => department.IsActiveDirectory == true).Where(department => department.IsDeleted == false).Select(department => department.Name).Intersect(allDepartmentAD).Count();

			ViewData["IsActiveDirectorySyncServiceStart"] = ActiveDirectorySyncService.IsRun;

			return View(new ActiveDirectorySyncInfoViewModel()
			{
				DepartmentCount = new SyncInfoViewModel
				{
					InActiveDirectory = allDepartmentAD.Count(),
					InAsp = allDepartmentADinASP
				},
				FirmCount = new SyncInfoViewModel
				{
					InActiveDirectory = allFirmsAD.Count(),
					InAsp = allFirmsADinASP
				},
				UsersCount = new SyncInfoViewModel
				{
					InActiveDirectory = allUsersAD.Count(),
					InAsp = allUsersADinASP
				}
			});
		}

		[ValidateJsonAntiForgeryToken, ActionName("Synchronize")]
		public async Task<JsonResult> SynchronizeAsync(string parametrs)
		{
			var data = Newtonsoft.Json.JsonConvert.DeserializeObject<SyncStatusViewModel>(parametrs);
			if (ActiveDirectorySetting.IsUsingActiveDirectory.Value)
			{
				/*if (data.Sync == true)
				{
					ActiveDirectorySyncService.Sync(new ActiveDirectoryFilterEvent { Filter = "(objectclass=user)", SearchScope = SearchScope.Subtree });
				}
				else
				{
					ActiveDirectorySyncService.Stop();
				}*/

				//var company = ActiveDirectorySetting.GetCurrentUserDirectoryEntry().GetProperty(ActiveDirectoryPropertis.Company);

				///TODO: При переименовании фирмы/отдела/юю будет терятся вся информация.


				var allUsers = new List<System.DirectoryServices.DirectoryEntry>();
				foreach (var group in EnumerableRoles.ActiveDirectoryGroups)
				{
					var groupPrincipal = ActiveDirectorySetting.GetGroupPrincipal(group);
					var groupMembers = groupPrincipal.GetMembers(true);
					allUsers.AddRange(groupMembers.ToDirectoryEntries());
				}

				allUsers = allUsers.Distinct().ToList();
				//var allUsers = ActiveDirectorySetting.GetDirectoryEntry().GetDirectorySercherResult($"objectClass=User;group={group}");
				var companies = allUsers.Where(res => res.Properties.Contains("company")).Select(res => res.Properties["company"][0].ToString()).ToList().Distinct();
				var allFirmsADinASP = DBRepository.Firms.Where(firm => firm.IsActiveDirectory == true).Select(firm => firm);
				foreach (var firm in allFirmsADinASP)
				{
					firm.IsDeleted = !companies.Contains(firm.Name);
					DBRepository.Entry(firm).State = System.Data.Entity.EntityState.Modified;
				}
				foreach (var company in companies)
				{
					var departments = ActiveDirectorySetting.GetDirectoryEntry().GetDirectorySercherResult($"company={company}").Where(res => res.Properties.Contains("department")).Select(res => res.Properties["department"][0].ToString()).ToList().Distinct();

					var findingFirm = DBRepository.Firms.Where(firm => firm.IsActiveDirectory == true).Where(firm => firm.Name.Equals(company)).Select(firm => firm).FirstOrDefault();
					if (findingFirm == null)
					{
						var newFirm = new Firm()
						{
							IsActiveDirectory = true,
							Name = company
						};
						findingFirm = DBRepository.Firms.Add(newFirm);
						DBRepository.Entry(newFirm).State = System.Data.Entity.EntityState.Added;
					}
					var allDepInFirmASP = DBRepository.Departments.Where(department => department.IsActiveDirectory == true).Select(department => department);
					foreach (var department in allDepInFirmASP)
					{
						department.IsDeleted = !departments.Contains(department.Name);
						DBRepository.Entry(department).State = System.Data.Entity.EntityState.Modified;
					}

					foreach (var department in departments)
					{
						var findingDep = findingFirm.Departments.Where(departmentRepos => departmentRepos.Name.Equals(department)).Select(departmentRepos => departmentRepos).FirstOrDefault();
						if (findingDep == null)
						{
							var dep = new Department()
							{
								Name = department,
								IsActiveDirectory = true,
								Firm = findingFirm
							};
							findingFirm.Departments.Add(dep);
							findingDep = DBRepository.Departments.Add(dep);
							DBRepository.Entry(dep).State = System.Data.Entity.EntityState.Added;
						}
					}
				}




				var sAMAccountNames = allUsers.Where(res => res.Properties.Contains("sAMAccountName")).Select(res => res.Properties["sAMAccountName"][0].ToString()).ToList().Distinct();
				var allUsersADinASP = DBRepository.Users.Where(user => user.IsActiveDirectory == true).Select(user => user);
				foreach (var user in allUsersADinASP)
				{
					user.IsDeleted = !sAMAccountNames.Contains(user.UserName);
					DBRepository.Entry(user).State = System.Data.Entity.EntityState.Modified;
				}

				var x = await DBRepository.SaveChangesAsync();

				foreach (var sAMAccountName in sAMAccountNames)
				{
					var adUser = await ActiveDirectorySetting.FindByEmailAsync(sAMAccountName);
					if (adUser != null)
					{
						if (adUser.EmailAddress == null)
						{
							continue;
						}
						var company = (string)adUser.GetProperty("company");
						Firm companyEntity = null;
						Department departmentEnity = null;
						EmployeeFirm employeeFirm = null;
						if (company != null)
						{
							companyEntity = DBRepository.Firms.Where(firm => firm.IsActiveDirectory == true).Where(firm => firm.Name.Equals(company)).FirstOrDefault();
							var department = (string)adUser.GetProperty("department");
							if (department != null)
							{
								departmentEnity = companyEntity.Departments.Where(dep => dep.IsActiveDirectory == true).Where(dep => dep.Name.Equals(department)).Select(dep => dep).FirstOrDefault();
							}
						}
						var aspUser = DBRepository.Users.Where(asuser => asuser.UserName.Equals(adUser.SamAccountName)).FirstOrDefault();
						if (aspUser == null)
						{
							aspUser = new ApplicationUser { Employee = new Employee(), UserName = adUser.SamAccountName, Email = adUser.EmailAddress, IsActiveDirectory = true, Id =  adUser.Guid.ToString() };
							var result = await UserManager.CreateAsync(aspUser);
							if (result.Succeeded == false)
							{
								continue;
							}
						}

						if (companyEntity != null)
						{							
							aspUser = DBRepository.Users.Where(asuser => asuser.UserName.Equals(adUser.SamAccountName)).FirstOrDefault();
							var person = new Entities.Firm.Dictionary.Person()
							{
								FamilyName = adUser.Surname ?? " ",
								Name = adUser.GivenName ?? " ",
								MiddleName = adUser.MiddleName ?? " "
							};
							aspUser.Employee.Person = person;
							DBRepository.Persons.Add(person);
							DBRepository.Entry(person).State = System.Data.Entity.EntityState.Added;
							try
							{

								var x1 = await DBRepository.SaveChangesAsync();
							}
							catch (DbEntityValidationException e)
							{
								Trace.Fail(e.Message);
							}

							employeeFirm = aspUser.Employee.EmployeeFirms.Where(firm => firm.Firm.Name.Equals(companyEntity.Name)).FirstOrDefault();
							if (employeeFirm == null)
							{
								employeeFirm = new EmployeeFirm()
								{
									EmploymentDate = DateTime.Now.AddHours(-1),
									ApplicationUserId = aspUser,
									Firm = companyEntity,
									IsActiveDirectory = true
								};
								aspUser.Employee.EmployeeFirms.Add(employeeFirm);
								DBRepository.EmployeeFirms.Add(employeeFirm);
								DBRepository.Entry(employeeFirm).State = System.Data.Entity.EntityState.Added;
								companyEntity.EmployeeFirms.Add(employeeFirm);
							}
							employeeFirm.Departments.Clear();
							if (departmentEnity != null)
							{
								employeeFirm.Departments.Add(departmentEnity);
								departmentEnity.EmployeeFirms.Add(employeeFirm);
							}
							try
							{
								var x1 = await DBRepository.SaveChangesAsync();
							}
							catch (DbEntityValidationException e)
							{
								Trace.Fail(e.Message);
							}
						}
					}
				}

				var allADUsersNotDel = DBRepository.Users.Where(user => user.IsActiveDirectory == true).Where(user => user.IsDeleted == false).Select(user => user);
				foreach (var user in allADUsersNotDel)
				{
					var result = user.Employee.EmployeeFirms.Where(ef => ef.IsActiveDirectory).Select(ef => ef);
					foreach (var employeeFirm in result)
					{
						employeeFirm.EmployeeFirms.Clear();
						DBRepository.Entry(employeeFirm).State = System.Data.Entity.EntityState.Modified;
					}
				}

				var x12 = await DBRepository.SaveChangesAsync();
				//	var managers = allUsers.Where(res => res.Properties.Contains("manager")).Select(res => res.Properties["manager"][0].ToString()).ToList().Distinct();

				foreach (var sAMAccountName in sAMAccountNames)
				{
					var aspUser = DBRepository.Users.Where(asuser => asuser.UserName.Equals(sAMAccountName)).FirstOrDefault();
					if (aspUser != null)
					{
						var adUser = await ActiveDirectorySetting.FindByEmailAsync(sAMAccountName);
						var company = (string)adUser.GetProperty("company");

						var firmAD = DBRepository.Firms.Where(firm => firm.Name.Equals(company)).First();
						var manager = allUsers.Where(res => res.Properties.Contains("manager")).Where(res => res.Properties["sAMAccountName"].Equals(sAMAccountName)).Select(res => res.Properties["manager"][0].ToString()).FirstOrDefault();
						if (manager != null)
						{
							var managerASP = DBRepository.Users.Where(asuser => asuser.UserName.Equals(manager)).FirstOrDefault();
							var employeeFirmManager = managerASP.Employee.FindEFirm(firmAD);
							employeeFirmManager.EmployeeFirms.Add(aspUser.Employee.FindEFirm(firmAD));
							DBRepository.Entry(employeeFirmManager).State = System.Data.Entity.EntityState.Modified;
						}
					}
				}

				var x13 = await DBRepository.SaveChangesAsync();
			}



			return Json(new { }, JsonRequestBehavior.AllowGet);
		}





		public JsonResult IsUsed()
		{
			return Json(new { @result = ActiveDirectorySetting.IsUsingActiveDirectory }, JsonRequestBehavior.AllowGet);
		}
	}
}