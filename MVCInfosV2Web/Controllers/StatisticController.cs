﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MvcInfos.ActiveDirectory;
using MvcInfos.ActiveDirectory.Extensions;
using MvcInfos.Entities;
using MvcInfos.Entities.UserConfigurations;
using MvcInfos.Extensions;
using MvcInfos.Models.ViewModels;
using MvcInfos.Models.ViewModels.Statistic;
using MvcInfos.Repository;

namespace MvcInfos.Controllers
{
	[AuthorizeActiveDirectory]
	public class StatisticController : Controller
	{
		private IApplicationRepository DBRepository { get; set; }
		public StatisticController(IApplicationRepository repos)
		{
			DBRepository = repos;
		}




		[ActionName("Statistics")]
		public async Task<ActionResult> StatisticsAsync()
		{
			var statistics = new StatisticsViewModel();
			if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
			{
				//statistics.AllNotifications = await DBRepository.ResolutionNotifiers.CountAsync();
				statistics.AllFirms = await DBRepository.Firms.CountAsync();
				statistics.AllUsers = await DBRepository.Users.CountAsync();
				statistics.AllMails = await DBRepository.Mails.CountAsync();
				statistics.CurrentResolutions = await DBRepository.ResolutionNotifiers.Where(resol => resol.IsDelete == false).Where(resol => resol.IsNotifi == true).CountAsync();
				if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
				{
					statistics.Firms = await GetMyFirmsToSelectedItemAsync(true);
				}
				else
				{
					statistics.Firms = await GetMyFirmsToSelectedItemAsync(false);
				}
				///OrderBy(p => p.ServerStatus.ServerDateTime).GroupBy(p => p.RawName).Select(p => p.OrderByDescending(x => x.Id).Take(1).Single())
				///
				int currentMails = 0, closedMails = 0;

				foreach (var mail in DBRepository.Mails)
				{
					var status = mail.MailStatuses.LastOrDefault();
					if (status != null)
					{
						if (status.MailStatusEnum == Entities.Mails.MailStatusEnum.InAction)
						{
							currentMails++;
						}
						else
						{
							if (status.MailStatusEnum == Entities.Mails.MailStatusEnum.Spent)
							{
								closedMails++;
							}
						}
					}
				}

				statistics.CurrentMails = currentMails;
				statistics.ClosedMails = closedMails;
			}
			else
			{
				await Task.Factory.StartNew(async () =>
				{
					var user = await ((DbSet<ApplicationUser>)DBRepository.Users).FindAsync(User.Identity.GetUserId());
					statistics.AllFirms = (from ef in user.Employee.EmployeeFirms
										   select ef.Firm).Count();

					statistics.AllUsers = (from ef in user.Employee.EmployeeFirms
										   from efInFirm in ef.Firm.EmployeeFirms
										   select efInFirm.Id).Count();

					statistics.AllMails = (from ef in user.Employee.EmployeeFirms
										   from efInFirm in ef.Firm.EmployeeFirms
										   select efInFirm.Firm.Mails.Count).Sum();


					//statistics.AllNotifications = (from ef in user.Employee.EmployeeFirms
					//							   from efInFirm in ef.Firm.EmployeeFirms
					//							   from mail in efInFirm.Firm.Mails
					//							   select mail.Resolution.ResolutionNotifier.Count).Sum();

					statistics.CurrentResolutions = (from ef in user.Employee.EmployeeFirms
													 from efInFirm in ef.Firm.EmployeeFirms
													 from mail in efInFirm.Firm.Mails
													 from resolution in mail.Resolution.ResolutionNotifier
														 // where resolution.IsNotifi == true
													 where resolution.IsDelete == false
													 select mail.Resolution.ResolutionNotifier.Count).Sum();
					//	statistics.CurrentResolutions = await DBRepository.ResolutionNotifiers.Where(resol => resol.IsDelete == false).Where(resol => resol.IsNotifi == true).CountAsync();




					int currentMails = 0, closedMails = 0;
					var mails = from ef in user.Employee.EmployeeFirms
								from efInFirm in ef.Firm.EmployeeFirms
								from mail in efInFirm.Firm.Mails
								select mail;

					foreach (var mail in mails)
					{
						var status = mail.MailStatuses.LastOrDefault();
						if (status != null)
						{
							if (status.MailStatusEnum == Entities.Mails.MailStatusEnum.InAction)
							{
								currentMails++;
							}
							else
							{
								if (status.MailStatusEnum == Entities.Mails.MailStatusEnum.Spent)
								{
									closedMails++;
								}
							}
						}
					}

					statistics.CurrentMails = currentMails;
					statistics.ClosedMails = closedMails;



					//statistics.CurrentMails = (from ef in user.Employee.EmployeeFirms
					//						   from efInFirm in ef.Firm.EmployeeFirms
					//						   from mail in efInFirm.Firm.Mails
					//						   where (mail.MailStatuses.LastOrDefault() != null) ? mail.MailStatuses.LastOrDefault().MailStatusEnum == Entities.Mails.MailStatusEnum.Spent : false
					//						   select mail).Count();

					//statistics.ClosedMails = (from ef in user.Employee.EmployeeFirms
					//						  from efInFirm in ef.Firm.EmployeeFirms
					//						  from mail in efInFirm.Firm.Mails
					//						  where (mail.MailStatuses.LastOrDefault() != null) ? mail.MailStatuses.LastOrDefault().MailStatusEnum == Entities.Mails.MailStatusEnum.Spent : false
					//						  select mail).Count();

				});

			}
			return View(statistics);
		}
		[ActionName("MailStatistic")]
		public async Task<JsonResult> MailStatisticAsync(string parametrs)
		{
			var localThanReplace = HttpUtility.UrlDecode(System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(parametrs)));
			var dateRange = Newtonsoft.Json.JsonConvert.DeserializeObject<DateMailInfoViewModel>(localThanReplace);
			var userId = User.Identity.GetUserId();
			ICollection<StatisticMailsViewModel> mails = null;
			/*if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
			{
				mails = from mail in DBRepository.Mails
						where dateRange.Start <= mail.RegisterDate
						where mail.FinishDate <= dateRange.End
						select new StatisticMails {
							MailId = mail.MailId,
							MailStatus = mail.MailStatuses.LastOrDefault()
						};
		}*/
			if (dateRange.FirmId == null)
			{
				return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest));
			}
			var firm = await DBRepository.Firms.FindAsync(dateRange.FirmId);
			if (firm == null)
			{
				return Json(new HttpStatusCodeResult(HttpStatusCode.BadRequest));
			}

			if (User.IsHasAccessRights(EnumerableRoles.administrator_server) || User.IsHasAccessRights(EnumerableRoles.administrator_firm) || (User.IsHasAccessRights(EnumerableRoles.manager)))
			{

				mails = (from mail in firm.Mails
							 // where dateRange.Start <= mail.RegisterDate
						 select new StatisticMailsViewModel
						 {
							 MailId = mail.MailId
						 }).ToList();


				foreach (var item in mails)
				{
					var mail = await DBRepository.Mails.FindAsync(item.MailId);
					item.MailStatus = (mail.MailStatuses.Count > 0) ? mail.MailStatuses.Last() : null;
				}


			}
			else
			{
				if (User.IsHasAccessRights(EnumerableRoles.project_manager))
				{
					mails = await (from employeeFirm in DBRepository.EmployeeFirms
								   from project in employeeFirm.Firm.Projects
								   from projectEf in project.ProjectEmployeeFirms
								   from mail in project.Mails
								   where dateRange.FirmId == employeeFirm.Firm.FirmID
								   where project.IsDeleted == false
								   where projectEf.IsDeleted == false
								   where projectEf.IsTeamLead == true
								   where projectEf.StopWorking == null
								   where projectEf.EmployeeFirm.ApplicationUserId.Id.Equals(userId)

								   // where dateRange.Start <= mail.RegisterDate
								   //  where mail.FinishDate <= dateRange.End
								   select new StatisticMailsViewModel
								   {
									   MailId = mail.MailId
								   }).ToListAsync();
					foreach (var item in mails)
					{
						var mail = await DBRepository.Mails.FindAsync(item.MailId);
						item.MailStatus = (mail.MailStatuses.Count > 0) ? mail.MailStatuses.Last() : null;
					}
				}
				else
				{
					if (User.IsHasAccessRights(EnumerableRoles.user))
					{
						mails = await (from employeeFirm in DBRepository.EmployeeFirms
									   from projectEf in employeeFirm.ProjectEmployeeFirms
									   from mail in projectEf.Project.Mails
									   where dateRange.FirmId == employeeFirm.Firm.FirmID
									   where employeeFirm.ApplicationUserId.Id.Equals(userId)
									   where projectEf.IsDeleted == false
									   where projectEf.StopWorking == null
									   //  where dateRange.Start <= mail.RegisterDate
									   //  where mail.FinishDate <= dateRange.End
									   select new StatisticMailsViewModel
									   {
										   MailId = mail.MailId
									   }).ToListAsync();
						foreach (var item in mails)
						{
							var mail = await DBRepository.Mails.FindAsync(item.MailId);
							item.MailStatus = (mail.MailStatuses.Count > 0) ? mail.MailStatuses.Last() : null;
						}
					}
				}
			}

			if (mails.Count > 0)
			{
				var InAction = mails.Where(mail => mail.MailStatus != null)
					   .Where(mail => dateRange.Start <= mail.MailStatus.AproveDate).Where(mail => mail.MailStatus.AproveDate <= dateRange.End)
					   .Where(mail => mail.MailStatus.MailStatusEnum == Entities.Mails.MailStatusEnum.InAction).Count();
				var Spent = mails.Where(mail => mail.MailStatus != null)
					   .Where(mail => dateRange.Start <= mail.MailStatus.AproveDate).Where(mail => mail.MailStatus.AproveDate <= dateRange.End)
					   .Where(mail => mail.MailStatus.MailStatusEnum == Entities.Mails.MailStatusEnum.Spent).Count();
				var Outbox = mails.Where(mail => mail.MailStatus != null)
					   .Where(mail => dateRange.Start <= mail.MailStatus.AproveDate).Where(mail => mail.MailStatus.AproveDate <= dateRange.End)
					   .Where(mail => mail.MailStatus.MailStatusEnum == Entities.Mails.MailStatusEnum.Outbox).Count();
				var CurrentQuestions = mails.Where(mail => mail.MailStatus != null)
					   .Where(mail => dateRange.Start <= mail.MailStatus.AproveDate).Where(mail => mail.MailStatus.AproveDate <= dateRange.End)
					   .Where(mail => mail.MailStatus.MailStatusEnum == Entities.Mails.MailStatusEnum.CurrentQuestions).Count();
				//var NotAproved = allMails - (InAction + Spent + Outbox + CurrentQuestions);

				List<ChartMailInfoViewModel> forSend = new List<ChartMailInfoViewModel>() {
					new ChartMailInfoViewModel(){ Name = "В работе", Percent = Convert.ToInt32( (double)InAction / (InAction + Spent + Outbox+ CurrentQuestions) * 100)},
					new ChartMailInfoViewModel(){ Name = "Отвеченные", Percent = Convert.ToInt32((double)Spent / (InAction + Spent + Outbox+ CurrentQuestions)  * 100 )},
					new ChartMailInfoViewModel(){ Name = "Исходящие", Percent = Convert.ToInt32((double)Outbox / (InAction + Spent + Outbox+ CurrentQuestions)  * 100)},
					new ChartMailInfoViewModel(){ Name = "Текущие вопросы", Percent = Convert.ToInt32((double) CurrentQuestions / (InAction + Spent + Outbox+ CurrentQuestions)  * 100)}
					//,new ChartMailInfo(){ Name = "Нет реакции", Percent = Convert.ToInt32((double)NotAproved / allMails * 100)}
				};
				return this.JsonNetIso(forSend, JsonRequestBehavior.AllowGet);
			}
			else
			{
				List<ChartMailInfoViewModel> forSend = new List<ChartMailInfoViewModel>() {
					new ChartMailInfoViewModel(){ Name = "В работе", Percent = 0 },
					new ChartMailInfoViewModel(){ Name = "Отвеченные", Percent = 0 },
					new ChartMailInfoViewModel(){ Name = "Исходящие", Percent = 0 },
					new ChartMailInfoViewModel(){ Name = "Текущие вопросы", Percent = 0 }
					//,new ChartMailInfo(){ Name = "Нет реакции", Percent = 0 }
				};
				return this.JsonNetIso(forSend, JsonRequestBehavior.AllowGet);
			}

		}

		[ActionName("GetNotificationClicksByRange")]
		public async Task<JsonResult> GetNotificationClicksByRangeAsync([Bind] string parametrs)
		{
			var localThanReplace = HttpUtility.UrlDecode(System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(parametrs)));
			var dateRange = Newtonsoft.Json.JsonConvert.DeserializeObject<DateMailInfoViewModel>(localThanReplace);
			var userId = User.Identity.GetUserId();
			var model = new PlotNotificationClicksInRangeViewModel()
			{
				End = dateRange.End,
				FirmId = dateRange.FirmId,
				Start = dateRange.Start,

				PlotDictionary = new Dictionary<string, PlotNotificationClicksInDayViewModel>()
				{

				},
				Legend = new Dictionary<string, string>() {
					{"previews", "Предпросмотр" },
					{ "aproves", "Подтверждены получения" },
					{ "setAsides", "Отложены" },
					{"nones", "Нет действий" },
					{ "ignoreClosings", "Закрыли" },
					{ "ignoreNotClosings", "Проигнорировали " }

				}
			};
			ICollection<Entities.Notifications.NotificationsClick> notificationClicks = null;

			if (dateRange.FirmId > -1)
			{
				var firm = await DBRepository.Firms.FindAsync(dateRange.FirmId);


				if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
				{
					notificationClicks = (from mail in firm.Mails
										  from resolution in mail.Resolution.ResolutionNotifier
										  from click in resolution.NotificationsClicks
										  where dateRange.Start <= click.ClickedTime
										  where click.ClickedTime <= dateRange.End
										  select click).ToList();
				}
				else
				{
					var user = await ((DbSet<ApplicationUser>)DBRepository.Users).FindAsync(userId);
					notificationClicks = (from ef in user.Employee.EmployeeFirms
										  from efInFirm in ef.Firm.EmployeeFirms
										  from mail in efInFirm.Firm.Mails
										  from resolution in mail.Resolution.ResolutionNotifier
										  from click in resolution.NotificationsClicks
										  where ef.Firm.FirmID == firm.FirmID
										  where dateRange.Start <= click.ClickedTime
										  where click.ClickedTime <= dateRange.End
										  select click).ToList();
				}
			}
			else
			{
				if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
				{
					notificationClicks = (from click in DBRepository.NotificationsClicks
										  where dateRange.Start <= click.ClickedTime
										  where click.ClickedTime <= dateRange.End
										  select click).ToList();
				}
				else
				{
					var user = await ((DbSet<ApplicationUser>)DBRepository.Users).FindAsync(userId);
					notificationClicks = (from ef in user.Employee.EmployeeFirms
										  from efInFirm in ef.Firm.EmployeeFirms
										  from mail in efInFirm.Firm.Mails
										  from resolution in mail.Resolution.ResolutionNotifier
										  from click in resolution.NotificationsClicks
										  where dateRange.Start <= click.ClickedTime
										  where click.ClickedTime <= dateRange.End
										  select click).ToList();
				}
			}
			foreach (var click in notificationClicks)
			{
				var date = click.ClickedTime.ToShortDateString();
				if (model.PlotDictionary.ContainsKey(date) == false)
				{
					model.PlotDictionary.Add(date, new PlotNotificationClicksInDayViewModel { });
				}
				model.PlotDictionary[date].Date = click.ClickedTime;
				if (model.PlotDictionary[date].Сounts == null)
				{
					model.PlotDictionary[date].Сounts = new Dictionary<string, int>() {
						{ "previews",0 },
						{ "aproves", 0},
						{ "setAsides",0 },
						{ "nones", 0 },
						{ "ignoreClosings", 0},
						{ "ignoreNotClosings", 0 }
					};
				}
				switch (click.NotificationsClickType)
				{
					case Entities.Notifications.NotificationsClickType.Preview:
						var key = "previews";
						CounterStatisticsForClicks(model, date, key);
						//model.PlotDictionary[date].Previews++;
						break;
					case Entities.Notifications.NotificationsClickType.Aprove:
						CounterStatisticsForClicks(model, date, "aproves");
						//model.PlotDictionary[date].Aproves++;
						break;
					case Entities.Notifications.NotificationsClickType.SetAside:
						CounterStatisticsForClicks(model, date, "setAsides");
						//	model.PlotDictionary[date].SetAsides++;
						break;
					case Entities.Notifications.NotificationsClickType.None:
						CounterStatisticsForClicks(model, date, "nones");
						//	model.PlotDictionary[date].Nones++;
						break;
					case Entities.Notifications.NotificationsClickType.IgnoreClosing:
						CounterStatisticsForClicks(model, date, "ignoreClosings");
						//	model.PlotDictionary[date].IgnoreClosing++;
						break;
					case Entities.Notifications.NotificationsClickType.IgnoreNotClosing:
						CounterStatisticsForClicks(model, date, "ignoreNotClosings");
						//model.PlotDictionary[date].IgnoreNotClosing++;
						break;

					default:
						break;
				}
			}
			return this.JsonNetIso(model, JsonRequestBehavior.AllowGet);
		}

		private static void CounterStatisticsForClicks(PlotNotificationClicksInRangeViewModel model, string date, string key)
		{
			InitCounters(model, date, key);
			model.PlotDictionary[date].Сounts[key]++;
		}

		private static void InitCounters(PlotNotificationClicksInRangeViewModel model, string date, string key)
		{
			if (model.PlotDictionary[date].Сounts.ContainsKey(key) == false)
			{
				model.PlotDictionary[date].Сounts.Add(key, 0);
			}
		}

		private async Task<List<SelectListItem>> GetMyFirmsToSelectedItemAsync(bool flag)
		{
			return await Task.Run(async () =>
			{
				List<SelectListItem> list = new List<SelectListItem>
				{
					new SelectListItem() { Value = "-1", Text = "Все", Selected = true }
				};

				if (flag)
				{

					var firms = await DBRepository.Firms.Where(cust => cust.IsDeleted == false)
					.Select(firm => new SelectListItem()
					{
						Text = firm.Name,
						Value = SqlFunctions.StringConvert((double)firm.FirmID).Trim(),
						Selected = false
					}).ToListAsync();
					list.AddRange(firms);
				}
				else
				{

					var userId = User.Identity.GetUserId();
					var user = await ((DbSet<ApplicationUser>)DBRepository.Users).FindAsync(userId);
					var firms = user.Employee.EmployeeFirms.Where(cust => cust.IsDeleted == false)
					.Select(firm => new SelectListItem()
					{
						Text = firm.Firm.Name,
						Value = SqlFunctions.StringConvert((double)firm.Firm.FirmID).Trim(),
						Selected = false
					}).ToList();
					list.AddRange(firms);
				}
				return list;
			});
		}
	}
}