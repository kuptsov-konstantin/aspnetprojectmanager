﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MvcInfos.ActiveDirectory;
using MvcInfos.Extensions;
using MvcInfos.Models.ViewModels;
using MvcInfos.Repository;

namespace MvcInfos.Controllers
{
	[AuthorizeActiveDirectory]
	public class FirmController : Controller
	{
		private IApplicationRepository DBRepository { get; set; }
		public FirmController(IApplicationRepository applicationRepository)
		{			
			DBRepository = applicationRepository;
		}

		[ActionName("GetMyFirms")]
		public async Task<JsonResult> GetMyFirmsAsync()
		{

			var UserInfo = DBRepository.Users.Find(User.Identity.GetUserId());
			var EmployeeFirmInfo = await DBRepository.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(UserInfo.Id)).Select(item => new FirmModel() { IdFirm = item.Firm.FirmID, NameFirm = item.Firm.Name }).ToListAsync();


			return this.JsonNetIso(EmployeeFirmInfo);
		}
	}
}