﻿using MvcInfos.ActiveDirectory;
using MvcInfos.ActiveDirectory.Extensions;
using MvcInfos.Extensions;
using MvcInfos.FileTypes;
using MvcInfos.Models.ViewModels.Upload;
using MvcInfos.Plugin.Interfaces;
using MvcInfos.Repository;
using MvcInfos.Settings;
using System;
using System.Data.Entity;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MvcInfos.Controllers
{
	[AuthorizeActiveDirectory]
	public class UploadController : Controller
	{
		private ActiveDirectorySetting ActiveDirectorySetting { get; set; }
		private IApplicationRepository DBRepository { get; set; }
		public IPlugins Plugins { get; set; }

		public UploadController(IApplicationRepository repos, IPlugins plugins, ActiveDirectorySetting activeDirectorySetting)
		{
			ActiveDirectorySetting = activeDirectorySetting;
			Plugins = plugins;
			DBRepository = repos;
		}

		public FileResult LoadUserImageFromActiveDirectory()
		{
			MemoryStream ms = ActiveDirectorySetting.GetImageFromActiveDirectory();
			if (ms == null)
			{
				return File("/images/user.png", MimeMapping.GetMimeMapping("/images/user.png"));
			}
			return File(ms.ToArray(), "image/jpeg");


			//return File(System.IO.File.ReadAllBytes(filePath), System.Web.MimeMapping.GetMimeMapping(filePath)); ;
		}

		public string GetTempFolder()
		{

#if DEBUG || DEBUGSERVERSQL
			return Path.GetTempPath();
#elif RELEASE && RELEASESERVERSQL
			return MvcInfos.Properties.Resources.TempFolder;
#elif (AZUREDEBUG || AZURERELEASE)

#endif

		}

		// GET: Upload
		public async Task<ActionResult> Base(int jsonerrors, HttpPostedFileBase file = null)
		{
			if (file != null && file.ContentLength > 0)
			{
				try
				{

					var validation = new FroalaEditor.FileValidation(
					 new string[] {
							"txt", "pdf", "doc", "xsl", "rtf", "csv", "docx", "xlsx",
							"dwg", "dfx", "m3d", "max", "rar", "zip", "gzip",
							"gif", "png", "jpg", "jpeg", "tiff", "tif", "djvu"
					 },
					 new string[] { "text/plain", "application/msword", "application/x-pdf", "application/pdf", "application/vnd.ms-excel",
						"image/gif", "image/jpeg", "image/pjpeg",
						"image/png", "image/tiff","text/csv",
						"application/vnd.oasis.opendocument.text",
						"application/vnd.oasis.opendocument.spreadsheet",
						"application/vnd.oasis.opendocument.presentation",
						"application/vnd.oasis.opendocument.graphics",
						"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
						"application/vnd.ms-powerpoint",
						"application/vnd.openxmlformats-officedocument.presentationml.presentation",
						"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
						"application/x-rar-compressed",
						"application/zip",
						"application/gzip",
						"application/octet-stream"
					 });
					var options = new FroalaEditor.FileOptions()
					{
						Fieldname = "file",
						Validation = validation
					};
					var uploadPath = GetTempFolder();
					var savedFile = file.Upload(uploadPath, options);
					var storedFile = new Entities.Files.UploadFile()
					{
						UploadFileID = savedFile.ID,
						AdditionalFileType = Entities.Files.UploadFileType.Temp,
						UploadedDate = DateTime.Now,
						Url = savedFile.SavedFileName,
						OrignFileName = savedFile.FileName
					};

					DBRepository.UploadFiles.Add(storedFile);
					DBRepository.Entry(storedFile).State = EntityState.Added;
					await DBRepository.SaveChangesAsync();

					return Json(new { @file = savedFile.ID }, JsonRequestBehavior.AllowGet);

				}
				catch (Exception ex)
				{
					//return Json(new { }, JsonRequestBehavior.AllowGet);

					ViewBag.Message = "ERROR:" + ex.Message.ToString();
					Trace.Fail(ex.Message.ToString());
				}
			}
			else
			{
				ViewBag.Message = "You have not specified a file.";
				Trace.Fail("You have not specified a file.");
			}
			return Json(new { }, JsonRequestBehavior.AllowGet);
		}


		private Tuple<string, double, double> CheckImage(string url)
		{
			try
			{
				var image = Image.FromFile(url);
				return new Tuple<string, double, double>(image.GetImageFormatName().ToUpper(), image.Width, image.Height);
			}
			catch (Exception)
			{
				return null;
			}
		}
		[ActionName("Info")]
		public async Task<JsonResult> InfoAsync(int jsonerrors, string file_id)
		{
			var fileId = Guid.Empty;
			if (Guid.TryParse(file_id, out fileId))
			{
				var fileInfo = await FileInfoAsync(fileId);
				if (fileInfo == null) Json(new HttpStatusCodeResult(HttpStatusCode.NotFound), JsonRequestBehavior.AllowGet);
				return this.JsonNetIso(fileInfo, JsonRequestBehavior.AllowGet);


			}
			else
			{
				return Json(new { }, JsonRequestBehavior.AllowGet);
			}
		}

		private async Task<UploadcareFileViewModel> FileInfoAsync(Guid fileId)
		{
			var file = await DBRepository.UploadFiles.FindAsync(fileId);
			if (file == null) return null;
			string serverPath = ResolveServerPath(file);
			var info = new FileInfo(serverPath);
			string mimeType = MimeMapping.GetMimeMapping(info.Name);
			var checkImg = CheckImage(info.FullName);
			var upload = new UploadcareFileViewModel()
			{
				IsStored = true,
				Done = info.Length,
				FileId = fileId,
				Total = info.Length,
				Size = info.Length,
				Uuid = fileId,
				Filename = file.Url,
				IsReady = true,
				OriginalFilename = file.OrignFileName,
				MimeType = mimeType
			};
			if (checkImg != null)
			{
				upload.IsImage = true;
				upload.ImageInfo = new UploadcareImageViewModel()
				{
					Format = checkImg.Item1,
					Height = checkImg.Item3,
					Width = checkImg.Item2,
					DatetimeOriginal = file.UploadedDate
				};
			}
			else
			{
				upload.IsImage = false;
			}
			return upload;
		}

		private string ResolveServerPath(Entities.Files.UploadFile file)
		{
			if (file.HostingFile == null)
			{
				var uploadPath = GetTempFolder();
#if DEBUG || DEBUGSERVERSQL
				return uploadPath + file.Url;
#elif RELEASE && RELEASESERVERSQL
				return FroalaEditor.File.GetAbsoluteServerPath(uploadPath + file.Url);
#elif (AZUREDEBUG || AZURERELEASE)

#endif
			}
			else
			{
				var path = Plugins.LoadedPlugins[file.HostingFile.PluginConnectionType].ToString(file.HostingFile.Settings);

				return path + "\\" + file.Url;
			}
		}

		[Route("Upload/{fileId}/-/preview/")]
		public async Task<ActionResult> GetPreviewFileById(Guid fileId)
		{
			var image = (Image)Plugins.GetPluginLogo(fileId);
			var filePath = "icon.bng";
			if (image == null)
			{
				var file = await DBRepository.UploadFiles.FindAsync(fileId);
				if (file == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);
				filePath = ResolveServerPath(file);
				return File(System.IO.File.ReadAllBytes(filePath), MimeMapping.GetMimeMapping(filePath), file.OrignFileName);
			}
			else
			{
				return File(image.ToPng(), MimeMapping.GetMimeMapping("f.png"));
			}
		}

		[Route("Upload/{id}/-/crop/{width}x{height}/{x},{y}/-/preview/")]
		public async Task<ActionResult> GetFileById(string id, double? width, double? height, double? x, double? y)
		{
			var fileId = Guid.Empty;
			if (Guid.TryParse(id, out fileId))
			{
				var image = (Image)Plugins.GetPluginLogo(fileId);
				var filePath = "icon.bng";
				if (image == null)
				{
					var file = await DBRepository.UploadFiles.FindAsync(fileId);
					if (file == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);
					filePath = ResolveServerPath(file);
					image = Image.FromFile(filePath);
				}
				Rectangle cropRect = new Rectangle(Convert.ToInt32(x.Value), Convert.ToInt32(y.Value), Convert.ToInt32(width.Value), Convert.ToInt32(height.Value));
				var cropped = image.Crop(cropRect);
				return File(cropped.ToPng(), MimeMapping.GetMimeMapping("f.png"));
			}

			return null;
		}

		/*
		 * best — useful for hi-res images such as photos in case you want to get perfect quality without paying attention to file size. ≈170% file size.
		 * better — can be used to render relatively small and detailed previews. ≈125% file size compared to the normal quality.	
			normal — the default setting, is fine in most cases.				
			lighter — is especially useful when applied to relatively large images in order to save traffic without significant losses in quality. ≈80% file size.
			lightest — useful for retina resolutions, when you don’t have to worry about the quality of each individual pixel. ≈50% file size.
		 */

		[Route("Upload/{id}/-/quality/{type}/")]
		///Upload/ff0f1b57-860e-4306-95a9-edc4fbdb3262/-/quality/lightest/-/scale_crop/110x110/center/
		public async Task<ActionResult> GetQualityAsync(string id, string type)
		{
			var fileId = Guid.Empty;
			if (Guid.TryParse(id, out fileId))
			{
				var image = (Image)Plugins.GetPluginLogo(fileId);
				var filePath = "icon.bng";
				if (image == null)
				{
					var file = await DBRepository.UploadFiles.FindAsync(fileId);
					if (file == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound);
					filePath = ResolveServerPath(file);
					image = Image.FromFile(filePath);
				}
				ImageCodecInfo jpgEncoder = image.GetImageFormat().GetEncoder();
				Encoder myEncoder = Encoder.Quality;
				EncoderParameters myEncoderParameters = new EncoderParameters(1);

				long quality = SetQuality(type);

				EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, quality);
				myEncoderParameters.Param[0] = myEncoderParameter;
				MemoryStream ms = new MemoryStream();
				image.Save(ms, jpgEncoder, myEncoderParameters);
				return File(ms.ToPng(), MimeMapping.GetMimeMapping("f.png"));
			}
			return null;
		}

		private static Int64 SetQuality(string type)
		{
			Int64 quality = 50L;
			//	Bitmap bmp1 = new Bitmap(image);
			switch (type)
			{
				case "best":
					{
						quality = 100L;
						break;
					}
				case "better":
					{
						quality = 80L;
						break;
					}
				case "normal":
					{
						quality = 60L;
						break;
					}
				case "lighter":
					{
						quality = 40L;
						break;
					}
				case "lightest":
					{
						quality = 20L;
						break;
					}
			}

			return quality;
		}

		[Route("Upload/{id}/-/quality/{type}/-/scale_crop/{width}x{height}/{position}/")]
		///Upload/ff0f1b57-860e-4306-95a9-edc4fbdb3262/-/quality/lightest/-/scale_crop/110x110/center/
		public async Task<FileResult> GetQualityAsync(string id, double? width, double? height, string type, string position)
		{
			var fileId = Guid.Empty;
			if (Guid.TryParse(id, out fileId))
			{
				var image = (Image)Plugins.GetPluginLogo(fileId);
				var filePath = "icon.bng";
				if (image == null)
				{
					var file = await DBRepository.UploadFiles.FindAsync(fileId);
					filePath = ResolveServerPath(file);
					image = Image.FromFile(filePath);
				}
				Int64 quality = SetQuality(type);

				var imageByte = image.SaveCroppedImage(quality, Convert.ToInt32(width.Value), Convert.ToInt32(height.Value));
				MemoryStream ms = new MemoryStream(imageByte);
				return File(ms.ToPng(), MimeMapping.GetMimeMapping("f.png"));

			}
			return null;
		}


		//[Route("/Upload/{id}/-/preview/{width}x{height}/-/setfill/{color}/-/format/{format}/-/progressive/{progressive}/")]
		//public async Task<FileResult> Preview(string id, double? width, double? height, string color, string format, string progressive)
		//{

		//}
		[ActionName("group")]
		public async Task<ActionResult> Group(string pub_key, string[] files)
		{

			var group = new Entities.Files.UploadGroupFile()
			{
				DatetimeCreated = DateTime.Now,
				DatetimeStored = DateTime.Now
			};
			var uploadcareGroupFileModelView = new UploadcareGroupFileViewModel()
			{
				DatetimeCreated = group.DatetimeCreated,
				DatetimeStored = group.DatetimeStored,
				Id = group.GroupFileId,
				Url = $"/group/{group.GroupFileId}"
			};
			foreach (var item in files)
			{
				var file = item.Substring(1, item.IndexOf('/', 2) - 1);
				var filesF = await DBRepository.UploadFiles.FindAsync(Guid.Parse(file));
				group.UploadFiles.Add(filesF);
				var fileInfo = await FileInfoAsync(Guid.Parse(file));
				uploadcareGroupFileModelView.Files.Add(fileInfo);
			}
			DBRepository.GroupFiles.Add(group);
			DBRepository.Entry(group).State = EntityState.Added;
			await DBRepository.SaveChangesAsync();

			uploadcareGroupFileModelView.Id = group.GroupFileId;
			uploadcareGroupFileModelView.Url = $"/group/{group.GroupFileId}";
			uploadcareGroupFileModelView.CdnUrl = $"/group/{group.GroupFileId}";


			return this.JsonNetIso(uploadcareGroupFileModelView, JsonRequestBehavior.AllowGet);
		}

		[ActionName("FileTypeResolve")]
		public async Task<FileResult> FileTypeResolveAsync(Guid fileId)
		{
			var file = await DBRepository.UploadFiles.FindAsync(fileId);
			if (file == null) return null;
			string filePath = ResolveServerPath(file);
			var typeFile = FileTypeResolver.FileTypeResolve(filePath);
			return File(typeFile, MimeMapping.GetMimeMapping("file.svg"));

		}


	}
}