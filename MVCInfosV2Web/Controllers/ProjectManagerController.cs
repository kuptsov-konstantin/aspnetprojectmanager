﻿using LingvoNET;
using Microsoft.AspNet.Identity;
using MvcInfos.ActiveDirectory;
using MvcInfos.ActiveDirectory.Extensions;
using MvcInfos.Attributes;
using MvcInfos.Entities.Firm;
using MvcInfos.Entities.UserConfigurations;
using MvcInfos.Models.ViewModels;
using MvcInfos.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace MvcInfos.Controllers
{
	[AuthorizeActiveDirectory(Roles = "project_manager,manager,administrator_server,administrator_firm", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager,PMServerSManager")]
    public class ProjectManagerController : Controller
    {
        private IApplicationRepository DBRepository { get; set; }
        public ProjectManagerController(IApplicationRepository repos)
        {
            DBRepository = repos;
        }

        // GET: ProjectManager
        public ActionResult Index()
        {
            return RedirectToAction("stats");
        }

        public ActionResult ProjectDetail(int idProject)
        {
            return View();
        }
        public async Task<ActionResult> PrintProjects(int? idFirm)
        {
            if (idFirm == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var currentUserId = User.Identity.GetUserId();
            var employeeFirm = await DBRepository.EmployeeFirms.Where(firm => firm.Firm.FirmID == idFirm).Where(ff => ff.ApplicationUserId.Id.Equals(currentUserId)).Select(p => p).FirstOrDefaultAsync();
            if (employeeFirm != default(EmployeeFirm))
            {
                ViewData["firmName"] = employeeFirm.Firm.Name;
                ViewData["idFirm"] = idFirm;
            }

            return View();
        }
        public async Task<ActionResult> GetProjects(int? idFirm, bool? isCurrent)
        {
            if (idFirm == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (User.IsHasAccessRights(EnumerableRoles.project_manager))
            {
                var currentUserId = User.Identity.GetUserId();
                var employeeFirm = await DBRepository.EmployeeFirms.Where(firm => firm.Firm.FirmID == idFirm).Where(ff => ff.ApplicationUserId.Id.Equals(currentUserId)).Select(p => p).FirstOrDefaultAsync();
                if (employeeFirm != default(EmployeeFirm))
                {
                    ViewData["firmName"] = employeeFirm.Firm.Name;
                    ViewData["idFirm"] = idFirm;
                    if (isCurrent != null)
                    {
                        var myProjects = employeeFirm.ProjectEmployeeFirms.Where(prj => (prj.IsDeleted == false) && (prj.IsCurrent == isCurrent)).Select(o => o.Project.ProjectID);
                        return Json(myProjects.ToList(), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var myProjects = employeeFirm.ProjectEmployeeFirms.Where(prj => (prj.IsDeleted == false)).Select(o => o.Project.ProjectID);
                        return Json(myProjects.ToList(), JsonRequestBehavior.AllowGet);

                    }
                }
            }
            if (User.IsHasAccessRights(EnumerableRoles.manager) || User.IsHasAccessRights(EnumerableRoles.administrator_firm))
            {
                var currentUserId = User.Identity.GetUserId();
                var employeeFirm = await DBRepository.EmployeeFirms.Where(firm => firm.Firm.FirmID == idFirm).Where(ff => ff.ApplicationUserId.Id.Equals(currentUserId)).Select(p => p).FirstOrDefaultAsync();
                if (employeeFirm != default(EmployeeFirm))
                {
                    ViewData["firmName"] = employeeFirm.Firm.Name;
                    ViewData["idFirm"] = idFirm;
                    if (isCurrent != null)
                    {
                        var myProjects = await DBRepository.Projects.Where(project => (project.PlannedFinishDate > DateTime.Now) == isCurrent).Select(o => o.ProjectID).ToListAsync();
                        return Json(myProjects, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var myProjects = await DBRepository.Projects.Select(o => o.ProjectID).ToListAsync();
                        return Json(myProjects, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
            {
                ViewData["firmName"] = (await DBRepository.Firms.FindAsync(idFirm)).Name;
                ViewData["idFirm"] = idFirm;
                if (isCurrent != null)
                {
                    var myProjects = await DBRepository.Projects.Where(project => (project.PlannedFinishDate > DateTime.Now) == isCurrent).Select(o => o.ProjectID).ToListAsync();
                    return Json(myProjects, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var myProjects = await DBRepository.Projects.Select(o => o.ProjectID).ToListAsync();
                    return Json(myProjects, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new List<int>(), JsonRequestBehavior.AllowGet);
        }

       private ActionResult GetProjectStatus(int? idProject)
        {
            ///TODO: Реализовать получение текущего статуса
            return null;
        }
        public async Task<ActionResult> _ProjectRowPartial(int? idFirm, int? idProject)
        {
            if ((idFirm == null) || (idProject == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (User.IsHasAccessRights(EnumerableRoles.project_manager))
            {
                var currentUserId = User.Identity.GetUserId();
                var employeeFirm = await DBRepository.EmployeeFirms.Where(firm => firm.Firm.FirmID == idFirm).Where(ff => ff.ApplicationUserId.Id.Equals(currentUserId)).Select(p => p).FirstOrDefaultAsync();
                if (employeeFirm != default(EmployeeFirm))
                {
                    var project = employeeFirm.ProjectEmployeeFirms.Where(proj => proj.Project.ProjectID == idProject.Value).Select(proj => new { @projectName = proj.Project.ProjectName, @createdDate = proj.Project.ProjectStartDate, @projectCode = proj.Project.ProjectCode }).FirstOrDefault();
                    if (project != null)
                    {
                        var userInfo = (await DBRepository.Projects.FindAsync(idProject)).ProjectEmployeeFirms.Where(projectEF => projectEF.StopWorking == null).Select(projectEF => new Tuple<string, string, string>(projectEF.EmployeeFirm.ApplicationUserId.Id, projectEF.EmployeeFirm.ApplicationUserId.Employee.Person.FamilyName, projectEF.EmployeeFirm.ApplicationUserId.Employee.Person.PhotoUrl)).ToList();
                        return PartialView("ProjectManagePartials\\_ProjectRowPartial", new ProjectRowPartialModel() { IdFirm = idFirm.Value, IdProject = idProject.Value, CreatedData = project.createdDate, ProjectName = project.projectName, ProcentComplete = 0, Stage = "Разрабатывается", UserInfo = userInfo, ProjectCod = project.projectCode });
                    }
                }
            }
            if (User.IsHasAccessRights(EnumerableRoles.manager) || User.IsHasAccessRights(EnumerableRoles.administrator_firm))
            {
                var currentUserId = User.Identity.GetUserId();
                var employeeFirm = await DBRepository.EmployeeFirms.Where(firm => firm.Firm.FirmID == idFirm).Where(ff => ff.ApplicationUserId.Id.Equals(currentUserId)).Select(p => p).FirstOrDefaultAsync();
                if (employeeFirm != default(EmployeeFirm))
                {
                    var project = (await DBRepository.Firms.FindAsync(idFirm)).Projects.Where(proj => proj.ProjectID == idProject).Select(proj => new { @projectName = proj.ProjectName, @createdDate = proj.ProjectStartDate, @projectCode = proj.ProjectCode }).FirstOrDefault();
                    if (project != null)
                    {
                        var userInfo = (await DBRepository.Projects.FindAsync(idProject)).ProjectEmployeeFirms.Where(projectEF => projectEF.StopWorking == null).Select(projectEF => new Tuple<string, string, string>(projectEF.EmployeeFirm.ApplicationUserId.Id, projectEF.EmployeeFirm.ApplicationUserId.Employee.Person.FamilyName, projectEF.EmployeeFirm.ApplicationUserId.Employee.Person.PhotoUrl)).ToList();
                        return PartialView("ProjectManagePartials\\_ProjectRowPartial", new ProjectRowPartialModel() { IdFirm = idFirm.Value, IdProject = idProject.Value, CreatedData = project.createdDate, ProjectName = project.projectName, ProcentComplete = 0, Stage = "Разрабатывается", UserInfo = userInfo, ProjectCod = project.projectCode });
                    }
                }
            }
            if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
            {
                var project = (await DBRepository.Firms.FindAsync(idFirm)).Projects.Where(proj => proj.ProjectID == idProject).Select(proj => new { @projectName = proj.ProjectName, @createdDate = proj.ProjectStartDate, @projectCode = proj.ProjectCode }).FirstOrDefault();
                if (project != null)
                {
                    var userInfo = (await DBRepository.Projects.FindAsync(idProject)).ProjectEmployeeFirms.Where(projectEF => projectEF.StopWorking == null).Select(projectEF => new Tuple<string, string, string>(projectEF.EmployeeFirm.ApplicationUserId.Id, projectEF.EmployeeFirm.ApplicationUserId.Employee.Person.FamilyName, projectEF.EmployeeFirm.ApplicationUserId.Employee.Person.PhotoUrl)).ToList();
                    return PartialView("ProjectManagePartials\\_ProjectRowPartial", new ProjectRowPartialModel() { IdFirm = idFirm.Value, IdProject = idProject.Value, CreatedData = project.createdDate, ProjectName = project.projectName, ProcentComplete = 0, Stage = "Разрабатывается", UserInfo = userInfo, ProjectCod = project.projectCode });
                }
            }
            return PartialView("ProjectManagePartials\\_ProjectRowPartial", new ProjectRowPartialModel() { });
        }
        /// <summary>
        /// Получает список сотрудников по отделам
        /// </summary>
        /// <param name="idFirm">ID организации</param>
        /// <returns></returns>
        public async Task<List<SelectListItem>> GetAllEmployeeToSelectedItemAsync(int idFirm)
        {
            return await Task.Run(async () =>
			{
				var notInDep = new SelectListGroup() { Name = "Не состоит в отделе" };
				var employee = (from _department in (await DBRepository.Firms.FindAsync(idFirm)).Departments.Select(_department => new { SGroup = new SelectListGroup() { Name = _department.Name }, id = _department.DepartmentID })
								from _ef in DBRepository.Departments.Find(_department.id).EmployeeFirms
                                select new SelectListItem()
                                {
                                    Selected = false,
                                    Value = _ef.ApplicationUserId.Id,
                                    Text = _ef.ApplicationUserId?.Employee?.Person?.Fio ?? _ef.ApplicationUserId.UserName,
                                    Group = _department.SGroup
                                }).ToList();

                var employeeWithoutDep = (await DBRepository.Firms.FindAsync(idFirm)).EmployeeFirms.Where(_ef => (_ef.Departments == null) || (_ef.Departments.Count() == 0)).Select(_ef =>
                                  new SelectListItem()
                                  {
                                      Selected = false,
                                      Value = _ef.ApplicationUserId.Id,
                                      Text = _ef.ApplicationUserId?.Employee?.Person?.Fio ?? _ef.ApplicationUserId.UserName,
                                      Group = notInDep
								  }).ToList();


                employee.AddRange(employeeWithoutDep);
                return employee;
            });
        }
        public async Task<ActionResult> CreateProject(int? idFirm)
        {
            var result = await GetAllEmployeeToSelectedItemAsync(idFirm.Value);
            if (result.Count > 0)
            {
                result[0].Selected = true;
            }
            ViewData["idFirm"] = idFirm;
            return View("_CreateProject", new CreateProjectModel()
            {
                // pmvm = InitMenu().pmvm,
                ProjectStartDate = DateTime.Now,
                PlannedFinishDate = DateTime.Now.AddYears(1),
                Items = result
            });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateProject([Bind]CreateProjectModel projectModel, int? idFirm)
        {
            if (ModelState.IsValid)
            {
                var findFirm = DBRepository.Firms.Find(idFirm);
                if (findFirm.GetProject(projectModel.ProjectCod) != null)
                {
                    ModelState.AddModelError("project_cod", "Такой код уже зарегистрирован.");
                }
                var _user = DBRepository.Users.Find(projectModel.IdAUser);
                if (_user == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }


                var project = new Project()
                {
                    PlannedFinishDate = projectModel.PlannedFinishDate,
                    ProjectName = projectModel.ProjectName,
                    ProjectCode = projectModel.ProjectCod,
                    ProjectStartDate = projectModel.ProjectStartDate
                };
                project = DBRepository.Projects.Add(project);
                var ef_user = _user.Employee.EmployeeFirms.Where(ef => ef.Firm.FirmID == idFirm).FirstOrDefault();
                var projectEF = new ProjectEmployeeFirm()
                {
                    IsCurrent = true,
                    IsTeamLead = true,
                    StartWorking = projectModel.ProjectStartDate
                };

                DBRepository.ProjectEmployeeFirms.Add(projectEF);
                project.ProjectEmployeeFirms.Add(projectEF);
                ef_user.ProjectEmployeeFirms.Add(projectEF);
                findFirm.Projects.Add(project);

                ViewData["isValid"] = true;
                ViewData["success"] = true;
                ViewData["id_firm"] = idFirm;
                DBRepository.Entry(findFirm).State = EntityState.Modified;
                await DBRepository.SaveChangesAsync();
                var project1 = (await DBRepository.Firms.FindAsync(idFirm)).Projects.Where(_prj => _prj.ProjectCode.Equals(projectModel.ProjectCod)).FirstOrDefault();
                return RedirectToAction("PrintProjects", new { @idFirm = idFirm, @isCurrent = true });
            }
            ViewData["success"] = false;
            ViewData["isValid"] = false;
            projectModel.Items = await GetAllEmployeeToSelectedItemAsync(idFirm.Value);
            return View("_createproject", projectModel);
        }
        public async Task<ActionResult> ProjectManage(int? idFirm, int? idProject)
        {
            if ((idFirm == null) || (idProject == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var project = (await DBRepository.Projects.FindAsync(idProject));
            if (project == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            var typeLayers = project.ProjectStageTypes.Select(types => types.Type);
            List<string> layers = new List<string>();
            List<string> adddedMessage = new List<string>();

            foreach (var item in typeLayers)
            {
                layers.Add(Nouns.FindOne(item, animacy: Animacy.Inanimate)[Case.Accusative, LingvoNET.Number.Plural]);
                try
                {
                    adddedMessage.Add($"Добавить {Adjectives.FindOne("новый", Comparability.Comparable)[Case.Accusative, Nouns.FindOne(item).Gender]} {Nouns.FindOne(item.ToLower(), animacy: Animacy.Inanimate)[Case.Accusative, LingvoNET.Number.Singular]}");
                }
                catch (Exception)
                {
                    adddedMessage.Add(null);
                }

            }
            ViewData["adddedMessage"] = adddedMessage;
            ViewData["layers"] = string.Join("/", layers.ToArray());
            ViewData["projectCode"] = project?.ProjectCode;
            ViewData["idFirm"] = idFirm;
            ViewData["idProject"] = idProject;
            return View();
        }
        public async Task<ActionResult> _DropdownPartial(int? idFirm, int? idProject)
        {
            if ((idFirm == null) || (idProject == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var result = await GetAllEmployeeToSelectedItemAsync(idFirm.Value);
            ViewData["idFirm"] = idFirm;

            return PartialView("ProjectManagePartials\\_DropdownPartial", new ForDropdownOne() { Items = result });
        }
        [HttpGet]
        public async Task<ActionResult> _ProjectInfoBasicEditorPartial(int? idFirm, int? idProject)
        {
            if ((idFirm == null) || (idProject == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var result = (await DBRepository.Firms.FindAsync(idFirm)).Projects
                .AsParallel().Where(proj => proj.ProjectID == idProject)
                .AsParallel().Select(proj => new
                {
                    @id = proj.ProjectID,
                    @projectCode = proj.ProjectCode,
                    @projectName = proj.ProjectName,
                    @projectStartDate = proj.ProjectStartDate,
                    @projectEndDate = proj.PlannedFinishDate,
                    @teamLead = proj.ProjectEmployeeFirms.AsParallel().Where(projectEmployeeFirms => ((projectEmployeeFirms.IsTeamLead == true) && (projectEmployeeFirms.StopWorking == null))).Select(pef => new
                    {
                        @id = pef.EmployeeFirm.ApplicationUserId.Id,
                        @fio = pef.EmployeeFirm.ApplicationUserId.Employee.Person.Fio
                    }).FirstOrDefault()
                }).FirstOrDefault();



            return PartialView("ProjectManagePartials\\_ProjectInfoBasicEditorPartial", new ProjectInfoModel()
            {
                Id = result.id,
                ProjectEndDate = result.projectEndDate,
                ProjectName = result.projectName,
                ProjectStartDate = result.projectStartDate,
                ProjectCode = result.projectCode,
                TeamLead = new ProjectUserInfoModel()
                {
                    Fio = result.teamLead.fio,
                    Id = result.teamLead.id
                }
            });
        }
        [HttpGet]
        public async Task<ActionResult> _GetMainProjectInfo(int? idFirm, int? idProject)
        {
            if ((idFirm == null) || (idProject == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var result = (await DBRepository.Firms.FindAsync(idFirm)).Projects
               .Where(proj => proj.ProjectID == idProject)
               .Select(proj => new
               {
                   @id = proj.ProjectID,
                   @projectName = proj.ProjectName,
                   @projectStartDate = proj.ProjectStartDate,
                   @projectEndDate = proj.PlannedFinishDate,
                   @teamLead = proj.ProjectEmployeeFirms.Where(projectEmployeeFirms => ((projectEmployeeFirms.IsTeamLead == true) && (projectEmployeeFirms.StopWorking == null))).Select(pef => new
                   {
                       @id = pef.EmployeeFirm.ApplicationUserId.Id,
                       @fio = pef.EmployeeFirm.ApplicationUserId.Employee.Person.Fio
                   }).FirstOrDefault()
               }).FirstOrDefault();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public async Task<ActionResult> _GetLayers(int? idFirm, int? idProject)
        {
            if ((idFirm == null) || (idProject == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var project = (await DBRepository.Firms.FindAsync(idFirm)).Projects.Where(proj => proj.ProjectID == idProject).FirstOrDefault();
            var steps = project.ProjectStageTypes.Select(types => new { @id = types.ID, @level = types.Level, @type = types.Type });
            return Json(steps, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateJsonAntiForgeryToken]
        public async Task<ActionResult> _SetLayer(string parametrs)
        {
            if (parametrs == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var localThanReplace = HttpUtility.UrlDecode(System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(parametrs)));
            var convertedParametrs = HttpUtility.ParseQueryString(localThanReplace);
            var idFirm = Convert.ToInt32(convertedParametrs["idFirm"]);
            var idProject = Convert.ToInt32(convertedParametrs["idProject"]);
            var layersJson = convertedParametrs["layersJson"];
            var listLayers = new JavaScriptSerializer().Deserialize<LevelType[]>(layersJson);

            var project = (await DBRepository.Firms.FindAsync(idFirm)).Projects.Where(proj => proj.ProjectID == idProject).FirstOrDefault();
            for (int i = 0; i < listLayers.Count(); i++)
            {
                var typ = project.ProjectStageTypes.Where(types => types.Level == i).FirstOrDefault();
                if (typ == null)
                {
                    var projectStageType = new ProjectStageType()
                    {
                        Level = i,
                        Type = listLayers[i].Type,
                        Project = project
                    };
                    project.ProjectStageTypes.Add(projectStageType);
                    DBRepository.ProjectStageTypes.Add(projectStageType);
                }
                else
                {
                    typ.Type = listLayers[i].Type;
                }
            }
            DBRepository.Entry(project).State = EntityState.Modified;
            await DBRepository.SaveChangesAsync();

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        [HttpGet]
        public async Task<ActionResult> _GetStagesEdit(int? idFirm, int? idProject, int? idStage)
        {
            if ((idFirm == null) || (idProject == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if ((idStage == -1) || (idStage == null))
            {
                var result = (await DBRepository.Firms.FindAsync(idFirm)).Projects
                    .Where(proj => proj.ProjectID == idProject)
                    .Select(proj => proj.ProjectStages
                    .Where(stages => stages.IsDeleted == false)
                    .Select(stages => new
                    {
                        @stageName = stages.Name,
                        @idCurrent = stages.ProjectStageId,
                        @hasChildren = stages.SubProjectStages.Count > 0,
                        @levelId = stages.TypeOfStage.ID,
                        @intersectionDate = stages.IntersectionDate,
                        @idParent = (stages.Parent == null) ? Guid.Empty : stages.Parent.ProjectStageId,
                        @startDate = stages.StartStage,
                        @stopDate = stages.StopStage
                    })).ToList();
                if (result.Count > 0)
                {
                    return Json(result[0], JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var result = (await DBRepository.ProjectStages.FindAsync(idStage)).SubProjectStages.Where(stages => stages.IsDeleted == false).Select(stages => new { @stageName = stages.Name, @idCurrentStage = stages.ProjectStageId, @hasChildren = stages.SubProjectStages.Count > 0 }).ToList();
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        private async Task SaveStageNode(Project project, ProjectStage parent, ICollection<ProjectStages> arrayPS)
        {
            foreach (var item in arrayPS)
            {
                var findStage = project.ProjectStages.Where(stage => stage.ProjectStageId.Equals(item.Id.Id)).FirstOrDefault();

                if (item.IsChanged == true)
                {
                    var level = project.ProjectStageTypes.Where(sType => sType.ID == item.LevelId).FirstOrDefault();
                    if (findStage == null)
                    {
                        findStage = new ProjectStage()
                        {
                            IsDeleted = false,
                            Name = item.Name,
                            StartStage = item.StartDate,
                            StopStage = item.EndDate,
                            Status = ProjectStageStatus.Wait,
                            Project = project,
                            IntersectionDate = item.IntersectionDate,
                            ProjectStageId = item.Id.Id,
                            TypeOfStage = level
                        };
                        if (parent != null)
                        {
                            findStage.Parent = parent;
                            parent.SubProjectStages.Add(findStage);
                        }
                        DBRepository.Entry(findStage).State = EntityState.Added;
                        level.ProjectStages.Add(findStage);
                        project.ProjectStages.Add(findStage);
                        DBRepository.ProjectStages.Add(findStage);
                    }
                    else
                    {
                        findStage.IsDeleted = false;
                        findStage.Name = item.Name;
                        findStage.StartStage = item.StartDate;
                        findStage.StopStage = item.EndDate;
                        findStage.Status = ProjectStageStatus.Wait;
                        findStage.Project = project;
                        findStage.IntersectionDate = item.IntersectionDate;
                        findStage.TypeOfStage = level;
                        DBRepository.Entry(findStage).State = EntityState.Modified;

                    }
                }
                if ((item.Children != null) && (item.Children.Count > 0))
                {
                    await SaveStageNode(project, findStage, item.Children);
                }
            }
        }

        [HttpPost]
        [ValidateJsonAntiForgeryToken]
        public async Task<ActionResult> _SaveNewStage([Bind]string parametrs)
        {
            if (parametrs == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var localThanReplace = HttpUtility.UrlDecode(System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(parametrs)));
            var convertedParametrs = HttpUtility.ParseQueryString(localThanReplace);
            var idFirm = Convert.ToInt32(convertedParametrs["idFirm"]);
            var idProject = Convert.ToInt32(convertedParametrs["idProject"]);
            var idParrent = Convert.ToInt32(convertedParametrs["idParrent"]);
            var tree = convertedParametrs["treeStage"];
            var serializedResult = JsonConvert.DeserializeObject<ProjectStages[]>(tree);
            var project = (await DBRepository.Firms.FindAsync(idFirm)).Projects.Where(proj => proj.ProjectID == idProject).FirstOrDefault();
            await SaveStageNode(project, null, serializedResult);
            DBRepository.Entry(project).State = EntityState.Modified;
            await DBRepository.SaveChangesAsync();
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateJsonAntiForgeryToken]
        public async Task<ActionResult> _SaveMainProjectInfo(int? idFirm, int? idProject, string whatReplace, string thanReplace)
        {
            if ((idFirm == null)|| (idProject == null)|| (whatReplace == null) || (thanReplace == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var localWhatReplace = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(whatReplace));
            var localThanReplace = HttpUtility.UrlDecode(System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(thanReplace)));
            var project = (await DBRepository.Firms.FindAsync(idFirm)).Projects.Where(proj => proj.ProjectID == idProject.Value).FirstOrDefault();
            switch (localWhatReplace)
            {
                case "projectName":
                    {
                        project.ProjectName = localThanReplace;
                        await DBRepository.SaveChangesAsync();
                        break;
                    }

                case "projectCod":
                    {
                        project.ProjectCode = localThanReplace;
                        await DBRepository.SaveChangesAsync();
                        break;
                    }

                case "projectStartDate":
                    {

                        project.ProjectStartDate = Convert.ToDateTime(localThanReplace, System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat);
                        await DBRepository.SaveChangesAsync();
                        break;
                    }

                case "projectFinishDate":
                    {
                        project.PlannedFinishDate = Convert.ToDateTime(localThanReplace, System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat);
                        await DBRepository.SaveChangesAsync();
                        break;
                    }

                case "projectTeamLead":
                    {

                        var currentTeamLead = project.ProjectEmployeeFirms.Where(pef => (pef.IsTeamLead == true) && (pef.StopWorking == null)).FirstOrDefault();
                        if (currentTeamLead != null)
                        {
                            currentTeamLead.StopWorking = DateTime.Now;
                        }
                        var newTeamLead = project.ProjectEmployeeFirms.Where(pef => pef.EmployeeFirm.ApplicationUserId.Id.Equals(localThanReplace)).FirstOrDefault();
                        if (newTeamLead != null)
                        {
                            newTeamLead.StopWorking = DateTime.Now;
                        }

                        var newTeamLeadEF = (await DBRepository.Firms.FindAsync(idFirm)).EmployeeFirms.Where(employeeFirm => employeeFirm.ApplicationUserId.Id.Equals(localThanReplace)).FirstOrDefault();

                        var newPEF = new ProjectEmployeeFirm()
                        {
                            IsTeamLead = true,
                            EmployeeFirm = newTeamLeadEF,
                            StartWorking = DateTime.Now,
                            Project = project
                        };

                        DBRepository.ProjectEmployeeFirms.Add(newPEF);
                        project.ProjectEmployeeFirms.Add(newPEF);

                        await DBRepository.SaveChangesAsync();
                        break;
                    }
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        public async Task<ActionResult> _GetEmployeesEdit(int? idFirm, int? idProject, bool? isCurrent)
        {
            if ((idFirm == null) || (idProject == null))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (User.IsHasAccessRights(EnumerableRoles.project_manager))
            {
                var findingProject = (await DBRepository.Firms.FindAsync(idFirm)).Projects.Where(project => project.ProjectID == idProject).FirstOrDefault();
                if (findingProject == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                }
                var userId = User.Identity.GetUserId();
                var userIsInProject = findingProject.ProjectEmployeeFirms.Where(pef => pef.EmployeeFirm.ApplicationUserId.Id.Equals(userId)).FirstOrDefault();
                if (userIsInProject == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                }

                if (isCurrent == null)
                {
                    var selected = findingProject.ProjectEmployeeFirms.Select(pef => new
                    {
                        @Id = pef.EmployeeFirm.ApplicationUserId.Id,
                        @Fio = pef.EmployeeFirm.ApplicationUserId.Employee.Person.Fio
                    });
                    return Json(selected, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var selected = findingProject.ProjectEmployeeFirms.Where(pef=>(pef.StopWorking == null) == isCurrent).Select(pef => new
                    {
                        @Id = pef.EmployeeFirm.ApplicationUserId.Id,
                        @Fio = pef.EmployeeFirm.ApplicationUserId.Employee.Person.Fio
                    });
                    return Json(selected, JsonRequestBehavior.AllowGet);
                }


                //(project.PlannedFinishDate > DateTime.Now)
            }
            if (User.IsHasAccessRights(EnumerableRoles.manager) || User.IsHasAccessRights(EnumerableRoles.administrator_firm))
            {
                var findingProject = (await DBRepository.Firms.FindAsync(idFirm)).Projects.Where(project => project.ProjectID == idProject).FirstOrDefault();
                if (findingProject == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                }
                var userId = User.Identity.GetUserId();
                var userIsInFirm = (await DBRepository.Firms.FindAsync(idFirm)).EmployeeFirms.Where(pef => pef.ApplicationUserId.Id.Equals(userId)).FirstOrDefault();
                if (userIsInFirm == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                }

                if (isCurrent == null)
                {
                    var selected = findingProject.ProjectEmployeeFirms.Select(pef => new
                    {
                        @Id = pef.EmployeeFirm.ApplicationUserId.Id,
                        @Fio = pef.EmployeeFirm.ApplicationUserId.Employee.Person.Fio
                    });
                    return Json(selected, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var selected = findingProject.ProjectEmployeeFirms.Where(pef => (pef.StopWorking == null) == isCurrent).Select(pef => new
                    {
                        @Id = pef.EmployeeFirm.ApplicationUserId.Id,
                        @Fio = pef.EmployeeFirm.ApplicationUserId.Employee.Person.Fio
                    });
                    return Json(selected, JsonRequestBehavior.AllowGet);
                }
                //(project.PlannedFinishDate > DateTime.Now)
            }
            if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
            {
                var findingProject = (await DBRepository.Firms.FindAsync(idFirm)).Projects.Where(project => project.ProjectID == idProject).FirstOrDefault();
                if (findingProject == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                }             
                if (isCurrent == null)
                {
                    var selected = findingProject.ProjectEmployeeFirms.Select(pef => new
                    {
                        @Id = pef.EmployeeFirm.ApplicationUserId.Id,
                        @Fio = pef.EmployeeFirm.ApplicationUserId.Employee.Person.Fio
                    });
                    return Json(selected, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var selected = findingProject.ProjectEmployeeFirms.Where(pef => (pef.StopWorking == null) == isCurrent).Select(pef => new
                    {
                        @Id = pef.EmployeeFirm.ApplicationUserId.Id,
                        @Fio = pef.EmployeeFirm.ApplicationUserId.Employee.Person.Fio
                    });
                    return Json(selected, JsonRequestBehavior.AllowGet);
                }
                //(project.PlannedFinishDate > DateTime.Now)
            }
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateJsonAntiForgeryToken]
        public async Task<ActionResult> _SaveEmployeeEdit([Bind]string data, int? idFirm, int? idProject)
        {
            var localThanReplace = HttpUtility.UrlDecode(System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(data)));
            var serializedResult = JsonConvert.DeserializeObject<ProjectUserInfoModel[]>(localThanReplace);
            var project = (await DBRepository.Firms.FindAsync(idFirm)).Projects.Where(proj => proj.ProjectID == idProject).FirstOrDefault();

            if (project == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var allPef = project.ProjectEmployeeFirms.Where(pef => pef.StopWorking == null).Select(pef=>pef).ToList();
            var notChanged = serializedResult.Where(recived => recived.IsChanged == false).Select(recived => recived.Id).ToList();
            var isChanged = serializedResult.Where(recived => recived.IsChanged == true).Select(recived => recived.Id).ToList();

            var notChangedInDb = (from pef in allPef
                                  from nChanged in notChanged
                                  where pef.EmployeeFirm.ApplicationUserId.Id.Equals(nChanged)
                                  select pef).ToList();


            var isChangedInDb = (from pef in allPef
                                 from iChanged in isChanged
                                 where pef.EmployeeFirm.ApplicationUserId.Id.Equals(iChanged)
                                 select pef).ToList();


            var forSuspension = project.ProjectEmployeeFirms.Where(pef => pef.StopWorking == null).Except(notChangedInDb).Select(pef => pef).ToList();
           
            foreach (var item in forSuspension)
            {
                item.StopWorking = DateTime.Now;
                DBRepository.Entry(item).State = EntityState.Modified;
            }

            var newEmployeeInProject = (from ef in (await DBRepository.Firms.FindAsync(idFirm)).EmployeeFirms
                                        from iChanged in isChanged
                                        where ef.ApplicationUserId.Id.Equals(iChanged)
                                        select ef).ToList();
            foreach (var item in newEmployeeInProject)
            {
                var newPEF = new ProjectEmployeeFirm()
                {
                    IsTeamLead = false,
                    EmployeeFirm = item,
                    StartWorking = DateTime.Now,
                    Project = project
                };
                DBRepository.ProjectEmployeeFirms.Add(newPEF);
                project.ProjectEmployeeFirms.Add(newPEF);
                DBRepository.Entry(newPEF).State = EntityState.Added;
                DBRepository.Entry(project).State = EntityState.Modified;
            }

            if ((isChanged.Count > 0)||(forSuspension.Count > 0))
            {
               await DBRepository.SaveChangesAsync();
            }

            return Json(new { }, JsonRequestBehavior.AllowGet);
        }


        public bool Persfind(List<string> a, string b)
        {
            foreach (var item in a)
            {
                if (item.CompareTo(b) == 0)
                {
                    return true;
                }
            }
            return false;
        }
        public PartialViewResult _modal_project_add_user(int? idFirm, int idProj)
        {

            var _proj = DBRepository.Projects.Find(idProj);
            var _firm = DBRepository.Firms.Find(idFirm);
            var employee = (
                from _ef in _firm.EmployeeFirms
                select new
                {
                    persid = _ef.ApplicationUserId.Id,
                    FIO = _ef.ApplicationUserId.Employee.Person.Fio
                }).ToList();

            var _people_in_project = (
                from pep in _proj.ProjectEmployeeFirms
                select new
                {
                    FIO = pep.EmployeeFirm.ApplicationUserId.Employee.Person.Fio,
                    auserid = pep.EmployeeFirm.ApplicationUserId.Id,
                    isruk = pep.IsTeamLead

                }).ToList();

            var _people_in_project_string_list = (from item in _people_in_project select item.auserid).ToList();


            CModalVyb cmv = new CModalVyb();
            foreach (var item in _people_in_project)
            {
                if (item.isruk == true)
                {
                    cmv.Le.Add(new Employee1() { FIO = item.FIO, IdAUser = item.auserid, IsUsing = true, IsRukov = true });
                }
                else
                {
                    cmv.Le.Add(new Employee1() { FIO = item.FIO, IdAUser = item.auserid, IsUsing = true });
                }
            }
            foreach (var item in employee)
            {

                if (Persfind(_people_in_project_string_list, item.persid) == false)
                {
                    cmv.Le.Add(new Employee1() { FIO = item.FIO, IdAUser = item.persid, IsUsing = false });
                }
            }
            var _people_in_firm_string_list = (from item in cmv.Le select item.IdAUser).ToList();
            ViewData["peopleinfirm"] = string.Join(",", _people_in_firm_string_list);
            ViewData["success"] = false;
            ViewData["isValid"] = false;
            ViewData["idFirm"] = idFirm;
            ViewData["idProj"] = idProj;
            return PartialView("modal/_modal_project_add_user", cmv);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult _modal_project_add_user([Bind(Include = "le")] CModalVyb CModalVyb_model, int idFirm, int idProj, string users)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    var list_users = users.Split(',');
                    var project = DBRepository.Projects.Find(idProj);
                    var firm = DBRepository.Firms.Find(idFirm);
                    ViewData["idProj"] = idProj;

                    var rukovoditel = (
                        from pep in project.ProjectEmployeeFirms
                        where pep.IsTeamLead == true
                        select pep.EmployeeFirm.ApplicationUserId.Id).FirstOrDefault();


                    for (int i = 0; i < list_users.Count(); i++)
                    {
                        if (list_users[i].CompareTo(rukovoditel) != 0)
                        {
                            if (CModalVyb_model.Le[i].IsUsing == true)
                            {
                                var pempl = project.FindInProjectEmployeeFirm(list_users[i]);
                                if (pempl == null)
                                {
                                    var ef = firm.GetEF(list_users[i]);
                                    var sef = project.SetEF(ef);
                                    ef.AddProject(sef);
                                    DBRepository.ProjectEmployeeFirms.Add(sef);
                                    DBRepository.Entry(project).State = EntityState.Modified;
                                    DBRepository.SaveChanges();
                                }

                            }
                        }
                    }

                    for (int i = 0; i < list_users.Count(); i++)
                    {
                        if (list_users[i].CompareTo(rukovoditel) != 0)
                        {
                            if (CModalVyb_model.Le[i].IsUsing == false)
                            {
                                var pempl = project.FindInProjectEmployeeFirm(list_users[i]);
                                if (pempl != null)
                                {
                                    var ef = firm.GetEF(list_users[i]);
                                    ef.DelEFinProject(ef);
                                    project.DelEFinProject(ef);
                                    DBRepository.ProjectEmployeeFirms.Remove(pempl);
                                    DBRepository.Entry(project).State = EntityState.Deleted;
                                    DBRepository.SaveChanges();

                                }
                            }
                        }
                    }
                    ViewData["isValid"] = true;
                    ViewData["success"] = true;
                    ViewData["id_firm"] = idFirm;
                    DBRepository.Entry(project).State = EntityState.Modified;
                    DBRepository.SaveChanges();
                    return PartialView("modal/_modal_project_add_user", CModalVyb_model);

                }
                ViewData["success"] = false;
                ViewData["isValid"] = false;
                ViewData["id_firm"] = idFirm;
                return PartialView("modal/_modal_project_add_user", CModalVyb_model);
            }

            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var sb = new System.Text.StringBuilder();
                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new Exception(sb.ToString());
            }

        }
        public PartialViewResult ViewProject(int idFirm, int idProj)
        {



            ViewData["idFirm"] = idFirm;
            var _project = DBRepository.Projects.Find(idProj);


            var empl = (
                from pef in _project.ProjectEmployeeFirms
                select new
                {
                    isRukovod = pef.IsTeamLead,
                    Fio = pef.EmployeeFirm.ApplicationUserId.Employee.Person.Fio
                }).ToList();

            List<string> employes = new List<string>();
            string otv = "";
            foreach (var item in empl)
            {
                if (item.isRukovod == true)
                {
                    otv = item.Fio;
                }
                else
                {
                    employes.Add(item.Fio);
                }
            }

            var dogovor = (
                from dog in _project.Contracts
                where dog.ДатаНачалаВыполнения <= DateTime.Now && DateTime.Now <= dog.ДатаЗавершенияВыполнения
                select dog.Name).ToList();
            return PartialView("_ViewProject",
                new ViewProject()
                {
                    Otvetstven = otv,
                    Employes = employes,
                    //pmvm = InitMenu().pmvm,
                    КОД = _project.ProjectCode,
                    Название = _project.ProjectName,
                    ProjectId = idProj,
                    Dogovors = new List<string>(dogovor)
                });

        }
        public PartialViewResult _modal_project_edit(int idFirm, int idProj)
        {

            List<SelectListItem> le = new List<SelectListItem>();
            var _project = DBRepository.Projects.Find(idProj);
            var _user = (
                from _us in _project.ProjectEmployeeFirms
                where _us.IsTeamLead == true
                select _us).ToList();
            var employee = (
                from _ef in DBRepository.Firms.Find(idFirm).EmployeeFirms
                select new
                {
                    persid = _ef.ApplicationUserId.Id,
                    FIO = _ef.ApplicationUserId.Employee.Person.Fio
                }).ToList();

            foreach (var item in employee)
            {
                if (item.persid.CompareTo(_user[0].EmployeeFirm.ApplicationUserId.Id) == 0)
                {
                    le.Add(new SelectListItem { Selected = true, Text = item.FIO, Value = item.persid });
                }
                else
                {
                    le.Add(new SelectListItem { Selected = false, Text = item.FIO, Value = item.persid });
                }

            }


            ViewData["success"] = false;
            ViewData["isValid"] = false;
            ViewData["idFirm"] = idFirm;
            ViewData["idProj"] = idProj;

            return PartialView("modal/_modal_project_edit", new CreateProjectModal()
            {
                Items = le,
                PlannedFinishDate = _project.PlannedFinishDate,
                ProjectName = _project.ProjectName,
                ProjectStartDate = _project.ProjectStartDate,
                IdAUser = _user[0].EmployeeFirm.ApplicationUserId.Id,
                ProjectCod = _project.ProjectCode
            });

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult _modal_project_edit([Bind(Include = "PlannedFinishDate,ProjectStartDate,project_cod,ProjectName,idauser,Items")] CreateProjectModal crm, int idFirm, int idProj)
        {

            var employee = (from _ef in DBRepository.Firms.Find(idFirm).EmployeeFirms
                            select new
                            {
                                persid = _ef.ApplicationUserId.Id,
                                FIO = _ef.ApplicationUserId.Employee.Person.Fio
                            }).ToList();
            List<SelectListItem> le = new List<SelectListItem>();
            foreach (var item in employee)
            {
                le.Add(new SelectListItem { Selected = false, Text = item.FIO, Value = item.persid });
            }
            crm.Items = le;
            if (ModelState.IsValid)
            {
                var project = DBRepository.Projects.Find(idProj);
                project.ProjectCode = crm.ProjectCod;
                project.ProjectName = crm.ProjectName;
                project.PlannedFinishDate = crm.PlannedFinishDate;
                project.ProjectStartDate = crm.ProjectStartDate;

                var _project = DBRepository.Projects.Find(idProj);
                var _user = (from _us in _project.ProjectEmployeeFirms
                             where _us.IsTeamLead == true
                             select _us).FirstOrDefault();
                if (_user.EmployeeFirm.ApplicationUserId.Employee.ApplicationUserId.CompareTo(crm.IdAUser) != 0)
                {
                    var _user1 = (
                        from _us in _project.ProjectEmployeeFirms
                        where _us.EmployeeFirm.ApplicationUserId.Employee.ApplicationUserId.CompareTo(crm.IdAUser) == 0
                        select _us).FirstOrDefault();

                    _user.IsTeamLead = !_user.IsTeamLead;
                    _user1.IsTeamLead = !_user1.IsTeamLead;
                }
                ViewData["isValid"] = true;
                ViewData["success"] = true;
                ViewData["id_firm"] = idFirm;
                DBRepository.Entry(project).State = EntityState.Modified;
                DBRepository.SaveChanges();
                return PartialView("modal/_modal_project_edit", crm);
            }
            ViewData["success"] = false;
            ViewData["isValid"] = false;
            ViewData["id_firm"] = idFirm;
            return PartialView("modal/_modal_project_edit", crm);

        }
        public PartialViewResult _modal_project_delete(int idProj, EnumerableForDelete efd = EnumerableForDelete.None)
        {
            ViewData["success"] = false;
            ViewData["isValid"] = false;
            ViewData["idProj"] = idProj;

            var _project = DBRepository.Projects.Find(idProj);
            return PartialView("modal/_modal_project_delete", new ViewProject()
            {

                Название = _project.ProjectName,
                ProjectId = idProj,

            });


        }
        public ActionResult Delete(int idProj = -1, EnumerableForDelete efd = EnumerableForDelete.None)
        {
            switch (efd)
            {
                case EnumerableForDelete.Firm:
                    break;
                case EnumerableForDelete.Department:
                    break;
                case EnumerableForDelete.User:
                    break;
                case EnumerableForDelete.Project:
                    {
                        return RedirectToAction("Index");

                    }

                case EnumerableForDelete.None:
                    break;
                default:
                    break;
            }
            return View();

        }
        public PartialViewResult _modal_project_add_dogovor(int idFirm, int idProj)
        {
            ViewData["success"] = false;
            ViewData["isValid"] = false;
            ViewData["idFirm"] = idFirm;
            ViewData["idProj"] = idProj;

            return PartialView("modal/_modal_project_add_dogovor", new CModalAddDogovor() { ProjectId = idProj });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult _modal_project_add_dogovor([Bind(Include = "Name,Cena,ДатаЗаключенияКонтракта,PlannedFinishDate,ProjectStartDate,idProject,Family_people,Name_people,Middlename_people,PhoneNumber,Firm,EMail")] CModalAddDogovor cmad, int idFirm, int idProj)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    var _project = DBRepository.Projects.Find(idProj);
                    var contract = new MvcInfos.Entities.Firm.Contract()
                    {
                        Project = _project,
                        Name = cmad.Name,
                        ДатаЗавершенияВыполнения = cmad.PlannedFinishDate,
                        ДатаЗаключенияКонтракта = cmad.ДатаЗаключенияКонтракта,
                        ДатаНачалаВыполнения = cmad.ProjectStartDate,
                        Reason_of_contract = ""
                    };
                    if (DBRepository.Customers.ToList().Count == 0)
                    {
                        var CustomerFirm = new MvcInfos.Entities.Firm.Dictionary.CustomerFirm()
                        {
                            CompanyName = cmad.Firm
                        };
                        var t1 = new Customer()
                        {
                            CustomerFirm = CustomerFirm
                        };
                        t1.CustomerFirm.CustomerEmployees.Add(
                            new MvcInfos.Entities.Firm.Dictionary.CustomerEmployee()
                            {
                                CustomerFirm = t1.CustomerFirm,
                                Email = cmad.EMail,
                                Phone = cmad.PhoneNumber,
                                Person = new MvcInfos.Entities.Firm.Dictionary.Person()
                                {
                                    BirthData = DateTime.Now.AddYears(-20),
                                    Name = cmad.Name_people,
                                    FamilyName = cmad.Family_people,
                                    MiddleName = cmad.Middlename_people
                                }
                            }
                            );
                        contract.Customer = t1;
                    }
                    else
                    {
                        var заказчик = (
                            from Customer in DBRepository.Customers
                            where Customer.CustomerFirm.CompanyName.CompareTo(cmad.Firm) == 0
                            select Customer).ToList().FirstOrDefault();
                        if (заказчик == null)
                        {
                            var CustomerFirm = new Entities.Firm.Dictionary.CustomerFirm()
                            {
                                CompanyName = cmad.Firm
                            };
                            var t1 = new Customer()
                            {
                                CustomerFirm = CustomerFirm
                            };
                            t1.CustomerFirm.CustomerEmployees.Add(
                                new Entities.Firm.Dictionary.CustomerEmployee()
                                {
                                    CustomerFirm = t1.CustomerFirm,
                                    Email = cmad.EMail,
                                    Phone = cmad.PhoneNumber,
                                    Person = new Entities.Firm.Dictionary.Person()
                                    {
                                        BirthData = DateTime.Now.AddYears(-20),
                                        Name = cmad.Name_people,
                                        FamilyName = cmad.Family_people,
                                        MiddleName = cmad.Middlename_people
                                    }
                                }
                                );
                            contract.Customer = t1;
                        }
                        else
                        {
                            contract.Customer = заказчик;
                        }
                    }
                    _project.Contracts.Add(contract);
                    DBRepository.Entry(_project).State = EntityState.Modified;
                    DBRepository.SaveChanges();

                    ViewData["isValid"] = true;
                    ViewData["success"] = true;
                    ViewData["id_firm"] = idFirm;

                    return PartialView("modal/_modal_project_add_dogovor", cmad);
                }
                ViewData["success"] = false;
                ViewData["isValid"] = false;
                ViewData["id_firm"] = idFirm;

                return PartialView("modal/_modal_project_add_dogovor", cmad);
            }
            catch (DbUpdateException dbUpdEx)
            {
                throw dbUpdEx;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }
        public ViewResult ViewDepartment(int idFirm, int idProj, string dogovorName)
        {
            var _project = DBRepository.Projects.Find(idProj);
            var _dogovor = (
                from dog in _project.Contracts
                where dog.Name.CompareTo(dogovorName) == 0
                select dog).FirstOrDefault();
            var _dog_fio = (
                from ff in _dogovor.Customer.CustomerFirm.CustomerEmployees
                select ff).ToList();

            var cview = new CViewDogovor()
            {
                Firm = _dogovor.Customer.CustomerFirm.CompanyName,
                Cena = _dogovor.Prise,
                //pmvm = InitMenu().pmvm,
                NameContract = _dogovor.Name,
                PlannedFinishDate = _dogovor.ДатаЗавершенияВыполнения,
                ProjectStartDate = _dogovor.ДатаНачалаВыполнения,
                ДатаЗаключенияКонтракта = _dogovor.ДатаЗаключенияКонтракта
                //  ,EMail = _dogovor.Customer.CustomerFirm.CustomerEmployees

            };
            foreach (var item in _dog_fio)
            {
                cview.FIOs.Add(new CFIO()
                {
                    EMail = item.Email,
                    FIO = item.Person.Fio,
                    PhoneNumber = item.Phone

                });
            }
            return View(cview);
        }
    }
}