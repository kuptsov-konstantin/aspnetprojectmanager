﻿using Microsoft.AspNet.Identity;
using MvcInfos.ActiveDirectory;
using MvcInfos.ActiveDirectory.Extensions;
using MvcInfos.Entities.UserConfigurations;
using MvcInfos.Models.ViewModels;
using MvcInfos.Repository;
using MvcInfos.Repository.IdentityModels;
using MvcInfos.Settings;
using System.Data.Entity;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MvcInfos.Models.ViewModels.Home;

namespace MvcInfos.Controllers
{
	public class HomeController : Controller
	{
		private IApplicationRepository DBRepository { get; set; }
		private ActiveDirectorySetting ActiveDirectorySetting { get; set; }
		public HomeController(IApplicationRepository applicationRepository, ActiveDirectorySetting activeDirectorySetting)
		{
			ActiveDirectorySetting = activeDirectorySetting;
			DBRepository = applicationRepository;
			//http://metanit.com/sharp/mvc5/21.1.php
		}
		public ActionResult Init()
		{
			ContextInitializer init = new ContextInitializer();
			init.Init(new ApplicationDbContext());
			return RedirectToAction("Index");
		}	
		[AuthorizeActiveDirectory]
		public ActionResult Index()
		{
		/*	if (User.Identity.IsAuthenticated)
			{*/
				int count = DBRepository.Employees.Count();
				ViewData["CountEmployee"] = count;

			if(User.Identity.IsAuthenticated == true)
			{
				return RedirectToAction("Statistics", "Statistic");
			}
			else
			{
				return RedirectToAction("Login", "Account");
			}



			//if (User.IsHasAccessRights(EnumerableRoles.administrator_server) || User.IsHasAccessRights(EnumerableRoles.administrator_firm))
			//	{
			//return RedirectToAction("Statistics", "Administration");
			//	}
			//	else
			//	{
			//		if (User.IsHasAccessRights(EnumerableRoles.manager) || User.IsHasAccessRights(EnumerableRoles.user) || User.IsHasAccessRights(EnumerableRoles.project_manager))
			//		{
			//			return RedirectToAction("Status", "Home");
			//		}
			//	}
			/*}
			else
			{
				return RedirectToAction("Login", "Account");
			}*/
			//return View();
		}

		public ActionResult About()
		{
			ViewBag.Message = "Послание.";

			return View();
		}
		[AuthorizeActiveDirectory]
		public ActionResult Contact()
		{
			ViewBag.Message = "Мои контакты.";

			return View();
		}

		public PartialViewResult _LeftPhoto()
		{

			var model = new LayoutViewModel() { };
			if (Request.IsAuthenticated == true)
			{
				model = GetPersonInfo();
			}
			return PartialView("partials/_LeftPhotoPartial", model);
		}
	

		[AuthorizeActiveDirectory]
		public async Task<PartialViewResult> _FirmsProjectsPartial()
		{
			var UserInfo = DBRepository.Users.Find(User.Identity.GetUserId());
			var EmployeeFirmInfo = await DBRepository.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(UserInfo.Id)).Select(item => new FirmModel() { IdFirm = item.Firm.FirmID, NameFirm = item.Firm.Name }).ToListAsync();
			return PartialView("partials/_FirmsProjectsPartial", EmployeeFirmInfo);
		}

		[AuthorizeActiveDirectory]
		public async Task<PartialViewResult> _FirmsTasksPartial()
		{
			var UserInfo = DBRepository.Users.Find(User.Identity.GetUserId());
			var EmployeeFirmInfo = await DBRepository.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(UserInfo.Id)).Select(item => new FirmModel() { IdFirm = item.Firm.FirmID, NameFirm = item.Firm.Name }).ToListAsync();
			return PartialView("partials/_FirmsTasksPartial", EmployeeFirmInfo);
		}

		[AuthorizeActiveDirectory]
		public async Task<PartialViewResult> _FirmsMailsPartial()
		{
			var UserInfo = DBRepository.Users.Find(User.Identity.GetUserId());
			var EmployeeFirmInfo = await DBRepository.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(UserInfo.Id)).Select(item => new FirmModel() { IdFirm = item.Firm.FirmID, NameFirm = item.Firm.Name }).ToListAsync();
			return PartialView("partials/_FirmsMailsPartial", EmployeeFirmInfo);
		}


		public PartialViewResult SidebarMenu()
		{
			
				var UserInfo = DBRepository.Users.Find(User.Identity.GetUserId());
				var EmployeeFirmInfo = DBRepository.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(UserInfo.Id)).Select(item => new FirmModel() { IdFirm = item.Firm.FirmID, NameFirm = item.Firm.Name }).ToList();

			
		
			return PartialView("partials/_SidebarMenuPartial", EmployeeFirmInfo);
		}


		public PartialViewResult _LoginPartial()
		{
			var model = new LayoutViewModel() { };
			model.IsFirst = !(DBRepository.Users.Count() > 0);

			if (Request.IsAuthenticated == true)
			{
				model = GetPersonInfo();
			}
			return PartialView("partials/_LoginPartial", model);
		}
		[AuthorizeActiveDirectory]
		public ActionResult Status()
		{
			return View();
		}


		[AuthorizeActiveDirectory]
		private LayoutViewModel GetPersonInfo()
		{
			var task = Task.Factory.StartNew(() =>
			{
				var model = new LayoutViewModel() { };
				var UserInfo = DBRepository.Users.Find(User.Identity.GetUserId());
				if (UserInfo != null)
				{
					if (UserInfo.IsActiveDirectory == true)
					{
						var userPrincipal = UserPrincipal.FindByIdentity(this.ActiveDirectorySetting.GetPrincipalContext(), IdentityType.SamAccountName, UserInfo.UserName);
						model.FI = userPrincipal.DisplayName;
						model.IsAD = true;
					}
					else
					{
						if (UserInfo?.Employee?.Person?.Name == null && UserInfo?.Employee?.Person?.MiddleName == null)
						{
							model.FI = User.Identity.Name;
						}
						else
						{
							model.FI = $"{UserInfo?.Employee?.Person?.Name} {UserInfo?.Employee?.Person?.MiddleName}";
						}
						model.IsAD = false;
						model.PhotoURL = UserInfo?.Employee?.Person?.PhotoUrl;
					}
				}
				return model;

			});
			Task.WaitAll(task);
			return task.Result;
		}
	}
}