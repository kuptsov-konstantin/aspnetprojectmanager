﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using MvcInfos.Models.ViewModels;
using MvcInfos.Entities.Firm;

namespace MvcInfos.Controllers
{
    public interface INewFirmController
    {
        ActionResult Autocomplete(int? id_firm, string term);
        ActionResult Index();
        Task<ActionResult> RegisterStep1([Bind] AboutFirm aboutFirm);
        Task<ActionResult> RegisterStep2([Bind] Models.ViewModels.DepartmentInView department, int firmId);
        Task<ActionResult> RegisterStep3([Bind] ICollection<Employee> employees);
        ActionResult TagSearch(string term);
        Task<JsonResult> ValidUserName(NewFirmController.EValidUser validate, string str);
        ActionResult _AddFirmPartial();
        Task<ActionResult> _AddFirmPartial(OrganizationObject data1);
        PartialViewResult _CreateStepOneFirm(string userName);
        ActionResult _CreateStepOneFirm([Bind(Include = "NameFirm")] AdministrationAddFirm aaf, string userName);
        ActionResult _CreateStepThreeEmployees(int id_firm);
        Task<ActionResult> _CreateStepThreeEmployees(AdministrationAddEmployee model, int id_firm, string btnSubmit);
        ActionResult _CreateStepTwoDepartment(int id_firm);
        ActionResult _CreateStepTwoDepartment([Bind(Include = "NameNewDep,idFirm,NameFirm")] AdministrationAddDepartment aad, int id_firm, string userName, string btnSubmit);
    }
}