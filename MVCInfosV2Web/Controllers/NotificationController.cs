﻿using MvcInfos.Entities.Mails;
using MvcInfos.Extensions;
using MvcInfos.IdentityConfig;
using MvcInfos.Models.ViewModels.Notification;
using MvcInfos.Notification;
using MvcInfos.Notification.Models;
using MvcInfos.Repository;
using MvcInfos.Settings;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MvcInfos.Entities.Notifications;

namespace MvcInfos.Controllers
{
	public class NotificationController : Controller
	{
		private IApplicationRepository DBRepository { get; set; }
		private NotificationServiceManager NotificationService { get; set; }
		private ActiveDirectorySetting ActiveDirectorySetting { get; set; }
		public NotificationController(IApplicationRepository repos, NotificationServiceManager notificationService, ActiveDirectorySetting activeDirectorySetting)
		{
			ActiveDirectorySetting = activeDirectorySetting;
			DBRepository = repos;
			NotificationService = notificationService;
		}

		[ActionName("GetPcs")]
		public async Task<JsonResult> GetPcsAsync()
		{
			var list = DBRepository.Users.Select(user => new UserInfo() { Id = user.Id, UserName = user.UserName }).ToList();
			var userPCs = await NotificationService.UpdatePCsInfoAsync(list);
			return this.JsonNetIso(userPCs, JsonRequestBehavior.AllowGet);
		}
		public JsonResult SendNotify()
		{
			//NotificationService.SendNotify(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), "kupcov", "zxcvzxcvxccvzxcv");
			return Json(new { }, JsonRequestBehavior.AllowGet);

		}
		public JsonResult GetNotifierPath(Guid serviceId)
		{
			if (serviceId.Equals(Properties.Settings.Default.ServiceID))
			{

				return Json(new RequestPath() { Path = Extensions.Extensions.GetAbsoluteServerPath("\\bin\\MvcInfos.Notification.Notificator.exe") }, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json(new { }, JsonRequestBehavior.AllowGet);
			}
		}
		public FileResult GetNotifier()
		{
			var stream = System.IO.File.OpenRead(Extensions.Extensions.GetAbsoluteServerPath("\\bin\\MvcInfos.Notification.Notificator.exe"));
			return File(stream, "application/octet-stream", "MvcInfos.Notification.Notificator.exe");
		}
		public JsonResult GetAdminInfo(Guid serviceId)
		{
			if (serviceId.Equals(Properties.Settings.Default.ServiceID))
			{
				return Json(new AdminInfo() { Login = ActiveDirectorySetting.UserName, Password = ActiveDirectorySetting.Password }, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json(new { }, JsonRequestBehavior.AllowGet);
			}
		}
		[ActionName("GetNotificationById")]
		public async Task<JsonResult> GetNotificationByIdAsync(Guid serviceId, Guid notificationId)
		{
			if (serviceId.Equals(Properties.Settings.Default.ServiceID))
			{
				var notification = await DBRepository.ResolutionNotifiers.FindAsync(notificationId);
				if ((notification.IsDelete) || (notification.IsNotifi == false))
				{
					return Json(new { }, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return this.JsonNetIso(new NotificationViewModel
					{
						MailId = notification.Mail.MailId,
						NotifyTimeKey = notification.NotifyTimeKey,
						ResolutionNotifierId = notification.ResolutionNotifierId,
						Description = notification.Mail.Description,
						NotificationMessage = "Уведомления будут постоянно приходить пока вы не сделаете ответ",
						UserName = (notification.EmployeeFirm.ApplicationUserId.IsActiveDirectory == true) ? Environment.UserDomainName + '\\' + notification.EmployeeFirm.ApplicationUserId.UserName : notification.EmployeeFirm.ApplicationUserId.UserName,
						UserId = notification.EmployeeFirm.ApplicationUserId.Id
					}, JsonRequestBehavior.AllowGet);
				}
			}
			else
			{
				return Json(new { }, JsonRequestBehavior.AllowGet);
			}
		}

		[ActionName("GetCurrentNotifications")]
		public async Task<JsonResult> GetCurrentNotificationsAsync(Guid serviceId)
		{
			if (serviceId.Equals(Properties.Settings.Default.ServiceID))
			{
				var notifications = await DBRepository.ResolutionNotifiers.Where(notif => notif.IsNotifi == true).Where(notif => notif.IsDelete == false).Select(notif => new NotificationViewModel
				{
					MailId = notif.Mail.MailId,
					NotifyTimeKey = notif.NotifyTimeKey,
					ResolutionNotifierId = notif.ResolutionNotifierId,
					Description = notif.Mail.Description,
					NotificationMessage = "Уведомления будут постоянно приходить пока вы не сделаете ответ",
					UserName = (notif.EmployeeFirm.ApplicationUserId.IsActiveDirectory == true) ? Environment.UserDomainName + "\\" + notif.EmployeeFirm.ApplicationUserId.UserName : notif.EmployeeFirm.ApplicationUserId.UserName,
					UserId = notif.EmployeeFirm.ApplicationUserId.Id
				}).ToListAsync();
				return this.JsonNetIso(notifications, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json(new { }, JsonRequestBehavior.AllowGet);
			}
		}


		[ActionName("Click")]
		public async Task<JsonResult> ClickAsync(Guid notificationId, Guid mailId, Guid userId, int clickReport)
		{
			var notification = await DBRepository.ResolutionNotifiers.FindAsync(notificationId);

			var type = (NotificationsClickType)clickReport;


			var notif = new NotificationsClick()
			{
				ClickedTime = DateTime.Now,
				NotificationsClickType = type,
				ResolutionNotifier = notification
			};

			DBRepository.Entry(notif).State = EntityState.Added;
			DBRepository.NotificationsClicks.Add(notif);
			await DBRepository.SaveChangesAsync();
			return Json(notification, JsonRequestBehavior.AllowGet);
		}



		//[ActionName("SetAside")]
		//public async Task<JsonResult> SetAsideAsync(Guid notificationId, Guid mailId, Guid userId)
		//{
		//	var notification = await DBRepository.ResolutionNotifiers.FindAsync(notificationId);

		//	var notif = new NotificationsClick()
		//	{
		//		ClickedTime = DateTime.Now,
		//		NotificationsClickType = NotificationsClickType.SetAside,
		//		ResolutionNotifier = notification
		//	};

		//	DBRepository.Entry(notif).State = EntityState.Added;
		//	DBRepository.NotificationsClicks.Add(notif);
		//	await DBRepository.SaveChangesAsync();
		//	return Json(notification, JsonRequestBehavior.AllowGet);
		//}

		//[ActionName("Aprove")]
		//public async Task<JsonResult> AproveAsync(Guid notificationId, Guid mailId, Guid userId)
		//{
		//	var notification = await DBRepository.ResolutionNotifiers.FindAsync(notificationId);

		//	var notif = new NotificationsClick()
		//	{
		//		ClickedTime = DateTime.Now,
		//		NotificationsClickType = NotificationsClickType.Aprove,
		//		ResolutionNotifier = notification
		//	};

		//	DBRepository.Entry(notif).State = EntityState.Added;
		//	DBRepository.NotificationsClicks.Add(notif);
		//	await DBRepository.SaveChangesAsync();
		//	return Json(notification, JsonRequestBehavior.AllowGet);
		//}
		//[ActionName("IgnoreClosing")]
		//public async Task<JsonResult> IgnoreClosingAsync(Guid notificationId, Guid mailId, Guid userId)
		//{
		//	var notification = await DBRepository.ResolutionNotifiers.FindAsync(notificationId);

		//	var notif = new NotificationsClick()
		//	{
		//		ClickedTime = DateTime.Now,
		//		NotificationsClickType = NotificationsClickType.IgnoreClosing,
		//		ResolutionNotifier = notification
		//	};

		//	DBRepository.Entry(notif).State = EntityState.Added;
		//	DBRepository.NotificationsClicks.Add(notif);
		//	await DBRepository.SaveChangesAsync();
		//	return Json(notification, JsonRequestBehavior.AllowGet);
		//}


		//[ActionName("IgnoreNotClosing")]
		//public async Task<JsonResult> IgnoreNotClosingAsync(Guid notificationId, Guid mailId, Guid userId)
		//{
		//	var notification = await DBRepository.ResolutionNotifiers.FindAsync(notificationId);

		//	var notif = new NotificationsClick()
		//	{
		//		ClickedTime = DateTime.Now,
		//		NotificationsClickType = NotificationsClickType.IgnoreNotClosing,
		//		ResolutionNotifier = notification
		//	};

		//	DBRepository.Entry(notif).State = EntityState.Added;
		//	DBRepository.NotificationsClicks.Add(notif);
		//	await DBRepository.SaveChangesAsync();
		//	return Json(notification, JsonRequestBehavior.AllowGet);
		//}





		[ActionName("NotificationsInfo")]
		public async Task<JsonResult> NotificationsInfoAsync(Guid mailId)
		{
			var mail = await DBRepository.Mails.FindAsync(mailId);
			if (mail == null)
			{
				return this.JsonNetIso(HttpNotFound(), JsonRequestBehavior.AllowGet);
			}
			var mailNotifications = new MailNotificationsResultViewModel
			{
				MailId = mailId,
				Date = DateTime.Now
			};

			var result = mail.Resolution.ResolutionNotifier.Select(notif => new NotificationsResultViewModel
			{
				Date = (notif.NotificationsClicks.LastOrDefault() != null) ? notif.NotificationsClicks.LastOrDefault().ClickedTime : DateTime.MinValue,
				ResolutionNotificationId = notif.ResolutionNotifierId,
				ClickType = (notif.NotificationsClicks.LastOrDefault() != null) ? notif.NotificationsClicks.LastOrDefault().NotificationsClickType : NotificationsClickType.None,
				User = new MailEmployeeInfoViewModel {
					FIO = notif.EmployeeFirm.ApplicationUserId?.Employee?.Person.Fio ?? "",
					UserId = Guid.Parse(notif.EmployeeFirm.ApplicationUserId.Id)
				}
			}).ToList();

			mailNotifications.NotificationsResult = result;

			return this.JsonNetIso(mailNotifications, JsonRequestBehavior.AllowGet);
		}
	}
}