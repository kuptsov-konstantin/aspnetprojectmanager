﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcInfos.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult Index()
        {
            return RedirectToAction("Report");
        }
        public ActionResult Report()
        {
            return View();
        }
        public ActionResult ValidReport()
        {
            return View();
        }
    }
}