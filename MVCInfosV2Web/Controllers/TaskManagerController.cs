﻿using Microsoft.AspNet.Identity;
using MvcInfos.ActiveDirectory;
using MvcInfos.ActiveDirectory.Extensions;
using MvcInfos.Attributes;
using MvcInfos.Entities.Tasks;
using MvcInfos.Entities.UserConfigurations;
using MvcInfos.Extensions;
using MvcInfos.Models.ViewModels;
using MvcInfos.Plugin.Interfaces;
using MvcInfos.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MvcInfos.Controllers
{
	[AuthorizeActiveDirectory]
	public class TaskManagerController : Controller
	{
		private IApplicationRepository DBRepository { get; set; }
		private IPlugins Plugins { get; set; }
		public TaskManagerController(IApplicationRepository repos, IPlugins plugins)
		{
			Plugins = plugins;
			DBRepository = repos;
		}
		// GET: Index
		public ActionResult Index()
		{
			return View();
		}
		// GET: Calendar

		public ActionResult Calendar()
		{
			return View();
		}
		public ActionResult Tasks()
		{
			return View();
		}
		[AuthorizeActiveDirectory(Roles = "project_manager,manager,administrator_server,administrator_firm", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager,PMServerSManager")]
		public ActionResult TaskManager(int? idFirm, bool? isCurrent)
		{
			ViewData["idFirm"] = idFirm;
			ViewData["isCurrent"] = isCurrent;
			return View();
		}
		[AuthorizeActiveDirectory(Roles = "project_manager,manager,administrator_server,administrator_firm", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager,PMServerSManager")]
		public ActionResult NewTask()
		{
			return View();
		}

		public async Task<JsonResult> GetMyFirms()
		{
			var UserInfo = DBRepository.Users.Find(User.Identity.GetUserId());
			var EmployeeFirmInfo = await DBRepository.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(UserInfo.Id)).Select(item =>
			new FirmModel()
			{
				IdFirm = item.Firm.FirmID,
				NameFirm = item.Firm.Name
			}).ToListAsync();

			foreach (var item in EmployeeFirmInfo)
			{
				foreach (var item1 in await GetUserFromFirm(item.IdFirm))
				{
					if (!item1.Fio.Equals(""))
					{
						item.Employees.Add(new TaskManagerModelEmployee()
						{
							Fio = item1.Fio,
							Id = item1.Id
						});
					}
				}
			}
			return this.JsonNetIso(EmployeeFirmInfo, JsonRequestBehavior.AllowGet);
		}

		[AuthorizeActiveDirectory(Roles = "project_manager,manager,administrator_server,administrator_firm", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager,PMServerSManager")]
		public async Task<ActionResult> GetProjects(int? idFirm)
		{
			var currentUserId = User.Identity.GetUserId();
			if (User.IsHasAccessRights(EnumerableRoles.project_manager))
			{
				var firm = (await DBRepository.Firms.FindAsync(idFirm));
				var employeeFirm = firm.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(currentUserId) && ef.IsDeleted == false).FirstOrDefault();
				if (employeeFirm == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				var projects = employeeFirm.ProjectEmployeeFirms.Where(pef => pef.StopWorking == null && pef.IsDeleted == false).Select(pef => new
				{
					@id = pef.Project.ProjectID,
					@code = pef.Project.ProjectCode,
					@name = pef.Project.ProjectName,
					@startDate = pef.Project.ProjectStartDate,
					@endDate = pef.Project.PlannedFinishDate
				});
				return Json(projects, JsonRequestBehavior.AllowGet);
			}
			if (User.IsHasAccessRights(EnumerableRoles.manager) || User.IsHasAccessRights(EnumerableRoles.administrator_firm))
			{
				var firm = (await DBRepository.Firms.FindAsync(idFirm));
				var employeeFirm = firm.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(currentUserId) && ef.IsDeleted == false).FirstOrDefault();
				if (employeeFirm == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				var projects = firm.Projects.Where(project => project.PlannedFinishDate > DateTime.Now && project.IsDeleted == false).Select(proj => new
				{
					@id = proj.ProjectID,
					@code = proj.ProjectCode,
					@name = proj.ProjectName,
					@startDate = proj.ProjectStartDate,
					@endDate = proj.PlannedFinishDate
				});
				//var projects = employeeFirm.ProjectEmployeeFirms.Where(pef => pef.StopWorking == null).Select(pef => new { @Id = pef.Project.ProjectID, @Name = pef.Project.ProjectName });
				return Json(projects, JsonRequestBehavior.AllowGet);
			}
			if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
			{
				var firm = (await DBRepository.Firms.FindAsync(idFirm));
				var projects = firm.Projects.Where(project => project.PlannedFinishDate > DateTime.Now && project.IsDeleted == false).Select(proj => new
				{
					@id = proj.ProjectID,
					@code = proj.ProjectCode,
					@name = proj.ProjectName,
					@startDate = proj.ProjectStartDate,
					@endDate = proj.PlannedFinishDate
				});
				//var projects = employeeFirm.ProjectEmployeeFirms.Where(pef => pef.StopWorking == null).Select(pef => new { @Id = pef.Project.ProjectID, @Name = pef.Project.ProjectName });
				return Json(projects, JsonRequestBehavior.AllowGet);

			}
			return Json(new { }, JsonRequestBehavior.AllowGet);
		}

		[AuthorizeActiveDirectory(Roles = "project_manager,manager,administrator_server,administrator_firm", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager,PMServerSManager")]
		public async Task<IEnumerable<TaskManagerModelEmployee>> GetUserFromFirm(int? idFirm)
		{
			var currentUserId = User.Identity.GetUserId();
			if (User.IsHasAccessRights(EnumerableRoles.project_manager))
			{
				var firm = (await DBRepository.Firms.FindAsync(idFirm));
				var employeeFirm = firm.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(currentUserId) && ef.IsDeleted == false).FirstOrDefault();
				if (employeeFirm == null)
				{
					return null;
				}
				var employees = firm.EmployeeFirms
					.Where(pef => pef.IsDeleted == false)
					.Select(pef => new TaskManagerModelEmployee()
					{
						Id = pef.ApplicationUserId.Id,
						Fio = (pef.ApplicationUserId.Employee.Person == null) ? "" : pef.ApplicationUserId.Employee.Person.Fio
					});
				return employees;
			}

			if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
			{
				var firm = (await DBRepository.Firms.FindAsync(idFirm));
				var employees = firm.EmployeeFirms
					.Where(pef => pef.IsDeleted == false)
					.Select(pef => new TaskManagerModelEmployee()
					{
						Id = pef.ApplicationUserId.Id,
						Fio = (pef.ApplicationUserId.Employee.Person == null) ? "" : pef.ApplicationUserId.Employee.Person.Fio
					});
				return employees;
			}
			return new List<TaskManagerModelEmployee>();
		}


		[AuthorizeActiveDirectory(Roles = "project_manager,manager,administrator_server,administrator_firm", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager,PMServerSManager")]
		public async Task<ActionResult> GetUserFromProject(int? idFirm, int? idProject)
		{
			var currentUserId = User.Identity.GetUserId();
			if (User.IsHasAccessRights(EnumerableRoles.project_manager))
			{
				var firm = (await DBRepository.Firms.FindAsync(idFirm));
				var employeeFirm = firm.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(currentUserId) && ef.IsDeleted == false).FirstOrDefault();
				if (employeeFirm == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				var employees = DBRepository.ProjectEmployeeFirms
					.Where(pef => pef.StopWorking == null && pef.IsDeleted == false && pef.Project.ProjectID == idProject)
					.Select(pef => new
					{
						@Id = pef.EmployeeFirm.ApplicationUserId.Id,
						@Fio = pef.EmployeeFirm.ApplicationUserId.Employee.Person.Fio
					});
				return Json(employees, JsonRequestBehavior.AllowGet);
			}

			if (User.IsHasAccessRights(EnumerableRoles.manager) || User.IsHasAccessRights(EnumerableRoles.administrator_firm))
			{
				var firm = (await DBRepository.Firms.FindAsync(idFirm));
				var employeeFirm = firm.EmployeeFirms.Where(ef => ef.ApplicationUserId.Id.Equals(currentUserId) && ef.IsDeleted == false).FirstOrDefault();
				if (employeeFirm == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}

				var project = firm.Projects.Where(proj => proj.ProjectID == idProject).FirstOrDefault();
				var employees = project.ProjectEmployeeFirms
				   .Where(pef => pef.StopWorking == null && pef.IsDeleted == false && pef.Project.ProjectID == idProject)
				   .Select(pef => new
				   {
					   @Id = pef.EmployeeFirm.ApplicationUserId.Id,
					   @Fio = pef.EmployeeFirm.ApplicationUserId.Employee.Person.Fio
				   });
				return Json(employees, JsonRequestBehavior.AllowGet);
			}
			if (User.IsHasAccessRights(EnumerableRoles.administrator_server))
			{
				var firm = (await DBRepository.Firms.FindAsync(idFirm));
				var project = firm.Projects.Where(proj => proj.ProjectID == idProject).FirstOrDefault();
				var employees = project.ProjectEmployeeFirms
				   .Where(pef => pef.StopWorking == null && pef.IsDeleted == false && pef.Project.ProjectID == idProject)
				   .Select(pef => new
				   {
					   @Id = pef.EmployeeFirm.ApplicationUserId.Id,
					   @Fio = pef.EmployeeFirm.ApplicationUserId.Employee.Person.Fio
				   });
				return Json(employees, JsonRequestBehavior.AllowGet);
			}
			return Json(new { }, JsonRequestBehavior.AllowGet);
		}


		[ValidateJsonAntiForgeryToken]
		[ActionName("AddNewTask")]
		public ActionResult AddNewTask(string parametrs)
		{
			if (parametrs == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var localThanReplace = HttpUtility.UrlDecode(System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(parametrs)));
			var convertedParametrs = HttpUtility.ParseQueryString(localThanReplace);
			var idFirm = Convert.ToInt32(convertedParametrs["idFirm"]);
			var idProj = convertedParametrs["idProject"];
			var idProject = -1;
			if (idProj != null)
			{
				idProject = Convert.ToInt32(idProj);
			}
			var taskJson = HttpUtility.UrlDecode(System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(convertedParametrs["taskJson"])));
			var task = JsonConvert.DeserializeObject<TaskManagerModelTask>(taskJson);


			var personalTask = new PersonalTask()
			{

			};



			return Json(new { }, JsonRequestBehavior.AllowGet);
		}

	}
}