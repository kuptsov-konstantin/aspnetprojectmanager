﻿using Microsoft.AspNet.Identity;
using MvcInfos.ActiveDirectory;
using MvcInfos.ActiveDirectory.Extensions;
using MvcInfos.Date;
using MvcInfos.Entities;
using MvcInfos.Entities.CountryInfo;
using MvcInfos.Entities.Firm;
using MvcInfos.Entities.Firm.Dictionary;
using MvcInfos.Entities.Settings;
using MvcInfos.Entities.UserConfigurations;
using MvcInfos.IdentityConfig;
using MvcInfos.Models.ViewModels;
using MvcInfos.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MvcInfos.Controllers
{
	[AuthorizeActiveDirectory(Roles = "administrator_server,administrator_firm", Groups = "AdministratorServerSManager,AdministratorOrganizationSManager")]
    public partial class NewFirmController : Controller
    {
        private ApplicationUserManager UserManager;
        private IApplicationRepository DBRepository;
        public NewFirmController(IApplicationRepository repos, ApplicationUserManager _userManager)
        {
            UserManager = _userManager;
            DBRepository = repos;
        }

        // GET: NewFirm
        public ActionResult Index()
        {
            return View();
        }
        ///TODO: Реализовать добавление фирмы на AJAX на одной странице!
        public ActionResult _AddFirmPartial()
        {
            return PartialView();
        }

		private async Task CreateUserAsync(Firm firm, EmployeeInView empl, string userId, List<DepartmentInView> dep)
		{
			await Task.Factory.StartNew(() =>
				{
					var user = DBRepository.Users.FirstOrDefault(_user => _user.Id.Equals(userId));
					var person = new Person()
					{
						FamilyName = empl.FamilyName.str as string,
						MiddleName = empl.MiddleName.str as string,
						Name = empl.Name.str as string,
						BirthData = Convert.ToDateTime(empl.BirthDay.str, System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)
					};
					DBRepository.Persons.Add(person);
					user.Employee = new Employee()
					{
						Person = person
					};
					DBRepository.Employees.Add(user.Employee);
					user.Employee.ApplicationUserId = user.Id;
					if (user.Employee.EmployeeFirms == null)
					{
						user.Employee.EmployeeFirms = new HashSet<EmployeeFirm>();
					}
					var rez_add_tofirm = user.Employee.AddToFirm(firm);
					DBRepository.EmployeeFirms.Add(rez_add_tofirm);

					rez_add_tofirm.EmploymentDate = Convert.ToDateTime(empl.Emploement.str, System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat);
					rez_add_tofirm.ApplicationUserId = user;
					if (empl.Department != -1)
					{
						var rez_add_Department = firm.AddDepartment(dep[empl.Department].department.str as string);
						if (rez_add_Department != null)
						{
							user.Employee.AddDepartment(firm, rez_add_Department);
							DBRepository.EmployeeFirms.Add(rez_add_tofirm);
						}
					}
				});
		}

        /// <summary>
        /// Добавление в базу - фирмы и ее местоположение
        /// </summary>
        /// <param name="aboutFirm"></param>
        /// <returns></returns>
        [HttpPost/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> RegisterStep1([Bind]AboutFirm aboutFirm)
        {
            if (User.IsHasAccessRights(EnumerableRoles.administrator_firm) || User.IsHasAccessRights(EnumerableRoles.administrator_server))
            {
                if (ModelState.IsValid)
                {
                    // return Json(new { statusCode = HttpStatusCode.OK, firmId = 1 }, JsonRequestBehavior.AllowGet);
                    var findCountry = await DBRepository.Countries.FirstOrDefaultAsync(p => p.Country.Equals((string)aboutFirm.Country.str));
                    if (findCountry == default(CountryTable))
                    {
                        findCountry = DBRepository.Countries.Add(new CountryTable() { Country = (string)aboutFirm.Country.str });
                    }
                    var findCity = findCountry.CityTables.FirstOrDefault(localcity => localcity.City.Equals((string)aboutFirm.City.str));
                    if (findCity == default(CityTable))
                    {
                        findCity = DBRepository.Cities.Add(new CityTable() { City = (string)aboutFirm.City.str });
                        findCountry.CityTables.Add(findCity);
                        findCity.Country = findCountry;
                    }
                    var findAddress = findCity.AddressTables.FirstOrDefault(localaddress => localaddress.Address.Equals((string)aboutFirm.Address.str));
                    if (findAddress == default(AddressTable))
                    {
                        findAddress = DBRepository.Addresses.Add(new AddressTable() { Address = (string)aboutFirm.Address.str });
                        findCity.AddressTables.Add(findAddress);
                        findAddress.City = findCity;
                    }
                    var findFirm = findAddress.Firms.FirstOrDefault(p => p.Name.Equals((string)aboutFirm.OrgName.str));
                    if (findFirm == default(Firm))
                    {
                        findFirm = DBRepository.Firms.Add(
                            new Firm()
                            {
                                Address = findAddress,
                                Name = aboutFirm.OrgName.str as string
                            });
                        findAddress.Firms.Add(findFirm);
                        //if (User.IsHasAccessRights(EnumerableRoles.administrator_firm)) {
                        var userId = User.Identity.GetUserId();
                        var user = DBRepository.Users.FirstOrDefault(_user => _user.Id.Equals(userId));
                        var ef = new EmployeeFirm()
                        {
                            EmploymentDate = DateTime.Now.AddDays(-1),
                            ApplicationUserId = user,
                            Firm = findFirm
                        };
                        findFirm.EmployeeFirms.Add(ef);
                        DBRepository.EmployeeFirms.Add(ef);

                        //    }

                    }
                    else
                    {
                        ///TODO: Вернуть ошибку, что такая организация/фирма существует.      
                        aboutFirm.OrgName.isValidate = false;
                        return PartialView(aboutFirm);
                    }
                    try
                    {
                        await DBRepository.SaveChangesAsync();
                    }
                    catch (Exception)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.Conflict);
                    }
               

                    return Json(new Response { statusCode = (int)HttpStatusCode.OK, firmId = findFirm.FirmID }, JsonRequestBehavior.AllowGet);
                }
                return Json(aboutFirm, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
        }

        /// <summary>
        /// Добавление отдела в фирму
        /// </summary>
        /// <param name="department"></param>
        /// <param name="firmId"></param>
        /// <returns></returns>
        [HttpPost/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> RegisterStep2([Bind]Valid department, int firmId)
        {
            if (User.IsHasAccessRights(EnumerableRoles.administrator_firm) || User.IsHasAccessRights(EnumerableRoles.administrator_server))
            {
                if (ModelState.IsValid)
                {
                    var findFirm = await DBRepository.Firms.FindAsync(firmId);
                    if (findFirm == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                    }
                    var createDep = DBRepository.Departments.Add(new Entities.Firm.Department() { Name = department.str as string, Firm = findFirm });
                    findFirm.Departments.Add(createDep);
                    await DBRepository.SaveChangesAsync();
                    return Json(new Response { statusCode = (int)HttpStatusCode.OK, firmId = findFirm.FirmID }, JsonRequestBehavior.AllowGet);
                }
                return View(department);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
        }
        [HttpPost/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> RegisterStep3([Bind]EmployeeInView employee, int firmId)
        {
            if (User.IsHasAccessRights(EnumerableRoles.administrator_firm) || User.IsHasAccessRights(EnumerableRoles.administrator_server))
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        var user = new ApplicationUser { UserName = employee.UserName.str as string, Email = employee.Email.str as string };
                        IdentityResult result = await UserManager.CreateAsync(user, employee.Pass.str as string);
                        if (result.Succeeded)
                        {
                            var roleResult = await UserManager.AddToRoleAsync(user.Id, employee.Role.str as string);
                            if (roleResult.Succeeded)
                            {

                            }
                            return Json(new Response { statusCode = (int)HttpStatusCode.Created, firmId = firmId, userId = user.Id, email = (string)employee.Email.str }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    catch (Exception e)
                    {
                        Trace.TraceError("Регистрация шаг 3. {0}", e.Message);
                        return new HttpStatusCodeResult(HttpStatusCode.Conflict);                       
                    }
                    
                 //   return Json(new Response { statusCode = (int)HttpStatusCode.Conflict, firmId = firmId }, JsonRequestBehavior.AllowGet);
                }
                return Json(employee, JsonRequestBehavior.AllowGet);
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }
        [HttpPost/*, ValidateAntiForgeryToken*/]
        public async Task<ActionResult> RegisterStep4([Bind]EmployeeInView employee, string departments, string userId, int firmId)
        {
            if (User.IsHasAccessRights(EnumerableRoles.administrator_firm) || User.IsHasAccessRights(EnumerableRoles.administrator_server))
            {
                var decodeDepartments = (System.Web.Helpers.Json.Decode<ICollection<DepartmentInView>>(departments)).ToList();

                if (ModelState.IsValid)
                {
                    var findFirm = await DBRepository.Firms.FindAsync(firmId);
                    if (findFirm == null)
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                    }
                    try
                    {
                        await CreateUserAsync(findFirm, employee, userId, decodeDepartments);
                        await DBRepository.SaveChangesAsync();
                        return Json(new Response { statusCode = (int)HttpStatusCode.Created, firmId = firmId }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception e)
                    {
                        Trace.TraceError("Регистрация шаг 4. {0}", e.Message);
                        return new HttpStatusCodeResult(HttpStatusCode.Conflict);
                    }

                }
                return Json(employee, JsonRequestBehavior.AllowGet);
            }
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        private bool FindNameFirm(string nameFirm)
        {
            var li = this.DBRepository.Firms.ToList();
            foreach (var item in li)
            {
                if (item.Name.CompareTo(nameFirm) == 0)
                {
                    return true;
                }
            }
            return false;
        }
        private bool FindNameDepartment(ICollection<Entities.Firm.Department> L_dep, string nameDepartment)
        {

            foreach (var item in L_dep)
            {
                if (item.Name.CompareTo(nameDepartment) == 0)
                {
                    return true;
                }
            }
            return false;
        }


        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }
		[ActionName("Autocomplete")]
		public async Task< ActionResult> AutocompleteAsync(int? id_firm, string term)
		{
			var filteredItems = (await this.DBRepository.Firms.FindAsync(id_firm)).Departments.Where(dep => dep.Name.IndexOf(term) >= 0).Select(dep => dep.Name).ToList();
			return Json(filteredItems, JsonRequestBehavior.AllowGet);
		}

        public async Task<JsonResult> ValidUserName(EValidUser validate, string str)
        {

            bool[] erp = { false };
            if (validate == EValidUser.userName)
            {
                if (str != null)
                {
                    var res = await Task.Factory.StartNew(async () =>
                    {
                        bool[] erpLocal = { false };
                        var find = await DBRepository.Users.FirstOrDefaultAsync(p => p.UserName.Equals(str));
                        if (find != default(ApplicationUser))
                        {
                            erpLocal[0] = false;
                        }
                        else
                        {
                            erpLocal[0] = true;
                        }
                        return erpLocal;

                    });
                    erp = res.Result;
                }
            }

            if (validate == EValidUser.email)
            {
                if (str != null)
                {
                    var res = await Task.Factory.StartNew(async () =>
                    {
                        bool[] erpLocal = { false };
                        var find = await DBRepository.Users.FirstOrDefaultAsync(p => p.Email.Equals(str));
                        if (find != default(ApplicationUser))
                        {
                            erpLocal[0] = false;
                        }
                        else
                        {
                            erpLocal[0] = true;
                        }
                        return erpLocal;
                    });
                    erp = res.Result;
                }
            }
            if (validate == EValidUser.birthDate)
            {
                if (str != null)
                {
                    var res = await Task.Factory.StartNew(async () =>
                    {
                        bool[] erpLocal = { false };

                        Setting find = await DBRepository.Settings.FirstOrDefaultAsync(p => p.SettingKey.Equals("StartWorkingAge"));
                        ///TODO: Заменить статическое значение возраста на динамические настрройки по умолчанию
                        int validAge = 18;
                        if (find != null)
                        {
                            validAge = Convert.ToInt32(find.SettingValue);
                        }

                        DateTime birthday = Convert.ToDateTime(str, System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat);
                    //    var birthday = Convert.ToDateTime(str);
                        var age = (new DateTime() + (DateTime.Now - birthday)).Year;

                        if (validAge > age)
                        {
                            erpLocal[0] = false;
                        }
                        else
                        {
                            erpLocal[0] = true;
                        }
                        return erpLocal;
                    });
                    erp = res.Result;
                }
            }
            if (validate == EValidUser.employement)
            {
                if (str != null)
                {
                    erp[0] = (new CustomDateRangeAttribute()).IsValid(DateTime.Parse(str));

                }
            }

            return Json(erp, JsonRequestBehavior.AllowGet);
        }     
    }
}