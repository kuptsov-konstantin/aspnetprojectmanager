﻿using System.Web;
using System.Web.Optimization;

namespace MvcInfos
{
	public class BundleConfig
	{
		//Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.UseCdn = true;
			if (HttpContext.Current.IsDebuggingEnabled)
			{
				bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/dist/jquery-bundle.js"));
				bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/dist/jquery-val-bundle.js"));
				bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include("~/Scripts/dist/jquery-ui-bundle.js"));
				bundles.Add(new ScriptBundle("~/bundles/moment_with_locales_min").Include("~/Scripts/dist/moment-bundle.js"));
				bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/dist/modernizr.js"));
				bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/dist/bootstrap-bundle.js"));

				bundles.Add(new ScriptBundle("~/bundles/vendors").Include(
					"~/Scripts/dist/vendors-bundle.js",
					"~/Scripts/dist/fullcalendar-bundle.js",
					"~/Scripts/dist/flot-bundle.js"));

				bundles.Add(new ScriptBundle("~/bundles/sitejs").Include(
					"~/Scripts/build/siteTheme.js",
					"~/Scripts/build/tsbuild.js"));

				bundles.Add(new ScriptBundle("~/bundles/summernote").Include("~/Scripts/dist/summernote-bundle.js"));
				bundles.Add(new ScriptBundle("~/bundles/uploadcare").Include("~/Scripts/dist/uploadcare/uploadcare.full.js"));
				bundles.Add(new StyleBundle("~/Content/css/summernote").Include("~/Content/dist/css/summernote-style.css"));


				bundles.Add(new StyleBundle("~/Content/css/vendor").Include(
					"~/Content/dist/css/fullcalendar-style.css",
					"~/Content/dist/css/vendors-style.css"));

				bundles.Add(new StyleBundle("~/Content/css/fonts").Include(
					"~/Content/dist/css/font-awesome-style.css", new CssRewriteUrlTransform()));


				bundles.Add(new StyleBundle("~/Content/themes/base/css").Include("~/Content/dist/css/jquery-ui-style.css"));

				bundles.Add(new StyleBundle("~/Content/css/main").Include(
					"~/Content/dist/css/bootstrap-style.css", new CssRewriteUrlTransform()).Include(
					"~/Content/build/gentelella.css",
					"~/Content/build/site.css"
					));		
			}
			else
			{
				bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/dist/jquery-bundle.min.js"));
				bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/dist/jquery-val-bundle.min.js"));
				bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include("~/Scripts/dist/jquery-ui-bundle.js"));
				bundles.Add(new ScriptBundle("~/bundles/moment_with_locales_min").Include("~/Scripts/dist/moment-bundle.min.js"));


				// Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
				// используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
				bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/dist/modernizr.min.js"));
				bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/dist/bootstrap-bundle.min.js"));

				bundles.Add(new ScriptBundle("~/bundles/vendors").Include(
					"~/Scripts/dist/vendors-bundle.min.js",
					"~/Scripts/dist/fullcalendar-bundle.min.js",
					"~/Scripts/dist/flot-bundle.min.js"));

				/*
					"~/vendors/Chart.js/dist/Chart.min.js",
					"~/vendors/gauge.js/dist/gauge.min.js",
					"~/vendors/skycons/skycons.js",
					"~/vendors/Flot/jquery.flot.js",
					"~/vendors/Flot/jquery.flot.pie.js",
					"~/vendors/Flot/jquery.flot.time.js",
					"~/vendors/Flot/jquery.flot.stack.js",
					"~/vendors/Flot/jquery.flot.resize.js",
					"~/vendors/flot.orderbars/js/jquery.flot.orderBars.js",
					"~/vendors/flot-spline/js/jquery.flot.spline.min.js",
					"~/vendors/flot.curvedlines/curvedLines.js",
					"~/vendors/DateJS/build/date.js",
					"~/vendors/jqvmap/dist/jquery.vmap.js",
					"~/vendors/jqvmap/dist/maps/jquery.vmap.world.js",
					"~/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js",
					"~/vendors/moment/min/moment.min.js",
					"~/vendors/bootstrap-daterangepicker/daterangepicker.js"
				*/

				bundles.Add(new ScriptBundle("~/bundles/sitejs").Include(
					"~/Scripts/build/siteTheme.min.js",
					"~/Scripts/build/tsbuild.min.js"));

				bundles.Add(new StyleBundle("~/Content/css/summernote").Include("~/Content/dist/css/summernote-style.min.css"));

				bundles.Add(new ScriptBundle("~/bundles/uploadcare").Include("~/Scripts/dist/uploadcare/uploadcare.full.min.js"));

				bundles.Add(new StyleBundle("~/Content/themes/base/css").Include("~/Content/dist/css/jquery-ui-style.min.css"));

				bundles.Add(new StyleBundle("~/Content/css/vendor").Include(
								"~/Content/dist/css/fullcalendar-style.min.css",
								"~/Content/dist/css/font-awesome-style.min.css",
								"~/Content/dist/css/vendors-style.css",

								"~/Content/jQuery-Smart-Wizard-2/smart_wizard.css",
								"~/Content/nprogress.css",
								"~/Content/iCheck/flat/green.css",
								"~/Content/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css",
								"~/Content/fullcalendar/fullcalendar.min.css",
								"~/Content/jquery.treegrid.css",
								"~/vendors/jqvmap/dist/jqvmap.min.css",
								"~/vendors/bootstrap-daterangepicker/daterangepicker.css"));

				bundles.Add(new StyleBundle("~/Content/css/main").Include(
					"~/Content/dist/css/bootstrap-style.min.css", new CssRewriteUrlTransform())
					.Include(
					"~/Content/build/gentelella.min.css",
					"~/Content/build/site.min.css"
					));

				BundleTable.EnableOptimizations = true;
			}

		}
	}
}