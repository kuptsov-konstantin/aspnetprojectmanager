﻿using MvcInfos.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcInfosTests.Fake
{
	public class FakeApplicationUserSet : FakeDbSet<ApplicationUser>
	{
		public override ApplicationUser Find(params object[] keyValues)
		{
			return this.SingleOrDefault(d => d.Id == (string)keyValues.Single());
		}
	}

	public class FakeDbSet<T> : IDbSet<T>	where T : class
	{
		IQueryable _query;

		public FakeDbSet()
		{
			Local = new ObservableCollection<T>();
			Query = Local.AsQueryable();
		}

		public virtual T Find(params object[] keyValues)
		{
			throw new NotImplementedException("Derive from FakeDbSet<T> and override Find");
		}

		public T Add(T item)
		{
			Local.Add(item);
			return item;
		}

		public T Remove(T item)
		{
			Local.Remove(item);
			return item;
		}

		public T Attach(T item)
		{
			Local.Add(item);
			return item;
		}

		public T Detach(T item)
		{
			Local.Remove(item);
			return item;
		}

		public T Create()
		{
			return Activator.CreateInstance<T>();
		}

		public TDerivedEntity Create<TDerivedEntity>() where TDerivedEntity : class, T
		{
			return Activator.CreateInstance<TDerivedEntity>();
		}

		public ObservableCollection<T> Local { get; }

		Type IQueryable.ElementType
		{
			get { return Query.ElementType; }
		}

		System.Linq.Expressions.Expression IQueryable.Expression
		{
			get { return Query.Expression; }
		}

		IQueryProvider IQueryable.Provider
		{
			get { return Query.Provider; }
		}

		public IQueryable Query { get => _query; set => _query = value; }

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return Local.GetEnumerator();
		}

		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			return Local.GetEnumerator();
		}
	}
}
