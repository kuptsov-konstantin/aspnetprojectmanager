﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MvcInfosTests.Fake
{
	public class HttpContextManager : IServiceProvider
	{
		private static HttpContextBase m_context;
		public static HttpContextBase Current
		{
			get
			{
				if (m_context != null)
					return m_context;

				if (HttpContext.Current == null)
					throw new InvalidOperationException("HttpContext not available");

				return new HttpContextWrapper(HttpContext.Current);
			}
		}

		public static void SetCurrentContext(HttpContextBase context)
		{
			m_context = context;
		}

		public object GetService(Type serviceType)
		{
			return Current.GetService(serviceType);
		}
	}
}
