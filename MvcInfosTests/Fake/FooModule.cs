﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using MvcInfos;
using MvcInfos.Entities;
using MvcInfos.HKey;
using MvcInfos.IdentityConfig;
using MvcInfos.Plugin;
using MvcInfos.Plugin.Interfaces;
using MvcInfos.Repository;
using MvcInfos.Repository.IdentityModels;
using MvcInfos.Settings;
using Ninject.Modules;
using Owin;
using System;
using System.DirectoryServices.AccountManagement;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MvcInfosTests.Fake
{
	class OwinTestConf
	{
		public void Configuration(IAppBuilder app)
		{


			// Настройка контекста базы данных, диспетчера пользователей и диспетчера входа для использования одного экземпляра на запрос
#if DEBUG
			app.CreatePerOwinContext(() => new ActiveDirectorySetting(ContextType.Machine));
#elif RELEASE
			app.CreatePerOwinContext(() => new ActiveDirectorySetting(ContextType.Domain));
#elif (AZUREDEBUG || AZURERELEASE)

#endif
			app.CreatePerOwinContext(ApplicationDbContext.Create);
			app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
			app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);
			app.CreatePerOwinContext<ApplicationRoleManager>(ApplicationRoleManager.Create);
			app.CreatePerOwinContext(() => new HKCurrentUser());

			// Включение использования файла cookie, в котором приложение может хранить информацию для пользователя, выполнившего вход,
			// и использование файла cookie для временного хранения информации о входах пользователя с помощью стороннего поставщика входа
			// Настройка файла cookie для входа
			app.UseCookieAuthentication(new CookieAuthenticationOptions
			{
				AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
				LoginPath = new PathString("/Account/Login"),
				Provider = new CookieAuthenticationProvider
				{
					// Позволяет приложению проверять метку безопасности при входе пользователя.
					// Эта функция безопасности используется, когда вы меняете пароль или добавляете внешнее имя входа в свою учетную запись.  
					OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
						 validateInterval: TimeSpan.FromMinutes(30),
						 regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
				}
			});
			app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

			// Позволяет приложению временно хранить информацию о пользователе, пока проверяется второй фактор двухфакторной проверки подлинности.
			app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

			// Позволяет приложению запомнить второй фактор проверки имени входа. Например, это может быть телефон или почта.
			// Если выбрать этот параметр, то на устройстве, с помощью которого вы входите, будет сохранен второй шаг проверки при входе.
			// Точно так же действует параметр RememberMe при входе.
			app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

			// Раскомментируйте приведенные далее строки, чтобы включить вход с помощью сторонних поставщиков входа
			//app.UseMicrosoftAccountAuthentication(
			//    clientId: "",
			//    clientSecret: "");

			//app.UseTwitterAuthentication(
			//   consumerKey: "",
			//   consumerSecret: "");

			//app.UseFacebookAuthentication(
			//   appId: "",
			//   appSecret: "");

			//app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
			//{
			//    ClientId = "",
			//    ClientSecret = ""
			//});
		}
	}

	public class FooModule : NinjectModule
	{
		public override void Load()
		{

			Bind<IUserStore<ApplicationUser>>().To<UserStore<ApplicationUser>>();
			Bind<UserManager<ApplicationUser>>().ToSelf();
			//Bind<HttpContextBase>().ToMethod(ctx => HttpContextManager.Current.InTransientScope());
			Bind<ApplicationSignInManager>().ToMethod((context) => HttpContextManager.Current.GetOwinContext().Get<ApplicationSignInManager>());
			Bind<ApplicationRoleManager>().ToMethod((context) => HttpContextManager.Current.GetOwinContext().GetUserManager<ApplicationRoleManager>());
			Bind<ApplicationUserManager>().ToMethod((context) => HttpContextManager.Current.GetOwinContext().GetUserManager<ApplicationUserManager>());
			Bind<PrincipalContext>().ToMethod((context) => HttpContextManager.Current.GetOwinContext().Get<PrincipalContext>());
			Bind<IPlugins>().To<LoadPlugins>().InSingletonScope();
			Bind<IApplicationRepository>().To<ApplicationRepository>();
			Bind<IHKCurrentUser>().ToMethod((context) => HttpContextManager.Current.GetOwinContext().Get<HKCurrentUser>());


#if (DEBUG || RELEASE)
			Bind<ActiveDirectorySetting>().ToMethod((context) => HttpContextManager.Current.GetOwinContext().Get<ActiveDirectorySetting>());
			//	Bind<PrincipalContext>().ToMethod((context) => (new HttpContextWrapper(HttpContext.Current)).GetOwinContext().Get<PrincipalContext>());
#endif
		}
	}
}
