﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using MvcInfosTests.Fake;
using Ninject;
using System;
using Moq;
using System.Collections.Specialized;
using System.Security.Principal;

using System.Web.Mvc;
using System.Web.Routing;
using System.Web;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using System.Web.Hosting;
using System.IO;
using MvcInfos.Repository;
using MvcInfos.Entities;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Net.Http;
using Microsoft.Owin.Testing;
using Microsoft.QualityTools.Testing.Fakes;
using System.Security.Principal.Fakes;
using Microsoft.Owin.Fakes;



namespace MvcInfos.Controllers.Tests
{
	[TestClass()]
	public class AdministrationControllerTests
	{
		//private void PrepareData()
		//{
		//	using (var model = new ApplicationDbContext())
		//	{
		//		ContextInitializer init = new ContextInitializer();
		//		init.InitializeDatabase(model);
		//	}
		//}
		//private HttpContextBase GetMockedHttpContext()
		//{
		//	var context = new Mock<HttpContextBase>();
		//	var request = new Mock<HttpRequestBase>();
		//	var response = new Mock<HttpResponseBase>();
		//	var session = new Mock<HttpSessionStateBase>();
		//	var server = new Mock<HttpServerUtilityBase>();
		//	var user = new Mock<IPrincipal>();
		//	var identity = new Mock<IIdentity>();
		//	var urlHelper = new Mock<UrlHelper>();

		//	var routes = new RouteCollection();
		//	routes.MapMvcAttributeRoutForTesting(Assembly.GetAssembly(typeof(AdministrationController)));
		//	MvcApplication.RegisterRoutes(routes);
		//	var requestContext = new Mock<RequestContext>();
		//	requestContext.Setup(x => x.HttpContext).Returns(context.Object);
		//	context.Setup(ctx => ctx.Request).Returns(request.Object);
		//	context.Setup(ctx => ctx.Response).Returns(response.Object);
		//	context.Setup(ctx => ctx.Session).Returns(session.Object);
		//	context.Setup(ctx => ctx.Server).Returns(server.Object);
		//	context.Setup(ctx => ctx.User).Returns(user.Object);
		//	user.Setup(ctx => ctx.Identity).Returns(identity.Object);
		//	identity.Setup(id => id.IsAuthenticated).Returns(true);
		//	identity.Setup(id => id.Name).Returns("test");
		//	request.Setup(req => req.Url).Returns(new Uri("http://www.google.com"));
		//	request.Setup(req => req.RequestContext).Returns(requestContext.Object);
		//	requestContext.Setup(x => x.RouteData).Returns(new RouteData());
		//	request.SetupGet(req => req.Headers).Returns(new NameValueCollection());

		//	return context.Object;
		//}

		private AdministrationController _controller;
		[TestInitialize]
		public void MyTestInitialize()
		{
			//using (var server = TestServer.Create<OwinTestConf>())
			//{
			//	using (var client = new HttpClient(server.Handler))
			//	{
			//		var response = await client.GetAsync("http://testserver/api/values");
			//		var result = await response.Content.ReadAsAsync<List<string>>();
			//		Assert.IsTrue(result.Any());
			//	}
			//}

			EffortProviderFactory.ResetDb();
			using (ShimsContext.Create())
			{
				var context = new System.Web.Fakes.ShimHttpContext();
				var user = new StubIPrincipal
				{
					IdentityGet = () =>
					{
						var identity = new StubIIdentity { NameGet = () => "foo" };
						return identity;
					}
				};
				
				context.UserGet = () => user;
				System.Web.Fakes.ShimHttpContext.CurrentGet = () => { return context; };
				
				var kernel = new StandardKernel(new FooModule());
				_controller = kernel.Get<AdministrationController>();
			}
			//HttpContextManager.SetCurrentContext(GetMockedHttpContext());

			//var cont = "TestConnection";
			//SimpleWorkerRequest request = new SimpleWorkerRequest("", "$(SolutionDir)\\MVCInfosV2Web\\", "", null, new StringWriter());
			//HttpContext.Current = new HttpContext(request);
			//HttpContext.Current.GetOwinContext();



			//var kernel = NinjectWebCommon.CreatePublicKernel();

		}

		[AssemblyInitialize]
		public static void AssemblyInit(TestContext context)
		{
			Effort.Provider.EffortProviderConfiguration.RegisterProvider();
		}

		[TestMethod()]
		//[HostType("ASP.NET")]
		//[UrlToTest("http://localhost:49964/Administration/_modal_for_user_edit")]		
		//[AspNetDevelopmentServerHost("$(SolutionDir)\\MVCInfosV2Web\\","/")]
		//[Credential("admin@server.com", "ZXCasdqwe123!")]
		public void ModalViewUserEditTest()
		{

			/*var context = new FakeApplicationUserSet
			{

			};*/

			Assert.Fail();
			// your test code using Shims here

			//PrepareData();

			/*var result = controller.Index();

			Assert.IsInstanceOfType(result.ViewData.Model, typeof(IEnumerable<Department>));
			var departments = (IEnumerable<Department>)result.ViewData.Model;
			Assert.AreEqual("AAA", departments.ElementAt(0).Name);
			Assert.AreEqual("BBB", departments.ElementAt(1).Name);
			Assert.AreEqual("ZZZ", departments.ElementAt(2).Name);*/
		}
	}
}