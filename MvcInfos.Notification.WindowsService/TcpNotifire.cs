﻿using MvcInfos.Models.ViewModels.Notification;
using MvcInfos.Network.Extensions;
using MvcInfos.Notification.Models;
using MvcInfos.Notification.PCs;
using MvcInfos.Notification.WindowsService.Properties;
using Newtonsoft.Json;
using PsInstruments;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace MvcInfos.Notification.WindowsService
{
	public class TcpNotifire : IDisposable
	{

		public System.Timers.Timer UpdatePCsTimmer { get; set; }
		public System.Timers.Timer SenderTimer { get; set; }


		public event EventHandler<LoadedEventArgs> Loaded;

		private TcpListener Listener { get; set; } // Объект, принимающий TCP-клиентов
		private AdminInfo AdminInfo { get; set; }
		public string NotifierPath { get; set; }
		public NotifiPCs AcceptablePCs { get; set; }

		public StackNotifications stackNotifications = new StackNotifications();
		public Notifications notificationsMessages = null;

		private Dictionary<Guid, List<ForNotificator>> MessagesToSend = new Dictionary<Guid, List<ForNotificator>>();

		private static bool ChechAccessToServer(string _HostURI, int _PortNumber)
		{
			try
			{
				TcpClient client = new TcpClient(_HostURI, _PortNumber);
				return true;
			}
			catch (Exception ex)
			{
				MyEventLog.WriteEntry(ex.Message, EventLogEntryType.Error, MyEventLog.EStatus.Load);
				return false;
			}
		}


		public async Task LoadPathNotificatorAsync()
		{
			while (true)
			{
				if (ChechAccessToServer("localhost", 443) == true)
				{
					MyEventLog.WriteEntry("Веб сервер откликнулся", EventLogEntryType.Information, MyEventLog.EStatus.Load);
					break;
				}
				Thread.Sleep(1000);
			}

			var nameValues = new NameValueCollection
			{
				{ "serviceId", Settings.Default.ServiceID.ToString() }
			};

			using (WebClient webClient = new WebClient())
			{
				webClient.Headers.Add(HttpRequestHeader.Accept, "application/json");
				var resultPath = await webClient.UploadValuesTaskAsync(new Uri("https://localhost/Notification/GetNotifierPath"), nameValues);
				var pathResult = JsonConvert.DeserializeObject<RequestPath>(Encoding.UTF8.GetString(resultPath));
				if (pathResult != null)
				{

					NotifierPath = pathResult.Path;
				}
				else
				{
					MyEventLog.WriteEntry($"{nameof(RequestPath)} пуст", EventLogEntryType.Error, MyEventLog.EStatus.Load);
				}
				var resultAdmin = await webClient.UploadValuesTaskAsync(new Uri("https://localhost/Notification/GetAdminInfo"), nameValues);
				AdminInfo = JsonConvert.DeserializeObject<AdminInfo>(Encoding.UTF8.GetString(resultAdmin));
			}

			Loaded?.Invoke(this, new LoadedEventArgs());
		}
		public async Task<NotifiPCs> UpdatePCsAsync()
		{
			var nameValues = new NameValueCollection
			{
				//{ "serviceId", Settings.Default.ServiceID.ToString() }
			};
			using (WebClient webClient = new WebClient())
			{
				webClient.Headers.Add(HttpRequestHeader.Accept, "application/json");
				var resultAdmin = await webClient.UploadValuesTaskAsync(new Uri("https://localhost/Notification/GetPCs"), nameValues);

				MyEventLog.WriteEntry($"Загрузка текущего состояния сети {Encoding.UTF8.GetString(resultAdmin)}", EventLogEntryType.Information, MyEventLog.EStatus.Load);
				return JsonConvert.DeserializeObject<NotifiPCs>(Encoding.UTF8.GetString(resultAdmin));
			}
		}
		public async Task<NotificationViewModel> LoadNotificationAsync(Guid notificationId)
		{
			var nameValues = new NameValueCollection
			{
				{ "serviceId", Settings.Default.ServiceID.ToString() },
				{ "notificationId",  notificationId.ToString() }
			};
			using (WebClient webClient = new WebClient())
			{
				webClient.Headers.Add(HttpRequestHeader.Accept, "application/json");
				var resultAdmin = await webClient.UploadValuesTaskAsync(new Uri("https://localhost/Notification/GetNotificationById"), nameValues);
				MyEventLog.WriteEntry($"Загрузка доп информации к уведомлению {notificationId}", EventLogEntryType.Information, MyEventLog.EStatus.Load);
				return JsonConvert.DeserializeObject<NotificationViewModel>(Encoding.UTF8.GetString(resultAdmin));
			}
		}
		public async Task<List<NotificationViewModel>> LoadAllCurrentNotificationsAsync()
		{
			var nameValues = new NameValueCollection
			{
				{ "serviceId", Settings.Default.ServiceID.ToString() }
			};
			using (WebClient webClient = new WebClient())
			{
				webClient.Headers.Add(HttpRequestHeader.Accept, "application/json");
				var resultAdmin = await webClient.UploadValuesTaskAsync(new Uri("https://localhost/Notification/GetCurrentNotifications"), nameValues);
				MyEventLog.WriteEntry($"Загрузка текущих уведомлений", EventLogEntryType.Information, MyEventLog.EStatus.Load);
				return JsonConvert.DeserializeObject<List<NotificationViewModel>>(Encoding.UTF8.GetString(resultAdmin));
			}
		}

		/**
		 * <summary>Запуск сервера</summary> 
		 */
		public TcpNotifire()
		{
			notificationsMessages = new Notifications(stackNotifications.Add);
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
			ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, error) =>
			{
				if (error == System.Net.Security.SslPolicyErrors.None)
				{
					return true;
				}
				MyEventLog.WriteEntry($"X509Certificate [{cert.Subject}] Policy Error: '{error.ToString()}'", EventLogEntryType.Information, MyEventLog.EStatus.Load);
				return true;
			};
			UpdatePCsTimmer = new System.Timers.Timer(1800000);
			UpdatePCsTimmer.Elapsed += (o, e) =>
			{
				Task.Run(async () =>
				{
					AcceptablePCs = await UpdatePCsAsync();
					MyEventLog.WriteEntry($"Обновлена информация о сетевых ПК в {AcceptablePCs.LastUpdate}", EventLogEntryType.Information, MyEventLog.EStatus.Load);
				});
			};
			SenderTimer = new System.Timers.Timer(300000);
			SenderTimer.Elapsed += (o, e) =>
			{
				stackNotifications.SendCurrent(SendNotification);
				MyEventLog.WriteEntry($"Отправка пачки уведомлений завершена {DateTime.Now}", EventLogEntryType.Information, MyEventLog.EStatus.Load);

			};

		}
		/**
		 * <summary>Остановка сервера</summary>
		 */
		public void Stop()
		{
			Listener.Stop();
			UpdatePCsTimmer.Stop();
			SenderTimer.Stop();
			serverThread?.Abort();
		}
		Thread serverThread = null;
		public void Start()
		{
			this.Loaded += async (o, e) =>
			{
				UpdatePCsTimmer.Start();
				SenderTimer.Start();

				AcceptablePCs = await UpdatePCsAsync();
				var notificationsView = await LoadAllCurrentNotificationsAsync();
				if (notificationsView.Count > 0)
				{
					var notifications = new List<Notification>();
					foreach (var item in notificationsView)
					{
						var notif = new Notification(item);
						if (notif != null)
						{
							notifications.Add(notif);
						}
						else
						{
							MyEventLog.WriteEntry($"Cust error", EventLogEntryType.Error, MyEventLog.EStatus.Load);
						}
					}

					//var notifications = notificationsView.Select(not => (Notification)not).ToList();

					notificationsMessages.AddRange(notifications);
					MyEventLog.WriteEntry($"Число уведомлений получено - {notifications.Count}", EventLogEntryType.Information, MyEventLog.EStatus.Load);
				}
				MyEventLog.WriteEntry("Инициализация завершена", EventLogEntryType.Information, MyEventLog.EStatus.Load);

				serverThread = new Thread(new ThreadStart(InitializeServer));
				serverThread.Start();
				//InitializeServer();
			};

			Task.WaitAll(LoadPathNotificatorAsync());
		}

		private void InitializeServer()
		{
			try
			{
				Task.Factory.StartNew(async () =>
			   {
				   Listener = new TcpListener(IPAddress.Any, 10010);
				   Listener.Start();
				   byte[] bytes = new byte[1024];
				   while (true)
				   {
					   MyEventLog.WriteEntry("Ожидание клиента", EventLogEntryType.Warning, MyEventLog.EStatus.WaitClient);
					   var tcpClient = await Listener.AcceptTcpClientAsync();
					   await Task.Factory.StartNew(() => TcpClientConnected(tcpClient));
				   }
			   });
			}
			catch (Exception e)
			{
				MyEventLog.WriteEntry($"Сообщение: {e.Message}" + Environment.NewLine
									+ $"Стек: {e.StackTrace}", EventLogEntryType.Error, MyEventLog.EStatus.SendMessage);
			}
		}

		private async Task TcpClientConnected(TcpClient tcpClient)
		{
			MyEventLog.WriteEntry($"Клиент подключен: {((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString()}", EventLogEntryType.Warning, MyEventLog.EStatus.WaitClient);
			var stream = tcpClient.GetStream();
			var recive = await stream.ReciverAsync();
			var request = recive.MyDeserialize<string>();
			MyEventLog.WriteEntry(request, EventLogEntryType.Information, MyEventLog.EStatus.Load);


			if (request.Equals("Echo"))
			{
				MyEventLog.WriteEntry($"Эхо запрос: {((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString()}", EventLogEntryType.Warning, MyEventLog.EStatus.TcpEcho);

				await stream.SenderAsync(recive);
			}

			if (request.Equals("Add"))
			{
				MyEventLog.WriteEntry($"Запрос добавления: {((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString()}", EventLogEntryType.Warning, MyEventLog.EStatus.TcpAdd);

				recive = await stream.ReciverAsync();
				var requests1 = recive.MyDeserialize<List<TcpNotification>>();
				foreach (var item in requests1)
				{
					var notification = await LoadNotificationAsync(item.NotificationId);
					notificationsMessages.Add((Notification)notification);
				}

			}
			if (request.Equals("Update"))
			{
				MyEventLog.WriteEntry($"Запрос обновления: {((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString()}", EventLogEntryType.Warning, MyEventLog.EStatus.TcpUpdate);

				recive = await stream.ReciverAsync();
				var requests1 = recive.MyDeserialize<List<TcpNotification>>();
				foreach (var item in requests1)
				{
					var notification = await LoadNotificationAsync(item.NotificationId);
					notificationsMessages.Update((Notification)notification);
				}
			}
			if (request.Equals("GetMessages"))
			{
				MyEventLog.WriteEntry($"Запрос сообщений: {((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString()}", EventLogEntryType.Warning, MyEventLog.EStatus.TcpGetMessages);

				recive = await stream.ReciverAsync();
				var notifMessID = recive.MyDeserialize<Guid>();
				if (MessagesToSend.ContainsKey(notifMessID))
				{
					var obd = MessagesToSend[notifMessID];
					//var localThanReplace = HttpUtility.UrlEncode(JsonConvert.SerializeObject(obd));
					var count = obd.Count;
					var counts = BitConverter.GetBytes(count);
					await stream.SenderAsync(counts);

					foreach (var item in obd)
					{
						await stream.SendObjectAsync(item.MailId);
						await stream.SendObjectAsync(item.Message);

						await stream.SendObjectAsync(item.ResolutionNotifierId);
						await stream.SendObjectAsync(item.Description);

					}


				}
				else
				{
					MyEventLog.WriteEntry("GetMessages. Пустой ответ", EventLogEntryType.Warning, MyEventLog.EStatus.WaitClient);
					await stream.SenderAsync(new byte[] { });
				}




			}

			tcpClient.Close();

		}
		private Uri IgnoreNotClosingClickNotifi => new Uri($"https://{Environment.MachineName}/Notification/Click");

		private List<Thread> SendNotification(Guid userId, List<ForNotificator> forNotificator)
		{
			List<Thread> list = new List<Thread>();


			if (AcceptablePCs == null)
			{
				return list;
			}

			if (AcceptablePCs.UsersPC == null)
			{
				return list;
			}
			if (AcceptablePCs.UsersPC.ContainsKey(userId))
			{
				foreach (var item in AcceptablePCs.UsersPC[userId])
				{
					try
					{
						if (forNotificator == null)
						{
							continue;
						}


						var messageId = Guid.NewGuid();
						MessagesToSend.Add(messageId, forNotificator);

						//var localThanReplace = HttpUtility.UrlEncode(JsonConvert.SerializeObject(forNotificator));
						Thread threadd = new Thread(() =>
						{
							var process = PsExec.RunRemoteProgram(item.Name, AdminInfo.Login, AdminInfo.Password, NotifierPath,
									$@"/ServerIP https://{Environment.MachineName} /UserID {userId} /MessagesId {messageId}");
							if (process.ErrorMessage.Contains("Error copying"))
							{
								var messages = MessagesToSend[messageId];
								foreach (var message in messages)
								{
									var nameValues = new NameValueCollection
									{
										{ "notificationId", message.ResolutionNotifierId.ToString() },
										{ "mailId", message.MailId.ToString() },
										{ "userId", userId.ToString() },
										{ "clickReport", Convert.ToString( (int)Entities.Notifications.NotificationsClickType.IgnoreNotClosing )}
									};
									using (var webClient = new WebClient())
									{
										webClient.Headers.Add(HttpRequestHeader.Accept, "application/json");
										webClient.UploadValuesCompleted += (sender, e) =>
										{
											MessagesToSend.Remove(messageId);
										};
										webClient.UploadValuesAsync(IgnoreNotClosingClickNotifi, nameValues);
									}
								}
							}
							else
							{
								if (process.ErrorMessage.Contains("with error code"))
								{
									MessagesToSend.Remove(messageId);
								}
								MyEventLog.WriteEntry($"Сообщение: {process.ErrorMessage}", EventLogEntryType.Error, MyEventLog.EStatus.SendMessage);
							}
						
						
						});






						list.Add(threadd);
						threadd.Start();
					}
					catch (Win32Exception e)
					{
						MyEventLog.WriteEntry($"Сообщение: {e.Message}" + Environment.NewLine
							+ $"Код ошибки: {e.NativeErrorCode}" + Environment.NewLine
							+ $"Стек: {e.StackTrace}", EventLogEntryType.Error, MyEventLog.EStatus.SendMessage);
					}
					catch (Exception e)
					{
						MyEventLog.WriteEntry($"Сообщение: {e.Message}" + Environment.NewLine
							+ $"Стек: {e.StackTrace}", EventLogEntryType.Error, MyEventLog.EStatus.SendMessage);
					}
				}
			}
			return list;
		}

		public void Dispose()
		{
			if (Listener != null)
			{
				// Остановим его
				Stop();
			}
		}
	}
}
