﻿using System.Collections.Generic;

namespace MvcInfos.Notification.WindowsService
{
	public class UsersNotifications
	{
		Dictionary<int, List<Notification>> notifications = new Dictionary<int, List<Notification>>();
		public Dictionary<int, List<Notification>> Notifications => notifications;
		public void Add(Notification notification)
		{
			if (!Notifications.ContainsKey(notification.NotifyTimeKey))
			{
				Notifications.Add(notification.NotifyTimeKey, new List<Notification>());
			}
			Notifications[notification.NotifyTimeKey].Add(notification);
		}
		//	public List<NotificationViewModel> GetNext()
		//	{
		//		List<NotificationViewModel> nextNotifications = new List<NotificationViewModel>();
		//		var currentTime = DateTime.Now.TimeOfDay;
		//		var nextTime = currentTime.Add(new TimeSpan(0, 5, 0));
		//		foreach (var item in NotificationPeriod.NotificationTimes)
		//		{
		//			if (item.Value.PeriodTimes == NotificationPeriod.PeriodTimes.Period)
		//			{
		//				var d = currentTime % item.Value.Period;
		//				if ((currentTime <= item.Value.Period) && (item.Value.Period < nextTime))
		//				{

		//				}
		//			}
		//			if (item.Value.PeriodTimes == NotificationPeriod.PeriodTimes.EveryDay)
		//			{
		//				if ((currentTime <= item.Value.Period) && (item.Value.Period < nextTime))
		//				{
		//					nextNotifications.AddRange(Notifications[item.Key]);
		//				}
		//			}
		//		}
		//	}
	}
}
