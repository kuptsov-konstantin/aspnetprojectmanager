﻿using System;

namespace MvcInfos.Notification.WindowsService
{
	public class NotificationEventArgs : EventArgs
	{
		public Notification Notification { get; set; }
	}
}