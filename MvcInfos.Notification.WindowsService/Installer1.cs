﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace MvcInfos.Notification.WindowsService
{
	[RunInstaller(true)]
	public partial class Installer1 : Installer
	{
		ServiceInstaller serviceInstaller;
		ServiceProcessInstaller processInstaller;
		public Installer1()
		{
			InitializeComponent();
			serviceInstaller = new ServiceInstaller()
			{
				DelayedAutoStart = true,
				StartType = ServiceStartMode.Automatic,

#if DEBUG
				ServiceName = Properties.Settings.Default.ServiceName + ".DEBUG",
				DisplayName = Properties.Settings.Default.DisplayName + " (Отладка)",
				Description = Properties.Settings.Default.ServiceDescription + " (Отладка)",

#else
				ServiceName = Properties.Settings.Default.ServiceName,
				DisplayName = Properties.Settings.Default.DisplayName,	
				Description = Properties.Settings.Default.ServiceDescription,
#endif

				ServicesDependedOn = new string []{"was", "w3svc" }
				
			};
			processInstaller = new ServiceProcessInstaller()
			{
				Account = ServiceAccount.LocalSystem
			};
			Installers.Add(processInstaller);
			Installers.Add(serviceInstaller);
		}
	}
}
