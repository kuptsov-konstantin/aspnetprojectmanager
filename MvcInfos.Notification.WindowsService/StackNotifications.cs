﻿using MvcInfos.Notification.Models;
using PsInstruments;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace MvcInfos.Notification.WindowsService
{
	public class StackNotifications
	{
		Dictionary<string, Stack<Notification>> currentNotifications = new Dictionary<string, Stack<Notification>>();

		public void Add(Notification notification)
		{
			try
			{
				if (!currentNotifications.ContainsKey(notification.UserName))
				{
					currentNotifications.Add(notification.UserName, new Stack<Notification>());
				}
				currentNotifications[notification.UserName].Push(notification);
			}
			catch (Exception e)
			{
				MyEventLog.WriteEntry($"Сообщение: {e.Message}" + Environment.NewLine
						+ $"Стек: {e.StackTrace}", EventLogEntryType.Error, MyEventLog.EStatus.SendMessage);

			}

		}
		public void SendCurrent(Func<Guid, List<ForNotificator>, List<Thread>> sender)
		{
			EventLog.WriteEntry("SManager", $"CurrentNotifications {currentNotifications.Count}");
			foreach (var item in currentNotifications)
			{
				List<ForNotificator> forNotificator = new List<ForNotificator>();
				var userID = item.Value.First().GetUserId();

				while (item.Value.Count != 0)
				{
					var notif = item.Value.Pop();
					forNotificator.Add(new ForNotificator()
					{
						MailId = notif.MailId,
						Message = notif.NotificationMessage,
						ResolutionNotifierId = notif.ResolutionNotifierId,
						Description = notif.Description
					});
				}
				sender?.Invoke(userID, forNotificator);
			}
		}
	}
}
