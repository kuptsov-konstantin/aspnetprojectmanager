﻿using System;
using System.Collections.Generic;

namespace MvcInfos.Notification.WindowsService
{
	public class Notifications
	{
		public Notifications(Action<Notification> addToSend)
		{
			this.addToSend = addToSend;
		}



		public void Add(Notification notification)
		{
			if (!Users.ContainsKey(notification.UserName))
			{
				Users.Add(notification.UserName, new UsersNotifications());
			}
			Users[notification.UserName].Add(notification);
			notification.Send += (o, e) =>
			{
				addToSend?.Invoke(e.Notification);
			};
		}
		public void AddRange(List<Notification> notifications)
		{
			foreach (var notification in notifications)
			{
				Add(notification);
			}			
		}

		public void Update(Notification notification)
		{
			if (!Users.ContainsKey(notification.UserName))
			{
				Add(notification);
			}
		}

		Dictionary<string, UsersNotifications> users = new Dictionary<string, UsersNotifications>();
		private Action<Notification> addToSend;
		public Dictionary<string, UsersNotifications> Users => users;
	}
}
