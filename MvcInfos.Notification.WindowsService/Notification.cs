﻿using MvcInfos.Entities.UserConfigurations;
using MvcInfos.Models.ViewModels.Notification;
using System;
using System.Diagnostics;
using System.Threading;

namespace MvcInfos.Notification.WindowsService
{

	public class Notification : NotificationViewModel
	{
		Timer events = null;
		public event EventHandler<NotificationEventArgs> Send;

		public Notification()
		{


		}
		public Notification(NotificationViewModel model)
		{
			this.Description = model.Description;
			this.MailId = model.MailId;
			this.NotificationMessage = model.NotificationMessage;
			this.NotifyTimeKey = model.NotifyTimeKey;
			this.ResolutionNotifierId = model.ResolutionNotifierId;
			this.UserId = model.UserId;
			this.UserName = model.UserName;
			try
			{
				Start();
			}
			catch (Exception e)
			{
				MyEventLog.WriteEntry($"Сообщение: {e.Message}" + Environment.NewLine
				+ $"Стек: {e.StackTrace}", EventLogEntryType.Error,  MyEventLog.EStatus.SendMessage);

			}

		}


		public void Start()
		{
			if (NotificationPeriod.NotificationTimes[NotifyTimeKey].PeriodTimes == NotificationPeriod.PeriodTimes.Period)
			{
				events = new Timer((state) =>
				{
					Send?.Invoke(this, new NotificationEventArgs()
					{
						Notification = this
					});
				}, this, 0, Convert.ToInt32(NotificationPeriod.NotificationTimes[NotifyTimeKey].Period.TotalMilliseconds));
			}
			else
			{
				events = new Timer((state) =>
				{
					var currnetTime = DateTime.Now.TimeOfDay;
					var nextTime = currnetTime.Add(new TimeSpan(0, 5, 0));
					if ((currnetTime.TotalMilliseconds < Convert.ToInt32(NotificationPeriod.NotificationTimes[NotifyTimeKey].Period.TotalMilliseconds))	
						&& (Convert.ToInt32(NotificationPeriod.NotificationTimes[NotifyTimeKey].Period.TotalMilliseconds) < nextTime.TotalMilliseconds))
					{
						Send?.Invoke(this, new NotificationEventArgs()
						{
							Notification = this
						});
					}

				}, this, 0, 300000);
			}
		}

	
	}
}
