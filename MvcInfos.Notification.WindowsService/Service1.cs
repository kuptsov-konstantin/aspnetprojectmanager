﻿using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;

namespace MvcInfos.Notification.WindowsService
{

	public partial class Service1 : ServiceBase
	{
	
		private TcpNotifire tcpNotifire = new TcpNotifire();
		Thread loggerThread = null;

		public Service1()
		{
			InitializeComponent();
	
		}

		protected override void OnStart(string[] args)
		{
			MyEventLog.WriteEntry("In OnStart", EventLogEntryType.Information, MyEventLog.EStatus.Start);
			loggerThread = new Thread(new ThreadStart(tcpNotifire.Start));
			loggerThread.Start();
		}

		protected override void OnStop()
		{
			MyEventLog.WriteEntry("In onStop.", EventLogEntryType.Information, MyEventLog.EStatus.Stop);
			loggerThread.Abort();
		}
	}
}
