﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using MvcInfos.Models.ViewModels.Notification;

namespace MvcInfos.Notification.Models
{
	public class RequestPath
	{
		[JsonProperty("path")]
		public string Path { get; set; }
	}


	public interface ITcpMethod
	{
		string Method { get; set; }
	}

	public class TcpNotification : ITcpMethod
	{
		public string Method { get; set; }
		public Guid NotificationId { get; set; }
	}

	public class TcpNotificationUpdate : ITcpMethod
	{
		public string Method { get; set; }
		public NotificationViewModel NotificationViewModel { get; set; }
	}
}
