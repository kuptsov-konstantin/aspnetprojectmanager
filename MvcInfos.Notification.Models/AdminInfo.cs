﻿using Newtonsoft.Json;

namespace MvcInfos.Notification.Models
{
	public class AdminInfo
	{
		[JsonProperty("login")]
		public string Login { get; set; }

		[JsonProperty("password")]
		public string Password { get; set; }

	}
}
