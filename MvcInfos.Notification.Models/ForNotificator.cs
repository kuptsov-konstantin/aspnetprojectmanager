﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcInfos.Notification.Models
{
	[Serializable]
	public class ForNotificator
	{
		[JsonProperty("message")]
		public string Message { get; set; }
		[JsonProperty("description")]
		public string Description { get; set; }
		[JsonProperty("mailId")]
		public Guid MailId { get; set; }
		[JsonProperty("resolutionNotifierId")]
		public Guid ResolutionNotifierId { get; set; }
	}
}
