﻿using Microsoft.AspNet.Identity.Owin;
using MvcInfos.Settings;
using System;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MvcInfos.ActiveDirectory
{
	public class AuthorizeActiveDirectoryAttribute : AuthorizeAttribute
	{
		private ActiveDirectorySetting ActiveDirectorySetting;
		public string Groups { get; set; }
		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			if (httpContext.User.Identity.IsAuthenticated == false) return false;
			ActiveDirectorySetting = httpContext.GetOwinContext().Get<ActiveDirectorySetting>();
			return base.AuthorizeCore(httpContext) || this.CheckAccessInActiveDirectory(httpContext.User.Identity.Name);
		}

		private bool CheckAccessInActiveDirectory(string name)
		{
			/* Return true immediately if the authorization is not locked down to any particular AD group */
			if (String.IsNullOrEmpty(Groups))
				return true;

			// Get the AD groups
			var groups = Groups.Split(',').ToList();

			// Verify that the user is in the given AD group (if any)
			var context = ActiveDirectorySetting.GetPrincipalContext();

			var userPrincipal = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, name);
			if (userPrincipal == null)
			{
				return false;
			}
			foreach (var group in groups)
				if (userPrincipal.IsMemberOf(context, IdentityType.Name, group))
					return true;
			return false;
		}	
	}	
}
