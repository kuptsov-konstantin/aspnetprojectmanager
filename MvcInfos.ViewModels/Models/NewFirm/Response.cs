﻿namespace MvcInfos.Models.ViewModels
{
	public class Response
    {
        public int statusCode { get; set; }
        public int firmId { get; set; }
        public string email { get; set; }
        public string userId { get; set; }
    }
}