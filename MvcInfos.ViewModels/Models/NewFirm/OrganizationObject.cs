﻿using System.Collections.Generic;

namespace MvcInfos.Models.ViewModels
{
	public class OrganizationObject
    {
        //step1
        public AboutFirm AboutFirm { get; set; }
        //step2
        public ICollection<DepartmentInView> Departments { get; set; }
        //step3
        public ICollection<EmployeeInView> Employees { get; set; }

    };
}