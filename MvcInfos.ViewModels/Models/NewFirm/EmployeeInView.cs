﻿namespace MvcInfos.Models.ViewModels
{
	public class EmployeeInView
    {
        public Valid FamilyName { get; set; }
        public Valid Name { get; set; }
        public Valid MiddleName { get; set; }
        public Valid BirthDay { get; set; }
        //---
        public Valid UserName { get; set; }
        public Valid Email { get; set; }
        public Valid Pass { get; set; }
        public Valid Role { get; set; }
        public Valid Emploement { get; set; }
        //   public Valid isValidate { get; set; } 
        public bool isValidate()
        {
            return this.FamilyName.isValidate && this.Name.isValidate && this.MiddleName.isValidate && this.BirthDay.isValidate
                && this.UserName.isValidate && this.Email.isValidate && this.Pass.isValidate && this.Role.isValidate;
        }
        public int Department { get; set; }
    }
}