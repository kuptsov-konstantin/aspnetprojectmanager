﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class AboutFirm
    {
        [Required]
        public Valid OrgName { get; set; }
        [Required]
        public Valid Country { get; set; }
        [Required]
        public Valid City { get; set; }
        [Required]
        public Valid Address { get; set; }
    }
}