﻿using MvcInfos.Entities.Mails;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using MvcInfos.Entities.Notifications;

namespace MvcInfos.Models.ViewModels.Notification
{
	public class MailEmployeeInfoViewModel
	{
		[JsonProperty("userId")]
		public Guid UserId { get; set; }
		[JsonProperty("fio")]
		public string FIO { get; set; }
	}

	public class MailNotificationsResultViewModel
	{
		[JsonProperty("mailId")]
		public Guid MailId { get; set; }
		[JsonProperty("date")]
		public DateTime Date { get; set; }
		[JsonProperty("notificationsResult")]
		public List<NotificationsResultViewModel> NotificationsResult { get; set; }
	}

	public class NotificationsResultViewModel
	{
		[JsonProperty("resolutionNotificationId")]
		public Guid ResolutionNotificationId { get; set; }
		[JsonProperty("date")]
		public DateTime Date { get; set; }
		[JsonProperty("clickType")]
		public NotificationsClickType ClickType { get; set; }
		[JsonProperty("user")]
		public MailEmployeeInfoViewModel User { get; set; }
	}
}