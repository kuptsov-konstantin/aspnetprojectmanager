﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MvcInfos.Models.ViewModels.Notification
{



	public class NotificationViewModel
	{
		[JsonProperty("mailId")]
		public Guid MailId { get; set; }
		[JsonProperty("notifyTime")]
		public int NotifyTimeKey { get; set; }
		[JsonProperty("resolutionNotifierId")]
		public Guid ResolutionNotifierId { get; set; }
		[JsonProperty("description")]
		public string Description { get; set; }
		[JsonProperty("notificationMessage")]
		public string NotificationMessage { get; set; }
		[JsonProperty("userName")]
		public string UserName { get; set; }
		[JsonProperty("userId")]
		public string UserId { get; set; }
		public Guid GetUserId() {
			return Guid.Parse(UserId);
		}
	}
}