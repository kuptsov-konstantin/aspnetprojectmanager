﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MvcInfos.Models.ViewModels.Mail
{

	public class MailProjectViewModel
	{
		[JsonProperty("projectId")]
		public int ProjectId { get; set; }
		[JsonProperty("projectCode")]
		public string ProjectCode { get; set; }
	}
	public class MailUploadFileViewModel
	{
		[JsonProperty("uploadFileID")]
		public Guid UploadFileID { get; set; }
	}
	public class MailUploadGroupViewModel
	{
		[JsonProperty("groupFileId")]
		public Guid GroupFileId { get; set; }
		[JsonProperty("uploadFiles")]
		public List<MailUploadFileViewModel> UploadFiles { get; set; }
	}

	public class MailResolutionNotifierViewModel
	{
		[JsonProperty("resolutionNotifierId")]
		public Guid ResolutionNotifierId { get; set; }
		[JsonProperty("notifyTimeKey")]
		public int NotifyTimeKey { get; set; }
		[JsonProperty("userId")]
		public Guid UserId { get; set; }
		[JsonProperty("fio")]
		public string FIO { get; set; }
	}
	public class MailResolutionViewModel
	{
		[JsonProperty("resolutionId")]
		public Guid ResolutionId { get; set; }
		[JsonProperty("comment")]
		public string Comment { get; set; }
		[JsonProperty("resolutionNotifier")]
		public List<MailResolutionNotifierViewModel> ResolutionNotifier { get; set; }
	}

	public class MailViewModel
	{
		[JsonProperty("mailId")]
		public Guid MailId { get; set; }
		[JsonProperty("documentID")]
		public int DocumentID { get; set; }
		[JsonProperty("registerDate")]
		public DateTime RegisterDate { get; set; }
		[JsonProperty("numberDocument")]
		public string NumberDocument { get; set; }
		[JsonProperty("documentDate")]
		public DateTime DocumentDate { get; set; }
		[JsonProperty("description")]
		public string Description { get; set; }
		[JsonProperty("finishDate")]
		public DateTime FinishDate { get; set; }
		[JsonProperty("uploadGroupFile")]
		public MailUploadGroupViewModel UploadGroupFile { get; set; }
		[JsonProperty("resolution")]
		public MailResolutionViewModel Resolution { get; set; }
		[JsonProperty("isCanEdit")]
		public bool IsCanEdit { get; set; }
	}
	public class MailListItemViewModel
	{
		[JsonProperty("mailId")]
		public Guid MailId { get; set; }
		[JsonProperty("registerDate")]
		public DateTime RegisterDate { get; set; }
		[JsonProperty("custumer")]
		public string Custumer { get; set; }
		[JsonProperty("description")]
		public string Description { get; set; }
		[JsonProperty("hasInnerFiles")]
		public bool HasInnerFiles { get; set; }
		[JsonProperty("isEditeble")]
		public bool IsEditeble { get; set; } = false;
	}
}
