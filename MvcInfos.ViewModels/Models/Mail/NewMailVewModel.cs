﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MvcInfos.Models.ViewModels.Mail
{
	public class NewMailVewModel
	{
		[Required, DisplayName("Вход №№ документа п-п")]
		public int DocumentID { get; set; }
		[Required, DisplayName("Дата регистрации входящего документа")]
		public DateTime RegisterDate { get; set; }
		[Required, DisplayName("От кого поступил")]
		public string Custumer { get; set; }
		[Required, DisplayName("№№ документа")]
		public string NumberDocument { get; set; }
		[Required, DisplayName("Дата документа")]
		public DateTime DocumentDate { get; set; }
		[Required, DisplayName("По какому вопросу")]
		public string Description { get; set; }
		[DisplayName("Комментарий")]
		public string Comment { get; set; }

		[Required, DisplayName("Резолюция руководства")]
		public string[] Resolution { get; set; }
		[Required, DisplayName("Срок исполнения")]
		public DateTime FinishDate { get; set; }
		[Required, DisplayName("Напоминание")]
		public bool IsNotify { get; set; }
		[DisplayName("Напоминание")]
		public string NotifyTime { get; set; }
		public string GroupFiles { get; set; }
		[DisplayName("Номер проекта")]
		public string ProjectCode { get; set; }
		public IEnumerable<SelectListItem> Employees { get; set; }
		public IEnumerable<SelectListItem> NotifyTimes { get; set; }
		public IEnumerable<SelectListItem> Projects { get; set; }
	}
}
