﻿using Newtonsoft.Json;

namespace MvcInfos.Models.ViewModels
{
	public class TaskFile
	{
		[JsonProperty("uuid")]
		public string Uuid { get; set; }
	}
}