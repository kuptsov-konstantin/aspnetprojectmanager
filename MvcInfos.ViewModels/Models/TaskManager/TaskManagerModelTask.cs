﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MvcInfos.Models.ViewModels
{
	public class TaskManagerModelTask
	{
		[JsonProperty("taskTheme")]
		public string TaskTheme { get; set; }
		[JsonProperty("taskDate")]
		public DateTime TaskDate { get; set; }
		[JsonProperty("taskPriority")]
		public int TaskPriority { get; set; }
		[JsonProperty("taskDescription")]
		public string TaskDescription { get; set; }
		[JsonProperty("taskEndEmployee")]
		public string TaskEndEmployee { get; set; }
		[JsonProperty("taskStage")]
		public string TaskStage { get; set; }
		[JsonProperty("taskFiles")]
		public ICollection<TaskFile> TaskFiles { get; set; }
	}
}