﻿using Newtonsoft.Json;

namespace MvcInfos.Models.ViewModels
{
	public class TaskManagerModelEmployee
	{
		[JsonProperty("id")]
		public string Id { get; set; }
		[JsonProperty("fio")]
		public string Fio { get; set; }
	}
}