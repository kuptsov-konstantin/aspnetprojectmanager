﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcInfos.Models.ViewModels.ActiveDirectory
{
	public struct SyncInfoViewModel
	{
		public int InActiveDirectory { get; set; }
		public int InAsp { get; set; }
	}
	public class ActiveDirectorySyncInfoViewModel
	{
		public SyncInfoViewModel FirmCount { get; set; }
		public SyncInfoViewModel DepartmentCount { get; set; }
		public SyncInfoViewModel UsersCount { get; set; }

	}
	public class SyncStatusViewModel
	{
		[JsonProperty("sync")]
		public bool Sync { get; set; }
	}
}
