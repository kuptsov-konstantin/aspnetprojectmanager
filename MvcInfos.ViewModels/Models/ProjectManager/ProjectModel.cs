﻿namespace MvcInfos.Models.ViewModels
{
	public class ProjectModel
    {
        public string ProjectName { get; set; }
        public int ProjectId { get; set; }
    }
}