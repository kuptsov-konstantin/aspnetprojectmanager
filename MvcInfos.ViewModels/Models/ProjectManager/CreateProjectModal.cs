﻿using MvcInfos.Date;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace MvcInfos.Models.ViewModels
{
	public class CreateProjectModal
    {
        public IEnumerable<SelectListItem> Items { get; set; }
        public string IdAUser { get; set; }
        /// <summary>
        /// Название проекта.
        /// </summary>
        [Required, Display(Name = "Название проекта")]
        public string ProjectName { get; set; }

        [Required, Display(Name = "Код по договору")]
        public string ProjectCod { get; set; }


        [Required, Display(Name = "Старт проекта"), DatabaseGenerated(DatabaseGeneratedOption.Computed),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
            CreateDateRange]
        public DateTime ProjectStartDate { get; set; }


        [Required, Display(Name = "Ожидаемое завершение"),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
            CreateDateRange]
        public DateTime PlannedFinishDate { get; set; }


    }
}