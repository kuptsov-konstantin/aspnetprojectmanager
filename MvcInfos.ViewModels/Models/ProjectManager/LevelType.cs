﻿namespace MvcInfos.Models.ViewModels
{
	public class LevelType
    {
        public int Id { get; set; }
        public int Level { get; set; }
        public string Type { get; set; }
    }
}