﻿namespace MvcInfos.Models.ViewModels
{
	public class ProjectUserInfoModel
    {
        public string Id { get; set; }
        public string Fio { get; set; }
        public bool? IsChanged { get; set; }
    }
}