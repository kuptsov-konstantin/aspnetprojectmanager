﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MvcInfos.Models.ViewModels
{
	public class FirmModel
	{
		[JsonProperty("idFirm")]
		public int IdFirm { get; set; }
		[JsonProperty("nameFirm")]
		public string NameFirm { get; set; }
		[JsonProperty("employees")]
		public List<TaskManagerModelEmployee> Employees { get; set; } = new List<TaskManagerModelEmployee>();
	}
}