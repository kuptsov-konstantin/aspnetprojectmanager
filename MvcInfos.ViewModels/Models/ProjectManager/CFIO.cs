﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class CFIO
    {
        [Display(Name = "ФИО")]
        public string FIO { get; set; }

        [Display(Name = "Телефонный номер"), Required(ErrorMessage = "Нет телефона")]
        [DisplayFormat(DataFormatString = "{0:+###(##) ###-##-##}", ApplyFormatInEditMode = true)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "EMail")]
        public string EMail { get; set; }

    }
}