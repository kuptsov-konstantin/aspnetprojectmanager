﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class People
    {
        [Display(Name = "Фамилия"), DataType(DataType.Text)]
        public string Family_name { get; set; }
        [Display(Name = "Имя"), DataType(DataType.Text)]
        public string Name { get; set; }
        [Display(Name = "Отчество"), DataType(DataType.Text)]
        public string Middle_name { get; set; }
    }
}