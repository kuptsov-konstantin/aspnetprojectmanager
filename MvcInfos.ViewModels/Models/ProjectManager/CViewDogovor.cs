﻿using MvcInfos.Date;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcInfos.Models.ViewModels
{
	public class CViewDogovor : _LayoutProject
    {
        public CViewDogovor()
        {
            FIOs = new List<CFIO>();
        }
        public int IdProject { get; set; }

        [Display(Name = "Название контракта/Номер контракта")]
        public string NameContract { get; set; }

        public List<CFIO> FIOs { get; set; }

        [Display(Name = "Договор с фирмой")]
        public string Firm { get; set; }

        [Display(Name = "Заключаемая стоимость в бур")]
        public double Cena { get; set; }


        [Required, Display(Name = "Начало договора"), DatabaseGenerated(DatabaseGeneratedOption.Computed),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
            CreateDateRange]
        public DateTime ProjectStartDate { get; set; }

        [Required, Display(Name = "Завершение договора"),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
            CreateDateRange]
        public DateTime PlannedFinishDate { get; set; }

        [Required, Display(Name = "Дата заключения контракта"),
             DataType(DataType.Date),
             DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
             CreateDateRange]
        public DateTime ДатаЗаключенияКонтракта { get; set; }

    }
}