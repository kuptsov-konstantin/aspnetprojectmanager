﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MvcInfos.Models.ViewModels
{
	public class ModalPeplees
    {
        public string IdAUser { get; set; }
        public IEnumerable<SelectListItem> Items { get; set; }
        public List<Employee1> lp = new List<Employee1>();
    }
}