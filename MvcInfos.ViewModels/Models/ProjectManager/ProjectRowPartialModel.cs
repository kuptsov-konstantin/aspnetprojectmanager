﻿using System;
using System.Collections.Generic;

namespace MvcInfos.Models.ViewModels
{
	public class ProjectRowPartialModel
    {
        public int IdFirm { get; set; }
        public int IdProject { get; set; }
        public string ProjectName { get; set; }
        public DateTime CreatedData { get; set; }
        public List<Tuple<string, string, string>> UserInfo { get; set; }
        public int ProcentComplete { get; set; }
        public string Stage { get; set; }
        public string ProjectCod { get; set; }
    }
}