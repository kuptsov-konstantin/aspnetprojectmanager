﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class Employee1
    {
        [Display(Name = "ФИО"), DataType(DataType.Text)]
        public string FIO { get; set; }
        public string IdAUser { get; set; }
        public bool IsUsing { get; set; }
		public bool IsRukov { get; set; } = false;

	}
}