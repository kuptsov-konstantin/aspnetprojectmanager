﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class ViewProject : _LayoutProject
    {
        public int ProjectId { get; set; }
        [Display(Name = "Код по договору проекта")]
        public string КОД { get; set; }
        public string Название { get; set; }
        [Display(Name = "Ответственное лицо:")]
        public string Otvetstven { get; set; }
        [Display(Name = "Задействованные сотрудники:")]
        public List<string> Employes { get; set; }
        [Display(Name = "Выполняемые договора:")]
        public List<string> Dogovors { get; set; }
    }
}