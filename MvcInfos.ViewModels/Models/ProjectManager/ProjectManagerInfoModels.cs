﻿using System.Collections.Generic;

namespace MvcInfos.Models.ViewModels
{
	public class ProjectManagerInfoModels
    {
        public int ColVoPrj { get; set; }
        public int ColVoPrjDost { get; set; }
        public FirmModel Firm { get; set; }
        public List<ProjectModel> Projects = new List<ProjectModel>();
    }
}