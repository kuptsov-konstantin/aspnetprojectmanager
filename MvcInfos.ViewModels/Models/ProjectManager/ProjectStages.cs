﻿using System;
using System.Collections.Generic;

namespace MvcInfos.Models.ViewModels
{
	public class ProjectStages
    {
        public bool IsChanged { get; set; }
        public bool IsRoot { get; set; }
        public MyGuid Id { get; set; }
        public int LevelId { get; set; }
        public int RealId { get; set; }
        public ICollection<ProjectStages> Children { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IntersectionDate { get; set; }
    }
}