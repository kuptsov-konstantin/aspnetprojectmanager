﻿using System;

namespace MvcInfos.Models.ViewModels
{
	public class ProjectInfoModel
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public DateTime ProjectStartDate { get; set; }
        public DateTime ProjectEndDate { get; set; }
        public ProjectUserInfoModel TeamLead { get; set; }
        public string ProjectCode { get; set; }
    }
}