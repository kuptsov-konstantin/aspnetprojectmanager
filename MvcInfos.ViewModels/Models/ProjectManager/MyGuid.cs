﻿using System;
using Newtonsoft.Json;

namespace MvcInfos.Models.ViewModels
{
	public class MyGuid
    {
		[JsonProperty("id")]
		public Guid Id { get; set; }

		public static explicit operator MyGuid(Guid v)
		{
			var id = new MyGuid()
			{
				Id = v
			};
			return id;
		}

		public static explicit operator Guid(MyGuid v)
		{
			return v.Id;
		}
	}
}