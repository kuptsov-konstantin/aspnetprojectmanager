﻿using MvcInfos.Date;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcInfos.Models.ViewModels
{
	public class CModalAddDogovor
    {
        public CModalAddDogovor()
        {
            PlannedFinishDate = DateTime.Now.AddYears(1);
            ProjectStartDate = DateTime.Now;
            ДатаЗаключенияКонтракта = DateTime.Now;
        }
        public int ProjectId { get; set; }

        [Display(Name = "Название контракта/Номер контракта")]
        public string Name { get; set; }

        [Display(Name = "Фамилия")]
        public string Family_people { get; set; }
        [Display(Name = "Имя")]
        public string Name_people { get; set; }
        [Display(Name = "Отчество")]
        public string Middlename_people { get; set; }
        [Display(Name = "Договор с фирмой")]
        public string Firm { get; set; }

        [Display(Name = "Заключаемая стоимость в бур")]
        public double Cena { get; set; }

        [Display(Name = "Телефонный номер"), Required(ErrorMessage = "Нет телефона")]
        [DisplayFormat(DataFormatString = "{0:+###(##) ###-##-##}", ApplyFormatInEditMode = true)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required, Display(Name = "Начало договора"), DatabaseGenerated(DatabaseGeneratedOption.Computed),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
            CreateDateRange]
        public DateTime ProjectStartDate { get; set; }

        [Required, Display(Name = "Завершение договора"),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
            CreateDateRange]
        public DateTime PlannedFinishDate { get; set; }

        [Required, Display(Name = "Дата заключения контракта"),
             DataType(DataType.Date),
             DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
             CreateDateRange]
        public DateTime ДатаЗаключенияКонтракта { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "EMail")]
        public string EMail { get; set; }
    }
}