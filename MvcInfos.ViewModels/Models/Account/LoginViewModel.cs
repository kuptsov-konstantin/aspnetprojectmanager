﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class LoginViewModel
    {
        [Required]
        [Display(Name = "Пользователь")]
       // [EmailAddress]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня")]
        public bool RememberMe { get; set; }
    }
}
