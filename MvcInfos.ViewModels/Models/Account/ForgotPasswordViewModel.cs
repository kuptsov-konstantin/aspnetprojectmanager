﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Почта")]
        public string Email { get; set; }
    }
}
