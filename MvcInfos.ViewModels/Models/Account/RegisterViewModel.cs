﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class RegisterViewModel
    {
        public RegisterViewModel()
        {

        }
        [Required]
        [Display(Name = "Логин")]
        [StringLength(16, MinimumLength = 6)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }

        //[Display(Name = "Вы регистрируетесь как кто?")]
        //public string selectedItem { get; set; }


        ////public List<System.Web.Mvc.SelectListItem> Items {
        //public System.Web.Mvc.SelectList Items
        //{
        //    get; set;
        //}
    }
}
