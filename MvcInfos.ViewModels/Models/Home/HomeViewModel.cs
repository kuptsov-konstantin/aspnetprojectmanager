﻿namespace MvcInfos.Models.ViewModels.Home
{
	public class LayoutViewModel
    {
		public bool IsAD { get; set; }
        public string FI { get; set; }
        public bool IsFirst { get;  set; }
        public string PhotoURL { get; set; }
    }
}