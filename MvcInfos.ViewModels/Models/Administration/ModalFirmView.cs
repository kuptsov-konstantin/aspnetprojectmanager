﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class ModalFirmView : AdministrationFirmViewModels
    {
        [Display(Name = "Директора фирмы")]
        public string Directors { get; set; }
    }
}