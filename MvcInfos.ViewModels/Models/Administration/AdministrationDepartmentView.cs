﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class AdministrationDepartmentView
    {
        public int idFirm { get; set; }
        [Display(Name = "Название фирмы")]
        public string Name { get; set; }
    }
}