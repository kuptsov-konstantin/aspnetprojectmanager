﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class AdministrationAddDepartment
    {
        public AdministrationAddDepartment()
        {
            this.AllDepartment = new List<string>();
        }
        public int idFirm { get; set; }
        public string NameFirm { get; set; }

        [Required]
        [Display(Name = "Название отдела")]
        public string NameNewDep { get; set; }

        public List<string> AllDepartment { get; set; }

       

    }
}