﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class ModalFirmDelInfo
    {
        [Display(Name = "Наименование организации")]
        public string NameFirm { get; set; }
        public int idFirm { get; set; }
    }
}