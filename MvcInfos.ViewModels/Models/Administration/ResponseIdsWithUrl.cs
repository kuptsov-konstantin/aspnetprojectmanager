﻿using System.Collections.Generic;

namespace MvcInfos.Models.ViewModels
{
	public class ResponseIdsWithUrl
    {
        public List<int> Ids { get; set; } = new List<int>();
        public string NextUrl { get; set; } 
    }    
}