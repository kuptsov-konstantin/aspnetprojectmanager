﻿using MvcInfos.Date;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MvcInfos.Models.ViewModels
{
	public class AdministrationAddEmployee : RegisterViewModel
    {      
        [Required, Display(Name = "Фамилия"), DataType(DataType.Text)]
        public string Family_name { get; set; }

        [Required,Display(Name = "Имя"),DataType(DataType.Text)]
        public string Name { get; set; }

        [Required,Display(Name = "Отчество"),DataType(DataType.Text)]
        public string Middle_name { get; set; }


        public List<SelectListItem> DayList
        {
            get
            {
                var obj = new List<SelectListItem>();
                for (int i = 1; i < 31; i++)
                {
                    obj.Add(new System.Web.Mvc.SelectListItem() { Text = Convert.ToString(i) });
                }
                return obj;
            }
        }
  

        [Required,Display(Name = "Дата рождения"),DataType(DataType.Date), DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [CustomDateRange]
        public DateTime BirthData { get; set; }

        [Required,Display(Name = "Отдел назначения")]
        public string NameDepartment { get; set; }

        public int DepartmentID { get; set; }

        //[Required]
        //public int IdFirm { get; set; }
        //[Display(Name = "Фирма")]
        //public System.Web.Mvc.SelectList AllFirms { get; set; }


    }
}