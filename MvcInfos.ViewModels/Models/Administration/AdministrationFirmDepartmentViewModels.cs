﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class AdministrationFirmDepartmentViewModels
	{
		public AdministrationFirmDepartmentViewModels()
		{

		}
		public AdministrationFirmDepartmentViewModels(Entities.Firm.Department _department, int _idFirm, string _nameFirm, IEnumerable<string> fios)
		{

			this.IdFirm = _idFirm;
			this.Name = _department.Name;
			this.IdDepartment = _department.DepartmentID;
			this.FirmName = _nameFirm;
			FIO = new List<string>(fios);
		}
		[Display(Name = "Список сотрудников")]
		public List<string> FIO { get; set; } = new List<string>();
		public string GetFD { get { return string.Format("{0}-{1}", this.IdFirm, this.IdDepartment); } }
		public int IdFirm { get; set; }
		public int IdDepartment { get; set; }
		public string FirmName { get; set; }
		[Display(Name = "В отделе числится")]
		public int ColVoInDep { get { return FIO.Count; } }
		[Required, Display(Name = "Название отдела")]
		public string Name { get; set; }
	}
}