﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class CFirm
    {
        public int id_firm { get; set; }
        [Display(Name = "Название фирмы")]
        public string Name { get; set; }
        [Display(Name = "Количество сотрудников")]
        public int ColVoSotr { get; set; }
    }
}