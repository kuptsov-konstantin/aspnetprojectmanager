﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class AdministrationFirmViewModels
    {

        [Display(Name = "Количестов сотрудников")]
        public int ColVoSotr { get; set; }
        public int IdFirm { get; set; }

        [Display(Name = "Название фирмы")]
        public string Name { get; set; }
		public bool IsAD { get; set; } = false;
    }
}