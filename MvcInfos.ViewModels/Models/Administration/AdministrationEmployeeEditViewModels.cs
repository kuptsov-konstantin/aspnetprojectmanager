﻿using MvcInfos.Date;
using MvcInfos.Entities;
using MvcInfos.Entities.Firm;
using System;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class AdministrationEmployeeEditViewModels 
    {
        [Required, Display(Name = "Фамилия"), DataType(DataType.Text)]
        public String Family_name { get; set; }
        [Required, Display(Name = "Имя"), DataType(DataType.Text)]
        public String Name { get; set; }
        [Required, Display(Name = "Отчество"), DataType(DataType.Text)]
        public String Middle_name { get; set; }
        [Display(Name = "ФИО")]
        public String Fio
        {
            get
            {
                return String.Format("{0} {1} {2}", Family_name, Name, Middle_name);
            }
        }
        [Required,
            Display(Name = "Дата рождения(дд-мм-гггг)"),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true),
            CustomDateRange]
        //[Column(TypeName = "DateTime2")]   //Range(typeof(DateTime), "1-Jan-1910", "1-Jan-2060"),
        public DateTime BirthData { get; set; }
        /// <summary>
        /// ASPNETuser
        /// </summary>
        public string IdUser { get; set; }
      //  public int idDepurtment { get; set; }
        //public int idFirm { get; set; }

        public AdministrationEmployeeEditViewModels()
        {

        }
        public AdministrationEmployeeEditViewModels(ApplicationUser au)
        {
            this.IdUser = au.Id;
            try
            {
                this.Family_name = au.Employee.Person.FamilyName;

            }
            catch (Exception)
            {
                this.Family_name = "";
            }
            try
            {
                this.Name = au.Employee.Person.Name;
            }
            catch (Exception)
            {
                this.Name = "";
            }
            try
            {
                this.Middle_name = au.Employee.Person.MiddleName;
            }
            catch (Exception)
            {
                this.Middle_name = "";
            }
        }
        public AdministrationEmployeeEditViewModels(Employee au)
        { 
            this.Family_name = au.Person.FamilyName;
            this.Name = au.Person.Name;
            this.Middle_name = au.Person.MiddleName;
            this.IdUser = au.ApplicationUser.Id;
        }
    }
}