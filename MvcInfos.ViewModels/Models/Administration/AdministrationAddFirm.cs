﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class AdministrationAddFirm
    {

        [Required]
        [Display(Name = "Название фирмы")]
        [StringLength(16, MinimumLength = 2)]
        public string NameFirm { get; set; }
    }
}