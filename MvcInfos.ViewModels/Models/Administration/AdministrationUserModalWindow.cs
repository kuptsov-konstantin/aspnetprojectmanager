﻿using MvcInfos.Date;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{

	public class AdministrationUserModalWindow
	{
		private List<FirmInfo> firms = new List<FirmInfo>();
		public class FirmInfo
		{
			private List<string> departmetns = new List<string>();
			private List<string> dolznosts = new List<string>();


			[Display(Name = "Дата приема на работу"),
				DataType(DataType.Date),
				DisplayFormat(NullDisplayText = "2000-02-02", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
				CustomDateRange]
			//[Column(TypeName = "DateTime2")]   //Range(typeof(DateTime), "1-Jan-1910", "1-Jan-2060"),
			public DateTime? EmployeeDate { get; set; }
			public string FirmString { get; set; }
			public string DepartmentString { get; set; }
			public string Dolznost { get; set; }
			public string DolznostString { get; set; }
			public bool IsDirector { get; set; }

		
			[Display(Name = "Отделы")]
			public string DepartmentS { get { return string.Join(", ", Departmetns.ToArray()); } }
			[Display(Name = "Должность")]
			public string DolznostS { get { return string.Join(", ", Dolznosts.ToArray()); } }
			public List<string> Dolznosts { get => dolznosts; set => dolznosts = value; }
			public List<string> Departmetns { get => departmetns; set => departmetns = value; }
		}


		[Required, Display(Name = "Фамилия"), DataType(DataType.Text)]
        public string FamilyName { get; set; }
        [Required, Display(Name = "Имя"), DataType(DataType.Text)]
        public string Name { get; set; }
        [Required, Display(Name = "Отчество"), DataType(DataType.Text)]
        public string MiddleName { get; set; }
        [Display(Name = "ФИО")]
        public string Fio => $"{FamilyName} {Name} {MiddleName}";
     
        [Required,
            Display(Name = "Дата рождения"),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
            CustomDateRange]
        //[Column(TypeName = "DateTime2")]   //Range(typeof(DateTime), "1-Jan-1910", "1-Jan-2060"),
        public DateTime BirthDate { get; set; }

      
        public string Id { get; set; }
  
        public AdministrationUserModalWindow() { }


        [EmailAddress, Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }


        [Display(Name = "Логин")]
        public string UserName { get; set; }
     
        [DataType(DataType.PhoneNumber), Display(Name = "Телефонный номер"), Required(ErrorMessage = "Нет телефона"), DisplayFormat(DataFormatString = "{0:+###(##) ###-##-##}", ApplyFormatInEditMode = true)]
        public string PhoneNumber { get; set; }
		public List<FirmInfo> Firms { get => firms; set => firms = value; }
	}
}