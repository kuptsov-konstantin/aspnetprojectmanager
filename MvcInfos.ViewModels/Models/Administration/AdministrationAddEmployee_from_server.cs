﻿using MvcInfos.Date;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MvcInfos.Models.ViewModels
{
	public class AdministrationAddEmployee_from_server{
        public int idFirm { get; set; }

        [Required, Display(Name = "Фамилия"), DataType(DataType.Text)]
        public string Family_name { get; set; }
        [Required, Display(Name = "Имя"), DataType(DataType.Text)]
        public string Name { get; set; }
        [Required, Display(Name = "Отчество"), DataType(DataType.Text)]
        public string Middle_name { get; set; }
        [Display(Name = "ФИО")]
        public string Fio
        {
            get
            {
                return string.Format("{0} {1} {2}", Family_name, Name, Middle_name);
            }
        }
        [Required,
            Display(Name = "Дата рождения(дд-мм-гггг)"),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
            CustomDateRange]
        //[Column(TypeName = "DateTime2")]   //Range(typeof(DateTime), "1-Jan-1910", "1-Jan-2060"),
        public DateTime BirthData { get; set; }


        [Required]
        [Display(Name = "Логин")]
        [StringLength(16, MinimumLength = 6)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Адрес электронной почты")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Вы регистрируетесь как кто?")]
        public string selectedItem { get; set; }

        [Display(Name = "Фирмы")]
        public string firm_string { get; set; }

        [Display(Name = "Отделы")]
        public string department_string { get; set; }    
        //public List<System.Web.Mvc.SelectListItem> Items {
        public IEnumerable<SelectListItem> Items {get; set;}


        [Display(Name = "Телефонный номер"), Required(ErrorMessage = "Нет телефона")]
        [DisplayFormat(DataFormatString = "{0:+###(##) ###-##-##}", ApplyFormatInEditMode = true)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        public List<string> departmetns = new List<string>();
        public List<string> firms = new List<string>();

        [Required, Display(Name = "Устройство"), DataType(DataType.Date), DisplayFormat(NullDisplayText = "", DataFormatString = "{0:DD-MM-YYYY}", ApplyFormatInEditMode = true)]

        public DateTime EmploymentData { get; set; }

        [Display(Name = "Фирмы")]
        public string FirmsS
        {
            get
            {
                return string.Join(", ", firms.ToArray());
            }
        }
    }
}