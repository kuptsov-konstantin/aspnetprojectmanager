﻿using MvcInfos.Entities;
using System.Collections.Generic;

namespace MvcInfos.Models.ViewModels
{
	public class AdministrationViewModels
    {
        public AdministrationViewModels()
        {
            this.AllUsers = new List<AdministrationEmployeePrintViewModels>();
            this.AdministrationFirmsViewModels = new List<AdministrationFirmViewModels>();
        }
        public AdministrationViewModels(ApplicationUser asd)
        { 

        }
        public ICollection<AdministrationEmployeePrintViewModels> AllUsers { get; set; }
        public ICollection<AdministrationFirmViewModels> AdministrationFirmsViewModels { get; set; }
    }
}