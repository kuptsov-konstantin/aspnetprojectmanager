﻿using System.Collections.Generic;

namespace MvcInfos.Models.ViewModels
{
	public class AdministrationModalEditDepartment
    {
        public ICollection<string> Employees { get; set; }
        public string DepartmentName { get; set; }
        public int idDepartment { get; set; }
    }
}