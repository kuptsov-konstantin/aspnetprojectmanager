﻿namespace MvcInfos.Models.ViewModels
{
	public class DepartmentNode
    {
        public int idFirm { get; set; }
        public int idNode { get; set; }
        public string NameDepartment { get; set; }
        public int ColVoInDep { get; set; }
        public string GetFD { get { return string.Format("{0}-{1}", this.idFirm, this.idNode); } }
        public int? Parent { get; set; }
        //  public DepartmentNode Parent { get; set; }
    }
}