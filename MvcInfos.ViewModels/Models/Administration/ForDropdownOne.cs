﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MvcInfos.Models.ViewModels
{
	public class ForDropdownOne
    {
        public string Name { get; set; }
        public IEnumerable<SelectListItem> Items { get; set; }
        public string idAUser { get; set; }
    }
}