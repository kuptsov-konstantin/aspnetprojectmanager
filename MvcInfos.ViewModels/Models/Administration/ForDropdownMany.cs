﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MvcInfos.Models.ViewModels
{
	public class ForDropdownMany
    {
        public string Name { get; set; }
        public IEnumerable<SelectListItem> Items { get; set; }
        public IEnumerable<string> Directors { get; set; }
    }
}