﻿using System.Collections.Generic;

namespace MvcInfos.Models.ViewModels
{
	public class DepartmentRoot
    {
        public DepartmentNode DepartmentNode { get; set; }
        public ICollection<DepartmentNode> DepartmentNodes { get; set; }
    }
}