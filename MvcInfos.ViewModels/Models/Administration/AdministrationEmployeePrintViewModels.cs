﻿using MvcInfos.Date;
using MvcInfos.Entities;
using MvcInfos.Entities.Firm;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class AdministrationEmployeePrintViewModels
    {
        public AdministrationEmployeePrintViewModels()
        {

        }
        public AdministrationEmployeePrintViewModels(ApplicationUser au)
        {
            this.EMail = au.Email;
            this.UserName = au.UserName;
            this.IdUser = au.Id;
            if(au.Employee!=null)
            {
                if (au.Employee.Person!=null)
                {
                    this.FamilyName = au.Employee.Person.FamilyName;
                    this.Name = au.Employee.Person.Name;
                    this.MiddleName = au.Employee.Person.MiddleName;
                }
                else
                {
                    this.FamilyName = "";
                    this.Name = "";
                    this.MiddleName = "";
                }
            }
            else
            {
                this.FamilyName = "";
                this.Name = "";
                this.MiddleName = "";
            }
        }
        public AdministrationEmployeePrintViewModels(Employee au)
        {
            this.EMail = au.ApplicationUser.Email;
            this.UserName = au.ApplicationUser.UserName;
            if (au.Person != null)
            {
                this.FamilyName = au.Person.FamilyName;
                this.Name = au.Person.Name;
                this.MiddleName = au.Person.MiddleName;
            }
            else
            {
                this.FamilyName = "";
                this.Name = "";
                this.MiddleName = "";
            }

            this.IdUser = au.ApplicationUser.Id;
        }
	
		[Required, Display(Name = "Фамилия"), DataType(DataType.Text), JsonProperty("familyName")]
        public string FamilyName { get; set; }
		[Required, Display(Name = "Имя"), DataType(DataType.Text), JsonProperty("name")]
        public string Name { get; set; }
        [Required, Display(Name = "Отчество"), DataType(DataType.Text), JsonProperty("middleName")]
		public string MiddleName { get; set; }
        [Display(Name = "ФИО"), JsonProperty("fio")]
		public string Fio
        {
            get
            {
                return string.Format("{0} {1} {2}", FamilyName, Name, MiddleName);
            }
        }
        [Required,
            Display(Name = "Дата рождения(дд-мм-гггг)"),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true),
            CustomDateRange,
			JsonProperty("birthData")]
		public DateTime BirthData { get; set; }
		/// <summary>
		/// ASPNETuser
		/// </summary>
		[JsonProperty("idUser")]
		public string IdUser { get; set; }

        [Required]
        [Display(Name = "Логин")]
		[JsonProperty("userName")]
		public string UserName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Электронная почта")]
		[JsonProperty("eMail")]
		public string EMail { get; set; }
		[JsonProperty("isADUser")]
		public bool IsADUser { get; set; }
		[JsonProperty("isNotDelete")]
		public bool IsNotDelete { get; set; }
	}
}