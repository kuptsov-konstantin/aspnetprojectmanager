﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MvcInfos.Models.ViewModels
{
	public class ModalFirmEdit
    {
        [Display(Name = "Наименование организации")]
        public string NameFirm { get; set; }
        public int idFirm { get; set; }
        public IEnumerable<SelectListItem> Items { get; set; }
        public IEnumerable<string>  Directors { get; set; }
    }
}