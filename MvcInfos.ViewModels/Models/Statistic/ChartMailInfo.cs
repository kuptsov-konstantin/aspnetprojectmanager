﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace MvcInfos.Models.ViewModels.Statistic
{
	public class StatisticMailsViewModel
	{
		public Guid MailId { get; set; }
		public Entities.Mails.MailStatus MailStatus { get; set; }
	}

	public class DateMailInfoViewModel
	{
		[JsonProperty("start")]
		public DateTime Start { get; set; }
		[JsonProperty("end")]
		public DateTime End { get; set; }
		[JsonProperty("firmId")]
		public int? FirmId { get; set; }
	}

	public class PlotNotificationClicksInRangeViewModel: DateMailInfoViewModel
	{
		[JsonProperty("legend")]
		public Dictionary<string, string> Legend { get; set; }
		[JsonProperty("plotDictionary")]
		public Dictionary<string,PlotNotificationClicksInDayViewModel> PlotDictionary { get; set; }
	}

	public class PlotNotificationClicksInDayViewModel
	{
		[JsonProperty("date")]
		public DateTime Date { get; set; }
		[JsonProperty("counts")]
		public Dictionary<string, int> Сounts { get; set; }
	}

	public class ChartMailInfoViewModel
	{
		[JsonProperty("label")]
		public string Name { get; set; }
		[JsonProperty("percent")]
		public int Percent { get; set; }
	}
	public class StatisticsViewModel
	{
		public int? AllUsers { get; set; }
		public int? AllFirms { get; set; }

		public int? AllMails { get; set; }
		public int? CurrentMails { get; set; }
		public int? ClosedMails { get; set; }
		public int? Activity { get; set; }
		public int? AllNotifications { get; set; }
		public int? CurrentResolutions { get; set; }
		public string Firm { get; set; }
		public IEnumerable<SelectListItem> Firms { get; set; }
	}
}
