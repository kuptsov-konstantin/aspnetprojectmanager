﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity;

namespace MvcInfos.Models.ViewModels
{
	public class IndexViewModel
    {
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
        public bool HasUserName { get; set; }
        public string UserName { get; set; }
        public string FIO { get; set; }
        public string Firm { get; set; }
    }
}