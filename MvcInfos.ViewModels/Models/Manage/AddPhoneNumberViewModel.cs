﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Models.ViewModels
{
	public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Номер телефона")]
        public string Number { get; set; }
    }
}