﻿using System;
using Newtonsoft.Json;

namespace MvcInfos.Models.ViewModels.Upload
{
	public class UploadcareFileViewModel
	{
		[JsonProperty("is_stored")]
		public bool IsStored { get; set; }
		[JsonProperty("done")]
		public long Done { get; set; }
		[JsonProperty("file_id")]
		public Guid FileId { get; set; }
		[JsonProperty("total")]
		public long Total { get; set; }
		[JsonProperty("size")]
		public long Size { get; set; }
		[JsonProperty("uuid")]
		public Guid Uuid { get; set; }
		[JsonProperty("is_image")]
		public bool IsImage { get; set; }
		[JsonProperty("filename")]
		public string Filename { get; set; }
		[JsonProperty("is_ready")]
		public bool IsReady { get; set; }
		[JsonProperty("original_filename")]
		public string OriginalFilename { get; set; }
		[JsonProperty("mime_type")]
		public string MimeType { get; set; }
		[JsonProperty("image_info")]
		public UploadcareImageViewModel ImageInfo { get; set; }

	}
}
