﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MvcInfos.Models.ViewModels.Upload
{
	public class UploadcareGroupFileViewModel
	{
		public UploadcareGroupFileViewModel()
		{
			Files = new List<UploadcareFileViewModel>();
		}

		[JsonProperty("id")]
		public Guid Id { get; set; }
		[JsonProperty("datetime_created")]
		public DateTime DatetimeCreated { get; set; }
		[JsonProperty("datetime_stored")]
		public DateTime DatetimeStored { get; set; }
		[JsonProperty("files_count")]
		public int FilesCount { get; set; }
		[JsonProperty("cdn_url")]
		public string CdnUrl { get; set; }
		[JsonProperty("url")]
		public string Url { get; set; }
		[JsonProperty("files")]
		public ICollection<UploadcareFileViewModel> Files { get; set; }
	}
}
