﻿using System;
using Newtonsoft.Json;

namespace MvcInfos.Models.ViewModels.Upload
{
	public class UploadcareImageViewModel
	{
		[JsonProperty("format")]
		public string Format { get; set; }
		[JsonProperty("height")]
		public double Height { get; set; }
		[JsonProperty("width")]
		public double Width { get; set; }
		[JsonProperty("datetime_original")]
		public DateTime DatetimeOriginal { get; set; }

	}
}
