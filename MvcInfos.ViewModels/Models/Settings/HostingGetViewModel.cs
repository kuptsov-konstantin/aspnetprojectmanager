﻿using Newtonsoft.Json;

namespace MvcInfos.Models.ViewModels
{
	public class HostingGetViewModel
	{
		[JsonProperty("hostingName")]
		public string HostingName { get; set; }
		[JsonProperty("hostingId")]
		public MyGuid HostingId { get; set; }
		[JsonProperty("pluginConnectionType")]
		public MyGuid PluginConnectionType { get; set; }
		[JsonProperty("pluginInfo")]
		public string PluginInfo { get; set; }
		[JsonProperty("isNotDelete")]
		public bool IsNotDelete { get; set; }
		[JsonProperty("isDefault")]
		public bool IsDefault { get; set; }
	}
}