﻿using Newtonsoft.Json;

namespace MvcInfos.Models.ViewModels
{
	public class HostingViewModel: HostingGetViewModel
	{
		[JsonProperty("hostingUploadUrl")]
		public string HostingUploadUrl { get; set; }
		[JsonProperty("token")]
		public string Token { get; set; }
		[JsonProperty("login")]
		public string Login { get; set; }
		[JsonProperty("password")]
		public string Password { get; set; }
	}
}