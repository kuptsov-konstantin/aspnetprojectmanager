﻿using Newtonsoft.Json;

namespace MvcInfos.Models.ViewModels
{
	public class ActiveDirectorySyncSettingsViewModel
	{
		[JsonProperty("isUsingSync")]
		public bool IsUsingSync { get; set; }
	}
}