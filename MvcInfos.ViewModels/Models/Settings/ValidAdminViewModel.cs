﻿using Newtonsoft.Json;

namespace MvcInfos.Models.ViewModels
{
	public class ValidAdminViewModel
	{
		[JsonProperty("login")]
		public string Login { get; set; }
		[JsonProperty("password")]
		public string Password { get; set; }
	}
}