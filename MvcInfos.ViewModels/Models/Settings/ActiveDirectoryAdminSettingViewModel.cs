﻿using Newtonsoft.Json;

namespace MvcInfos.Models.ViewModels
{
	public class ActiveDirectoryAdminSettingViewModel
	{
		[JsonProperty("isUsingActiveDirectory")]
		public bool IsUsingActiveDirectory { get; set; }
		[JsonProperty("isNotUsingCurrentUser")]
		public bool IsNotUsingCurrentUser { get; set; }
		[JsonProperty("password")]
		public string Password { get; set; }
		[JsonProperty("userName")]
		public string UserName { get; set; }

	}
}