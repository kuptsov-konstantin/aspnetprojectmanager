﻿using MvcInfos.Plugin.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace MvcInfos.Plugin.LocalStorage
{
	public class LocalStorage : IPlugin
	{
		public Guid PluginID => Guid.Parse(Properties.Resources.Id);
		public string PluginName => Properties.Resources.Name;
		public Bitmap Icon => Properties.Resources.FolderGreyIcon;
		public string Form => Encoding.UTF8.GetString(Properties.Resources.form).TrimEnd('\0');

		public string GetSettingJSON(Dictionary<string, string> settings)
		{
			return Newtonsoft.Json.JsonConvert.SerializeObject(settings);
		}

		public Action SendFile(string fileName, string settings)
		{
			if (settings == null)
			{
				return () =>
				{
					File.Move(fileName, Properties.Resources.Folder + fileName);
				};
			}
			else
			{
				var dictionarySettings = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(settings);
				return () =>
				{
					File.Move(fileName, dictionarySettings["Folder"] + fileName);
				};
			}
		}
		public string ToString(string settings)
		{
			if (settings == null)
			{
				return Properties.Resources.Folder;
			}
			else
			{
				var dictionarySettings = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(settings);
				return dictionarySettings["Folder"];
			}
		}
	}
}
