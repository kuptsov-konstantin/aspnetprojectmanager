﻿using System;
using System.Runtime.Serialization;

namespace MvcInfos.Notification
{
	[Serializable]
	internal class NotCreatedMethodException : Exception
	{
		public NotCreatedMethodException()
		{
		}

		public NotCreatedMethodException(string message) : base(message)
		{
		}

		public NotCreatedMethodException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected NotCreatedMethodException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}