﻿using System;

namespace MvcInfos.Notification
{
	public class UserInfo
	{
		public string Id { get; set; }
		public string UserName { get; set; }
		public Guid GetIdGuid()
		{
			return Guid.Parse(Id);
		}
	}
}
