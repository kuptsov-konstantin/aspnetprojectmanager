﻿using MvcInfos.Notification.Notificator;
using MvcInfos.Settings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Management;
using System.Net;
using System.Threading.Tasks;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using MvcInfos.Notification.PCs;
using MvcInfos.Network.Extensions;
using System.Linq;
using MvcInfos.Notification.Models;

namespace MvcInfos.Notification
{
	public class NotificationService : IDisposable
	{
		private ActiveDirectorySetting ActiveDirectorySetting { get; set; }
		private TcpClient TcpClientService { get; set; } = null;
		private List<UserInfo> Users { get; set; } = new List<UserInfo>();

		public NotificationService(ActiveDirectorySetting activeDirectorySetting)
		{
			ActiveDirectorySetting = activeDirectorySetting;
			var taskUpdate = Task.Factory.StartNew(async () =>
			{
				await InitClientServiceAsync();
				await UpdatePCsInfoAsync(Users);
			});
			Task.WaitAll(taskUpdate);
		}

		public async Task InitClientServiceAsync()
		{
			try
			{

				TcpClientService = new TcpClient(AddressFamily.InterNetwork);
				await TcpClientService.ConnectAsync(IPAddress.Any, 10010);
			}
			catch (Exception e)
			{
				Trace.TraceError(e.Message);
			}
		}

		public async Task<NotifiPCs> UpdatePCsInfoAsync(List<UserInfo> users)
		{
			return await Task.Factory.StartNew(() =>
			{
				NotifiPCs userspc = new NotifiPCs();
				Users = users;
				userspc.UsersPC.Clear();
				var context = ActiveDirectorySetting.GetPrincipalContext();
				using (var searcher = new PrincipalSearcher(new ComputerPrincipal(context)))
				{
					foreach (var result in searcher.FindAll())
					{
						if (result is AuthenticablePrincipal auth)
						{
							var username = GetLastUserLoggedOn(auth.Name);
							if (username != null)
							{
								string login = username;
								if (username.Contains("\\"))
								{
									login = username.Split('\\')[1];
								}
								var User = users.Where(user => user.UserName.Equals(login)).FirstOrDefault();
								if (User == null)
								{
									continue;
								}
								if (userspc.UsersPC.ContainsKey(User.GetIdGuid()))
								{
									userspc.UsersPC[User.GetIdGuid()].Add(new PCInfo(auth.Name, Dns.GetHostAddresses(auth.Name)));
								}
								else
								{
									userspc.UsersPC.Add(User.GetIdGuid(), new List<PCInfo>() {
									new PCInfo(auth.Name, Dns.GetHostAddresses(auth.Name))
								});
								}
							}
						}
					}
					userspc.UpdateDate(DateTime.Now);
				}
				return userspc;
			});
		}

		static List<string> GetARP()
		{
			List<string> _ret = new List<string>();

			Process netUtility = new Process();
			netUtility.StartInfo.FileName = "arp.exe";
			netUtility.StartInfo.CreateNoWindow = true;
			netUtility.StartInfo.Arguments = "-a";
			netUtility.StartInfo.RedirectStandardOutput = true;
			netUtility.StartInfo.UseShellExecute = false;
			netUtility.StartInfo.RedirectStandardError = true;
			netUtility.Start();

			StreamReader streamReader = new StreamReader(netUtility.StandardOutput.BaseStream, netUtility.StandardOutput.CurrentEncoding);

			string line = "";
			while ((line = streamReader.ReadLine()) != null)
			{

				if (line.StartsWith("  "))
				{
					var Itms = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

					if (Itms.Length == 3)
						_ret.Add(Dns.GetHostEntry(Itms[0]).HostName);
				}
			}

			streamReader.Close();

			return _ret;

		}


		private string GetLastUserLoggedOn(string machineName)
		{
			Ping Pinger = new Ping();
			PingReply Reply = Pinger.Send(machineName);
			if (Reply.Status == IPStatus.Success)
			{
				try
				{
					ConnectionOptions options = null;
					if (ActiveDirectorySetting.IsNotUsingCurrentUser == true)
					{
						options = new ConnectionOptions()
						{
							Impersonation = ImpersonationLevel.Impersonate,
							Username = ActiveDirectorySetting.UserName,
							SecurePassword = ActiveDirectorySetting.GetSecureString()
						};
					}
					else
					{
						options = new ConnectionOptions()
						{
							Impersonation = ImpersonationLevel.Impersonate,

						};
					}

					ManagementScope scope = new ManagementScope($@"\\{machineName}\root\cimv2", options);
					scope.Connect();
					//Query system for Operating System information
					ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_ComputerSystem");
					ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);
					ManagementObjectCollection queryCollection = searcher.Get();
					if (queryCollection.Count > 0)
					{
						foreach (var pc in queryCollection)
						{
							if (pc.Properties["UserName"].Value is string userName)
							{
								return userName;
							}
						}
					}
				}
				catch (Exception e)
				{
					Trace.TraceError(e.Message);
				}
			}
			else
			{
				Trace.TraceError($"{machineName} ping error");
			}
			return null;

		}

		public async Task AddNotificationAsync(Guid resolutionNotifierId)
		{

			var stream = TcpClientService.GetStream();
			 await stream.SendObjectAsync("Add");
			var response = new Models.TcpNotification() { NotificationId = resolutionNotifierId, Method = "Add" };
			var list = new List<Models.TcpNotification> { { response } };
			await stream.SenderAsync(list.MySerializeArray());
			//throw new NotCreatedMethodException();
		}
		public async Task AddNotificationAsync(Guid[] resolutionNotifierIds)
		{
			
			var list = new List<Models.TcpNotification> ();
			foreach (var resolutionNotifierId in resolutionNotifierIds)
			{
				var response = new Models.TcpNotification() { NotificationId = resolutionNotifierId, Method = "Add" };
				list.Add(response);
			}
			var stream = TcpClientService.GetStream();
			await stream.SendObjectAsync("Add");
			await stream.SenderAsync(list.MySerializeArray());
			//throw new NotCreatedMethodException();
		}

		void OpenDemoWindow()
		{
			//invoke-command -ComputerName Computer1 -ScriptBlock { & 'C:\Program Files\program.exe' -something "myArgValue" }

			MainWindow mainWindow = new MainWindow();
		}

		public List<string> GetComputers()
		{
			List<string> ComputerNames = new List<string>();
			DirectorySearcher mySearcher = ActiveDirectorySetting.GetDirectorySearcher();
			mySearcher.Filter = ("(objectClass=computer)");
			mySearcher.SizeLimit = int.MaxValue;
			mySearcher.PageSize = int.MaxValue;

			foreach (SearchResult resEnt in mySearcher.FindAll())
			{
				string ComputerName = resEnt.GetDirectoryEntry().Name;
				if (ComputerName.StartsWith("CN="))
				{
					ComputerName = ComputerName.Remove(0, "CN=".Length);
				}
				ComputerNames.Add(ComputerName);
			}
			return ComputerNames;
		}

		public void Dispose()
		{
			TcpClientService?.Close();
		}
	}
}
