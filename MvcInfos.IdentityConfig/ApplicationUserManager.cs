﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using MvcInfos.Entities;
using System.DirectoryServices.AccountManagement;
using MvcInfos.Settings;
using MvcInfos.Repository.IdentityModels;

namespace MvcInfos.IdentityConfig
{
	/// <summary>
	///  Настройка диспетчера пользователей приложения. UserManager определяется в ASP.NET Identity и используется приложением.
	/// </summary>
	public class ApplicationUserManager : UserManager<ApplicationUser>
	{
		public ApplicationUserManager(IUserStore<ApplicationUser> store) : base(store) { }
#if (DEBUG || RELEASE)
		private readonly ActiveDirectorySetting _adSettings;
		public ApplicationUserManager(IUserStore<ApplicationUser> store, ActiveDirectorySetting adSettings) : base(store)
		{
			_adSettings = adSettings;
		}
#endif
		public override async Task<bool> CheckPasswordAsync(ApplicationUser user, string password)
		{
#if (DEBUG || RELEASE)
			if (user.IsActiveDirectory)
			{
				return await Task.FromResult(_adSettings.ValidateCredentials(user.UserName, password, ContextOptions.Negotiate));
			}
#endif
			return await base.CheckPasswordAsync(user, password);
		}

		public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
		{
#if (DEBUG || RELEASE)
			var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()), context.Get<ActiveDirectorySetting>());

#else
			var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));

#endif
			// Настройка логики проверки имен пользователей
			manager.UserValidator = new UserValidator<ApplicationUser>(manager)
			{
				AllowOnlyAlphanumericUserNames = false,
				RequireUniqueEmail = true
			};

			// Настройка логики проверки паролей
			manager.PasswordValidator = new PasswordValidator
			{
				RequiredLength = 6,
				RequireNonLetterOrDigit = true,
				RequireDigit = true,
				RequireLowercase = true,
				RequireUppercase = true,
			};

			// Настройка параметров блокировки по умолчанию
			manager.UserLockoutEnabledByDefault = true;
			manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
			manager.MaxFailedAccessAttemptsBeforeLockout = 5;

			// Регистрация поставщиков двухфакторной проверки подлинности. Для получения кода проверки пользователя в данном приложении используется телефон и сообщения электронной почты
			// Здесь можно указать собственный поставщик и подключить его.
			manager.RegisterTwoFactorProvider("Код, полученный по телефону", new PhoneNumberTokenProvider<ApplicationUser>
			{
				MessageFormat = "Ваш код безопасности: {0}"
			});
			manager.RegisterTwoFactorProvider("Код из сообщения", new EmailTokenProvider<ApplicationUser>
			{
				Subject = "Код безопасности",
				BodyFormat = "Ваш код безопасности: {0}"
			});
			manager.EmailService = new EmailService();
			manager.SmsService = new SmsService();

			var dataProtectionProvider = options.DataProtectionProvider;
			if (dataProtectionProvider != null)
			{
				manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
			}
			return manager;
		}
	}
}
