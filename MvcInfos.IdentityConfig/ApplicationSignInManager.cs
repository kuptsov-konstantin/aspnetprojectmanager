﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using MvcInfos.Repository;
using MvcInfos.Entities;
using MvcInfos.Settings;
using MvcInfos.Entities.Firm;

namespace MvcInfos.IdentityConfig
{

	// Настройка диспетчера входа для приложения.
	public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
	{
		private readonly ActiveDirectorySetting _adSettings;
		public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager, ActiveDirectorySetting adSettings) : base(userManager, authenticationManager)
		{
			_adSettings = adSettings;
		}

		/// <summary>
		/// Аутентификация по емэйлу. 
		/// </summary>
		/// <param name="EMail"></param>
		/// <param name="password"></param>
		/// <param name="isPersistent"></param>
		/// <param name="shouldLockout"></param>
		/// <returns></returns>
		public async Task<SignInStatus> PasswordSignEmailInAsync(string EMail, string password, bool isPersistent, bool shouldLockout)
		{
			var user = await this.UserManager.FindByEmailAsync(EMail);
			if (user == null)
			{
				user = await this.UserManager.FindByNameAsync(EMail);
			}
			if (user == null)
			{
				var adUser = await _adSettings.FindByEmailAsync(EMail);
				if (adUser == null)
				{
					return SignInStatus.Failure;
				}
				user = new ApplicationUser { Employee = new Employee(), UserName = adUser.SamAccountName, Email = adUser.EmailAddress, IsActiveDirectory = true, Id = adUser.Guid.ToString() };
				var result = await UserManager.CreateAsync(user);
				if (result.Succeeded)
				{
					return await base.PasswordSignInAsync(user.UserName, password, isPersistent, shouldLockout);
				}
				else
				{
					return SignInStatus.Failure;
				}

			}
			if (await UserManager.IsLockedOutAsync(user.Id)) return SignInStatus.LockedOut;
			return await base.PasswordSignInAsync(user.UserName, password, isPersistent, shouldLockout);
		}

		/*public override Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        {
            return base.PasswordSignInAsync(userName, password, isPersistent, shouldLockout);
        }*/
		public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
		{
			return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
		}

		public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
		{
			return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication, context.Get<ActiveDirectorySetting>());
		}


	}
}
