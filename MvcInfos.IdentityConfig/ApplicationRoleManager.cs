﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using MvcInfos.Repository.IdentityModels;

namespace MvcInfos.IdentityConfig
{
	public class ApplicationRoleManager : RoleManager<IdentityRole>
	{
		public ApplicationRoleManager(IRoleStore<IdentityRole, string> store) : base(store)
		{
		}
		public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
		{
			var roleStore = new RoleStore<IdentityRole>(context.Get<ApplicationDbContext>());
			return new ApplicationRoleManager(roleStore);
		}
	}
}
