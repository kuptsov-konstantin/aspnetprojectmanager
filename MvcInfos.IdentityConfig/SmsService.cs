﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace MvcInfos.IdentityConfig
{
	public class SmsService : IIdentityMessageService
	{
		public Task SendAsync(IdentityMessage message)
		{
			// Подключите здесь службу SMS, чтобы отправить текстовое сообщение.
			return Task.FromResult(0);
		}
	}
}
