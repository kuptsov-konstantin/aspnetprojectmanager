﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using MvcInfos.HKey;
using MvcInfos.Settings;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace MvcInfos.ActiveDirectory
{
	public struct ActiveDirectoryFilterEvent
	{
		public SearchScope SearchScope { get; set; }
		public string Filter { get; set; }
	}
	public partial class ActiveDirectorySyncService : IDisposable
	{
		private LdapConnection connect = null;
		private ChangeNotifier notifier = null;
		private Thread mainThread = null;
		private ActiveDirectoryFilterEvent[] _filter;
		private HKCurrentUser HKCurrentUser { get; }
		private ActiveDirectorySetting Settings { get; }
		public ActiveDirectoryFilterEvent[] Filter { get => _filter;  }
		public bool IsRun { get; set; } = false;
		public ActiveDirectorySyncService(ActiveDirectorySetting settings)
		{
			Settings = settings;
			HKCurrentUser = new HKCurrentUser();
			Update();
		}

		public void Sync(params ActiveDirectoryFilterEvent[] filters)
		{

			_filter = filters;

			this.mainThread = new Thread(() => {
				connect = new LdapConnection(Environment.UserDomainName);
				notifier = new ChangeNotifier(connect, Settings.GetContextBasePartition());

				//register some objects for notifications (limit 5)
				foreach (var filter in filters)
				{
					notifier.Register(filter.Filter, filter.SearchScope);
				}
				IsRun = true;
				notifier.ObjectChanged += Notifier_ObjectChanged;
			});

			mainThread.Start();



		}
		private void Notifier_ObjectChanged(object sender, ObjectChangedEventArgs e)
		{

			Console.WriteLine(e.Result.DistinguishedName);
			foreach (string attrib in e.Result.Attributes.AttributeNames)
			{
				foreach (var item in e.Result.Attributes[attrib].GetValues(typeof(string)))
				{
					Console.WriteLine("\t{0}: {1}", attrib, item);
				}
			}

		}

		public void Save()
		{
			HKCurrentUser.SetValue(HKEnvironment.NotificationActiveDirectory, this.ToString());
			Update();
		}
		private void Update()
		{
			var storedValue = HKCurrentUser.GetValue(HKEnvironment.NotificationActiveDirectory);
			if (storedValue != null)
			{
				_filter = JsonConvert.DeserializeObject<ActiveDirectoryFilterEvent[]>(storedValue);
				Sync(Filter);
			}
		}

		public void Stop()
		{
			mainThread?.Abort();
			if (notifier != null)
			{
				notifier.ObjectChanged -= Notifier_ObjectChanged;
				notifier.Dispose();
			}
			if (connect != null)
			{
				connect.Dispose();
			}
		}
		public override string ToString()
		{
			return JsonConvert.SerializeObject(Filter);
		}

		public void Dispose()
		{
			mainThread?.Abort();
			if (notifier != null)
			{
				notifier.ObjectChanged -= Notifier_ObjectChanged;
				notifier.Dispose();
			}
			if (connect != null)
			{
				connect.Dispose();
			}
		}

		public static ActiveDirectorySyncService Create(IdentityFactoryOptions<ActiveDirectorySyncService> options, IOwinContext context)
		{
			var ActiveDirectorySetting = context.Get<ActiveDirectorySetting>();

			return new ActiveDirectorySyncService(ActiveDirectorySetting);
		}
	}
}