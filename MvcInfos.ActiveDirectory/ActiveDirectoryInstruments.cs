﻿using MvcInfos.Settings;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace MvcInfos.ActiveDirectory
{
	public class ActiveDirectoryInstruments
	{
		public void CreateGroup(ActiveDirectorySetting activeDirectorySetting, string[] names, string[] descriptions)
		{
			for (int i = 0; i < names.Length; i++)
			{
				try
				{
					CreateGroup(activeDirectorySetting, names[i], descriptions[i]);
				}
				catch (GroupAlreadyExistsException)
				{

				}
			}
		}
		public void CreateGroup(ActiveDirectorySetting activeDirectorySetting, string name, string description)
		{
			PrincipalContext gpc = activeDirectorySetting.GetPrincipalContext();
			GroupPrincipal gp = new GroupPrincipal(gpc, name)
			{
				Description = description,
				IsSecurityGroup = true,
				GroupScope = GroupScope.Global
			};
			gp.Save(gpc);


			/*GroupPrincipal
			var entry = activeDirectorySetting.GetDirectoryEntry();
			var groupLDAP = $"{entry.Path.Substring(0, 7)}CN={name},{entry.Path.Substring(7)}";
			if (!DirectoryEntry.Exists(groupLDAP))
			{
					// create group entry
					DirectoryEntry group = entry.Children.Add("CN=" + name, "group");

					// set properties
					group.Properties["sAmAccountName"].Value = name;

					// save group
					group.CommitChanges();
		
			}
			else {
				throw new GroupAlreadyExistsException(name);
			}*/
		}
	}
}
