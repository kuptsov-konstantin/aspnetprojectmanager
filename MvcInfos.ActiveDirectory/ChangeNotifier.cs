﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.Protocols;

namespace MvcInfos.ActiveDirectory
{

	public class ChangeNotifier : IDisposable
	{
		private LdapConnection _connection;
		private HashSet<IAsyncResult> _results;
		private string _context;

		public ChangeNotifier(LdapConnection connection, string context)
		{
			_results = new HashSet<IAsyncResult>();
			_connection = connection;
			_connection.AutoBind = true;
			_context = context;
		}

		public void Register(string dn, SearchScope scope)
		{
			SearchRequest request = new SearchRequest(dn + ";"+_context, "(objectClass=*)", scope, null);
			request.Controls.Add(new DirectoryNotificationControl());
			_results.Add(_connection.BeginSendRequest(request, TimeSpan.FromDays(1), PartialResultProcessing.ReturnPartialResultsAndNotifyCallback, this.Notify, request));
		}

		private void Notify(IAsyncResult result)
		{
			PartialResultsCollection prc = _connection.GetPartialResults(result);
			foreach (SearchResultEntry entry in prc)
			{
				OnObjectChanged(new ObjectChangedEventArgs(entry));
			}
		}

		private void OnObjectChanged(ObjectChangedEventArgs args)
		{
			ObjectChanged?.Invoke(this, args);
		}

		public event EventHandler<ObjectChangedEventArgs> ObjectChanged;

		#region IDisposable Members

		public void Dispose()
		{
			foreach (var result in _results)
			{
				//end each async search
				_connection.Abort(result);

			}
		}

		#endregion
	}


	public class ObjectChangedEventArgs : EventArgs
	{
		public ObjectChangedEventArgs(SearchResultEntry entry)
		{
			Result = entry;
		}

		public SearchResultEntry Result { get; set; }
	}
}
