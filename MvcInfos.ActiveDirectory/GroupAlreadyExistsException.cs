﻿using System;
using System.Runtime.Serialization;

namespace MvcInfos.ActiveDirectory
{
	[Serializable]
	internal class GroupAlreadyExistsException : Exception
	{
		public GroupAlreadyExistsException()
		{
		}

		public GroupAlreadyExistsException(string message) : base(message)
		{
		}

		public GroupAlreadyExistsException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected GroupAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}