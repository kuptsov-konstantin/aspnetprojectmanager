﻿using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using System.Threading.Tasks;

namespace MvcInfos.Crypto.Certificate
{

	public class CerificaateManager
	{
		public static Certificate Create()
		{
			return new Certificate();
		}
	}


	public class Certificate : IDisposable
	{
		public string CurrentTempFile = null;
#if (DEBUG || RELEASE)
		public static string HKLMStore = @"SOFTWARE\SManager\Certificates";
#else
			
#endif
		private X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
		public Certificate()
		{

		}
		public async Task UpdateCertificateAsync()
		{
			await Task.Factory.StartNew(() =>
			{
				using (var certificate = GenerateCertificate())
				{
					RegisterCertificate(certificate);
				}
			});
		}

		public static Certificate GenerateCertificate()
		{
			var Certificate = new Certificate();
			Certificate.GenerateNewCertificate();
			return Certificate;
		}
		public SslContext CreateCertificate(string filename)
		{
			var sslCert = new SslContext();
			byte[] certData = sslCert.UserCertificate.Export(X509ContentType.Pfx, Properties.Settings.Default.SecretKey);
			File.WriteAllBytes(filename, certData);
			return sslCert;
		}

		public void Dispose()
		{
			if(CurrentTempFile != null)
			{
				File.Delete(CurrentTempFile);
			}
		}

		public string GenerateNewCertificate()
		{
			//string certificate = $"MvcInfos {DateTime.Now.ToString("dd-MM-yyyy-HH-mm-s")}.cer.pfx";
			CurrentTempFile = Path.GetTempFileName();
			var ssl = CreateCertificate(CurrentTempFile);
			return CurrentTempFile;
		}
		public X509Certificate2Collection Open(string fileName)
		{
			X509Certificate2Collection collection = new X509Certificate2Collection();
			collection.Import(fileName, Properties.Settings.Default.SecretKey, X509KeyStorageFlags.PersistKeySet);
			return collection;
		}

		public static void RegisterCertificate(Certificate cert, string fileName = null)
		{
			if (fileName == null)
			{
				fileName = cert?.CurrentTempFile;
			}
			if (fileName == null)
			{
				throw new ArgumentNullException(nameof(fileName) + " не содержит значения");
			}

#if (DEBUG || RELEASE)
			Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(HKLMStore, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree, Microsoft.Win32.RegistryOptions.None);
			var oldKey = key.GetValue("NewCertificate");
#else
			object oldKey = null;
			try
			{
				oldKey = Properties.Settings.Default["NewCertificate"];
			}
			catch (Exception e)
			{
				oldKey = null;
			}
			Properties.Settings.Default.Reload();
#endif

			X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
			store.Open(OpenFlags.ReadWrite);
			X509Certificate2Collection collection = new X509Certificate2Collection();
			collection.Import(fileName, Properties.Settings.Default.SecretKey, X509KeyStorageFlags.PersistKeySet);
			foreach (var item in collection)
			{
				store.Add(item);

#if (DEBUG || RELEASE)

				if (oldKey != null)
				{
					key.SetValue("OldCertificate", oldKey);
				}
				key.SetValue("NewCertificate", item.Thumbprint);
				key.Close();
#else

				if (oldKey != null)
				{
					if (Properties.Settings.Default.Properties["OldCertificate"] == null)
					{
						System.Configuration.SettingsProperty property = new System.Configuration.SettingsProperty("OldCertificate")
						{
							DefaultValue = oldKey,
							IsReadOnly = false,
							PropertyType = typeof(string),
							Provider = Properties.Settings.Default.Providers["LocalFileSettingsProvider"]
						};
						property.Attributes.Add(typeof(System.Configuration.UserScopedSettingAttribute), new System.Configuration.UserScopedSettingAttribute());
						Properties.Settings.Default.Properties.Add(property);

					}
					else
					{
						Properties.Settings.Default["OldCertificate"] = oldKey;
					}
				}
				if (Properties.Settings.Default.Properties["NewCertificate"] == null)
				{
					System.Configuration.SettingsProperty property = new System.Configuration.SettingsProperty("NewCertificate")
					{
						DefaultValue = item.Thumbprint,
						IsReadOnly = false,
						PropertyType = typeof(string),
						Provider = Properties.Settings.Default.Providers["LocalFileSettingsProvider"]
					};
					property.Attributes.Add(typeof(System.Configuration.UserScopedSettingAttribute), new System.Configuration.UserScopedSettingAttribute());
					Properties.Settings.Default.Properties.Add(property);

				}
				else
				{
					Properties.Settings.Default["NewCertificate"] = item.Thumbprint;
				}
#endif
			}

			store.Close();
		}

		public X509Certificate2 OldCertificate
		{
			get
			{
#if (DEBUG || RELEASE)


				Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(HKLMStore, Microsoft.Win32.RegistryKeyPermissionCheck.ReadSubTree, Microsoft.Win32.RegistryOptions.None);
				var oldKey = key.GetValue("OldCertificate");
				key.Close();
				if (oldKey == null) return null;
				store.Open(OpenFlags.ReadOnly);
				X509Certificate2Collection certCollection = store.Certificates;
				X509Certificate2Collection currentCerts = certCollection.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
				X509Certificate2Collection signingCert = currentCerts.Find(X509FindType.FindByThumbprint, oldKey, false);
				if (signingCert.Count == 0)
					return null;
				store.Close();
				// Return the first certificate in the collection, has the right name and is current.
				return signingCert[0];


#else
				try
				{
					var oldKey = Properties.Settings.Default["OldCertificate"];
					store.Open(OpenFlags.ReadOnly);
					X509Certificate2Collection certCollection = store.Certificates;
					X509Certificate2Collection currentCerts = certCollection.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
					X509Certificate2Collection signingCert = currentCerts.Find(X509FindType.FindByThumbprint, oldKey, false);
					if (signingCert.Count == 0)
						return null;
					store.Close();
					// Return the first certificate in the collection, has the right name and is current.
					return signingCert[0];
				}
				catch (Exception)
				{
					return null;
				}
#endif
			}
		}
		public X509Certificate2 NewCertificate
		{
			get
			{
#if (DEBUG || RELEASE)
				Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(HKLMStore, Microsoft.Win32.RegistryKeyPermissionCheck.ReadSubTree, Microsoft.Win32.RegistryOptions.None);
				var newKey = key.GetValue("NewCertificate");
				key.Close();
				if (newKey == null) return null;
				store.Open(OpenFlags.ReadOnly);
				X509Certificate2Collection certCollection = store.Certificates;
				// If using a certificate with a trusted root you do not need to FindByTimeValid, instead:
				// currentCerts.Find(X509FindType.FindBySubjectDistinguishedName, certName, true);
				X509Certificate2Collection currentCerts = certCollection.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
				X509Certificate2Collection signingCert = currentCerts.Find(X509FindType.FindByThumbprint, newKey, false);
				if (signingCert.Count == 0)
					return null;
				store.Close();
				return signingCert[0];
#else
				try
				{
					var oldKey = Properties.Settings.Default["NewCertificate"];
					store.Open(OpenFlags.ReadOnly);
					X509Certificate2Collection certCollection = store.Certificates;
					X509Certificate2Collection currentCerts = certCollection.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
					X509Certificate2Collection signingCert = currentCerts.Find(X509FindType.FindByThumbprint, oldKey, false);
					if (signingCert.Count == 0)
						return null;
					store.Close();
					// Return the first certificate in the collection, has the right name and is current.
					return signingCert[0];
				}
				catch (Exception)
				{
					return null;
				}
#endif
			}
		}
	}

}
