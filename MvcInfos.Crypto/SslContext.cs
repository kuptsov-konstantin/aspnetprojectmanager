﻿using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto.Operators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Utilities;
using Org.BouncyCastle.X509;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace MvcInfos.Crypto
{
	public class SslContext
	{
		private string CurrentTempFolder { get; }
		public X509Certificate2 UserCertificate;   
		/**
		 * Signature Algorithm
		 */
		const string signatureAlgorithm = "SHA256WithRSA";

		public static X509Certificate2 GenerateSelfSignedCertificate(string subjectName, string issuerName, int keyStrength = 2048)
		{
			// Generating Random Numbers
			var randomGenerator = new CryptoApiRandomGenerator();
			var random = new SecureRandom(randomGenerator);

			// The Certificate Generator
			var certificateGenerator = new X509V3CertificateGenerator();

			// Serial Number
			var serialNumber = BigIntegers.CreateRandomInRange(BigInteger.One, BigInteger.ValueOf(Int64.MaxValue), random);
			certificateGenerator.SetSerialNumber(serialNumber);

		
	

			// Issuer and Subject Name
			var subjectDN = new X509Name(subjectName);
			var issuerDN = new X509Name(issuerName);
			certificateGenerator.SetIssuerDN(issuerDN);
			certificateGenerator.SetSubjectDN(subjectDN);

			// Valid For
			var notBefore = DateTime.UtcNow.Date;
			var notAfter = notBefore.AddYears(2);

			certificateGenerator.SetNotBefore(notBefore);
			certificateGenerator.SetNotAfter(notAfter);

			// Subject Public Key
			var keyGenerationParameters = new KeyGenerationParameters(random, keyStrength);
			var keyPairGenerator = new RsaKeyPairGenerator();
			keyPairGenerator.Init(keyGenerationParameters);
			var subjectKeyPair = keyPairGenerator.GenerateKeyPair();

			certificateGenerator.SetPublicKey(subjectKeyPair.Public);

			// Generating the Certificate
			var issuerKeyPair = subjectKeyPair;

			ISignatureFactory signatureFactory = new Asn1SignatureFactory(signatureAlgorithm, issuerKeyPair.Private, random);
			// selfsign certificate
			var certificate = certificateGenerator.Generate(signatureFactory);

			// correcponding private key
			PrivateKeyInfo info = PrivateKeyInfoFactory.CreatePrivateKeyInfo(subjectKeyPair.Private);


			// merge into X509Certificate2
			var x509 = new X509Certificate2(certificate.GetEncoded());

			var seq = (Asn1Sequence)Asn1Object.FromByteArray(info.ParsePrivateKey().GetDerEncoded());
			if (seq.Count != 9)
				throw new PemException("malformed sequence in RSA private key");

			var rsa = RsaPrivateKeyStructure.GetInstance(seq);
			RsaPrivateCrtKeyParameters rsaparams = new RsaPrivateCrtKeyParameters(rsa.Modulus, rsa.PublicExponent, rsa.PrivateExponent, rsa.Prime1, rsa.Prime2, rsa.Exponent1, rsa.Exponent2, rsa.Coefficient);

			x509.PrivateKey = DotNetUtilities.ToRSA(rsaparams);
			return x509;

		}


		public static AsymmetricKeyParameter GenerateCACertificate(string subjectName, int keyStrength = 2048)
		{
			// Generating Random Numbers
			var randomGenerator = new CryptoApiRandomGenerator();
			var random = new SecureRandom(randomGenerator);

			// The Certificate Generator
			var certificateGenerator = new X509V3CertificateGenerator();

			// Serial Number
			var serialNumber = BigIntegers.CreateRandomInRange(BigInteger.One, BigInteger.ValueOf(Int64.MaxValue), random);
			certificateGenerator.SetSerialNumber(serialNumber);
			
			//certificateGenerator.SetSignatureAlgorithm(signatureAlgorithm);

			// Issuer and Subject Name
			var subjectDN = new X509Name(subjectName);
			var issuerDN = subjectDN;
			certificateGenerator.SetIssuerDN(issuerDN);
			certificateGenerator.SetSubjectDN(subjectDN);

			// Valid For
			var notBefore = DateTime.UtcNow.Date;
			var notAfter = notBefore.AddYears(2);

			certificateGenerator.SetNotBefore(notBefore);
			certificateGenerator.SetNotAfter(notAfter);

			// Subject Public Key
			AsymmetricCipherKeyPair subjectKeyPair;
			var keyGenerationParameters = new KeyGenerationParameters(random, keyStrength);
			var keyPairGenerator = new RsaKeyPairGenerator();
			keyPairGenerator.Init(keyGenerationParameters);
			subjectKeyPair = keyPairGenerator.GenerateKeyPair();

			certificateGenerator.SetPublicKey(subjectKeyPair.Public);

			// Generating the Certificate
			var issuerKeyPair = subjectKeyPair;

			// selfsign certificate

			ISignatureFactory signatureFactory = new Asn1SignatureFactory(signatureAlgorithm, issuerKeyPair.Private, random);
			// selfsign certificate
			var certificate = certificateGenerator.Generate(signatureFactory);

			//	var certificate = certificateGenerator.Generate(issuerKeyPair.Private, random);
			var x509 = new X509Certificate2(certificate.GetEncoded());
			// Add CA certificate to Root store
			//addCertToStore(x509, StoreName.Root, StoreLocation.CurrentUser);

			return issuerKeyPair.Private;

		}

		public static bool AddCertToStore(X509Certificate2 cert, StoreName st, StoreLocation sl)
		{
			bool bRet = false;

			try
			{
				X509Store store = new X509Store(st, sl);
				store.Open(OpenFlags.ReadWrite);
				store.Add(cert);

				store.Close();
			}
			catch
			{

			}

			return bRet;
		}

		public SslContext()
		{
			//var caPrivKey = GenerateCACertificate("CN=root ca");
			UserCertificate = GenerateSelfSignedCertificate($"CN={Environment.UserDomainName}", "CN=SManager");

			//UserCertificate = GenerateSelfSignedCertificate(Environment.UserDomainName, "SManager");
			//CurrentTempFolder = Path.GetTempFileName();
			//File.WriteAllBytes(CurrentTempFolder, Properties.Resources.openssl);
			//InitCertificat(CurrentTempFolder);
		}
		public SslContext(string path)
		{
			//InitCertificat(path);
		}


	}

}

