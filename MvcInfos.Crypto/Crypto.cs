﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace MvcInfos.Crypto
{
	public enum CryptoMethod
	{
		Decrypt, Encrypt
	}
	public class Crypto
	{
		private Certificate.Certificate Certificate { get; }
		public Crypto()
		{
			Certificate = new Certificate.Certificate();		
		}

		public string Decrypt(byte[] array)
		{
			if(array == null)
			{
				return null;
			}
			try
			{
				// Decrypt the file using the private key from the certificate.
				return RsaCrypto.Decrypt(array, (RSACryptoServiceProvider)Certificate.NewCertificate.PrivateKey);

			}
			catch(Exception)
			{
				return null;
			}
		}

		public byte[] Encrypt(string str)
		{
			if (str == null)
			{
				return null;
			}
			// Encrypt the file using the public key from the certificate.
			return RsaCrypto.Encrypt(str, (RSACryptoServiceProvider)Certificate.NewCertificate.PublicKey.Key);

		}
	}
	public class RsaCrypto
	{
		public static byte[] Encrypt(string input, RSACryptoServiceProvider rsaPublicKey)
		{
			byte[] output = null;
			using (RSACryptoServiceProvider csp = rsaPublicKey)
			{
				byte[] bytesData = Encoding.UTF8.GetBytes(input);
				output = csp.Encrypt(bytesData, false);
			}
			return output;
		}

		public static string Decrypt(byte[] encrypted, RSACryptoServiceProvider rsaPublicKey)
		{
			string text = string.Empty;
			using (RSACryptoServiceProvider csp = rsaPublicKey)
			{
				byte[] bytesDecrypted = csp.Decrypt(encrypted, false);
				text = Encoding.UTF8.GetString(bytesDecrypted);
			}
			return text;
		}
	}

	public class AesCrypto
	{

		// Encrypt a file using a public key.
		public static byte[] Encrypt(string plainText, RSACryptoServiceProvider rsaPublicKey)
		{
			using (AesManaged aesManaged = new AesManaged())
			{
				// Create instance of AesManaged for
				// symetric encryption of the data.
				aesManaged.KeySize = 256;
				aesManaged.BlockSize = 128;
				aesManaged.Mode = CipherMode.CBC;
				using (ICryptoTransform transform = aesManaged.CreateEncryptor())
				{
					RSAPKCS1KeyExchangeFormatter keyFormatter = new RSAPKCS1KeyExchangeFormatter(rsaPublicKey);
					byte[] keyEncrypted = keyFormatter.CreateKeyExchange(aesManaged.Key, aesManaged.GetType());

					// Create byte arrays to contain
					// the length values of the key and IV.
					byte[] LenK = new byte[4];
					byte[] LenIV = new byte[4];

					int lKey = keyEncrypted.Length;
					LenK = BitConverter.GetBytes(lKey);
					int lIV = aesManaged.IV.Length;
					LenIV = BitConverter.GetBytes(lIV);

					using (MemoryStream outFs = new MemoryStream())
					{					

						outFs.Write(LenK, 0, 4);
						outFs.Write(LenIV, 0, 4);
						outFs.Write(keyEncrypted, 0, lKey);
						outFs.Write(aesManaged.IV, 0, lIV);

						// Now write the cipher text using
						// a CryptoStream for encrypting.
						using (CryptoStream outStreamEncrypted = new CryptoStream(outFs, transform, CryptoStreamMode.Write))
						{
							using (StreamWriter swEncrypt = new StreamWriter(outStreamEncrypted))
							{
								swEncrypt.Write(plainText);
							}							
							//outStreamEncrypted.FlushFinalBlock();
							outStreamEncrypted.Close();
						}
						var result = outFs.ToArray();
						outFs.Close();
						return result;
					}
				}
			}
		}


		// Decrypt a file using a private key.
		public static string Decrypt(byte[] cipherText, RSACryptoServiceProvider rsaPrivateKey)
		{

			// Create instance of AesManaged for
			// symetric decryption of the data.
			using (AesManaged aesManaged = new AesManaged())
			{
				aesManaged.KeySize = 256;
				aesManaged.BlockSize = 128;
				aesManaged.Mode = CipherMode.CBC;

				// Create byte arrays to get the length of
				// the encrypted key and IV.
				// These values were stored as 4 bytes each
				// at the beginning of the encrypted package.
				byte[] LenK = new byte[4];
				byte[] LenIV = new byte[4];
				string plaintext = null;

				// Use FileStream objects to read the encrypted
				// file (inFs) and save the decrypted file (outFs).
				using (MemoryStream inFs = new MemoryStream())
				{

					inFs.Seek(0, SeekOrigin.Begin);
					inFs.Seek(0, SeekOrigin.Begin);
					inFs.Read(LenK, 0, 3);
					inFs.Seek(4, SeekOrigin.Begin);
					inFs.Read(LenIV, 0, 3);

					// Convert the lengths to integer values.
					int lenK = BitConverter.ToInt32(LenK, 0);
					int lenIV = BitConverter.ToInt32(LenIV, 0);

					// Determine the start postition of
					// the ciphter text (startC)
					// and its length(lenC).
					int startC = lenK + lenIV + 8;
					int lenC = (int)inFs.Length - startC;

					// Create the byte arrays for
					// the encrypted AesManaged key,
					// the IV, and the cipher text.
					byte[] KeyEncrypted = new byte[lenK];
					byte[] IV = new byte[lenIV];

					// Extract the key and IV
					// starting from index 8
					// after the length values.
					inFs.Seek(8, SeekOrigin.Begin);
					inFs.Read(KeyEncrypted, 0, lenK);
					inFs.Seek(8 + lenK, SeekOrigin.Begin);
					inFs.Read(IV, 0, lenIV);	
					// Use RSACryptoServiceProvider
					// to decrypt the AesManaged key.
					byte[] KeyDecrypted = rsaPrivateKey.Decrypt(KeyEncrypted, false);

					// Decrypt the key.
					using (ICryptoTransform transform = aesManaged.CreateDecryptor(KeyDecrypted, IV))
					{ 
						using (MemoryStream outFs = new MemoryStream(cipherText))
						{
							inFs.Seek(startC, SeekOrigin.Begin);
							using (CryptoStream outStreamDecrypted = new CryptoStream(outFs, transform, CryptoStreamMode.Write))
							{
								using (StreamReader srDecrypt = new StreamReader(outStreamDecrypted))
								{
									plaintext = srDecrypt.ReadToEnd();
								}
							}
							outFs.Close();
						}
						inFs.Close();
					}
					return plaintext;
				}
			}
		}
	}
}