﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Date
{
	public class CustomDateRangeAttribute : RangeAttribute
    {
        public CustomDateRangeAttribute() : base(typeof(DateTime), DateTime.Now.AddYears(-100).ToString(), DateTime.Now.ToString())
        { }
    }
}
