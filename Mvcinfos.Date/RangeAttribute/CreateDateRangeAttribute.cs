﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Date
{
	public class CreateDateRangeAttribute : RangeAttribute
    {
        public CreateDateRangeAttribute() : base(typeof(DateTime), DateTime.Now.AddYears(-100).ToString(), DateTime.Now.AddYears(100).ToString())
        { }
    }
}
