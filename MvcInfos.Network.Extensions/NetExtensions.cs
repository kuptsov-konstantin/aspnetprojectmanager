﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MvcInfos.Network.Extensions
{
    public static class NetExtensions
	{
		public static byte[] GetBytes(this string str)
		{
			return Encoding.UTF8.GetBytes(str);
		}
		public static string GetString(this byte[] bytes)
		{
			return Encoding.UTF8.GetString(bytes);
		}

		public static char[] ToCharArray(this byte[] bytes)
		{
			char[] arr = new char[bytes.Length];
			for (int i = 0; i < bytes.Length; i++)
			{
				arr[i] = (char)bytes[i];
			}
			return arr;
		}
		public static byte[] MySerializeArray<TSource>(this IEnumerable<TSource> list1)
		{
			using (MemoryStream stream = new MemoryStream())
			{
				BinaryFormatter bin = new BinaryFormatter();
				var list = list1.ToArray();
				bin.Serialize(stream, list);
				return stream.ToArray();
			}
		}

		public static byte[] MySerializeObject<TSource>(this TSource list1)
		{
			using (MemoryStream stream = new MemoryStream())
			{
				BinaryFormatter bin = new BinaryFormatter();
				bin.Serialize(stream, list1);
				return stream.ToArray();
			}
		}

		public static bool IsMyIP(this IPAddress[] adresses, IPAddress adress)
		{
			return (adresses.Where(p => p.ToString().Equals(adress.ToString()) == true).Select(p => p)).ToList().Count > 0;
		}
		public static TSource MyDeserialize<TSource>(this byte[] list1)
		{
			BinaryFormatter bin = new BinaryFormatter();
			MemoryStream stream = new MemoryStream(list1);
			var list = list1.ToArray();
			return (TSource)bin.Deserialize(stream);
		}

		public static async Task SenderAsync(this Stream sslStream, byte[] byteMes)
		{
			var das = BitConverter.GetBytes(byteMes.Length);
			await sslStream.WriteAsync(das, 0, das.Length);
			var da1s = new byte[256];
			await sslStream.ReadAsync(da1s, 0, da1s.Length);
			await sslStream.WriteAsync(byteMes, 0, byteMes.Length);
		}

		public static async Task<byte[]> ReciverAsync(this Stream sslStream)
		{
			var das = new byte[256];
			await sslStream.ReadAsync(das, 0, das.Length);
			int reciveMessageLength = BitConverter.ToInt32(das, 0);
			var da1s = "ok".GetBytes();
			await sslStream.WriteAsync(da1s, 0, da1s.Length);
			das = new byte[reciveMessageLength];
			await sslStream.ReadAsync(das, 0, das.Length);
			return das;
		}

		public static async Task SendObjectAsync<TSource>(this Stream stream, TSource obj)
		{
			var forSend = obj.MySerializeObject();
			await stream.SenderAsync(forSend);
		}

		public static async Task<TSource> ReciveObjectAsync<TSource>(this Stream stream)
		{
		 	var bytes = await stream.ReciverAsync();
			var forSend = bytes.MyDeserialize<TSource>();
			return forSend;
		}
	}
}
