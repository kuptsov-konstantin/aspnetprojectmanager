﻿using MvcInfos.Entities;
using MvcInfos.Entities.CountryInfo;
using MvcInfos.Entities.Files;
using MvcInfos.Entities.Firm;
using MvcInfos.Entities.Firm.Dictionary;
using MvcInfos.Entities.Mails;
using MvcInfos.Entities.Settings;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using MvcInfos.Entities.Notifications;

namespace MvcInfos.Repository
{
	public interface IApplicationRepository
    {
		DbSet<Contract> Contracts { get; }
        DbSet<CustomerEmployee> CustomerEmployees { get; }
        DbSet<CustomerFirm> CustomerFirms { get; }
        DbSet<Customer> Customers { get; }
        DbSet<Department> Departments { get; }
        DbSet<EmployeeFirm> EmployeeFirms { get; }
        DbSet<Employee> Employees { get; }
        DbSet<Firm> Firms { get; }
        DbSet<ProjectEmployeeFirm> ProjectEmployeeFirms { get; }
        DbSet<Project> Projects { get; }
        IDbSet<ApplicationUser> Users { get; }
        DbSet<Setting> Settings { get; }
		DbSet<HostingFile> HostingFiles { get;  }
		DbSet<UploadFile> UploadFiles { get; }
		DbSet<CountryTable> Countries { get; }
        DbSet<CityTable> Cities { get; }
        DbSet<AddressTable> Addresses { get;}
        DbSet<Person> Persons { get;  }
        DbSet<EmployeePC> EmployeePCs { get; }
        DbSet<OS> OSs { get; }
        DbSet<ProjectStage> ProjectStages { get; }
        DbSet<ProjectStageType> ProjectStageTypes { get; }
		DbSet<ActiveDirectortSyncHistory> ActiveDirectortSyncHistories { get; }
		DbSet<UploadGroupFile> GroupFiles { get;  }
		DbSet<ResolutionNotifier> ResolutionNotifiers { get; }
		DbSet<Mail> Mails { get; }
		DbSet<NotificationsClick> NotificationsClicks { get; }
		DbSet<MailStatus> MailStatuses { get; }

		DbSet<Resolution> Resolutions { get; }
		int SaveChanges();
        Task<int> SaveChangesAsync();


        DbEntityEntry Entry(object entity);
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

    }
}
