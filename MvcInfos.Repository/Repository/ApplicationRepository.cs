﻿using MvcInfos.Entities;
using MvcInfos.Entities.Firm;
using MvcInfos.Entities.Firm.Dictionary;
using MvcInfos.Entities.Settings;
using MvcInfos.Repository;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using MvcInfos.Entities.Files;
using MvcInfos.Entities.CountryInfo;
using MvcInfos.Entities.Mails;
using MvcInfos.Repository.IdentityModels;
using MvcInfos.Entities.Notifications;

namespace MvcInfos.Repository
{
	public class ApplicationRepository : IDisposable, IApplicationRepository
    {  
        private ApplicationDbContext db = new ApplicationDbContext();
        public int SaveChanges()
        {
           return db.SaveChanges();
        }
        public virtual Task<int> SaveChangesAsync()
        {
            return db.SaveChangesAsync();
        }   
        public DbSet<Firm> Firms=> db.Firms; 
        public DbSet<Employee> Employees =>  db.Employees;   
        public DbSet<Department> Departments => db.Departments;
        public DbSet<Project> Projects => db.Projects;
        public DbSet<ProjectEmployeeFirm> ProjectEmployeeFirms => db.ProjectEmployeeFirms;
        public DbSet<EmployeeFirm> EmployeeFirms => db.EmployeeFirms;
        public DbSet<Contract> Contracts => db.Contracts;
        public DbSet<Customer> Customers => db.Customers;
        public DbSet<CustomerEmployee> CustomerEmployees => db.CustomerEmployees;
        public DbSet<CustomerFirm> CustomerFirms => db.CustomerFirms;
        public DbSet<Setting> Settings => db.Settings;
        public IDbSet<ApplicationUser> Users => db.Users;
        public DbSet<CountryTable> Countries => db.Countries;
        public DbSet<CityTable> Cities => db.Cities;
        public DbSet<AddressTable> Addresses=>db.Addresses;        
        public DbSet<Person> Persons => db.Persons;      
        public DbSet<EmployeePC> EmployeePCs=> db.EmployeePCs;
        public DbSet<OS> OSs => db.OSs;     
        public DbSet<ProjectStage> ProjectStages=> db.ProjectStages;     
        public DbSet<ProjectStageType> ProjectStageTypes =>  db.ProjectStageTypes;   
		public DbSet<HostingFile> HostingFiles => db.HostingFiles;
		public DbSet<UploadFile> UploadFiles => db.UploadFiles;
		public DbSet<ActiveDirectortSyncHistory> ActiveDirectortSyncHistories => db.ActiveDirectortSyncHistories;
		public DbSet<UploadGroupFile> GroupFiles => db.GroupFiles;
		public DbSet<ResolutionNotifier> ResolutionNotifiers => db.ResolutionNotifiers;
		public DbSet<Mail> Mails => db.Mails;
		public DbSet<NotificationsClick> NotificationsClicks => db.NotificationsClicks;
		public DbSet<Resolution> Resolutions => db.Resolutions;
		public DbSet<MailStatus> MailStatuses => db.MailStatuses;

		public DbEntityEntry Entry(object entity)
        {
            return this.db.Entry(entity);
        }
        public DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class {

            return this.db.Entry(entity);
        }
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}