﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MvcInfos.HKey;
using Microsoft.AspNet.Identity;

using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using MvcInfos.Entities;
using MvcInfos.Plugin.Interfaces;
using System.DirectoryServices.AccountManagement;
using MvcInfos.Settings;
using MvcInfos.Crypto.Certificate;
using MvcInfos.ActiveDirectory;
using Ninject.Web.Mvc.FilterBindingSyntax;
using System.Web;
using MvcInfos.IdentityConfig;

namespace MvcInfos.Repository
{
	public class NinjectDependencyResolver : IDependencyResolver
	{
		private IKernel kernel;
		public NinjectDependencyResolver(IKernel kernelParam)
		{
			kernel = kernelParam;
			AddBindings();
		}
		public object GetService(Type serviceType)
		{
			return kernel.TryGet(serviceType);
		}
		public IEnumerable<object> GetServices(Type serviceType)
		{
			return kernel.GetAll(serviceType);
		}
		private void AddBindings()
		{
			kernel.Bind<IUserStore<ApplicationUser>>().To<UserStore<ApplicationUser>>();
			kernel.Bind<UserManager<ApplicationUser>>().ToSelf();
			kernel.Bind<HttpContextBase>().ToMethod(ctx => new HttpContextWrapper(HttpContext.Current)).InTransientScope();
			kernel.Bind<ApplicationSignInManager>().ToMethod((context) => (new HttpContextWrapper(HttpContext.Current)).GetOwinContext().Get<ApplicationSignInManager>());
			kernel.Bind<ApplicationRoleManager>().ToMethod((context) => (new HttpContextWrapper(HttpContext.Current)).GetOwinContext().GetUserManager<ApplicationRoleManager>());
			kernel.Bind<ApplicationUserManager>().ToMethod((context) => (new HttpContextWrapper(HttpContext.Current)).GetOwinContext().GetUserManager<ApplicationUserManager>());
			kernel.Bind<PrincipalContext>().ToMethod((context) => (new HttpContextWrapper(HttpContext.Current)).GetOwinContext().Get<PrincipalContext>());
			kernel.Bind<IPlugins>().To<Plugin.LoadPlugins>().InSingletonScope();
			kernel.Bind<IApplicationRepository>().To<ApplicationRepository>();
			kernel.Bind<IHKCurrentUser>().ToMethod((context) => (new HttpContextWrapper(HttpContext.Current)).GetOwinContext().Get<HKCurrentUser>());

			kernel.Bind<Certificate>().ToMethod((context) => (new HttpContextWrapper(HttpContext.Current)).GetOwinContext().Get<Certificate>());

			kernel.Bind<NotificationServiceManager>().ToMethod((context) => new HttpContextWrapper(HttpContext.Current).GetOwinContext().Get<NotificationServiceManager>()).InSingletonScope();
#if (DEBUG || RELEASE)
			kernel.BindFilter<AuthorizeActiveDirectoryAttribute>(FilterScope.Action, null).WhenActionMethodHas<AuthorizeActiveDirectoryAttribute>();
			kernel.Bind<ActiveDirectorySetting>().ToMethod((context) => (new HttpContextWrapper(HttpContext.Current)).GetOwinContext().Get<ActiveDirectorySetting>());
			//kernel.Bind<ActiveDirectorySyncService>().ToMethod((context) => (new HttpContextWrapper(HttpContext.Current)).GetOwinContext().Get<ActiveDirectorySyncService>()).InSingletonScope();
			kernel.Bind<ActiveDirectorySyncService>().To<ActiveDirectorySyncService>().InSingletonScope();

#endif


		}
	}
}