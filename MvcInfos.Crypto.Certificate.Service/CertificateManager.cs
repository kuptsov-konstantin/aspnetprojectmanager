﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace MvcInfos.Crypto.Certificate.Service
{
	public class CertificateManager : IDisposable
	{
		Dictionary<string, Certificate> certificates = new Dictionary<string, Certificate>();
		public Dictionary<string, Certificate> Certificates { get => certificates; set => certificates = value; }

		private X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
#if (DEBUG || RELEASE)
		public static string HKLMStore = @"SOFTWARE\SManager\Certificates";
#else
			
#endif
		public static void RegisterCertificate(Certificate cert, string fileName = null)
		{
			if (fileName == null)
			{
				fileName = cert?.CurrentTempFile;
			}
			if (fileName == null)
			{
				throw new ArgumentNullException(nameof(fileName) + " не содержит значения");
			}

#if (DEBUG || RELEASE)
			Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(HKLMStore, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree, Microsoft.Win32.RegistryOptions.None);
			var oldKey = key.GetValue("NewCertificate");
#else
			object oldKey = null;
			try
			{
				oldKey = Properties.Settings.Default["NewCertificate"];
			}
			catch (Exception e)
			{
				oldKey = null;
			}
			Properties.Settings.Default.Reload();
#endif

			X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
			store.Open(OpenFlags.ReadWrite);
			X509Certificate2Collection collection = new X509Certificate2Collection();
			//collection.Import(fileName, Properties.Settings.Default.SecretKey, X509KeyStorageFlags.PersistKeySet);
			foreach (var item in collection)
			{
				store.Add(item);

#if (DEBUG || RELEASE)

				if (oldKey != null)
				{
					key.SetValue("OldCertificate", oldKey);
				}
				key.SetValue("NewCertificate", item.Thumbprint);
				key.Close();
#else

				if (oldKey != null)
				{
					if (Properties.Settings.Default.Properties["OldCertificate"] == null)
					{
						System.Configuration.SettingsProperty property = new System.Configuration.SettingsProperty("OldCertificate")
						{
							DefaultValue = oldKey,
							IsReadOnly = false,
							PropertyType = typeof(string),
							Provider = Properties.Settings.Default.Providers["LocalFileSettingsProvider"]
						};
						property.Attributes.Add(typeof(System.Configuration.UserScopedSettingAttribute), new System.Configuration.UserScopedSettingAttribute());
						Properties.Settings.Default.Properties.Add(property);

					}
					else
					{
						Properties.Settings.Default["OldCertificate"] = oldKey;
					}
				}
				if (Properties.Settings.Default.Properties["NewCertificate"] == null)
				{
					System.Configuration.SettingsProperty property = new System.Configuration.SettingsProperty("NewCertificate")
					{
						DefaultValue = item.Thumbprint,
						IsReadOnly = false,
						PropertyType = typeof(string),
						Provider = Properties.Settings.Default.Providers["LocalFileSettingsProvider"]
					};
					property.Attributes.Add(typeof(System.Configuration.UserScopedSettingAttribute), new System.Configuration.UserScopedSettingAttribute());
					Properties.Settings.Default.Properties.Add(property);

				}
				else
				{
					Properties.Settings.Default["NewCertificate"] = item.Thumbprint;
				}
#endif
			}

			store.Close();
		}


		public void Dispose()
		{

		}
	}
}
