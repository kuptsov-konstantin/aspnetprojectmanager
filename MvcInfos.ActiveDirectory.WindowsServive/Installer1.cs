﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace MvcInfos.ActiveDirectory.WindowsService
{
	[RunInstaller(true)]
	public partial class Installer1 : System.Configuration.Install.Installer
	{
		ServiceInstaller serviceInstaller;
		ServiceProcessInstaller processInstaller;
		public Installer1()
		{
			InitializeComponent();
			serviceInstaller = new ServiceInstaller()
			{
				StartType = ServiceStartMode.Manual,
				ServiceName = "SManagerADSync",
				Description = "Синхронизация SManager и Active Directory"
			};
			processInstaller = new ServiceProcessInstaller()
			{
				Account = ServiceAccount.LocalSystem
			};
			Installers.Add(processInstaller);
			Installers.Add(serviceInstaller);
		}
	}
}
