﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace MvcInfos.ActiveDirectory.WindowsService
{
	public partial class Service1 : ServiceBase
	{
		LdapConnection connect = null;
		ChangeNotifier notifier = null;
		public Service1()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			if (args.Length < 1) return;

			var ldap = args[0];
			connect = new LdapConnection(ldap);
			notifier = new ChangeNotifier(connect, "");

			//register some objects for notifications (limit 5)
			notifier.Register("dc=dunnry,dc=net", SearchScope.OneLevel);
			notifier.Register("cn=testuser1,ou=users,dc=dunnry,dc=net", SearchScope.Base);

			notifier.ObjectChanged += Notifier_ObjectChanged;
		}

		private void Notifier_ObjectChanged(object sender, ObjectChangedEventArgs e)
		{

			Console.WriteLine(e.Result.DistinguishedName);
			foreach (string attrib in e.Result.Attributes.AttributeNames)
			{
				foreach (var item in e.Result.Attributes[attrib].GetValues(typeof(string)))
				{
					Console.WriteLine("\t{0}: {1}", attrib, item);
				}
			}

		}

		protected override void OnStop()
		{
			notifier.ObjectChanged -= Notifier_ObjectChanged;
			notifier.Dispose();
			connect.Dispose();
		}
	}
}
