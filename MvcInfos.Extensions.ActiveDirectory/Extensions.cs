﻿using Microsoft.AspNet.Identity.Owin;
using MvcInfos.Extensions;
using MvcInfos.Settings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace MvcInfos.ActiveDirectory.Extensions
{
	public static partial class Extensions
	{
		public static bool IsHasAccessRights(this IPrincipal principal, string role)
		{
			if (principal.Identity.IsAuthenticated == false) return false;
			var splited = role.Split(',');
			if (splited.Length > 0)
			{
				return principal.IsInRole(splited[0]) || principal.ChechAccessInActiveDirectory(splited[1]);
			}
			else
			{
				return principal.IsInRole(role) || principal.ChechAccessInActiveDirectory(role);
			}

		
		}
		public static bool ChechAccessInActiveDirectory(this IPrincipal principal, string role)
		{
			var ActiveDirectorySetting = (new HttpContextWrapper(HttpContext.Current)).GetOwinContext().Get<ActiveDirectorySetting>();
			var userPrincipal = UserPrincipal.FindByIdentity(ActiveDirectorySetting.GetPrincipalContext(), IdentityType.SamAccountName, principal?.Identity?.Name);
			if (userPrincipal == null) return false;

			var groups = role.Split(',').ToList();
			foreach (var group in groups)
			{
				try
				{
					if (userPrincipal.IsMemberOf(ActiveDirectorySetting.GetPrincipalContext(), IdentityType.SamAccountName, group))
					{
						return true;
					}
				}
				catch (NoMatchingPrincipalException e)
				{
					Trace.Fail(e.Message);
				}
			}
			return false;
		}

		public static Bitmap GetThumbnailPhoto(this UserPrincipal userPrincipal)
		{
			DirectoryEntry directoryEntry = (DirectoryEntry)userPrincipal.GetUnderlyingObject();
			if (directoryEntry.Properties.Contains("thumbnailPhoto"))
			{
				PropertyValueCollection collection = directoryEntry.Properties["thumbnailPhoto"];

				if (collection.Value != null && collection.Value is byte[])
				{
					byte[] thumbnailInBytes = (byte[])collection.Value;
					return new Bitmap(new MemoryStream(thumbnailInBytes));
				}
			}
			return null;
		}

		public static object GetProperty(this UserPrincipal userPrincipal, string prop)
		{
			DirectoryEntry directoryEntry = (DirectoryEntry)userPrincipal.GetUnderlyingObject();
			return directoryEntry.GetProperty(prop);
		}

		public static UserPrincipal GetCurrentUser(this ActiveDirectorySetting ActiveDirectorySetting)
		{
			return UserPrincipal.FindByIdentity(ActiveDirectorySetting.GetPrincipalContext(), HttpContext.Current.User.Identity.Name);
		}

		public static UserPrincipal GetUserIdentity(this ActiveDirectorySetting ActiveDirectorySetting, string identity)
		{
			return UserPrincipal.FindByIdentity(ActiveDirectorySetting.GetPrincipalContext(), identity);
		}

		public static DirectoryEntry GetCurrentUserDirectoryEntry(this ActiveDirectorySetting ActiveDirectorySetting)
		{
			return (DirectoryEntry)ActiveDirectorySetting.GetCurrentUser().GetUnderlyingObject();
		}

		public static MemoryStream GetImageFromActiveDirectory(this ActiveDirectorySetting ActiveDirectorySetting)
		{
			if (HttpContext.Current.User.Identity.IsAuthenticated == false) return null;
			var userPrincipal = ActiveDirectorySetting.GetCurrentUser();
			Bitmap thumbnail = userPrincipal.GetThumbnailPhoto();
			if (thumbnail == null)
			{
				return null;
			}
			Image image = thumbnail;
			ImageCodecInfo jpgEncoder = image.GetImageFormat().GetEncoder();
			MemoryStream ms = new MemoryStream();
			image.Save(ms, jpgEncoder, new EncoderParameters());
			return ms;
		}

		public static object GetProperty(this DirectoryEntry directoryEntry, String property)
		{
			if (directoryEntry.Properties.Contains(property))
				return directoryEntry.Properties[property].Value;
			else
				return null;
		}
		public static List<SearchResult> GetDirectorySercherResult(this DirectoryEntry directoryEntry)
		{
			var resultList = new List<SearchResult>();
			var directorySercher = new DirectorySearcher(directoryEntry);
			var result = directorySercher.FindAll();
			foreach (SearchResult item in result)
			{
				resultList.Add(item);
			}
			return resultList;
		}

		public static List<SearchResult> GetDirectorySercherResult(this DirectoryEntry directoryEntry, string ldap)
		{
			var resultList = new List<SearchResult>();
			var directorySercher = new DirectorySearcher(directoryEntry, ldap);
			var result = directorySercher.FindAll();		
			foreach (SearchResult item in result)
			{
				resultList.Add(item);
			}
			return resultList;
		}

		public static List<DirectoryEntry> ToDirectoryEntries(this PrincipalSearchResult<Principal> principalSearchResult)
		{
			var resultList = new List<DirectoryEntry>();
			foreach (var item in principalSearchResult)
			{
				resultList.Add((DirectoryEntry)item.GetUnderlyingObject());
			}
			return resultList;

		}
	}
}
