﻿using MvcInfos.MyDatabase;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;
using Moq;

namespace NUnit.Tests
{
    [TestFixture]
    public class TestNewFirm
    {
        [SetUp]
        public void SetUp()
        {

        }

        [Test]
        public void TestMethod()
        {

            var mockSet = new Mock<DbSet<Blog>>();

            var mockContext = new Mock<BloggingContext>();
            mockContext.Setup(m => m.Blogs).Returns(mockSet.Object);

            var service = new BlogService(mockContext.Object);
            service.AddBlog("ADO.NET Blog", "http://blogs.msdn.com/adonet");

            mockSet.Verify(m => m.Add(It.IsAny<Blog>()), Times.Once());
            mockContext.Verify(m => m.SaveChanges(), Times.Once());

            // TODO: Add your test code here
            Assert.Pass("Your first passing test");
        }
    }
}
