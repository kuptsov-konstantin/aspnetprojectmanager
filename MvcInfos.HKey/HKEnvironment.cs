﻿namespace MvcInfos.HKey
{
	public struct HKEnvironment
	{
		public static string IsUsingActiveDirectory = "SettingsActiveDirectory";

		public static string NotificationActiveDirectory = "NotificationActiveDirectory";
	}
}
