﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MvcInfos.HKey
{
	public interface IHKCurrentUser
	{
		Dictionary<string, string> Store { get; }
		void Dispose();
		string GetValue(string key);
		void SetValue(string key, string value);
	}
}