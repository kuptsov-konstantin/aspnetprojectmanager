﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace MvcInfos.HKey
{

	public class HKCurrentUser : IDisposable, IHKCurrentUser 
	{
		private Crypto.Crypto crypto = new Crypto.Crypto();


#if (DEBUG || RELEASE)
		public static string HKLMStore = @"SOFTWARE\SManager\Settings";
		Microsoft.Win32.RegistryKey hKey = null;
#endif

		public HKCurrentUser()
		{
#if (DEBUG || RELEASE)
			hKey = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(HKLMStore, Microsoft.Win32.RegistryKeyPermissionCheck.ReadWriteSubTree, Microsoft.Win32.RegistryOptions.None);
#endif
			
			//Task.WaitAll(crypto.UpdateCertificateAsync());
		}



		public void SetValue(string key, string value)
		{			
			//await crypto.UpdateCertificateAsync();
			var result = crypto.Encrypt(value);
#if (DEBUG || RELEASE)
			hKey.SetValue(key, result);
#else
			if (Properties.Settings.Default.Properties[key] == null)
			{
				System.Configuration.SettingsProperty property = new System.Configuration.SettingsProperty(key);
				property.DefaultValue = result;
				property.IsReadOnly = false;
				property.PropertyType = typeof(string);
				property.Provider = Properties.Settings.Default.Providers["LocalFileSettingsProvider"];
				property.Attributes.Add(typeof(System.Configuration.UserScopedSettingAttribute), new System.Configuration.UserScopedSettingAttribute());
				Properties.Settings.Default.Properties.Add(property);

			}
			else
			{
				Properties.Settings.Default[key] = result;
			}
			Properties.Settings.Default.Reload();
#endif
		}

		public string GetValue(string key)
		{
			try
			{
				//await crypto.UpdateCertificateAsync();
#if (DEBUG || RELEASE)

				var value = hKey.GetValue(key);
#else
				var value = Properties.Settings.Default[key];
#endif
				return crypto.Decrypt(value as byte[]);
			}
			catch (Exception)
			{
				return null;
			}
		}




		public Dictionary<string, string> Store
		{
			get
			{
				var task = Task.Factory.StartNew(() =>
				{
					Dictionary<string, string> store = new Dictionary<string, string>();
					var props = typeof(HKEnvironment).GetFields();
					foreach (var item in props)
					{
						var field = item.GetValue(null) as string;
						var oldKey = GetValue(field) as string;
						store.Add(field, oldKey);
					}
					return store;
				});

				Task.WaitAny(task);
				return task.Result;
				/*var oldKey = key.GetValue("OldCertificate");
				

				store.Open(OpenFlags.ReadOnly);
				foreach (var item in store.Certificates)
				{
					if (item.Thumbprint.Equals(oldKey))
					{
						var cert = new X509Certificate2(item);
						store.Close();
						return cert;
					}
				}
				store.Close();
				return null;*/
			}
		}

		public void Dispose()
		{
#if (DEBUG || RELEASE)
			hKey.Close();

#else
			Properties.Settings.Default.Reload();
#endif
			//throw new NotImplementedException();
		}
	}
}