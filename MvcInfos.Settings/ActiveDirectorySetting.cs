﻿using MvcInfos.HKey;
using MvcInfos.Models.ViewModels;
using Newtonsoft.Json;
using System;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Reflection;
using System.Threading.Tasks;
using System.Security;

namespace MvcInfos.Settings
{
	public class ActiveDirectorySetting : IDisposable
	{
		private PrincipalContext principalContext = null;

		public string UserName => ActiveDirectoryAdminSetting?.UserName;
		public string Password => ActiveDirectoryAdminSetting?.Password;
		public bool? IsUsingActiveDirectory => ActiveDirectoryAdminSetting?.IsUsingActiveDirectory;
		public bool? IsNotUsingCurrentUser => ActiveDirectoryAdminSetting?.IsNotUsingCurrentUser;
		private HKCurrentUser HKCurrentUser { get; }
		private ActiveDirectoryAdminSettingViewModel ActiveDirectoryAdminSetting { get; }

		public PrincipalContext GetPrincipalContext()
		{
			if (ActiveDirectoryAdminSetting.UserName != null && ActiveDirectoryAdminSetting.Password != null)
			{
				try
				{
					principalContext = new PrincipalContext(contextType, Environment.UserDomainName, ActiveDirectoryAdminSetting.UserName, ActiveDirectoryAdminSetting.Password);
					//new PrincipalSearcher(new ComputerPrincipal(principalContext));
				}
				catch (System.Runtime.InteropServices.COMException)
				{
					principalContext = new PrincipalContext(contextType);

					//ActiveDirectoryAdminSetting.UserName = "";
					//ActiveDirectoryAdminSetting.IsUsingActiveDirectory = false;
					//ActiveDirectoryAdminSetting.IsNotUsingCurrentUser = false;
					//ActiveDirectoryAdminSetting.Password = "";
					//json = null;
					//Save();
				}
				//catch(Exception e)
				//{

				//}

			}
			else
			{
				principalContext = new PrincipalContext(contextType);
			}
			return principalContext;
		}

		private string json;
		private ContextType contextType;

		public string GetLDAP()
		{
			var queryCtx = GetPrincipalContext().GetType().GetProperty("QueryCtx", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(GetPrincipalContext(), null);
			var ldap = (string)queryCtx.GetType().GetProperty("ContextBasePartitionDN", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(queryCtx, null);
			return $"LDAP://{ldap}";
		}
		public string GetContextBasePartition()
		{
			var queryCtx = GetPrincipalContext().GetType().GetProperty("QueryCtx", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(GetPrincipalContext(), null);
			return (string)queryCtx.GetType().GetProperty("ContextBasePartitionDN", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(queryCtx, null);

		}
		public DirectoryEntry GetDirectoryEntry()
		{
			var queryCtx = GetPrincipalContext().GetType().GetProperty("QueryCtx", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(GetPrincipalContext(), null);
			return (DirectoryEntry)queryCtx.GetType().GetField("ctxBase", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(queryCtx);
		}
		public GroupPrincipal GetGroupPrincipal(string identityValue)
		{
		 	return GroupPrincipal.FindByIdentity(principalContext, identityValue);
		}

		public DirectorySearcher GetDirectorySearcher()
		{
			return new DirectorySearcher(GetDirectoryEntry());
		}

		public ActiveDirectorySetting(ContextType contextType)
		{
			this.contextType = contextType;
			ActiveDirectoryAdminSetting = new ActiveDirectoryAdminSettingViewModel();
			HKCurrentUser = new HKCurrentUser();
			Update();

			
		}

		private void Update()
		{
			var storedValue = HKCurrentUser.GetValue(HKEnvironment.IsUsingActiveDirectory);
			if (storedValue != null)
			{
				SetSettings(JsonConvert.DeserializeObject<ActiveDirectoryAdminSettingViewModel>(storedValue));
			}
		}

		public ActiveDirectorySetting(string json, ContextType contextType) : this(contextType)
		{
			this.json = json;
			SetJson(json);

		}
		public void SetJson(string json)
		{
			this.json = json;
			var res = JsonConvert.DeserializeObject<ActiveDirectoryAdminSettingViewModel>(json);
			SetSettings(res);
		}

		private void SetSettings(ActiveDirectoryAdminSettingViewModel res)
		{
			ActiveDirectoryAdminSetting.IsNotUsingCurrentUser = res.IsNotUsingCurrentUser;
			ActiveDirectoryAdminSetting.IsUsingActiveDirectory = res.IsUsingActiveDirectory;
			ActiveDirectoryAdminSetting.UserName = res.UserName;
			ActiveDirectoryAdminSetting.Password = res.Password;
		}

		public ActiveDirectorySetting(ContextType contextType, string login, string password) :this(contextType)
		{
			ActiveDirectoryAdminSetting.UserName = login;
			ActiveDirectoryAdminSetting.IsUsingActiveDirectory = true;
			ActiveDirectoryAdminSetting.IsNotUsingCurrentUser = true;
			ActiveDirectoryAdminSetting.Password = password;
		}
		public void Save()
		{
			if (json != null)
			{
				HKCurrentUser.SetValue(HKEnvironment.IsUsingActiveDirectory, json);
			}
			else
			{
				HKCurrentUser.SetValue(HKEnvironment.IsUsingActiveDirectory, this.ToString());
			}
			Update();
		}
		public override string ToString()
		{
			return JsonConvert.SerializeObject(ActiveDirectoryAdminSetting);
		}


		public bool ValidateCredentials(string userName, string password)
		{
			return this.GetPrincipalContext().ValidateCredentials(userName, password);
		}
		public bool ValidateCredentials(string userName, string password, ContextOptions negotiate)
		{
			return this.GetPrincipalContext().ValidateCredentials(userName, password, negotiate);
		}


		public async Task<UserPrincipal> FindByEmailAsync(string EmailAddress)
		{
			return await Task.FromResult(UserPrincipal.FindByIdentity(this.GetPrincipalContext(), EmailAddress));
		}

		public void Dispose()
		{
			HKCurrentUser?.Dispose();
			GetPrincipalContext()?.Dispose();
		}


		public SecureString GetSecureString()
		{
			if (IsNotUsingCurrentUser== true)
			{
				var secure = new SecureString();
				foreach (char c in Password)
				{
					secure.AppendChar(c);
				}
				return secure;
			}
			else
			{
				return null;
			}

		}

	}
}
