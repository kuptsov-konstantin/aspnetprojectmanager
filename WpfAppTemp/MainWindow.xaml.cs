﻿using MvcInfos.Models.ViewModels.Notification;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppTemp
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public async Task<List<NotificationViewModel>> LoadAllCurrentNotificationsAsync()
		{
			var nameValues = new NameValueCollection
			{
				{ "serviceId", "cd6cbea5-354d-4c29-8a93-5a85c158b8e6" }
			};
			using (WebClient webClient = new WebClient())
			{
				webClient.Headers.Add(HttpRequestHeader.Accept, "application/json");
				var resultAdmin = await webClient.UploadValuesTaskAsync(new Uri("https://server-sql/Notification/GetCurrentNotifications"), nameValues);
				
				return JsonConvert.DeserializeObject<List<NotificationViewModel>>(Encoding.Unicode.GetString(resultAdmin));
			}
		}

		public MainWindow()
		{
			InitializeComponent();
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Task.Run(async () =>
			{
				await LoadAllCurrentNotificationsAsync();

			});
			
		}
	}
}
