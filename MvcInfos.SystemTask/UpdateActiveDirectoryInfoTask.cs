﻿using Microsoft.Win32.TaskScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcInfos.SystemTask
{
	public class Tasks
	{
		public void InitialiseTask()
		{

		}
	}
    public class UpdateActiveDirectoryInfoTask: ISystemTask, IDisposable
    {
		public TaskService TaskService { get; set; }
		public UpdateActiveDirectoryInfoTask()
		{
			TaskService = new TaskService();
		}
		public void CreateTask()
		{
			TaskDefinition td = TaskService.NewTask();
			td.RegistrationInfo.Description = "Does something";
			td.Triggers.Add(new DailyTrigger { DaysInterval = 1 });
			td.Actions.Add(new ExecAction("notepad.exe", "c:\\test.log", null));
			TaskService.RootFolder.RegisterTaskDefinition(@"Test", td);
		}

		public void StartTask()
		{

		}

		public void StopTask()
		{

		}

		public void Execute()
		{

		}

		public void DeleteTask()
		{

			// Remove the task we just created
			TaskService.RootFolder.DeleteTask("Test");
		}
		public void Dispose()
		{
			throw new NotImplementedException();
		}
	}
}
