﻿namespace MvcInfos.SystemTask
{
	public interface ISystemTask
	{
		void CreateTask();
		void StartTask();
		void StopTask();
		void Execute();
	}
}