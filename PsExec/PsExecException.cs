﻿using System;
using System.Runtime.Serialization;

namespace PsInstruments
{
	[Serializable]
	internal class PsExecException : Exception
	{
		public PsExecException()
		{
		}

		public PsExecException(string message) : base(message)
		{
		}

		public PsExecException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected PsExecException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}