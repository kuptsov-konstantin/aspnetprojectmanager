﻿using PsInstruments.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management.Automation;
using System.Security;
using System.Text;
using System.Collections.ObjectModel;
using System.Management.Automation.Runspaces;

namespace PsInstruments
{
	public class PsExec
	{
		public static string PsExecName = "PsExec.exe";
		static string tempExeName = Path.Combine(Path.GetTempPath(), PsExecName);
		public PsExec()
		{
			if (!File.Exists(tempExeName))
			{
				using (FileStream fsDst = new FileStream(tempExeName, FileMode.CreateNew, FileAccess.Write))
				{
					byte[] bytes = Resources.PsExec;

					fsDst.Write(bytes, 0, bytes.Length);
				}
			}
		}

		public Process PsExecProcess { get; set; }
		public string Output { get; private set; }
		public string ErrorMessage { get; private set; }
		public Collection<PSObject> Result { get; private set; }
		public void PSInvoke(string powerShell, PSCredential credential, bool throwErrors = true)
		{
			Collection<PSObject> toReturn = new Collection<PSObject>();
			WSManConnectionInfo connectionInfo = new WSManConnectionInfo() { Credential = credential };
			using (Runspace runspace = RunspaceFactory.CreateRunspace(connectionInfo))
			{
				runspace.Open();
				using (PowerShell ps = PowerShell.Create())
				{
					ps.Runspace = runspace;
					//ps.AddScript("[Console]::OutputEncoding = [System.Text.Encoding]::GetEncoding(\"cp866\")");
					ps.AddScript(powerShell);
					toReturn = ps.Invoke();
					if (throwErrors)
					{
						if (ps.HadErrors)
						{
							ErrorMessage = string.Concat(ps.Streams?.Error?.Select(error => error.Exception.Message));
						}
					}
				}
				runspace.Close();
			}
			Result = toReturn;
		}

		public static PsExec RunRemoteProgram(string remoteMachine, string userName, string password, string program, string arguments)
		{
			var secure = new SecureString();
			foreach (char c in password)
			{
				secure.AppendChar(c);
			}

			PsExec psExec = new PsExec();
			psExec.PSInvoke($"{tempExeName} -accepteula -i -s \\\\{remoteMachine} -u {userName} -p {password} -c -f \"{program}\" {arguments}", new PSCredential(userName, secure), true);
			
			/*var ps = PowerShell.Create();
			ps.AddScript($"{tempExeName} -accepteula -i -s \\\\{remoteMachine} -u {userName} -p {password} -c -f \"{program}\" {arguments}");
			psExec.Result = ps.Invoke();

			if (ps.HadErrors == true)
			{
				psExec.ErrorMessage = string.Concat(ps.Streams?.Error?.Select(error => error.Exception.Message));
			}
			*/


			/*	

				var startInfo = new ProcessStartInfo()
				{
					Domain = userName.Split('\\')[0],
					UserName = userName.Split('\\')[1],
					//Password = secure,
					PasswordInClearText = password,
					UseShellExecute = false,
					RedirectStandardOutput = true,
					RedirectStandardError = true,
					RedirectStandardInput = true,
					//CreateNoWindow = true,
					//FileName = "notepad.exe",
					//WindowStyle = ProcessWindowStyle.Hidden,
					FileName = tempExeName,
					//Arguments = $"-accepteula -i \"\\\\localhost\" notepad.exe"
					//Arguments = $"-accepteula -h -i \"\\\\{remoteMachine}\" -u \"{userName}\" -p \"{password}\" -c -f \"{program}\" {arguments}"
					Arguments = $@"-accepteula -i -s \\{remoteMachine} -u {userName} -p {password} notepad.exe"

				};
				psExec.PsExecProcess.StartInfo = startInfo;
				psExec.PsExecProcess.Exited += PsExecProcess_Exited;
				psExec.PsExecProcess.ErrorDataReceived += PsExecProcess_ErrorDataReceived;
				psExec.PsExecProcess.Disposed += PsExecProcess_Disposed;
				psExec.PsExecProcess.OutputDataReceived += PsExecProcess_OutputDataReceived;

				var starts = psExec.PsExecProcess.Start();
				Trace.TraceInformation(Convert.ToString(starts));

				psExec.Output = psExec.PsExecProcess.StandardOutput.ReadToEnd();
				psExec.ErrorMessage = psExec.PsExecProcess.StandardError.ReadToEnd();
				psExec.PsExecProcess.WaitForExit();

		*/

			return psExec;
		}

		private static void PsExecProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
		{
			throw new NotImplementedException();
		}

		private static void PsExecProcess_Disposed(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		private static void PsExecProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
		{
			throw new NotImplementedException();
		}

		private static void PsExecProcess_Exited(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}
	}
}
