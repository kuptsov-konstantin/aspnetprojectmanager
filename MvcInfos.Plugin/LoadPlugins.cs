﻿using Ninject;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;
using System.Web;
using MvcInfos.Plugin.Interfaces;

namespace MvcInfos.Plugin
{
	public class LoadPlugins : IPlugins
	{
		public Dictionary<Guid, IPlugin> LoadedPlugins { get; }

		[Inject]
		public LoadPlugins()
		{
			var LocalStorage = new LocalStorage.LocalStorage();
			LoadedPlugins = GetPlugins();
		}
		public Dictionary<Guid, IPlugin> GetPlugins()
		{
			Dictionary<Guid, IPlugin> loadedPlugins = new Dictionary<Guid, IPlugin>();
			var findedPlugins = Directory.GetFiles(HttpContext.Current.Server.MapPath("/bin"), "MvcInfos.Plugin.*.dll");
			foreach (var item in findedPlugins)
			{
				var assembly = System.Reflection.Assembly.LoadFrom(item);
				var type = assembly.GetType(Path.GetFileNameWithoutExtension(item) + ".Properties.Resources");
				if (type != null)
				{
					var resources = new System.Resources.ResourceReader(assembly.GetManifestResourceStream(assembly.GetManifestResourceNames().Where(r => r.Contains(".resources")).FirstOrDefault()));
					var _object = (IPlugin)assembly.CreateInstance(String.Concat(Path.GetFileNameWithoutExtension(item), '.', Path.GetFileNameWithoutExtension(item).Split('.').Last()));
					loadedPlugins.Add(_object.PluginID, _object);
				}
			}
			return loadedPlugins;
		}

		public string GetHTMLOptionsStyles()
		{
			var styleOpen = "<style>";
			var styleClose = "</style>";
			List<string> styles = new List<string>();
			foreach (var plugin in LoadedPlugins)
			{
				var style =
					$".option-image-{plugin.Key.ToString("D")} {{background-image: url(/Upload/{plugin.Key.ToString("D")}/-/quality/lightest/-/scale_crop/20x20/center/);}}" +
					$".option-image-{plugin.Key.ToString("D")}:after {{background-image: url(/Upload/{plugin.Key.ToString("D")}/-/quality/lightest/-/scale_crop/20x20/center/);}}";
				styles.Add(style);
			}
			return string.Concat(styleOpen, string.Concat(styles.ToArray()), styleClose);
		}

		public string GetHTMLOptions()
		{
			var selectOpen = "<select id=\"pluginList\" class=\"selectpicker\">";
			var selectClose = "</select>";
			List<string> options = new List<string>();
			foreach (var plugin in LoadedPlugins)
			{
				options.Add($"<option value='{plugin.Key.ToString("D")}' data-content=\"<span class=\'option-image option-image-{plugin.Key.ToString("D")}\'></span><span class=\'option-header\'>{plugin.Value.PluginName}</span>\">{plugin.Value.PluginName}</option>");
			}
			return string.Concat(selectOpen, string.Concat(options.ToArray()), selectClose);
		}
		public Dictionary<string, string> GetForms()
		{
			var forms = new Dictionary<string, string>();
			foreach (var plugin in LoadedPlugins)
			{
				forms.Add(plugin.Key.ToString("D"), plugin.Value.Form);
			}
			return forms;
		}
		public System.Drawing.Bitmap GetPluginLogo(Guid id)
		{
			if (LoadedPlugins.ContainsKey(id))
			{
				return LoadedPlugins[id]?.Icon;
			}
			else
			{
				return null;
			}
		}
	}
}