﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using Microsoft.AspNet.Identity;
using MvcInfos.Entities;
using MvcInfos.Entities.Firm;
using MvcInfos.Entities.UserConfigurations;
using System;
using System.Linq;
using System.Reflection;

namespace MvcInfos.Repository.IdentityModels
{
	public class ContextInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
		public void Init(ApplicationDbContext context)
		{
			this.Seed(context);
		}

        protected override void Seed(ApplicationDbContext context)
		{
			//var adi = new ActiveDirectoryInstruments();
			//adi.CreateGroup((new HttpContextWrapper(HttpContext.Current)).GetOwinContext().Get<ActiveDirectorySetting>(), EnumerableRoles.SManagerRoles, EnumerableRoles.ActiveDirectoryGroups);

			context.Roles.AddOrUpdate(EnumerableRoles.SManagerRoles.Select(role => new IdentityRole() { Name  = role}).ToArray());

			//70fbb83e-554d-483b-9c74-99ebfa9948d6
			var mvcInfos = Assembly.Load("MvcInfos");
			System.Resources.ResourceManager rman = new System.Resources.ResourceManager("MvcInfos.Properties.Resources", mvcInfos);
			var key1 = Guid.Parse("70fbb83e-554d-483b-9c74-99ebfa9948d6");
			if (!(context.HostingFiles.Any(u => u.PluginConnectionType.Equals(key1) && u.IsNotDelete == true)))
			{

				context.HostingFiles.AddOrUpdate(
					new Entities.Files.HostingFile
					{
						IsDefault = true,
						PluginConnectionType = Guid.Parse("70fbb83e-554d-483b-9c74-99ebfa9948d6"),
						HostingName = rman.GetString("DefaultLocalHostingName"), // Properties.Resources.DefaultLocalHostingName,
						IsNotDelete = true
					});
			}

			if (!(context.Settings.Any(u => u.SettingKey == "IsUsingActiveDirectory")))
			{
				context.Settings.AddOrUpdate(
					new Entities.Settings.Setting
					{
						SettingKey = "IsUsingActiveDirectory",
						SettingValue = "false",
						SettingDescription = rman.GetString("IsUsingActiveDirectory"), // Properties.Resources.IsUsingActiveDirectory,

						IsNotDelete = true
					});
			}

			if (!(context.Settings.Any(u => u.SettingKey == "StartWorkingAge")))
			{
				context.Settings.AddOrUpdate(
					new Entities.Settings.Setting
					{
						SettingKey = "StartWorkingAge",
						SettingValue = "14",
						SettingDescription = rman.GetString("StartWorkingAge"), //SettingDescription = Properties.Resources.StartWorkingAge,
						IsNotDelete = true
					});
			}
            if (!(context.Users.Any(u => u.UserName == "adminserver")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser { UserName = "adminserver", Email = "admin@server.com", PhoneNumber = "+375257777777" };
                userManager.Create(userToInsert, "ZXCasdqwe123!");
                userManager.AddToRole(userToInsert.Id, EnumerableRoles.administrator_server);
                context.Employees.Add(new Employee() { ApplicationUser = userToInsert, ApplicationUserId = userToInsert.Id });
            }
            if (!(context.Users.Any(u => u.UserName == "adminfirm")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser { UserName = "adminfirm", Email = "admin@firm.com", PhoneNumber = "+375257777777" };
                userManager.Create(userToInsert, "ZXCasdqwe123!");
                userManager.AddToRole(userToInsert.Id, EnumerableRoles.administrator_firm);
                context.Employees.Add(new Employee() { ApplicationUser = userToInsert, ApplicationUserId = userToInsert.Id });
            }
            if (!(context.Users.Any(u => u.UserName == "managerfirm")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser { UserName = "managerfirm", Email = "manager@firm.com", PhoneNumber = "+375257777777" };
                userManager.Create(userToInsert, "ZXCasdqwe123!");
                userManager.AddToRole(userToInsert.Id, EnumerableRoles.manager);
                context.Employees.Add(new Employee() { ApplicationUser = userToInsert, ApplicationUserId = userToInsert.Id });
            }
            if (!(context.Users.Any(u => u.UserName == "projectmanagerfirm")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser { UserName = "projectmanagerfirm", Email = "projectmanager@firm.com", PhoneNumber = "+375257777777" };
                userManager.Create(userToInsert, "ZXCasdqwe123!");
                userManager.AddToRole(userToInsert.Id, EnumerableRoles.project_manager);
                context.Employees.Add(new Employee() { ApplicationUser = userToInsert, ApplicationUserId = userToInsert.Id });
            }
            if (!(context.Users.Any(u => u.UserName == "userfirm")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser { UserName = "userfirm", Email = "user@firm.com", PhoneNumber = "+375257777777" };
                userManager.Create(userToInsert, "ZXCasdqwe123!");
                userManager.AddToRole(userToInsert.Id, EnumerableRoles.user);
                context.Employees.Add(new Employee() { ApplicationUser = userToInsert, ApplicationUserId = userToInsert.Id });
            }
            if (!(context.Users.Any(u => u.UserName == "demouser")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser { UserName = "demo", Email = "demo@demo.com", PhoneNumber = "+375257777777" };
                userManager.Create(userToInsert, "ZXCasdqwe123!");
                userManager.AddToRole(userToInsert.Id, EnumerableRoles.administrator_firm);
                context.Employees.Add(new Employee() { ApplicationUser = userToInsert, ApplicationUserId = userToInsert.Id });
            }
            context.SaveChanges();
        }
    }
}