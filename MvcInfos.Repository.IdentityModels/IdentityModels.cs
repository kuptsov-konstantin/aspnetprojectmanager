﻿using EntityFramework.Triggers;
using Microsoft.AspNet.Identity.EntityFramework;
using MvcInfos.Attributes;
using MvcInfos.Entities;
using MvcInfos.Entities.CountryInfo;
using MvcInfos.Entities.Files;
using MvcInfos.Entities.Firm;
using MvcInfos.Entities.Firm.Dictionary;
using MvcInfos.Entities.Mails;
using MvcInfos.Entities.Settings;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;
using MvcInfos.Entities.Notifications;

namespace MvcInfos.Repository.IdentityModels
{
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        //EmployeesDBContext >> DefaultConnection
        public ApplicationDbContext() : base(/*"EmployeesDBContext", throwIfV1Schema: false*/)
        {
            Database.SetInitializer(new ContextInitializer());
        }

        #region If you're targeting EF 6
        public override int SaveChanges()
        {
            return this.SaveChangesWithTriggers(base.SaveChanges);
        }
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return this.SaveChangesWithTriggersAsync(base.SaveChangesAsync, cancellationToken);
        }
        #endregion


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        public DbSet<Firm> Firms { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectEmployeeFirm> ProjectEmployeeFirms { get; set; }
        public DbSet<EmployeeFirm> EmployeeFirms { get; set; }
        public DbSet<Contract> Contracts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerEmployee> CustomerEmployees { get; set; }
        public DbSet<CustomerFirm> CustomerFirms { get; set; }
        public DbSet<Setting> Settings { get; set; }
		public DbSet<HostingFile> HostingFiles { get; set; }
		public DbSet<UploadFile> UploadFiles { get; set; }
		public DbSet<Person> Persons { get; set; }
        public DbSet<EmployeePC> EmployeePCs { get; set; }
        public DbSet<OS> OSs { get; set; }
        public DbSet<ProjectStage> ProjectStages { get; set; }
        public DbSet<ProjectStageType> ProjectStageTypes { get; set; }
        public DbSet<CountryTable> Countries { get; set; }
        public DbSet<CityTable> Cities { get; set; }
        public DbSet<AddressTable> Addresses { get; set; }
		public DbSet<ActiveDirectortSyncHistory> ActiveDirectortSyncHistories { get; set; }
		public DbSet<ResolutionNotifier> ResolutionNotifiers { get; set; }
		public DbSet<Mail> Mails { get; set; }
		public DbSet<UploadGroupFile> GroupFiles { get; set; }
		public DbSet<NotificationsClick> NotificationsClicks { get; set; }
		public DbSet<Resolution> Resolutions { get; set; }
		public DbSet<MailStatus> MailStatuses { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Configurations.Add(new Configuration());
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Add(new DataTypePropertyAttributeConvention());

        }
    }
}