﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using MvcInfos.Settings;
using MvcInfos.Notification;

namespace MvcInfos.IdentityConfig
{
	public class NotificationServiceManager : NotificationService
	{
		public NotificationServiceManager(ActiveDirectorySetting activeDirectorySetting) : base(activeDirectorySetting)
		{
		}

		public static NotificationServiceManager Create(IdentityFactoryOptions<NotificationServiceManager> options, IOwinContext context)
		{
			var activeDirectorySetting = context.Get<ActiveDirectorySetting>();
			return new NotificationServiceManager(activeDirectorySetting);
		}

	}
}
