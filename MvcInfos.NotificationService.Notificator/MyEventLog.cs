﻿using System.Diagnostics;

namespace MvcInfos.Notification.Notificator
{
	public static class MyEventLog
	{
		public enum EStatus
		{
			Start = 1000,
			Stop = 1005,
			Load = 1010,
			WaitClient = 1020,
			SendMessage = 2020

		}


		static void Update()
		{
			if (!EventLog.SourceExists(Source))
			{
				EventLog.CreateEventSource(Source, "SManagerLog");
			}
		}

		public static string Source => "SManager Notificator";
		public static void WriteEntry(string message, EventLogEntryType type, EStatus eventId)
		{
			Update();
			EventLog.WriteEntry(Source, message, type, (int)eventId);
		}
	}
}
