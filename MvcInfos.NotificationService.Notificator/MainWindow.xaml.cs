﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Windows;

namespace MvcInfos.Notification.Notificator
{
	public static partial class Extension
	{
		public static void SendTrack(this Uri address, MainWindow window, List<ForNotificator> messages, Guid userId, NotificationsClickType type)
		{
			foreach (var item in messages)
			{
				address.SendTrack(window, item.ResolutionNotifierId, item.MailId, userId, type);
			}
		}

		public static void SendTrack(this Uri address, MainWindow window, Guid notificationId, Guid mailId, Guid userId, NotificationsClickType type)
		{
			window.Dispatcher.Invoke(new Action(() =>
			{
				window.Hide();
			}));
			var nameValues = new NameValueCollection
			{
				{ "notificationId", notificationId.ToString() },
				{ "mailId", mailId.ToString() },
				{ "userId", userId.ToString() },
				{"clickReport", Convert.ToString( (int)type )}
			};
			using (var webClient = new WebClient())
			{
				MyEventLog.WriteEntry(address + " >> ", EventLogEntryType.Warning, MyEventLog.EStatus.Stop);
				webClient.Headers.Add(HttpRequestHeader.Accept, "application/json");
				webClient.UploadValuesCompleted += (sender, e) =>
				{
					MyEventLog.WriteEntry(address + " >> ответ получен >> " + e.Result.GetNString() + Environment.NewLine+ e.Error.Message, EventLogEntryType.Warning, MyEventLog.EStatus.Stop);
					window.Dispatcher.Invoke(new Action(() =>
					{					
						window.Close();
					}));
				};
				webClient.UploadValuesAsync(address, nameValues);
			}
		}
	}




	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private string serverIP;
		private Guid userId;
		private List<ForNotificator> messages;
		private int buttonsCode = 10000;
		private Uri ClickNotifi => new Uri(serverIP + "/Notification/Click");
		private Uri PreviewBrowser => new Uri(serverIP + "/Mail/MailList");
		public MainWindow()
		{
			InitializeComponent();
			Closing += MainWindow_Closing;

		}

		private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Environment.ExitCode == 0)
			{
				ClickNotifi.SendTrack(this, messages, userId, NotificationsClickType.IgnoreClosing);
			}
		}

		public MainWindow(List<ForNotificator> messages, string serverIP, Guid userID) : this()
		{
			this.messages = messages;
			this.serverIP = serverIP;
			userId = userID;
			var list = messages.Select(m => m.Description).ToList();
			MessageBox.Text = string.Join(Environment.NewLine, list);
		}

		private void Preview_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				Environment.ExitCode = buttonsCode;
				Process.Start(PreviewBrowser.AbsoluteUri);
			}
			catch (Exception e1)
			{
				Environment.ExitCode = 100;
				MyEventLog.WriteEntry($"Сообщение: {e1.Message}" + Environment.NewLine + $"Стек: {e1.StackTrace}", EventLogEntryType.Error, MyEventLog.EStatus.SendMessage);
			}
			ClickNotifi.SendTrack(this,messages, userId, NotificationsClickType.Preview);		
		}
		private void SetAside_Click(object sender, RoutedEventArgs e)
		{
			Environment.ExitCode = buttonsCode;
			ClickNotifi.SendTrack(this,messages, userId, NotificationsClickType.SetAside);
			
		}
		private void Aprove_Click(object sender, RoutedEventArgs e)
		{
			Environment.ExitCode = buttonsCode;
			ClickNotifi.SendTrack(this,messages, userId, NotificationsClickType.Aprove);
		
		}
	}
}
