﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MvcInfos.Notification.Notificator
{
	public static class NetExtensions
	{
		public static byte[] GetNBytes(this string str)
		{
			return Encoding.UTF8.GetBytes(str);
		}
		public static string GetNString(this byte[] bytes)
		{
			return Encoding.UTF8.GetString(bytes);
		}

		public static char[] ToNCharArray(this byte[] bytes)
		{
			char[] arr = new char[bytes.Length];
			for (int i = 0; i < bytes.Length; i++)
			{
				arr[i] = (char)bytes[i];
			}
			return arr;
		}
		public static byte[] MyNSerializeArray<TSource>(this IEnumerable<TSource> list1)
		{
			using (MemoryStream stream = new MemoryStream())
			{
				BinaryFormatter bin = new BinaryFormatter();
				var list = list1.ToArray();
				bin.Serialize(stream, list);
				return stream.ToArray();
			}
		}
		public static byte[] MyNSerializeObject<TSource>(this TSource list1)
		{
			using (MemoryStream stream = new MemoryStream())
			{
				BinaryFormatter bin = new BinaryFormatter();
				bin.Serialize(stream, list1);
				return stream.ToArray();
			}
		}


		public static bool IsNMyIP(this IPAddress[] adresses, IPAddress adress)
		{
			return (adresses.Where(p => p.ToString().Equals(adress.ToString()) == true).Select(p => p)).ToList().Count > 0;
		}
		public static TSource MyNDeserialize<TSource>(this byte[] list1)
		{
			BinaryFormatter bin = new BinaryFormatter();
			MemoryStream stream = new MemoryStream(list1);
			var list = list1.ToArray();
			return (TSource)bin.Deserialize(stream);
		}



		public static void Sender(this Stream sslStream, byte[] byteMes)
		{
			var das = BitConverter.GetBytes(byteMes.Length);
			sslStream.Write(das, 0, das.Length);
			var da1s = new byte[256];
			sslStream.Read(da1s, 0, da1s.Length);
			sslStream.Write(byteMes, 0, byteMes.Length);
		}

		public static byte[] Reciver(this Stream sslStream)
		{
			var das = new byte[256];
			sslStream.Read(das, 0, das.Length);
			int reciveMessageLength = BitConverter.ToInt32(das, 0);
			var da1s = "ok".GetNBytes();
			sslStream.Write(da1s, 0, da1s.Length);
			das = new byte[reciveMessageLength];
			 sslStream.Read(das, 0, das.Length);
			return das;
		}



		public static void SendNObject(this Stream stream, object obj)
		{
			var mailIdList = new List<object>() { obj };
			var forSend = mailIdList.MyNSerializeArray();
			 stream.Sender(forSend);
		}

		public static TSource ReciveNObject<TSource>(this Stream stream)
		{
			var bytes = stream.Reciver();
			var forSend = bytes.MyNDeserialize<TSource>();
			return forSend;
		}
	}
}
