﻿namespace MvcInfos.Notification.Notificator
{
	public enum NotificationsClickType
	{
		Preview,
		Aprove,
		SetAside,
		IgnoreClosing,
		IgnoreNotClosing,
		None
	}
}