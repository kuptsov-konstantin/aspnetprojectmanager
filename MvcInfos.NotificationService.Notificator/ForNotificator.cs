﻿using System;

namespace MvcInfos.Notification.Notificator
{
	[Serializable]
	public class ForNotificator
	{
		//[JsonProperty("message")]
		public string Message { get; set; }
		//[JsonProperty("mailId")]
		public Guid MailId { get; set; }
		//[JsonProperty("resolutionNotifierId")]
		public Guid ResolutionNotifierId { get; set; }
		public string Description { get; set; }
	}
}
