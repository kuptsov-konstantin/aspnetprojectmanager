﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;

using System.Windows;

namespace MvcInfos.Notification.Notificator
{
	/// <summary>
	/// Логика взаимодействия для App.xaml
	/// </summary>
	public partial class App : Application
	{
		void App_Startup(object sender, StartupEventArgs e)
		{
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
			ServicePointManager.ServerCertificateValidationCallback += (sender1, cert, chain, error) =>
			{
				if (error == System.Net.Security.SslPolicyErrors.None)
				{
					return true;
				}
				MyEventLog.WriteEntry($"X509Certificate [{cert.Subject}] Policy Error: '{error.ToString()}'", EventLogEntryType.Information, MyEventLog.EStatus.Load);
				return true;
			};

			try
			{
				string serverName = "";
				MyEventLog.WriteEntry($"Уведомление получено", EventLogEntryType.Information, MyEventLog.EStatus.Start);
				List<ForNotificator> messages = new List<ForNotificator>();
				string serverIP = "";
				Guid userID = Guid.Empty;
				Guid messagesID = Guid.Empty;
				for (int i = 0; i != e.Args.Length; ++i)
				{
					if (e.Args[i] == "/MessagesId")
					{
						messagesID = Guid.Parse(e.Args[i + 1]);
						MyEventLog.WriteEntry($"Получен ключ сообщений: {messagesID}", EventLogEntryType.Information, MyEventLog.EStatus.Load);
					}

					if (e.Args[i] == "/ServerIP")
					{
						serverIP = e.Args[i + 1];
						serverName = serverIP.Substring(@"https://".Length);
						MyEventLog.WriteEntry($"Получен адрес сервера: {serverIP}:{serverName}", EventLogEntryType.Information, MyEventLog.EStatus.Load);
					}
					if (e.Args[i] == "/UserID")
					{
						userID = Guid.Parse(e.Args[i + 1]);
						MyEventLog.WriteEntry($"Получен ID пользователя: {userID}", EventLogEntryType.Information, MyEventLog.EStatus.Load);
					}

				}
				LoadInfoFromServer(serverName, messages, messagesID);
				MainWindow mainWindow = new MainWindow(messages, serverIP, userID);
				mainWindow.Show();
			}
			catch (Exception e1)
			{
				MyEventLog.WriteEntry($"Сообщение: {e1.Message}" + Environment.NewLine + $"Стек: {e1.StackTrace}", EventLogEntryType.Error, MyEventLog.EStatus.SendMessage);
				MyEventLog.WriteEntry($"Выход", EventLogEntryType.Warning, MyEventLog.EStatus.Stop);

				Current.Shutdown(100);
			}
		}

		private static void LoadInfoFromServer(string serverName, List<ForNotificator> messages, Guid messagesID)
		{
			MyEventLog.WriteEntry($"Подключение к серверу: {serverName} на порту 10010", EventLogEntryType.Information, MyEventLog.EStatus.Load);
			TcpClient TcpClientService = new TcpClient(serverName, 10010);
			MyEventLog.WriteEntry($"К серверу подключено", EventLogEntryType.Information, MyEventLog.EStatus.Load);
			var stream = TcpClientService.GetStream();
			stream.Sender("GetMessages".MyNSerializeObject());
			stream.Sender(messagesID.MyNSerializeObject());
			var bytes = stream.Reciver();
			var count = BitConverter.ToInt32(bytes, 0);
			for (int i = 0; i < count; i++)
			{
				var mailId = stream.ReciveNObject<Guid>();
				var message = stream.ReciveNObject<string>();
				var resolutionNotifireId = stream.ReciveNObject<Guid>();
				var description = stream.ReciveNObject<string>();

				messages.Add(new ForNotificator()
				{
					MailId = mailId,
					Message = message,
					ResolutionNotifierId = resolutionNotifireId,
					Description = description
				});
			}
			MyEventLog.WriteEntry($"Данные загружены", EventLogEntryType.Information, MyEventLog.EStatus.Load);
			TcpClientService.Close();
		}
	}
}