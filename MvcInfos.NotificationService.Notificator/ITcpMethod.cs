﻿namespace MvcInfos.Notification.Notificator
{
	public interface ITcpMethod
	{
		string Method { get; set; }
	}
}
