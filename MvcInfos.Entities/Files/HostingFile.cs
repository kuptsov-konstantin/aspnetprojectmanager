﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Entities.Files
{
	public class HostingFile
    {
        public HostingFile()
        {
            HostingFileID = Guid.NewGuid();
			UploadFiles = new HashSet<UploadFile>();
        }
        [Key]
        public Guid HostingFileID { get; set; }
		public Guid PluginConnectionType { get; set; } = Guid.Empty;
        public string HostingName { get; set; }
		public string Settings { get; set; }
        public virtual ISet<UploadFile> UploadFiles { get; set; }
		public bool IsDefault { get; set; } = false;
		public bool IsDeleted { get; set; } = false;
		public bool IsNotDelete { get; set; } = false;
	}
}
