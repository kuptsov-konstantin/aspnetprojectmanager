﻿using MvcInfos.Entities.Tasks;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcInfos.Entities.Files
{
	public class UploadFile
	{
        public UploadFile()
        {
            UploadFileID = Guid.NewGuid();
        }
        [Key]
        public Guid UploadFileID { get; set; }
        [DataType(DataType.Url)]
        public string Url { get; set; }
		public UploadFileType AdditionalFileType { get; set; }
		public DateTime UploadedDate { get; set; }
		public string OrignFileName { get; set; }
		public bool IsDeleted { get; set; }

		public virtual HostingFile HostingFile { get; set; }
        public virtual PersonalTask PersonalTask { get; set; }
		public virtual UploadGroupFile UploadGroupFile { get; set; }
    }
}
