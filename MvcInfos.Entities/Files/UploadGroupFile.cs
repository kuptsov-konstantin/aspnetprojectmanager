﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Entities.Files
{
	public class UploadGroupFile
	{
		public UploadGroupFile()
		{
			GroupFileId = Guid.NewGuid();
			UploadFiles = new HashSet<UploadFile>();
		}
		[Key]
		public Guid GroupFileId { get; set; }
		public DateTime DatetimeCreated { get; set; }
		public DateTime DatetimeStored { get; set; }

		public virtual ISet<UploadFile> UploadFiles { get; set; }
	}
}
