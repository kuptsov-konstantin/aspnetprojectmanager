﻿using MvcInfos.Entities.Files;
using MvcInfos.Entities.Firm;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcInfos.Entities.Tasks
{
	public class PersonalTask
    {
        public PersonalTask()
        {
            PersonalTaskID = Guid.NewGuid();
			UploadFiles = new HashSet<UploadFile>();
            CreatedDate = DateTime.Now;
            SubPersonalTasks = new HashSet<PersonalTask>();
        }
        [Key]
        public Guid PersonalTaskID { get; set; }
        public string Name { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; }
        public DateTime DeadLine { get; set; }
        public TaskPriority TaskPriority { get; set; }
        public virtual ISet<PersonalTask> SubPersonalTasks { get; set; }
        public virtual ISet<UploadFile> UploadFiles { get; set; }
        public virtual ProjectStage ProjectStages  { get; set; }
        public virtual ProjectEmployeeFirm ProjectEmployeeFirm { get; set; }

	}
}