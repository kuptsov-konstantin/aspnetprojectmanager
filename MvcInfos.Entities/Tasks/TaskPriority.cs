﻿namespace MvcInfos.Entities.Tasks
{
	public enum TaskPriority
    {
        Priority1 = 1,
        Priority2,
        Priority3,
        Priority4,
        Priority5,
		Priority6
    }
}