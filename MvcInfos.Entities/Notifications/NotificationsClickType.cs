﻿namespace MvcInfos.Entities.Notifications
{
	public enum NotificationsClickType
	{
		Preview,
		Aprove,
		SetAside,
		IgnoreClosing,
		IgnoreNotClosing,
		None
	}
}