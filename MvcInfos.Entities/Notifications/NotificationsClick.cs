﻿using System;
using System.ComponentModel.DataAnnotations;
using MvcInfos.Entities.Mails;

namespace MvcInfos.Entities.Notifications
{
	public class NotificationsClick
	{
		public NotificationsClick()
		{
			NotificationsClickId = Guid.NewGuid();
		}

		[Key]
		public Guid NotificationsClickId { get; set; }
		public DateTime ClickedTime { get; set; }
		public NotificationsClickType NotificationsClickType { get; set; }
		public virtual ResolutionNotifier ResolutionNotifier { get; set; }
	}
}