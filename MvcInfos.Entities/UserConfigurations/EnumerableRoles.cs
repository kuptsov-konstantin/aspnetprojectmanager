﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;

namespace MvcInfos.Entities.UserConfigurations
{
	public static class EnumerableRoles
	{
		public static List<IdentityRole> RolesToList(this IQueryable<IdentityRole> dbRoles)
		{
			var roles = dbRoles.OrderBy(x => x.Name);
			return roles.ToList();	
		}
		public static List<SelectListItem> GetRoles(this IPrincipal User, IQueryable<IdentityRole> dbRoles)
		{
			var roles = dbRoles.RolesToList();
			var dd = new List<SelectListItem>();
			foreach (var item in roles)
			{
				if (User.IsInRole(administrator_server))
				{
					dd.Add(new SelectListItem() { Value = item.Name, Text = item.Name });
				}

				if (User.IsInRole(administrator_firm))
				{
					if (item.Name.CompareTo(administrator_server) != 0)
					{
						dd.Add(new SelectListItem() { Value = item.Name, Text = item.Name });
					}
				}

				if (User.IsInRole(manager))
				{
					if (item.Name.CompareTo(administrator_server) != 0)
					{
						if (item.Name.CompareTo(administrator_firm) != 0)
						{
							dd.Add(new SelectListItem() { Value = item.Name, Text = item.Name });
						}
					}
				}
				if (User.IsInRole(user))
				{
					if (item.Name.CompareTo(administrator_server) != 0)
					{
						if (item.Name.CompareTo(administrator_firm) != 0)
						{
							if (item.Name.CompareTo(manager) != 0)
							{
								dd.Add(new SelectListItem() { Value = item.Name, Text = item.Name });
							}
						}
					}
				}
			}
			return dd;

		}

		//public static bool operator >(IPrincipal princ, ServerRoles er)
		//{
		//    princ.IsInRole
		//}
		//public static bool operator <(IPrincipal princ, ServerRoles er)
		//{

		//}


		public  static  string Join(params string[] parametrs)
		{
			return string.Join(",", parametrs);
		}
		public static string[] SManagerRoles = new[] {
			"administrator_server",
			"administrator_firm",
			"manager" ,
			"user",
			"project_manager"
		};
#if (AZUREDEBUG || AZURERELEASE)
		public static string administrator_server = SManagerRoles[0];
        public static string administrator_firm = SManagerRoles[1];
        public static string manager = SManagerRoles[2];
        public static string user = SManagerRoles[3];
        public static string project_manager = SManagerRoles[4];

#else
		public static string[] ActiveDirectoryDescriptionGroups = new[] {
			"Администраторы сервера СМенеджер",
			"Администраторы предприятия для сервера СМенеджер" ,
			"Менеджеры предприятия для сервера СМенеджер",
			"Пользователи сервера СМенеджер",
			"Руководители проектов для сервера СМенеджер"
		};
		public static string[] ActiveDirectoryGroups = new[] {
			"AdministratorServerSManager",
			"AdministratorOrganizationSManager" ,
			"ManagerServerSManager",
			"UsersServerSManager",
			"PMServerSManager"
		};

		public static string administrator_server = string.Join(",", new[] { SManagerRoles[0], ActiveDirectoryGroups[0], ActiveDirectoryDescriptionGroups[0] });
		public static string administrator_firm = string.Join(",", new[] { SManagerRoles[1], ActiveDirectoryGroups[1], ActiveDirectoryDescriptionGroups[1] });
		public static string manager = string.Join(",", new[] { SManagerRoles[2], ActiveDirectoryGroups[2], ActiveDirectoryDescriptionGroups[2] });
		public static string user = string.Join(",", new[] { SManagerRoles[3], ActiveDirectoryGroups[3], ActiveDirectoryDescriptionGroups[3] });
		public static string project_manager = string.Join(",", new[] { SManagerRoles[4], ActiveDirectoryGroups[4], ActiveDirectoryDescriptionGroups[4] });
		




#endif
	}
}