﻿using System;
using System.Collections.Generic;

namespace MvcInfos.Entities.UserConfigurations
{
	public static class NotificationPeriod
	{
		public enum PeriodTimes
		{
			EveryDay,
			Period
		}
		public class Times
		{
			public TimeSpan Period { get; set; }
			public PeriodTimes PeriodTimes { get; set; }
		}
		static Dictionary<int, Times> notificationTimes = new Dictionary<int, Times>()
		{
			{0, new Times() { Period = TimeSpan.FromMinutes(5), PeriodTimes = PeriodTimes.Period }  },
			{1, new Times() { Period = TimeSpan.FromMinutes(10), PeriodTimes = PeriodTimes.Period }  },
			{2, new Times() { Period = TimeSpan.FromMinutes(15), PeriodTimes = PeriodTimes.Period }  },
			{3, new Times() { Period = TimeSpan.FromMinutes(30), PeriodTimes = PeriodTimes.Period }  },
			{4, new Times() { Period = TimeSpan.FromHours(1), PeriodTimes = PeriodTimes.Period }  },
			{5, new Times() { Period = TimeSpan.FromHours(2), PeriodTimes = PeriodTimes.Period } },
			{6, new Times() { Period = TimeSpan.FromHours(3), PeriodTimes = PeriodTimes.Period } },
			{7, new Times() { Period = TimeSpan.FromHours(6), PeriodTimes = PeriodTimes.Period } },
			{8, new Times() { Period = TimeSpan.FromHours(9), PeriodTimes = PeriodTimes.EveryDay } },
			{9, new Times() { Period = TimeSpan.FromHours(12), PeriodTimes = PeriodTimes.EveryDay } },
			{10, new Times() { Period = TimeSpan.FromHours(15), PeriodTimes = PeriodTimes.EveryDay } }
		};
		public static Dictionary<int, Times> NotificationTimes { get => notificationTimes; }
	}
}