﻿using MvcInfos.Entities.Tasks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcInfos.Entities.Firm
{

    public class ProjectEmployeeFirm
    {
        public ProjectEmployeeFirm()
        {
            PersonalTasks = new HashSet<PersonalTask>();
            ID = Guid.NewGuid();
        }
        public bool IsTeamLead { get; set; }
        public Guid ID { get; set; }
        [Required]
        public virtual EmployeeFirm EmployeeFirm { get; set; }
        [Required]
        public virtual Project Project { get; set; }

        public bool IsCurrent { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime StartWorking { get; set; }
        public DateTime? StopWorking { get; set; }
        public virtual ISet<PersonalTask> PersonalTasks { get; set; }
    }
}