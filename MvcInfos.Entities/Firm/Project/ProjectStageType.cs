﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcInfos.Entities.Firm
{
    public class ProjectStageType
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public int Level { get; set; }
        public bool IsDeleted { get; set; }
        public virtual Project Project { get; set; }
        public HashSet<ProjectStage> ProjectStages { get; set; } 
        public ProjectStageType()
        {
            this.ProjectStages = new HashSet<ProjectStage>();
        }
    }
}