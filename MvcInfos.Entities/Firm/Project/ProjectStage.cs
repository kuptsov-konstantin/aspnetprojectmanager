﻿using EntityFramework.Triggers;
using MvcInfos.Date;
using MvcInfos.Entities.Tasks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MvcInfos.Entities.Firm
{
    public enum ProjectStageStatus
    {
        Wait,
        Work,
        Finish
    }

    /// <summary>
    /// Этап проекта
    /// </summary>
    public class ProjectStage
    {
        [Key]
        public Guid ProjectStageId { get; set; }
        public string Name { get; set; }
        public virtual Project Project { get; set; }

        /// <summary>
        /// Старт этапа
        /// </summary>
        [Display(Name = "Старт проекта"),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
            CreateDateRange]
        public DateTime? StartStage { get; set; }
        /// <summary>
        /// Завершение этапа
        /// </summary>
        [DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
            CreateDateRange]
        public DateTime? StopStage { get; set; }

        /// <summary>
        /// Пометка о законченности этапа
        /// </summary>
        public ProjectStageStatus Status { get; set; }

        public bool IsDeleted { get; set; }
        /// <summary>
        /// Раздел, подраздел, этап..
        /// </summary>
        public virtual ProjectStageType TypeOfStage { get; set; }
        /// <summary>
        /// Пересечение дат
        /// </summary>
        public bool IntersectionDate { get; set; }
        /// <summary>
        /// Под этапы
        /// </summary>
        public virtual ISet<ProjectStage> SubProjectStages { get; set; }
        public ProjectStage()
        {
            ProjectStageId = Guid.NewGuid();
            Status = ProjectStageStatus.Wait;
            SubProjectStages = new HashSet<ProjectStage>();
            PersonalTasks = new HashSet<PersonalTask>();
        }
        public virtual ProjectStage Parent { get; set; }
        public virtual  ICollection<PersonalTask> PersonalTasks { get; set; }

		static ProjectStage()
        {
            Triggers<ProjectStage>.Updating += (entry)=> {
                entry.Entity.StartStage = FindMinMax(entry.Entity, true);
                entry.Entity.StopStage = FindMinMax(entry.Entity, false);
            };
            Triggers<ProjectStage>.Inserting += entry => {
                entry.Entity.StartStage = FindMinMax(entry.Entity, true);
                entry.Entity.StopStage = FindMinMax(entry.Entity, false);
            };
        }

        private static DateTime FindMinMax(ProjectStage stage, bool isStartDate)
        {

            if (isStartDate == true) {
                if(stage.SubProjectStages.Count == 0)
                {
                    return stage.StartStage.Value;
                }

                var finding =  stage.SubProjectStages.AsParallel().Select(subroject => FindMinMax(subroject, isStartDate)).Min();
                return (stage.StartStage < finding) ? stage.StartStage.Value : finding;
            }
            else
            {
                if (stage.SubProjectStages.Count == 0)
                {
                    return stage.StopStage.Value;
                }

                var finding = stage.SubProjectStages.AsParallel().Select(subroject => FindMinMax(subroject, isStartDate)).Max();
                return (stage.StopStage > finding) ? stage.StopStage.Value : finding;

            }
        }
    }
}