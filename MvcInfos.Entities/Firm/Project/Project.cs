﻿using MvcInfos.Date;
using MvcInfos.Entities.Mails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MvcInfos.Entities.Firm
{   
    public class Project
    {
        public Project()
		{
			this.Mails = new HashSet<Mail>();
			this.ProjectStageTypes = new HashSet<ProjectStageType>();
            this.ProjectStages = new HashSet<ProjectStage>();
            this.Contracts = new HashSet<Contract>();
            this.ProjectEmployeeFirms = new HashSet<ProjectEmployeeFirm>();
            this.Departments = new HashSet<Department>();
            this.PlannedFinishDate = DateTime.Now.AddYears(1);
            this.ProjectStartDate = DateTime.Now;
        }

        [Key]
        public int ProjectID { get; set; }
        /// <summary>
        /// Код по договру проекта. Пример - 14UBNE
        /// </summary>
        [Column(name: "ProjectCode")]
        public string ProjectCode { get; set; }

        /// <summary>
        /// Название проекта.
        /// </summary>
        [Column(name: "ProjectName")]
        public string ProjectName { get; set; }


        public bool IsDeleted { get; set; }

        [Display(Name = "Старт проекта"),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
            CreateDateRange]
        public DateTime ProjectStartDate { get; set; }


        [Display(Name = "Ожидаемое завершение"),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true),
            CreateDateRange]
        public DateTime PlannedFinishDate { get; set; }


        [Display(Name = "Фаза проекта")]
        public virtual ISet<ProjectStage> ProjectStages { get; set; }
        public virtual ISet<Contract> Contracts { get; set; }
        public virtual ISet<Department> Departments { get; set; }
        public virtual ISet<ProjectEmployeeFirm> ProjectEmployeeFirms { get; set; }
        public virtual ISet<ProjectStageType> ProjectStageTypes { get; set; }
		public virtual ISet<Mail> Mails { get; set; }

		#region Фирма
		public virtual Firm Firm { get; set; }

		#endregion

		public ProjectEmployeeFirm FindInProjectEmployeeFirm(string id)
        {
            var _pef = (
               from pef in ProjectEmployeeFirms
               where pef.EmployeeFirm.ApplicationUserId.Id.CompareTo(id) == 0
               select pef).FirstOrDefault();
            if (_pef != null)
            {
                return _pef;
            }
            else
            {
                return null;
            }
        }
        public ProjectEmployeeFirm FindInProjectEmployeeFirm(EmployeeFirm _ef)
        {
            var _pef = (
                from pef in ProjectEmployeeFirms
                where pef.EmployeeFirm.Id == _ef.Id
                select pef).FirstOrDefault();
            if (_pef != null)
            {
                return _pef;
            }
            else
            {
                return null;
            }
        }
        public ProjectEmployeeFirm SetEF(EmployeeFirm _ef)
        {
            var _proj = (
                from pe in ProjectEmployeeFirms
                where pe.EmployeeFirm.Id == _ef.Id
                select pe).ToList();
          //  var _proj = ProjectEmployeeFirms.Where(p => p.EmployeeFirm.Id ==_ef.Id).Single();
            if (_proj.Count != 0)
            {
                return _proj.Single();
            }
            else
            {
                ProjectEmployeeFirm pmf = new ProjectEmployeeFirm()
                {
                    EmployeeFirm = _ef,
                    Project = this
                };
                ProjectEmployeeFirms.Add(pmf);
                return pmf;
            }
            //foreach (var item in ProjectEmployeeFirms)
            //{
            //    if (item.EmployeeFirm.Id == _ef.Id)
            //    {
            //        return item;
            //    }
            //}

       
        }
        public ProjectEmployeeFirm SetEF(EmployeeFirm _ef, bool _otvetstvennoe)
        {
            var _proj = (from pe in ProjectEmployeeFirms
                         where pe.EmployeeFirm.Id == _ef.Id
                         select pe).ToList();
            //  var _proj = ProjectEmployeeFirms.Where(p => p.EmployeeFirm.Id ==_ef.Id).Single();
            if (_proj.Count != 0)
            {
                return _proj.Single();
            }
            else
            {
                ProjectEmployeeFirm pmf = new ProjectEmployeeFirm()
                {
                    EmployeeFirm = _ef,
                    IsTeamLead = _otvetstvennoe,
                    Project = this
                };
                ProjectEmployeeFirms.Add(pmf);
                return pmf;
            }

            //foreach (var item in ProjectEmployeeFirms)
            //{
            //    if (item.EmployeeFirm.ApplicationUserId.Id.CompareTo(_ef.Id) == 0)
            //    {
            //        return item;
            //    }
            //}
            //ProjectEmployeeFirm pmf = new ProjectEmployeeFirm()
            //{
            //    EmployeeFirm = _ef,
            //    isRukovod = _otvetstvennoe,
            //    Project = this
            //};
            //ProjectEmployeeFirms.Add(pmf);
            //return pmf;
        }

        public bool DelEFinProject(EmployeeFirm employeeFirm)
        {
            ProjectEmployeeFirm pem = null;

            foreach (var item in ProjectEmployeeFirms)
            {
                if (item.EmployeeFirm.Id == employeeFirm.Id)
                {
                    pem = item;
                }
            }
            if (pem!= null)
            {
                

                return ProjectEmployeeFirms.Remove(pem);
            }
            else
            {
                return false;
            }
        }
    }

}
