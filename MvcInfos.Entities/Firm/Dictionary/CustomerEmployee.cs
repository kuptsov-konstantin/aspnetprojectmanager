﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Entities.Firm.Dictionary
{
    public class CustomerEmployee { 

        [Key]
        public int CustomerEmployeeID { get; set; }

        [Display(Name = "Адрес электронной почты")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Номер телефона")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
		public bool IsDeleted { get; set; }


		public virtual CustomerFirm CustomerFirm { get; set; }
        public virtual Person Person { get; set; }
    }
}