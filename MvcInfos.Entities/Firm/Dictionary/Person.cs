﻿using MvcInfos.Date;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcInfos.Entities.Firm.Dictionary
{
    [Table("Person")]
    public class Person
    {
        [Key]
        public Guid PersonId { get; set; }
        public Person()
        {
            this.PersonId = Guid.NewGuid();
            this.BirthData = DateTime.Now.AddYears(-18);
            this.FamilyName = "";
            this.Name = "";
            this.MiddleName = "";
            Employees = new HashSet<Employee>();
            CustomerEmployees = new HashSet<CustomerEmployee>();
        }
		public bool IsActiveDirectory { get; set; } = false;

        public bool IsDeleted { get; set; }
        [Required, Display(Name = "Фамилия"), DataType(DataType.Text), Column(name: "FamilyName")]
        public string FamilyName { get; set; }
        [Required, Display(Name = "Имя"), DataType(DataType.Text), Column(name: "Name")]
        public string Name { get; set; }
        [Display(Name = "Отчество"), DataType(DataType.Text), Column(name: "MiddleName")]
        public string MiddleName { get; set; }
        [DataType(DataType.Url), Column(name: "PhotoUrl")]
        public string PhotoUrl { get; set; }

        [Display(Name = "ФИО")]
        public string Fio
        {
            get
            {
                return string.Format("{0} {1} {2}", FamilyName, Name, MiddleName);
            }
        }
        [Required,
            Display(Name = "Дата рождения(дд-мм-гггг)"),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true),
            CustomDateRange]
        public DateTime BirthData { get; set; }


#region Связи
		public virtual ISet<Employee> Employees { get; set; }
		public virtual ISet<CustomerEmployee> CustomerEmployees { get; set; }
#endregion

	}
}