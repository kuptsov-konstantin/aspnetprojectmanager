﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MvcInfos.Entities.Firm.Dictionary
{
    /// <summary>
    /// Заказчик.Фирма
    /// </summary>
    public class CustomerFirm
    {
        public CustomerFirm()
		{
			CustomerEmployees = new HashSet<CustomerEmployee>();
        }

        [Key]
		public int CustomerFirmID { get; set; }

        [Required, Column("Company_name")]
        public string CompanyName { get; set;}
        public bool IsDeleted { get; set; }

		public virtual ISet<CustomerEmployee> CustomerEmployees { get; set; }
    }
}