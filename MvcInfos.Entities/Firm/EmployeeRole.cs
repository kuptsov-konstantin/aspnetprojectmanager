﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Entities.Firm
{
    /// <summary>
    /// Должность
    /// </summary>
    public class EmployeeRole
    {
        [Key]
        public int EmployeeRoleID { get; set; }

        [Display(Name = "Должность")]
        public string Role { get; set; }

        [Display(Name = "Идентификационный номер")]
        public string IdentificationNumber { get; set; }
        public bool IsDeleted { get; set; }
        [Display(Name = "Является ли директором")]
        public bool IsDirector { get; set; }
        public int EmployeeFirmID { get; set; }
        public virtual EmployeeFirm EmployeeFirm { get; set; }
        public virtual Firm Firm { get; set; }
    }
}