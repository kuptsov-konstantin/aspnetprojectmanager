﻿using MvcInfos.Entities.Firm.Dictionary;
using MvcInfos.Entities.Mails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcInfos.Entities.Firm
{
	public class Customer
	{
		public Customer()
		{
			Contracts = new HashSet<Contract>();
			Mails = new HashSet<Mail>();
		}


		[Key]
		public int CustomerID { get; set; }
		/// <summary>
		/// Заказчиком является как частное лицо, так и фирма
		/// </summary>
		public bool IsFirm { get; set; }
		public bool IsDeleted { get; set; }
		
		public virtual CustomerFirm CustomerFirm { get; set; }
		public virtual Firm Firm { get; set; }

		public virtual ISet<Contract> Contracts { get; set; }
		public virtual ISet<Mail> Mails { get; set; }
	}
}