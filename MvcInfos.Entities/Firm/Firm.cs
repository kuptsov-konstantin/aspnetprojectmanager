﻿using MvcInfos.Entities.CountryInfo;
using MvcInfos.Entities.Mails;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MvcInfos.Entities.Firm
{
	public class Firm
    {
        public Firm()
        {
            Departments = new HashSet<Department>();
            EmployeeFirms = new HashSet<EmployeeFirm>();
            Projects = new HashSet<Project>();
            EmployeeRoles = new HashSet<EmployeeRole>();
			Mails = new HashSet<Mail>();
		}
        
        [Key]
        public int FirmID { get; set; }
        [Required, Display(Name = "Название фирмы")]
        public string Name { get; set; }

		public bool IsDeleted { get; set; }

		public bool IsActiveDirectory { get; set; } = false;

		public virtual AddressTable Address { get; set; }

		/// <summary>
		/// Сотрудники
		/// </summary>
		public virtual ISet<EmployeeFirm> EmployeeFirms { get; set; }

		/// <summary>
		/// Проекты
		/// </summary>
		public virtual ISet<Project> Projects { get; set; }

		/// <summary>
		/// Отделы
		/// </summary>
		public virtual ISet<Department> Departments { get; set; }

		public virtual ISet<Mail> Mails { get; set; }
		public virtual ISet<EmployeeRole> EmployeeRoles { get; set; }

		public EmployeeFirm AddUser(ApplicationUser _user)
        {
            foreach (var item in EmployeeFirms)
            {

                if (item.ApplicationUserId.Id.CompareTo(_user.Id) ==0 )
                {
                    return item;
                }
            }
            var ef = new EmployeeFirm() { Firm = this, ApplicationUserId = _user };
            EmployeeFirms.Add(ef);
            return ef;
        }
        public Department AddDepartment(string _department)
        {
            var dep = Departments.FirstOrDefault(item => item.Name.Equals(_department));
            if (dep != default(Department))
            {
                return dep;
            }
            dep = new Department() { Name = _department, Firm = this };
            Departments.Add(dep);
            return dep;
        }

        public Department GetDepartment(int id_dep)
        {
            foreach (var item in Departments)
            {
                if (item.DepartmentID == id_dep)
                {
                    return item;
                    // return SostAddTo.uzeest;
                }
            }
            return null;
        }
        public EmployeeFirm GetEF(string AppID)
        {
            var _itm = (from ef in EmployeeFirms
                       where ef.ApplicationUserId.Id.CompareTo(AppID) == 0
                       select ef).ToList();
            if (_itm.Count > 0)
            {
                return _itm.Single();
            }
            return null;
        }
        public EmployeeRole AddDolzn(string userid, string _role)
        {
            foreach (var ef in EmployeeFirms)
            {
                if (ef.ApplicationUserId.Id.CompareTo(userid) == 0)
                {
                    return ef.AddDolzn(_role);
                     
                }
               
            }
            return null;
        }
        public Project AddProject(Project _prj)
        {
            foreach (var item in Projects)
            {
                if (item.ProjectCode.CompareTo(_prj.ProjectCode) == 0)
                {
                    return item;
                }
            }
            this.Projects.Add(_prj);
            return _prj;
        }

        public Project GetProject(string project_cod)
        {
            foreach (var item in Projects)
            {
                if (item.ProjectCode.CompareTo(project_cod) == 0)
                {
                    return item;
                }
            }
            return null;
        }
               
    }
}
