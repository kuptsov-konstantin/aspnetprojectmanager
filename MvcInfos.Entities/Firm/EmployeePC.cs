﻿using MvcInfos.Entities.Firm;
using System;
using System.Collections.Generic;

namespace MvcInfos.Entities.Firm
{
    //TODO: Использовать для отправки сообщений клиенту через msg
    public class EmployeePC
    {
        public EmployeePC()
        {
            LatestLogons = new HashSet<LatestLogon>();
        }
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public virtual EmployeeFirm EmployeeFirm { get; set; }
        public virtual OS OSs { get; set; }
        public string PCname { get; set; }
        public virtual ISet<LatestLogon> LatestLogons { get; set; }
    }
    public class OS
    {
        public OS()
        {
            EmployeePCs = new HashSet<EmployeePC>();
        }
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public virtual ISet<EmployeePC> EmployeePCs { get; set; }
        public string OSname { get; set; }
        public string VersionOs { get; set; }
    }
    public class LatestLogon
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string Login { get; set; }
        public string IP { get; set; }
        DateTime LogonDateTime { get; set; }
        public virtual EmployeePC EmployeePC { get; set; }
    }

    public class Browser
    {
        public int ID { get; set; }
        public bool IsDeleted { get; set; }
        public string Firm { get; set; }
        public string Version { get; set; }
    }
}