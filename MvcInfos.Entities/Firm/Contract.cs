﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Entities.Firm
{
    public class Contract
    {
        [Key]
        public int ContractID { get; set; }
        public double Prise { get; set; }
        [Display(Name = "Название контракта/Номер контракта")]
        public string Name { get; set; }
        [Display(Name = "Дата начала контракта")]
        public DateTime ДатаЗаключенияКонтракта { get; set; }
        [Display(Name = "Дата начала контракта")]
        public DateTime ДатаНачалаВыполнения { get; set; }
        [Display(Name ="Дата начала контракта")]
        public DateTime ДатаЗавершенияВыполнения { get; set; }
        [Display(Name ="Причина контракта")]
        public string Reason_of_contract { get; set; }

        public bool IsDeleted { get; set; }
        public virtual Project Project { get; set; }
        public virtual Customer Customer { get; set; }
    }
}