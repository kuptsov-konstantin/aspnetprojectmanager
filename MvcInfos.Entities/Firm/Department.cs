﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MvcInfos.Entities.Firm
{
    public class Department
    {
        public Department()
        {
            this.EmployeeFirms = new HashSet<EmployeeFirm>();
            this.Projects = new HashSet<Project>();
            this.SubDepartments = new HashSet<Department>();
        }
        [Key]
        public int DepartmentID { get; set; }
        public string Name { get; set; }
		public bool IsActiveDirectory { get; set; } = false;

		#region Связи
		public virtual ISet<EmployeeFirm> EmployeeFirms { get; set; }
        public virtual ISet<Project> Projects { get; set; }
        public virtual ISet<Department> SubDepartments { get; set; }
		#endregion

		public bool IsDeleted { get; set; }

        #region Фирма
        [Display(Name = "Фирма")]
        public virtual Firm Firm { get; set; }
        #endregion

        public void AddUser(EmployeeFirm ef) {
            EmployeeFirms.Add(ef);
        }
        public void DelUser(EmployeeFirm ef)
        {
            EmployeeFirms.Remove(ef);
        }
    }
}