﻿using MvcInfos.Date;
using MvcInfos.Entities.Mails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MvcInfos.Entities.Firm
{
	public class EmployeeFirm
    {

        [Key]
        public int Id { get; set; }

        [Required,
            Display(Name = "Дата приема на работу"),
            DataType(DataType.Date),
            DisplayFormat(NullDisplayText = "", DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true),
            CustomDateRange]
        //Range(typeof(DateTime), "1-Jan-1910", "1-Jan-2060"),
        public DateTime EmploymentDate { get; set; }

        /// <summary>
        /// Это начальник отдела или нет.
        /// </summary>     
        public bool IsChief { get; set; }

		public bool IsActiveDirectory { get; set; } = false;
		public bool IsDeleted { get; set; }
		
		#region Связи
		/// <summary>
		/// Должность
		/// </summary>     
		[Display(Name = "Должность")]
        public virtual ISet<EmployeeRole> EmployeeRoles { get; set; }
        public virtual ISet<Department> Departments { get; set; }
        public virtual ISet<ProjectEmployeeFirm> ProjectEmployeeFirms { get; set; }     
        public virtual ISet<EmployeePC> EmployeePCs { get; set; }
		public virtual ISet<ResolutionNotifier> ResolutionNotifiers { get; set; }
		/// <summary>
		/// Подчиненные
		/// </summary>   
		public virtual ISet<EmployeeFirm> EmployeeFirms { get; set; }

        [Display(Name = "Фирма")]
        [Required]
        public virtual Firm Firm { get; set; }

        [Required]
        [Display(Name = "Сотрудник")]
        public virtual ApplicationUser ApplicationUserId { get; set; }
		#endregion

		public EmployeeFirm()
        {
            EmployeeRoles = new HashSet<EmployeeRole>();
            Departments = new HashSet<Department>();
            ProjectEmployeeFirms = new HashSet<ProjectEmployeeFirm>();
            EmployeePCs = new HashSet<EmployeePC>();
            EmployeeFirms = new HashSet<EmployeeFirm>();
			ResolutionNotifiers = new HashSet<ResolutionNotifier>();
		}
        public ProjectEmployeeFirm AddProject(ProjectEmployeeFirm _pef)
        {
            foreach (var item in ProjectEmployeeFirms)
            {
                if (item.ID == _pef.ID)
                {
                    return item;
                }
            }
            ProjectEmployeeFirms.Add(_pef);
            return _pef;
        }
        public ICollection<Project> GetProjects(bool? isCurrent)
        {
            if (isCurrent == null)
            {
                return ProjectEmployeeFirms.Select(p => p.Project).ToList();
            }
            else
            {
                if (isCurrent == true)
                {
                    return ProjectEmployeeFirms.Where(proj => proj.IsCurrent == true).Select(p => p.Project).ToList();
                }
                else
                {
                    return ProjectEmployeeFirms.Where(proj => proj.IsCurrent == false).Select(p => p.Project).ToList();
                }
            }
        }
        public Department GetDep(string _department)
        {
            foreach (var item in Departments)
            {
                if (item.Name.CompareTo(_department)  == 0)
                {
                    return item;
                }
            }
            return null;
        }

        public EmployeeRole AddDolzn(string _role)
        {
            foreach (var item in EmployeeRoles)
            {
                if (item.Role.CompareTo(_role) == 0)
                {
                    return item;
                }
            }
            var dd = new EmployeeRole() { Role = _role, EmployeeFirm = this };
            EmployeeRoles.Add(dd);
            return dd;
        }
        public bool DelEFinProject(EmployeeFirm employeeFirm)
        {
            ProjectEmployeeFirm pem = null;

            foreach (var item in ProjectEmployeeFirms)
            {
                if (item.EmployeeFirm.Id == employeeFirm.Id)
                {
                    pem = item;
                }
            }
            if (pem != null)
            {


                return ProjectEmployeeFirms.Remove(pem);
            }
            else
            {
                return false;
            }
        }
    }
}