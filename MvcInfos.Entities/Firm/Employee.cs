﻿using MvcInfos.Entities.Firm.Dictionary;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MvcInfos.Entities.Firm
{
	public class Employee
    {
        [Key, ForeignKey("ApplicationUser")]
        public string ApplicationUserId { get; set; }
		public bool IsActiveDirectory { get; set; } = false;
		public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Person Person { get; set; }

        public bool IsDeleted { get; set; }
        public Employee()
        {
            this.EmployeeFirms = new HashSet<EmployeeFirm>();
        }

        #region Фирмы
        public virtual ISet<EmployeeFirm> EmployeeFirms { get; set; }
        #endregion


        public EmployeeFirm AddToFirm(Firm firm)
        {
            var findEFirm = EmployeeFirms.FirstOrDefault(efFirm => efFirm.Firm.Name.Equals(firm.Name));
            if (findEFirm != default(EmployeeFirm))
            {
                return findEFirm;
            }
            findEFirm = new EmployeeFirm() { Firm = firm };
            firm.EmployeeFirms.Add(findEFirm);
            EmployeeFirms.Add(findEFirm);
            return findEFirm;
        }

        public EmployeeFirm AddToFirm(EmployeeFirm ef)
        {
            foreach (var item in EmployeeFirms)
            {
                if (item.Firm.Name.CompareTo(ef.Firm.Name) == 0)
                {
                    return item;
                }
            }
            EmployeeFirms.Add(ef);
            return ef;
        }
        public EmployeeFirm FindEFirm(Firm _firm)
        {
            foreach (var ef in EmployeeFirms)
            {
                if (ef.Firm.Name.CompareTo(_firm.Name) == 0)
                {
                    return ef;
                }
            }
            return null;
        }


        public SostAddTo AddEmployeeDate(Firm _firm, DateTime _dt)
        {
            var Efir = FindEFirm(_firm);
            if (Efir == null)
            {
                Efir = new EmployeeFirm() { Firm = _firm };
                EmployeeFirms.Add(Efir);
            }
            var dep = Efir.EmploymentDate = _dt;
            return SostAddTo.Ok;
        }

        public SostAddTo AddDepartment(Firm _firm, Department _department)
        {
            var Efir = FindEFirm(_firm);
            if (Efir == null)
            {
                Efir = new EmployeeFirm() { Firm = _firm };
                EmployeeFirms.Add(Efir);
            }
            var dep = Efir.GetDep(_department.Name);
            if (dep != null)
            {
                return SostAddTo.uzeest;
            }
            else
            {
                Efir.Departments.Add(_department);
                return SostAddTo.Ok;
            }
        }
        public EmployeeFirm FindEFirm(int firmID)
        {
            foreach (var ef in EmployeeFirms)
            {
                if (ef.Firm.FirmID == firmID)
                {
                    return ef;
                }
            }
            return null;
        }
        //string userid,    
        public EmployeeRole AddDolzn(int firmid, string _role)
        {
            foreach (var ef in EmployeeFirms)
            {
                if (ef.Firm.FirmID == firmid)
                {
                    return ef.AddDolzn(_role);
                }
            }
            return null;
        }
    }

}
