﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace MvcInfos.Entities.CountryInfo
{
	public class CountryTable
	{
		public CountryTable()
		{
			CityTables = new HashSet<CityTable>();
		}
		[Key]
		public int ID { get; set; }
		[Required, Display(Name = "Страна")]
		public string Country { get; set; }
		public virtual ISet<CityTable> CityTables { get; set; }
		public bool IsDeleted { get; set; }
	}
}