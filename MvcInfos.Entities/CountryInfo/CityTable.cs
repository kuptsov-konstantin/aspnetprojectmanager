﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace MvcInfos.Entities.CountryInfo
{
	public class CityTable
	{
		public CityTable()
		{
			AddressTables = new HashSet<AddressTable>();
		}
		[Key]
		public int ID { get; set; }
		[Required, Display(Name = "City")]
		public string City { get; set; }
		public bool IsDeleted { get; set; }
		public virtual CountryTable Country { get; set; }
		public virtual ISet<AddressTable> AddressTables { get; set; }

	}
}