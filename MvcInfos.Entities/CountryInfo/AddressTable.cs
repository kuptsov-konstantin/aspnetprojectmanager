﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace MvcInfos.Entities.CountryInfo
{
	public class AddressTable
	{
		public AddressTable()
		{
			Firms = new HashSet<Firm.Firm>();
		}
		[Key]
		public int ID { get; set; }
		public string Address { get; set; }
		public virtual CityTable City { get; set; }
		public virtual ISet<Firm.Firm> Firms { get; set; }
		public bool IsDeleted { get; set; }
	}

}