﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcInfos.Entities.Settings
{
	public enum UpdateStatus
	{
		Ok, Error
	}
	public class ActiveDirectortSyncHistory
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid ActiveDirectortSyncHistoryId { get; set; }
		[DataType(DataType.DateTime)]
		public DateTime UpdateDate { get; set; }
		public UpdateStatus UpdateStatus { get; set; }
	}
}
