﻿using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Entities.Settings
{
    /// <summary>
    /// Таблица настроек
    /// TODO: Настройки для фирмы, для страны или для сервера??
    /// </summary>
    public class Setting 
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string SettingKey { get; set; }
        [Required]
        public string SettingValue { get; set; }
        public string SettingDescription { get;  set; }
        public bool IsNotDelete { get; set; }
        public bool IsDeleted { get; set; }
    }
}