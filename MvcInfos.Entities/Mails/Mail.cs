﻿using MvcInfos.Entities.Files;
using MvcInfos.Entities.Firm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Entities.Mails
{
	public class Mail
	{
		public Mail()
		{
			MailId = Guid.NewGuid();
			MailStatuses = new HashSet<MailStatus>();
		}
		[Key]
		public Guid MailId { get; set; }
		[Required]
		public int DocumentID { get; set; }
		[Required]
		public DateTime RegisterDate { get; set; }
		[Required]
		public string NumberDocument { get; set; }
		[Required]
		public DateTime DocumentDate { get; set; }
		[Required]
		public string Description { get; set; }
		[Required]
		public DateTime FinishDate { get; set; }
		public bool IsDelete { get; set; } = false;



		public virtual Firm.Firm Firm { get; set; }
		public virtual Resolution Resolution { get; set; }
		public virtual Project Project { get; set; }
		public virtual Customer Customer { get; set; }
		public virtual UploadGroupFile UploadGroupFile { get; set; }
		public virtual ISet<MailStatus> MailStatuses { get; set; }
	}
}
