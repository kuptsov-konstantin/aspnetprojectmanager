﻿namespace MvcInfos.Entities.Mails
{
	public enum MailStatusEnum
	{
		InAction,
		Spent,
		Outbox,
		CurrentQuestions
	}
}
