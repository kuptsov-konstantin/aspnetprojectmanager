﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Entities.Mails
{
	public class Resolution
	{
		public Resolution()
		{
			ResolutionNotifier = new HashSet<ResolutionNotifier>();
			ResolutionId = Guid.NewGuid();
		}

		[Key]
		public Guid ResolutionId { get; set; }
		public string Comment { get; set; }
		public virtual ISet<ResolutionNotifier> ResolutionNotifier { get; set; }
	}
}
