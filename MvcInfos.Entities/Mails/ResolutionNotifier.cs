﻿using MvcInfos.Entities.Firm;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MvcInfos.Entities.Notifications;

namespace MvcInfos.Entities.Mails
{
	public class ResolutionNotifier
	{
		public ResolutionNotifier()
		{
			NotificationsClicks = new HashSet<NotificationsClick>();
			ResolutionNotifierId = Guid.NewGuid();
		}

		[Key]
		public Guid ResolutionNotifierId { get; set; }
		[Range(0, 10), Required(AllowEmptyStrings = true)]
		public int NotifyTimeKey { get; set; }
		public bool IsDelete { get; set; }
		public bool IsNotifi { get; set; }

		public virtual Mail Mail { get; set; }
		public virtual EmployeeFirm EmployeeFirm { get; set; }
		public virtual ISet<NotificationsClick> NotificationsClicks { get; set; }
	}
}