﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MvcInfos.Entities.Mails
{
	public class MailStatus
	{
		public MailStatus()
		{
			MailStatusId = Guid.NewGuid();
		}
		[Key]
		public Guid MailStatusId { get; set; }
		public MailStatusEnum MailStatusEnum { get; set; }
		public DateTime AproveDate { get; set; }

		public virtual Mail Mail { get; set; }
	}
}
