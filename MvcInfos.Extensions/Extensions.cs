﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MvcInfos.Extensions
{

	public static class Extensions
	{
		public static byte[] ImageToByteArray(this System.Drawing.Image imageIn, ImageFormat imageFormat)
		{
			MemoryStream ms = new MemoryStream();
			imageIn.Save(ms, imageFormat);
			return ms.ToArray();
		}

		/// <summary>
		/// Обрезает <see cref="System.Drawing.Image"/> и возвращает результат
		/// </summary>
		/// <param name="img">Обрезаемое изображение</param>
		/// <param name="cropArea">Вырезаемая область</param>
		/// <returns></returns>
		public static Image Crop(this Image img, Rectangle cropArea)
		{
			var bmpImage = new System.Drawing.Bitmap(img);
			return bmpImage.Clone(cropArea, bmpImage.PixelFormat);
		}
		public static ImageCodecInfo GetEncoder(this ImageFormat format)
		{
			ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
			foreach (ImageCodecInfo codec in codecs)
			{
				if (codec.FormatID == format.Guid)
				{
					return codec;
				}
			}
			return null;
		}
		public static byte[] SaveCroppedImage(this Image image, Int64 quality, int targetWidth, int targetHeight)
		{
			ImageCodecInfo jpgInfo = image.GetImageFormat().GetEncoder();
			Image finalImage = image;
			Bitmap bitmap = null;
			try
			{
				int left = 0;
				int top = 0;
				int srcWidth = targetWidth;
				int srcHeight = targetHeight;
				bitmap = new System.Drawing.Bitmap(targetWidth, targetHeight);
				double croppedHeightToWidth = (double)targetHeight / targetWidth;
				double croppedWidthToHeight = (double)targetWidth / targetHeight;

				if (image.Width > image.Height)
				{
					srcWidth = (int)(Math.Round(image.Height * croppedWidthToHeight));
					if (srcWidth < image.Width)
					{
						srcHeight = image.Height;
						left = (image.Width - srcWidth) / 2;
					}
					else
					{
						srcHeight = (int)Math.Round(image.Height * ((double)image.Width / srcWidth));
						srcWidth = image.Width;
						top = (image.Height - srcHeight) / 2;
					}
				}
				else
				{
					srcHeight = (int)(Math.Round(image.Width * croppedHeightToWidth));
					if (srcHeight < image.Height)
					{
						srcWidth = image.Width;
						top = (image.Height - srcHeight) / 2;
					}
					else
					{
						srcWidth = (int)Math.Round(image.Width * ((double)image.Height / srcHeight));
						srcHeight = image.Height;
						left = (image.Width - srcWidth) / 2;
					}
				}
				using (Graphics g = Graphics.FromImage(bitmap))
				{
					g.SmoothingMode = SmoothingMode.HighQuality;
					g.PixelOffsetMode = PixelOffsetMode.HighQuality;
					g.CompositingQuality = CompositingQuality.HighQuality;
					g.InterpolationMode = InterpolationMode.HighQualityBicubic;
					g.DrawImage(image, new Rectangle(0, 0, bitmap.Width, bitmap.Height), new Rectangle(left, top, srcWidth, srcHeight), GraphicsUnit.Pixel);
				}
				finalImage = bitmap;
			}
			catch { }
			try
			{
				using (EncoderParameters encParams = new EncoderParameters(1))
				{
					encParams.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
					//quality should be in the range [0..100] .. 100 for max, 0 for min (0 best compression)
					MemoryStream ms = new MemoryStream();
					finalImage.Save(ms, jpgInfo, encParams);
					return ms.ToArray();
				}
			}
			catch { }
			if (bitmap != null)
			{
				bitmap.Dispose();
			}
			return null;
		}
		/// <summary>
		/// Content type string used in http multipart.
		/// </summary>
		public static string MultipartContentType = "multipart/form-data";
		public static bool CheckContentType(HttpPostedFileBase httpPostedFile)
		{
			return httpPostedFile.ContentType.StartsWith(MultipartContentType);
		}
		public static SavedFile Upload(this HttpPostedFileBase httpPostedFile, string fileRoute, FroalaEditor.FileOptions options)
		{
			// Use default file options.
			if (options == null)
			{
				options = FroalaEditor.File.defaultOptions;
			}

			if (httpPostedFile == null)
			{
				throw new Exception("Fieldname is not correct. It must be: " + options.Fieldname);
			}

			if (!CheckContentType(httpPostedFile) && !options.Validation.Check(httpPostedFile.FileName.ToLower(), httpPostedFile.ContentType))
			{
				throw new Exception("Invalid contentType. It must be " + FroalaEditor.File.MultipartContentType);
			}

			// Generate Random name.
			string extension = FroalaEditor.Utils.GetFileExtension(httpPostedFile.FileName);
			var tempFile = new SavedFile();
			string name = tempFile.ID + "." + extension;
			string link = (fileRoute + name).ToLower();

#if DEBUG || DEBUGSERVERSQL
			string serverPath = link;
#elif RELEASE && RELEASESERVERSQL
			string serverPath = GetAbsoluteServerPath(link);
#elif (AZUREDEBUG || AZURERELEASE)

#endif
			// Create directory if it doesn't exist.
			System.IO.FileInfo dir = new System.IO.FileInfo(System.IO.Path.GetDirectoryName(serverPath));
			dir.Directory.Create();

			// Copy contents to memory stream.
			System.IO.Stream stream;
#if netcore
            stream = new MemoryStream();
            file.CopyTo(stream);
            stream.Position = 0;
#else
			stream = httpPostedFile.InputStream;
#endif
			// Save file to disk.
			FroalaEditor.File.Save(stream, serverPath, options);
			// Check if the file is valid.
			if (options.Validation != null && !options.Validation.Check(serverPath, httpPostedFile.ContentType))
			{
				// Delete file.
				FroalaEditor.File.Delete(serverPath);
				throw new Exception("File does not meet the validation.");
			}

			// Make sure it is compatible with ASP.NET Core.

			tempFile.FileName = httpPostedFile.FileName;
			tempFile.SavedFileName = name;


			return tempFile;
		}
		public static object Upload(this HttpContext httpContext, string fileRoute, FroalaEditor.FileOptions options = null)
		{
			// Use default file options.
			if (options == null)
			{
				options = FroalaEditor.File.defaultOptions;
			}

			if (!FroalaEditor.File.CheckContentType(httpContext))
			{
				throw new Exception("Invalid contentType. It must be " + FroalaEditor.File.MultipartContentType);
			}

			var httpRequest = httpContext.Request;

			int filesCount = 0;
#if netcore
            filesCount = httpRequest.Form.Files.Count;
#else
			filesCount = httpRequest.Files.Count;
#endif

			if (filesCount == 0)
			{
				throw new Exception("No file found");
			}

			// Get HTTP posted file based on the fieldname. 
#if netcore
            var file = httpRequest.Form.Files.GetFile(options.Fieldname);
#else
			var file = httpRequest.Files.Get(options.Fieldname);
#endif

			if (file == null)
			{
				throw new Exception("Fieldname is not correct. It must be: " + options.Fieldname);
			}

			// Generate Random name.
			string extension = FroalaEditor.Utils.GetFileExtension(file.FileName);
			string name = FroalaEditor.Utils.GenerateUniqueString() + "." + extension;

			string link = fileRoute + name;
			String serverPath = GetAbsoluteServerPath(link);
			// Create directory if it doesn't exist.
			System.IO.FileInfo dir = new System.IO.FileInfo(System.IO.Path.GetDirectoryName(serverPath));
			dir.Directory.Create();

			// Copy contents to memory stream.
			System.IO.Stream stream;
#if netcore
            stream = new MemoryStream();
            file.CopyTo(stream);
            stream.Position = 0;
#else
			stream = file.InputStream;
#endif



			// Save file to disk.
			FroalaEditor.File.Save(stream, serverPath, options);

			// Check if the file is valid.
			if (options.Validation != null && !options.Validation.Check(serverPath, file.ContentType))
			{
				// Delete file.
				FroalaEditor.File.Delete(serverPath);
				throw new Exception("File does not meet the validation.");
			}

			// Make sure it is compatible with ASP.NET Core.
			return new { link = link.Replace("wwwroot/", "") };
		}
		public static ImageFormat GetImageFormat(this Image img)
		{
			if (img.RawFormat.Equals(ImageFormat.Jpeg))
				return ImageFormat.Jpeg;
			if (img.RawFormat.Equals(ImageFormat.Bmp))
				return ImageFormat.Bmp;
			if (img.RawFormat.Equals(ImageFormat.Png))
				return ImageFormat.Png;
			if (img.RawFormat.Equals(ImageFormat.Emf))
				return ImageFormat.Emf;
			if (img.RawFormat.Equals(ImageFormat.Exif))
				return ImageFormat.Exif;
			if (img.RawFormat.Equals(ImageFormat.Gif))
				return ImageFormat.Gif;
			if (img.RawFormat.Equals(ImageFormat.Icon))
				return ImageFormat.Icon;
			if (img.RawFormat.Equals(ImageFormat.MemoryBmp))
				return ImageFormat.MemoryBmp;
			if (img.RawFormat.Equals(ImageFormat.Tiff))
				return ImageFormat.Tiff;
			else
				return ImageFormat.Tiff;
		}
		public static string GetImageFormatName(this Image img)
		{
			if (img.RawFormat.Equals(ImageFormat.Jpeg))
				return "JPEG";
			if (img.RawFormat.Equals(ImageFormat.Bmp))
				return "BMP";
			if (img.RawFormat.Equals(ImageFormat.Png))
				return "PNG";
			if (img.RawFormat.Equals(ImageFormat.Emf))
				return "EMF";
			if (img.RawFormat.Equals(ImageFormat.Exif))
				return "Exif";
			if (img.RawFormat.Equals(ImageFormat.Gif))
				return "Gif";
			if (img.RawFormat.Equals(ImageFormat.Icon))
				return "Icon";
			if (img.RawFormat.Equals(ImageFormat.MemoryBmp))
				return "MemoryBmp";
			if (img.RawFormat.Equals(ImageFormat.Tiff))
				return "Tiff";
			else
				return "Wmf";
		}

		public static byte[] ToPng(this Image image)
		{
			MemoryStream stram = new MemoryStream();
			image.Save(stram, ImageFormat.Png);
			return stram.ToArray();
		}

		public static byte[] ToPng(this Stream stream)
		{
			var image =  Image.FromStream(stream);
			return image.ToPng();
		}

		public static String GetAbsoluteServerPath(string path)
		{
#if netcore
            return path;
#else
			return HttpContext.Current.Server.MapPath(path);
#endif
		}





		public static JsonResult JsonNetIso(this Controller controller, object data)
		{
			return JsonNetIso(controller, data, null /* contentType */, null /* contentEncoding */, JsonRequestBehavior.DenyGet);
		}

		public static JsonResult JsonNetIso(this Controller controller, object data, string contentType)
		{
			return JsonNetIso(controller, data, contentType, null /* contentEncoding */, JsonRequestBehavior.DenyGet);
		}

		public static JsonResult JsonNetIso(this Controller controller, object data, string contentType, Encoding contentEncoding)
		{
			return JsonNetIso(controller, data, contentType, contentEncoding, JsonRequestBehavior.DenyGet);
		}

		public static JsonResult JsonNetIso(this Controller controller, object data, JsonRequestBehavior behavior)
		{
			return JsonNetIso(controller, data, null /* contentType */, null /* contentEncoding */, behavior);
		}

		public static JsonResult JsonNetIso(this Controller controller, object data, string contentType, JsonRequestBehavior behavior)
		{
			return JsonNetIso(controller, data, contentType, null /* contentEncoding */, behavior);
		}

		public static JsonResult JsonNetIso(this Controller controller, object data, string contentType, Encoding contentEncoding, JsonRequestBehavior behavior)
		{
			return new Json.JsonNetResult
			{
				Data = data,
				ContentType = contentType,
				ContentEncoding = contentEncoding,
				JsonRequestBehavior = behavior
			};
		}

	}

}