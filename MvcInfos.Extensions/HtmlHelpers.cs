using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace MvcInfos.Extensions
{
    public static class MyHtmlHelper
    {
        public static MvcHtmlString AutocompleteFor<TModel, TProperty1, TProperty2>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty1>> valueExpression, Expression<Func<TModel, TProperty2>> idExpression, string actionName, string controllerName, System.Web.Routing.RouteValueDictionary routeValues, bool requestFocus)
        {
            string autocompleteUrl = UrlHelper.GenerateUrl(null, actionName, controllerName, null, html.RouteCollection, html.ViewContext.RequestContext, includeImplicitMvcValues: true);
            string @class = "form-control typeahead" + (requestFocus ? " focus" : string.Empty);
            // Get the fully qualified class name of the autocomplete id field
            string idFieldString = idExpression.Body.ToString();
            // We need to strip the 'model.' from the beginning
            int loc = idFieldString.IndexOf('.');
            // Also, replace the . with _ as this is done by MVC so the field name is js friendly
            string autocompleteIdField = idFieldString.Substring(loc + 1, idFieldString.Length - loc - 1).Replace('.', '_');
            return html.TextBoxFor(valueExpression, new { data_autocomplete_url = autocompleteUrl, @class, data_autocomplete_id_field = autocompleteIdField });
        }


		public static bool IsAzure<TModel>(this HtmlHelper<TModel> helper)
		{
#if (DEBUG || RELEASE)
			return false;
#else
			return true;
#endif
		}

		public static bool IsAzure(this HtmlHelper helper)
		{
#if (DEBUG || RELEASE)
			return true;
#else
			return false;
#endif
		}



		public static MvcHtmlString DropDownListBootstrapFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList) {
            return new MvcHtmlString("<div></div>");
        }
        //
        // ������:
        //     ���������� HTML-������� select ��� ������� �������� �������, ���������������
        //     ��������� ����������, ��������� ��������� �������� ������ � �������� HTML.
        //
        // ���������:
        //   htmlHelper:
        //     ��������� ���������������� ������ HTML, ������� ����������� ������ �������.
        //
        //   expression:
        //     ���������, ������� ���������� ������, ���������� �������� ��� �����������.
        //
        //   selectList:
        //     ��������� �������� System.Web.Mvc.SelectListItem, ������� ������������ ��� ����������
        //     ��������������� ������.
        //
        //   htmlAttributes:
        //     ������, ���������� �������� HTML, ������� ������� ������ ��� ��������.
        //
        // ��������� ����:
        //   TModel:
        //     ��� ������.
        //
        //   TProperty:
        //     ��� ��������.
        //
        // �������:
        //     HTML-������� select ��� ������� �������� �������, ��������������� ����������.
        //
        // ����������:
        //   T:System.ArgumentNullException:
        //     �������� expression ����� null.
        public static MvcHtmlString DropDownListBootstrapFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList,
            object htmlAttributes)
        {

            /*
             *                                 @*
                        <select id="first-disabled" class="selectpicker" data-hide-disabled="true" data-live-search="true">
          <optgroup disabled="disabled" label="disabled">
            <option>Hidden</option>
          </optgroup>
          <optgroup label="Fruit">
            <option>Apple</option>
            <option>Orange</option>
          </optgroup>
          <optgroup label="Vegetable">
            <option>Corn</option>
            <option>Carrot</option>
          </optgroup>
        </select>
                                *@
             */
            return new MvcHtmlString("<div></div>");
        }
        //
        // ������:
        //     ���������� HTML-������� select ��� ������� �������� �������, ���������������
        //     ��������� ����������, ��������� ��������� �������� ������ � �������� HTML.
        //
        // ���������:
        //   htmlHelper:
        //     ��������� ���������������� ������ HTML, ������� ����������� ������ �������.
        //
        //   expression:
        //     ���������, ������� ���������� ������, ���������� �������� ��� �����������.
        //
        //   selectList:
        //     ��������� �������� System.Web.Mvc.SelectListItem, ������� ������������ ��� ����������
        //     ��������������� ������.
        //
        //   htmlAttributes:
        //     ������, ���������� �������� HTML, ������� ������� ������ ��� ��������.
        //
        // ��������� ����:
        //   TModel:
        //     ��� ������.
        //
        //   TProperty:
        //     ��� ��������.
        //
        // �������:
        //     HTML-������� select ��� ������� �������� �������, ��������������� ����������.
        //
        // ����������:
        //   T:System.ArgumentNullException:
        //     �������� expression ����� null.
        public static MvcHtmlString DropDownListBootstrapFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, IDictionary<string, object> htmlAttributes)
        {
            return new MvcHtmlString("<div></div>");
        }
        //
        // ������:
        //     ���������� HTML-������� select ��� ������� �������� �������, ���������������
        //     ��������� ����������, ��������� ��������� �������� ������ � ����� ��������.
        //
        // ���������:
        //   htmlHelper:
        //     ��������� ���������������� ������ HTML, ������� ����������� ������ �������.
        //
        //   expression:
        //     ���������, ������� ���������� ������, ���������� �������� ��� �����������.
        //
        //   selectList:
        //     ��������� �������� System.Web.Mvc.SelectListItem, ������� ������������ ��� ����������
        //     ��������������� ������.
        //
        //   optionLabel:
        //     ����� ��� ������� �������� �� ���������.���� �������� ����� ����� �������� null.
        //
        // ��������� ����:
        //   TModel:
        //     ��� ������.
        //
        //   TProperty:
        //     ��� ��������.
        //
        // �������:
        //     HTML-������� select ��� ������� �������� �������, ��������������� ����������.
        //
        // ����������:
        //   T:System.ArgumentNullException:
        //     �������� expression ����� null.
        public static MvcHtmlString DropDownListBootstrapFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, string optionLabel)
        {
            return new MvcHtmlString("<div></div>");
        }
        //
        // ������:
        //     ���������� HTML-������� select ��� ������� �������� �������, ���������������
        //     ��������� ����������, ��������� ��������� �������� ������, ����� �������� � ��������
        //     HTML.
        //
        // ���������:
        //   htmlHelper:
        //     ��������� ���������������� ������ HTML, ������� ����������� ������ �������.
        //
        //   expression:
        //     ���������, ������� ���������� ������, ���������� �������� ��� �����������.
        //
        //   selectList:
        //     ��������� �������� System.Web.Mvc.SelectListItem, ������� ������������ ��� ����������
        //     ��������������� ������.
        //
        //   optionLabel:
        //     ����� ��� ������� �������� �� ���������.���� �������� ����� ����� �������� null.
        //
        //   htmlAttributes:
        //     ������, ���������� �������� HTML, ������� ������� ������ ��� ��������.
        //
        // ��������� ����:
        //   TModel:
        //     ��� ������.
        //
        //   TProperty:
        //     ��� ��������.
        //
        // �������:
        //     HTML-������� select ��� ������� �������� �������, ��������������� ����������.
        //
        // ����������:
        //   T:System.ArgumentNullException:
        //     �������� expression ����� null.
        public static MvcHtmlString DropDownListBootstrapFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, string optionLabel, IDictionary<string, object> htmlAttributes)
        {
            return new MvcHtmlString("<div></div>");
        }
        //
        // ������:
        //     ���������� HTML-������� select ��� ������� �������� �������, ���������������
        //     ��������� ����������, ��������� ��������� �������� ������, ����� �������� � ��������
        //     HTML.
        //
        // ���������:
        //   htmlHelper:
        //     ��������� ���������������� ������ HTML, ������� ����������� ������ �������.
        //
        //   expression:
        //     ���������, ������� ���������� ������, ���������� �������� ��� �����������.
        //
        //   selectList:
        //     ��������� �������� System.Web.Mvc.SelectListItem, ������� ������������ ��� ����������
        //     ��������������� ������.
        //
        //   optionLabel:
        //     ����� ��� ������� �������� �� ���������.���� �������� ����� ����� �������� null.
        //
        //   htmlAttributes:
        //     ������, ���������� �������� HTML, ������� ������� ������ ��� ��������.
        //
        // ��������� ����:
        //   TModel:
        //     ��� ������.
        //
        //   TProperty:
        //     ��� ��������.
        //
        // �������:
        //     HTML-������� select ��� ������� �������� �������, ��������������� ����������.
        //
        // ����������:
        //   T:System.ArgumentNullException:
        //     �������� expression ����� null.
        public static MvcHtmlString DropDownListBootstrapFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, string optionLabel, object htmlAttributes)
        {
            return new MvcHtmlString("<div></div>");
        }
    }
 }