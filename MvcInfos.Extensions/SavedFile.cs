﻿using System;

namespace MvcInfos.Extensions
{
	public class SavedFile
	{
		public string FileName { get; set; }
		public string SavedFileName { get; set; }
		public Guid ID { get; set; } = Guid.NewGuid();

	}
}