﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;

namespace MvcInfos.Notification.PCs
{
	public class PCInfo
	{
		private string name;
		private IPAddress[] iPAddress;

		public PCInfo(string name, IPAddress[] iPAddress)
		{
			this.name = name;
			this.iPAddress = iPAddress;
		}
		//[JsonProperty("userId")]
		//public Guid UserId { get; set; }

		[JsonProperty("name")]
		public string Name { get => name; set => name = value; }
		[JsonIgnore()]
		public IPAddress[] IPAddress { get => iPAddress; }
		[JsonProperty("ipStrings")]
		public string[] IPStrings { get => iPAddress?.Select(ip => ip?.ToString()).ToArray(); set => iPAddress = value.Select(ip => System.Net.IPAddress.Parse(ip)).ToArray(); }
	}
}