﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MvcInfos.Notification.PCs
{
	public class NotifiPCs
	{
		Dictionary<Guid, List<PCInfo>> usersPC = new Dictionary<Guid, List<PCInfo>>();
		DateTime? lastUpdate = null;

		public void UpdateDate(DateTime lastUpdate)
		{
			this.lastUpdate = lastUpdate;
		}
		[JsonProperty("lastUpdate")]
		public DateTime? LastUpdate { get => lastUpdate; }
		[JsonProperty("usersPC")]
		public Dictionary<Guid, List<PCInfo>> UsersPC => usersPC; 
	}
}
